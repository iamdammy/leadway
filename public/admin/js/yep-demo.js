
// Demo Setting Template
var htmlDemo = "<!-- demo settings panel-->\
        <div class='demo'>\
         <span id='demo-setting'><i class='fa fa-cog'></i></span>\
            <form >\
            <span id='demo-setting-data' data-id=''> </span>\
                <legend class='no-padding'><h4 class='bold'>Theme Options</h4></legend>\
                <div class='row'>\
                    <div class='col-md-6'>\
                        <div class='m-checkbox '>\
                            <input id='check-fix-header' type='checkbox'>\
                            <label for='check-fix-header' data-toggle='tooltip' title='asadasi dkakd'>\
                               <strong>Fixed Header</strong> \
                            </label>\
                        </div>\
                        <div class='m-checkbox '>\
                            <input id='check-fix-sidebar' type='checkbox'>\
                            <label for='check-fix-sidebar'>\
                               <strong>Fixed Sidebar</strong> \
                            </label>\
                        </div>\
                        <div class='m-checkbox '>\
                            <input id='check-fix-ribbon' type='checkbox' >\
                            <label for='check-fix-ribbon'>\
                               <strong>Fixed Ribbon</strong> \
                            </label>\
                        </div>\
                        <div class='m-checkbox '>\
                            <input id='check-fix-footer' type='checkbox' >\
                            <label for='check-fix-footer'>\
                               <strong>Fixed Footer</strong> \
                            </label>\
                        </div>\
                    </div>\
                    <div class='col-md-6'>\
                        <div class='m-checkbox '>\
                            <input id='check-boxed'  type='checkbox' >\
                            <label for='check-boxed' >\
                               <strong>Boxed Layout</strong> \
                            </label>\
                            <div class='note'>(non-responsive)</div>\
                        </div>\
                        <div class='m-checkbox '>\
                            <input id='check-rtl'  type='checkbox' >\
                            <label for='check-rtl' >\
                               <strong>RTL version</strong> \
                            </label>\
                        </div>\
                        <div class='m-checkbox '>\
                            <input id='check-top-menu'  type='checkbox' >\
                            <label for='check-top-menu' >\
                               <strong>Top Menu</strong> \
                            </label>\
                            <div class='note'>(with hover active)</div>\
                        </div>\
                    </div>\
                </div>  \
                <legend class='no-padding'>Backgrounds for Boxed Layout</legend>\
                <div class='images boxed-patterns'>\
                    <a id='bg_p1' href='#'><img alt='' src='../assets/img/bg/bg_p1.jpg' height='25' width='25'></a>\
                    <a id='bg_p2' href='#'><img alt='' src='../assets/img/bg/bg_p2.jpg' height='25' width='25'></a>\
                    <a id='bg_p3' href='#'><img alt='' src='../assets/img/bg/bg_p3.jpg' height='25' width='25'></a>\
                    <a id='bg_p4' href='#'><img alt='' src='../assets/img/bg/bg_p4.jpg' height='25' width='25'></a>\
                    <a id='bg_p5' href='#'><img alt='' src='../assets/img/bg/bg_p5.jpg' height='25' width='25'></a>\
                    <a id='bg_p6' href='#'><img alt='' src='../assets/img/bg/bg_p6.jpg' height='25' width='25'></a>\
                    <a id='bg_p7' href='#'><img alt='' src='../assets/img/bg/bg_p7.jpg' height='25' width='25'></a>\
                </div>   \
                <legend class='no-padding'>Skins</legend>\
                <div class='images boxed-skins'>\
                    <a id='bg_style_1' href='#'><img alt='' src='../assets/img/skin/skin-1.png' height='24' width='25'></a>\
                    <a id='bg_style_2' href='#'><img alt='' src='../assets/img/skin/skin-2.png' height='24' width='25'></a>\
                    <a id='bg_style_3' href='#'><img alt='' src='../assets/img/skin/skin-3.png' height='24' width='25'></a>\
                </div>\
            </form>\
        </div>";

var site_url=window.location.href.replace(/#/g, "").replace(/all_admin/g,"clients");
         function client_style(obj,id){
        $('#demo-setting').parent().toggleClass("activate");        
        // client_id=$(this).attr("data-id");
           client_id=  $(obj).attr("data-id");
         // document.getElementById("demo-setting-data").setAttribute("data-id",
         $("#demo-setting-data").data("id", client_id);
        var someurl=site_url+"/"+client_id;        
        get_style(someurl);  
        }   
$(function(){
    $('#container').after(htmlDemo);
    $('#demo-setting').hide();
    var login_client_id=$('#login_client_id').data("id");
    var site_url=window.location.href.replace(/#/g, "");
    var default_url=site_url+"/style/"+login_client_id;  
    
    default_style(default_url);  
    //demo settings show
    // $('#demo-setting').on('click', function() {
    //     $(this).parent().toggleClass("activate");
    //     var client_id=$(this).data("id");
    // });

var site_url=window.location.href.replace(/#/g, "").replace(/all_admin/g,"clients");

// var data_id=$("#demo-setting-data").data("id");

    /* SWITCH BETWEEN FULLWIDTH & BOXED */
    $("#check-boxed").change(function() {
         var client_id=$('#demo-setting-data').data("id");

        if(this.checked) {
            var style="layout_style";
            var param="True";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
            // Enable Boxed and Disable fix_ribbon , fix_sidebar
           $('#container').switchClass('container-fluid', 'container', 250, 'easeInOutQuad')
           $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-ribbon');
            $("#check-fix-sidebar")
                .prop('checked', false);
            $('#container').removeClass('fixed-sidebar');        
        } else{
            var style="layout_style";
            var param="False";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
            $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad')
        }
    });

    /* FIX HEADER */
    $("#check-fix-header").change(function() {
         var client_id=$('#demo-setting-data').data("id");
        if(this.checked) {
            var style="header_style";
            var param="True";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
           $('#container').addClass('fixed-header');
        } else{
            // Disable Sidebar & fix_ribbon & fix_header
            var style="header_style";
            var param="False";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
            $("#check-fix-sidebar")
                .prop('checked', false);
            $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-header');
            $('#container').removeClass('fixed-sidebar');
            $('#container').removeClass('fixed-ribbon');
        }
    });

    /* FIX SIDEBAR */
    $("#check-fix-sidebar").change(function() {
         var client_id=$('#demo-setting-data').data("id");
        if(this.checked) {
            var style="sidebar_style";
            var param="True";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
            // Enable fix_header, fix_sidebar and Disable Boxed
            $("#check-fix-header")
                .prop('checked', true);
            $("#check-boxed")
                .prop('checked', false);    
           $('#container').addClass('fixed-header');
           $('#container').addClass('fixed-sidebar');
           $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad');
        } else{
            var style="sidebar_style";
            var param="False";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
            // Disable fix_ribbon, fix_sidebar
            $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-sidebar');
            $('#container').removeClass('fixed-ribbon');
        }
    });

    /* FIX RIBBON*/
    $("#check-fix-ribbon").change(function() {
         var client_id=$('#demo-setting-data').data("id");
        if(this.checked) {
            var style="ribbon_style";
            var param="True";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
            // Enable fix_sidebar, fix_header, fix_ribbon and Disable Boxed
            $("#check-fix-header")
                .prop('checked', true);
            $("#check-fix-sidebar")
                .prop('checked', true);
            $("#check-fix-ribbon")
                .prop('checked', true);        
            $('#container').addClass('fixed-header');
            $('#container').addClass('fixed-sidebar');
            $('#container').addClass('fixed-ribbon');        
            $("#check-boxed")
                .prop('checked', false);
            $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad');
        } else{
            var style="ribbon_style";
            var param="False";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
            // Disable fix_ribbon
            $('#container').removeClass('fixed-ribbon');
        }
    });

    /* FIX FOOTER*/
    $("#check-fix-footer").change(function() {
         var client_id=$('#demo-setting-data').data("id");
        if(this.checked) {
            var style="footer_style";
            var param="True";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
           $('#container').addClass('fixed-footer');
        } else{
            var style="footer_style";
            var param="False";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
            $('#container').removeClass('fixed-footer');
        }
    });

    /* CHANGE BACKGROUND BOXED MODE*/ 
    $('.boxed-patterns > a > img').bind('click',function(){
         var client_id=$('#demo-setting-data').data("id");
        var $this = $(this);
        var $html = $('html')
        bgurl = ($this.attr("src"));
        $html.css("background-image", "url(" + bgurl + ")");
        var style="bg_style";
            var param=bgurl;
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
    })

    /* CHANGE SKINS */
    $('#bg_style_1').on('click',function(){
         var client_id=$('#demo-setting-data').data("id");
        var style="skin_style";
            var param="bg_style_1";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
        $('#container').removeClass('skin-1');
        $('#container').removeClass('skin-2');
        $('#container').addClass('skin-3');

    })

    $('#bg_style_2').on('click',function(){
         var client_id=$('#demo-setting-data').data("id");
        var style="skin_style";
            var param="bg_style_2";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
        $('#container').removeClass('skin-2');
        $('#container').removeClass('skin-3');
        $('#container').addClass('skin-1');                
    })

    $('#bg_style_3').on('click',function(){
         var client_id=$('#demo-setting-data').data("id");
        var style="skin_style";
            var param="bg_style_3";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
        $('#container').removeClass('skin-3');
        $('#container').removeClass('skin-1');
        $('#container').addClass('skin-2');        
    })

    /* ACTIVE RTL VERSION */
    $("#check-rtl").change(function() {
         var client_id=$('#demo-setting-data').data("id");
        var style="rtl_style";
        if(this.checked) {    

            var param="True";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);        
            $("head link[href='../assets/vendors/bootstrap/css/bootstrap.min.css'] ").last().after("<link rel='stylesheet' href='../assets/css/yep-rtl.css' type='text/css' media='screen'>");            
            $('body').addClass('rtl');
        } else {
            var param="False";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);  
            $("head link[href='../assets/css/yep-rtl.css'] ").remove();        
            $('body').removeClass('rtl');  
        }
    })

    /* ACTIVE TOP MENU LAYOUT */
    $("#check-top-menu").change(function() {
         var client_id=$('#demo-setting-data').data("id");
        var style="topmenu_style";
        if(this.checked) {          
         var param="True";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);    
           $('#container').addClass('top-menu');
           $('#container').addClass('hover-active');
        } else {
            var param="False";
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);  
           $('#container').removeClass('top-menu');  
           $('#container').removeClass('hover-active');  
        }
    })
}) 



        function update_style(someurl) {
      var xhttp = new XMLHttpRequest();                  
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    // var newLink=this.response;
                    // var json = JSON.parse(newLink);
                    console.log(someurl);
                    // document.getElementById("ifram_play").setAttribute("src",json.response.replace(/\\/g, ''));                    
                }
              };//replace(/\\/g, '')
              console.log(someurl);
            xhttp.open("GET", someurl, true);
                        
                xhttp.send();
            }



            function get_style(someurl) {
             var xhttp = new XMLHttpRequest();                  
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var newLink=this.response;
                    var json = JSON.parse(newLink);
                    // console.log(json.header_style);
            // START
            var header_style=json.header_style;
            var sidebar_style=json.sidebar_style;
            var ribbon_style=json.ribbon_style;
            var footer_style=json.footer_style;
            var rtl_style=json.rtl_style;
            var topmenu_style=json.topmenu_style;
            var bg_style=json.bg_style;
            var skin_style=json.skin_style;
            var layout_style=json.layout_style;

              /* FIX HEADER */
        if(header_style) {
            
            $("#check-fix-header").prop('checked', false);
           $('#container').addClass('fixed-header');
        } else{
            // Disable Sidebar & fix_ribbon & fix_header
            
            $("#check-fix-header").prop('checked', false);
            $("#check-fix-sidebar")
                .prop('checked', false);
            $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-header');
            $('#container').removeClass('fixed-sidebar');
            $('#container').removeClass('fixed-ribbon');
        }
    

    /* FIX SIDEBAR */
           
        if(layout_style) {        
            $("#check-boxed").prop('checked', true);
            // Enable Boxed and Disable fix_ribbon , fix_sidebar
           $('#container').switchClass('container-fluid', 'container', 250, 'easeInOutQuad');
           $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-ribbon');
            $("#check-fix-sidebar")
                .prop('checked', false);
            $('#container').removeClass('fixed-sidebar');        
        } else{
            $("#check-boxed").prop('checked', false);
            $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad');
        }
         
        if(sidebar_style) {
            
            $("#check-fix-sidebar").prop('checked', true);
            // Enable fix_header, fix_sidebar and Disable Boxed
            $("#check-fix-header")
                .prop('checked', true);
            $("#check-boxed")
                .prop('checked', false);    
           $('#container').addClass('fixed-header');
           $('#container').addClass('fixed-sidebar');
           $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad');
        } else{
            
            $("#check-fix-sidebar").prop('checked', false);
            // Disable fix_ribbon, fix_sidebar
            $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-sidebar');
            $('#container').removeClass('fixed-ribbon');
        }
    

    /* FIX RIBBON*/
        if(ribbon_style) {
            // Enable fix_sidebar, fix_header, fix_ribbon and Disable Boxed
            $("#check-fix-ribbon").prop('checked',true);
            $("#check-fix-header")
                .prop('checked', true);
            $("#check-fix-sidebar")
                .prop('checked', true);
            $("#check-fix-ribbon")
                .prop('checked', true);        
            $('#container').addClass('fixed-header');
            $('#container').addClass('fixed-sidebar');
            $('#container').addClass('fixed-ribbon');        
            $("#check-boxed")
                .prop('checked', false);
            $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad');
        } else{
            $("#check-fix-ribbon").prop('checked',false);
            // Disable fix_ribbon
            $('#container').removeClass('fixed-ribbon');
        }
    

    /* FIX FOOTER*/
            
        if(footer_style) {
            $("#check-fix-footer").prop("checked", true);
           $('#container').addClass('fixed-footer');
        } else{
            $("#check-fix-footer").prop("checked", false);
            $('#container').removeClass('fixed-footer');
        }
    

    /* CHANGE BACKGROUND BOXED MODE*/ 
    $('.boxed-patterns > a > img').bind('click',function(){
         var client_id=$('#demo-setting-data').data("id");
        var $this = $(this);
        var $html = $('html')
        bgurl = ($this.attr("src"));
        $html.css("background-image", "url(" + bgurl + ")");
        var style="bg_style";
            var param=bgurl;
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
    })

    /* CHANGE SKINS */
    if(bg_style=="bg_style_1"){
        $('#container').removeClass('skin-1');
        $('#container').removeClass('skin-2');
        $('#container').addClass('skin-3');

    }else if(bg_style=="bg_style_2"){        
        $('#container').removeClass('skin-2');
        $('#container').removeClass('skin-3');
        $('#container').addClass('skin-1');                
    }
    else if(bg_style=="bg_style_3"){        
        $('#container').removeClass('skin-3');
        $('#container').removeClass('skin-1');
        $('#container').addClass('skin-2');        
        }

    /* ACTIVE RTL VERSION */    
        if(rtl_style) {  
            $("#check-rtl").prop("checked", true);
            $("head link[href='../assets/vendors/bootstrap/css/bootstrap.min.css'] ").last().after("<link rel='stylesheet' href='../assets/css/yep-rtl.css' type='text/css' media='screen'>");            
            $('body').addClass('rtl');
        } else {        
            $("#check-rtl").prop("checked", false);
            $("head link[href='../assets/css/yep-rtl.css'] ").remove();        
            $('body').removeClass('rtl');  
        }    

    /* ACTIVE TOP MENU LAYOUT */            
        if(topmenu_style) {          
         $("#check-top-menu").prop("checked", true);
           $('#container').addClass('top-menu');
           $('#container').addClass('hover-active');
        } else {
            $("#check-top-menu").prop("checked", false);
           $('#container').removeClass('top-menu');  
           $('#container').removeClass('hover-active');  
        }

            // END


                    
                }
              };//replace(/\\/g, '')
              
            xhttp.open("GET", someurl, true);
                        
                xhttp.send();
            }



            function default_style(someurl) {
             var xhttp = new XMLHttpRequest();                  
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var newLink=this.response;
                    var json = JSON.parse(newLink);
                    // console.log(json.header_style);
            // START
            var header_style=json.header_style;
            var sidebar_style=json.sidebar_style;
            var ribbon_style=json.ribbon_style;
            var footer_style=json.footer_style;
            var rtl_style=json.rtl_style;
            var topmenu_style=json.topmenu_style;
            var bg_style=json.bg_style;
            var skin_style=json.skin_style;
            var layout_style=json.layout_style;

              /* FIX HEADER */
        if(header_style) {            
           $('#container').addClass('fixed-header');
        } else{
            // Disable Sidebar & fix_ribbon & fix_header            
            $("#check-fix-sidebar")
                .prop('checked', false);
            $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-header');
            $('#container').removeClass('fixed-sidebar');
            $('#container').removeClass('fixed-ribbon');
        }
    

    /* FIX SIDEBAR */
           
        if(layout_style) {                    
            // Enable Boxed and Disable fix_ribbon , fix_sidebar
           $('#container').switchClass('container-fluid', 'container', 250, 'easeInOutQuad');
           $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-ribbon');
            $("#check-fix-sidebar")
                .prop('checked', false);
            $('#container').removeClass('fixed-sidebar');        
        } else{            
            $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad');
        }
         
        if(sidebar_style) {            
            // Enable fix_header, fix_sidebar and Disable Boxed
            $("#check-fix-header")
                .prop('checked', true);
            $("#check-boxed")
                .prop('checked', false);    
           $('#container').addClass('fixed-header');
           $('#container').addClass('fixed-sidebar');
           $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad');
        } else{            
            // Disable fix_ribbon, fix_sidebar
            $("#check-fix-ribbon")
                .prop('checked', false);
            $('#container').removeClass('fixed-sidebar');
            $('#container').removeClass('fixed-ribbon');
        }
    

    /* FIX RIBBON*/
        if(ribbon_style) {
            // Enable fix_sidebar, fix_header, fix_ribbon and Disable Boxed            
            $("#check-fix-header")
                .prop('checked', true);
            $("#check-fix-sidebar")
                .prop('checked', true);
            $("#check-fix-ribbon")
                .prop('checked', true);        
            $('#container').addClass('fixed-header');
            $('#container').addClass('fixed-sidebar');
            $('#container').addClass('fixed-ribbon');        
            $("#check-boxed")
                .prop('checked', false);
            $('#container').switchClass('container','container-fluid', 250, 'easeInOutQuad');
        } else{            
            // Disable fix_ribbon
            $('#container').removeClass('fixed-ribbon');
        }
    

    /* FIX FOOTER*/
            
        if(footer_style) {
           $('#container').addClass('fixed-footer');
        } else{
            $('#container').removeClass('fixed-footer');
        }
    

    /* CHANGE BACKGROUND BOXED MODE*/ 
    $('.boxed-patterns > a > img').bind('click',function(){
         var client_id=$('#demo-setting-data').data("id");
        var $this = $(this);
        var $html = $('html')
        bgurl = ($this.attr("src"));
        $html.css("background-image", "url(" + bgurl + ")");
        var style="bg_style";
            var param=bgurl;
            var someurl=site_url+"/"+client_id+"/"+style+"/"+param
            update_style(someurl);
    })

    /* CHANGE SKINS */
    if(bg_style=="bg_style_1"){
        $('#container').removeClass('skin-1');
        $('#container').removeClass('skin-2');
        $('#container').addClass('skin-3');

    }else if(bg_style=="bg_style_2"){        
        $('#container').removeClass('skin-2');
        $('#container').removeClass('skin-3');
        $('#container').addClass('skin-1');                
    }
    else if(bg_style=="bg_style_3"){        
        $('#container').removeClass('skin-3');
        $('#container').removeClass('skin-1');
        $('#container').addClass('skin-2');        
        }

    /* ACTIVE RTL VERSION */    
        if(rtl_style) {              
            $("head link[href='../assets/vendors/bootstrap/css/bootstrap.min.css'] ").last().after("<link rel='stylesheet' href='../assets/css/yep-rtl.css' type='text/css' media='screen'>");            
            $('body').addClass('rtl');
        } else {                    
            $("head link[href='../assets/css/yep-rtl.css'] ").remove();        
            $('body').removeClass('rtl');  
        }    

    /* ACTIVE TOP MENU LAYOUT */            
        if(topmenu_style) {                   
           $('#container').addClass('top-menu');
           $('#container').addClass('hover-active');
        } else {            
           $('#container').removeClass('top-menu');  
           $('#container').removeClass('hover-active');  
        }

            // END
       
                }
              };//replace(/\\/g, '')
              console.log(someurl);
            xhttp.open("GET", someurl, true);
                        
                xhttp.send();
            }