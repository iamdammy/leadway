<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('styles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unique();
            $table->boolean('header_style')->nullable()->default(0);
            $table->boolean('sidebar_style')->nullable()->default(0);
            $table->boolean('ribbon_style')->nullable()->default(0);
            $table->boolean('layout_style')->nullable()->default(0);
            $table->boolean('rtl_style')->nullable()->default(0);
            $table->boolean('topmenu_style')->nullable()->default(0);
            $table->string('bg_style')->nullable();
            $table->string('skin_style')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('styles');
    }
}
