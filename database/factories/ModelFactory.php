<?php

use App\Admin;
use App\Department;
use App\Role;
use App\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/*$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});*/




/*$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'firstName' => 'Damilola',
        'lastName' => 'Student',
        'email' => 'olotudammy@gmail.com',
        'department' => 'ICT',
        'gender' => 'Male',
        'photo' => 'olotudammy.jpg',
        'password' => $password ?: $password = bcrypt('olotudammy'),
        'remember_token' => str_random(10),
    ];
});*/


$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'firstName' => 'Damilola',
        'lastName' => 'Student',
        'email' => 'olotudammy@gmail.com',
        'department' => 'ICT',
        'gender' => 'Male',
        'photo' => 'default.png',

        /*'firstName' => $faker->name,
        'lastName' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'department' => 'ICT',
        'gender' => 'Male',
        'photo' => 'olotudammy.jpg',
        */


        'remember_token' => str_random(10),
        'password' => $password ?: $password = bcrypt('olotudammy'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Admin::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'firstName' => 'Damilola',
        'lastName' => 'Admin',
        'email' => 'olotudammy@gmail.com',
        'department' => 'ICT',
        'gender' => 'Male',
        'photo' => 'olotudammy.jpg',
        'password' => $password ?: $password = bcrypt('olotudammy'),
        'remember_token' => str_random(10),
    ];
});



$factory->define(App\Role::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => 'Admin'
    ];
});


$factory->define(App\Department::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => 'ICT'
    ];
});



$factory->define(App\Course::class, function (Faker\Generator $faker) {

    return [
        'course_name' => $faker->lastName,
        'category_id' => 1,
        'course_video' => 'https://www.youtube.com/watch?v=cKl-6u8Dcb0',
        'course_doc' => $faker->lastName,
        'course_description' => 'This is a short decscription',
        'course_img' => '',
    ];
});


$factory->define(App\Logo::class, function (Faker\Generator $faker) {

    return [
        'logo' => 'default.png',
        // 'category_id' => 1,
        // 'course_video' => 'https://www.youtube.com/watch?v=cKl-6u8Dcb0',
        // 'course_doc' => $faker->lastName,
        // 'course_description' => 'This is a short decscription',
        // 'course_img' => '',
    ];
});


