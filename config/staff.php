<?php
return array (
    0 =>
        array (
            'staff_name' => 'Oregbemi Akeem',
            'staff_email' => 'oregbemiakeem@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 3',
            'staff_id' => '15006',
            'employment_date' => 'March, 04 2015 00:00:00 +0100',
        ),
    1 =>
        array (
            'staff_name' => 'Adewole Adedeji',
            'staff_email' => 'adewole.adedeji@anchoriaam.com',
            'department' => 'Research',
            'grade' => 'Associate 3',
            'staff_id' => '17011',
            'employment_date' => 'October, 03 2017 00:00:00 +0100',
        ),
    2 =>
        array (
            'staff_name' => 'Ekweanya Adaobi',
            'staff_email' => 'adaobi.ekweanya@anchoriaam.com',
            'department' => 'Executive Office',
            'grade' => 'Asst. Manager 2 Level 3',
            'staff_id' => '16005',
            'employment_date' => 'March, 07 2016 00:00:00 +0100',
        ),
    3 =>
        array (
            'staff_name' => 'Johnson Cecilia',
            'staff_email' => 'cecilia.johnson@anchoriaam.com',
            'department' => 'Compliance',
            'grade' => 'Associate 2',
            'staff_id' => '17017',
            'employment_date' => NULL,
        ),
    4 =>
        array (
            'staff_name' => 'Umar Jemila',
            'staff_email' => 'jemila.umar@anchoriaam.com',
            'department' => 'Investment Management',
            'grade' => 'Associate 1',
            'staff_id' => '18006',
            'employment_date' => 'January, 02 2018 00:00:00 +0100',
        ),
    5 =>
        array (
            'staff_name' => 'Onagoruwa Adebukola',
            'staff_email' => 'adebukola.onagoruwa@anchoriaam.com',
            'department' => 'Sales',
            'grade' => 'Associate 1',
            'staff_id' => '17009',
            'employment_date' => NULL,
        ),
    6 =>
        array (
            'staff_name' => 'Salami Busayo',
            'staff_email' => 'busayo.salami@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => 'VF0035',
            'employment_date' => NULL,
        ),
    7 =>
        array (
            'staff_name' => 'Adekoya Ayepada',
            'staff_email' => 'adekoyaayepada@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 3',
            'staff_id' => '16008',
            'employment_date' => 'September, 02 2016 00:00:00 +0100',
        ),
    8 =>
        array (
            'staff_name' => 'Akinrulie Lawrence',
            'staff_email' => 'lawrence.akinrulie@dre.ng',
            'department' => 'Executive Office',
            'grade' => 'MD/CEO',
            'staff_id' => '15001',
            'employment_date' => NULL,
        ),
    9 =>
        array (
            'staff_name' => 'Akinrulie Opeyemi',
            'staff_email' => 'opeyemi.akinrulie@dre.ng',
            'department' => 'Facility',
            'grade' => 'Facility Officer',
            'staff_id' => 'DRE0003',
            'employment_date' => NULL,
        ),
    10 =>
        array (
            'staff_name' => 'Yetu Titus Solomon',
            'staff_email' => 'yetusolomon@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 2',
            'staff_id' => '17002',
            'employment_date' => NULL,
        ),
    11 =>
        array (
            'staff_name' => 'Abubakar Mohammed',
            'staff_email' => 'mohammed.abubakar@dre.ng',
            'department' => 'Project',
            'grade' => 'Contract',
            'staff_id' => '17003',
            'employment_date' => NULL,
        ),
    12 =>
        array (
            'staff_name' => 'Fatoye Motunrayo Evelyn',
            'staff_email' => 'evelyn.fatoye@dre.ng',
            'department' => 'Administration',
            'grade' => 'Admin/HR Officer',
            'staff_id' => '18013',
            'employment_date' => NULL,
        ),
    13 =>
        array (
            'staff_name' => 'Surulere Gbenga',
            'staff_email' => 'gbenga.surulere@dre.ng',
            'department' => 'Quantity Survey',
            'grade' => 'Quantity Surveyor',
            'staff_id' => '18014',
            'employment_date' => NULL,
        ),
    14 =>
        array (
            'staff_name' => 'Ezeh Theresa Chioma',
            'staff_email' => 'theresa.ezeh@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Assistant Manager 1 Level 3',
            'staff_id' => '14002',
            'employment_date' => 'March, 03 2014 00:00:00 +0100',
        ),
    15 =>
        array (
            'staff_name' => 'Omogiafo Amanda Sherifat',
            'staff_email' => 'amanda.omogiafo@everdonbdc.com',
            'department' => 'Group Treasury',
            'grade' => 'Associate 2',
            'staff_id' => '15013',
            'employment_date' => NULL,
        ),
    16 =>
        array (
            'staff_name' => 'Ohakawa Chika',
            'staff_email' => 'chika.ohakawa@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Associate 1 Level 1',
            'staff_id' => '15009',
            'employment_date' => NULL,
        ),
    17 =>
        array (
            'staff_name' => 'Owoade Yemi',
            'staff_email' => 'adeyemi.owoade@everdonbdc.com',
            'department' => 'Finance',
            'grade' => 'Associate 2',
            'staff_id' => '14005',
            'employment_date' => 'October, 02 2014 00:00:00 +0100',
        ),
    18 =>
        array (
            'staff_name' => 'Okoh Chukwuemeka',
            'staff_email' => 'chukwuemeka.okoh@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Associate 2 Level 3',
            'staff_id' => '15014',
            'employment_date' => 'December, 14 2015 00:00:00 +0100',
        ),
    19 =>
        array (
            'staff_name' => 'Oseni Titilayo',
            'staff_email' => 'titilayo.oseni@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Experienced Analyst Level 2',
            'staff_id' => 'EB0016',
            'employment_date' => NULL,
        ),
    20 =>
        array (
            'staff_name' => 'Adediran Doyin',
            'staff_email' => 'adedirandoyin@yahoo.com',
            'department' => 'Administraion',
            'grade' => 'Supervisor Level 1',
            'staff_id' => '13001',
            'employment_date' => 'October, 05 2013 00:00:00 +0100',
        ),
    21 =>
        array (
            'staff_name' => 'Akpauma George Ekwe',
            'staff_email' => 'akpaumageorgea@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 3',
            'staff_id' => '17006',
            'employment_date' => 'May, 17 2017 00:00:00 +0100',
        ),
    22 =>
        array (
            'staff_name' => 'Okache Matthew Ogar',
            'staff_email' => 'okachemathew@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 3',
            'staff_id' => '17008',
            'employment_date' => 'June, 02 2017 00:00:00 +0100',
        ),
    23 =>
        array (
            'staff_name' => 'Adeniyi Richard',
            'staff_email' => 'richard.adeniyi@vfdgroup.com',
            'department' => 'Sales',
            'grade' => 'Analyst Level 2',
            'staff_id' => '15005',
            'employment_date' => 'February, 23 2015 00:00:00 +0100',
        ),
    24 =>
        array (
            'staff_name' => 'Abah Karen',
            'staff_email' => 'karen.abah@vfdgroup.com',
            'department' => 'Operations',
            'grade' => 'Senior Associate 1 Level 1',
            'staff_id' => '14001',
            'employment_date' => 'February, 11 2014 00:00:00 +0100',
        ),
    25 =>
        array (
            'staff_name' => 'Chigbu Queen Mary',
            'staff_email' => 'Queen.Chigbu@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Analyst Level 3',
            'staff_id' => '17010',
            'employment_date' => 'August, 01 2017 00:00:00 +0100',
        ),
    26 =>
        array (
            'staff_name' => 'Amos Francis',
            'staff_email' => 'Francis.Amos@Vfdgroup.com',
            'department' => 'Client Experience',
            'grade' => 'Associate 1',
            'staff_id' => '14003',
            'employment_date' => 'April, 01 2014 00:00:00 +0100',
        ),
    27 =>
        array (
            'staff_name' => 'Onanuga Oyindamola',
            'staff_email' => 'oyindamola.onanuga@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => 'EB0023',
            'employment_date' => NULL,
        ),
    28 =>
        array (
            'staff_name' => 'Ogundiya Temidayo',
            'staff_email' => 'dayo.ogundiya@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Associate 2 Level 2',
            'staff_id' => '18008',
            'employment_date' => 'January, 04 2018 00:00:00 +0100',
        ),
    29 =>
        array (
            'staff_name' => 'Adeniji Oladapo',
            'staff_email' => 'oladapo.adeniji@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Experienced Analyst',
            'staff_id' => '18002',
            'employment_date' => 'January, 02 2018 00:00:00 +0100',
        ),
    30 =>
        array (
            'staff_name' => 'Akinbile Taiwo',
            'staff_email' => 'taiwo.akinbile@everdonbdc.com',
            'department' => 'FX Operations',
            'grade' => 'Analyst',
            'staff_id' => 'EB0026',
            'employment_date' => NULL,
        ),
    31 =>
        array (
            'staff_name' => 'Omoboboye Timilehin',
            'staff_email' => 'timilehin.omoboboye@everdonbdc.com',
            'department' => 'FX Operations',
            'grade' => 'Associate 1',
            'staff_id' => '17013',
            'employment_date' => 'October, 23 2017 00:00:00 +0100',
        ),
    32 =>
        array (
            'staff_name' => 'Okpala Emeka',
            'staff_email' => 'okpalaemeka@yahoo.com',
            'department' => 'Farm',
            'grade' => 'Contract',
            'staff_id' => '17018',
            'employment_date' => 'December, 31 2017 00:00:00 +0100',
        ),
    33 =>
        array (
            'staff_name' => 'Dianabasi Benson Ukpong',
            'staff_email' => 'benson.dianabasi@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Analyst Level 3',
            'staff_id' => '18010',
            'employment_date' => 'January, 16 2018 00:00:00 +0100',
        ),
    34 =>
        array (
            'staff_name' => 'Tsekiri Arume',
            'staff_email' => 'arume.ighoroje@vfdgroup.com',
            'department' => 'Executive Office',
            'grade' => 'Associate 3 Level 1',
            'staff_id' => 'VF0007',
            'employment_date' => NULL,
        ),
    35 =>
        array (
            'staff_name' => 'Olalekan Rahman',
            'staff_email' => 'rahman.olalekan@vfdgroup.com',
            'department' => 'Administration',
            'grade' => 'Associate 1',
            'staff_id' => '16003',
            'employment_date' => 'January, 04 2016 00:00:00 +0100',
        ),
    36 =>
        array (
            'staff_name' => 'Okpala Nonso Michael',
            'staff_email' => 'nonso.okpala@vfdgroup.com',
            'department' => 'Executive Office',
            'grade' => 'Director',
            'staff_id' => '16001',
            'employment_date' => 'January, 04 2016 00:00:00 +0100',
        ),
    37 =>
        array (
            'staff_name' => 'Tafa Osisiye',
            'staff_email' => 'osisiye.tafa@vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Senior Associate 2 Level 1',
            'staff_id' => 'VF0013',
            'employment_date' => NULL,
        ),
    38 =>
        array (
            'staff_name' => 'Benson-Onaji Monica',
            'staff_email' => 'monica.benson-onaji@vfdgroup.com',
            'department' => 'Human Resources',
            'grade' => 'Asst. Manager 2 Level 3',
            'staff_id' => '15012',
            'employment_date' => 'November, 15 2015 00:00:00 +0100',
        ),
    39 =>
        array (
            'staff_name' => 'Shoda Gbeminiyi',
            'staff_email' => 'gbeminiyi.shoda@vfdgroup.com',
            'department' => 'Legal',
            'grade' => 'Senior Associate 2 Level 2',
            'staff_id' => '15007',
            'employment_date' => 'March, 30 2015 00:00:00 +0100',
        ),
    40 =>
        array (
            'staff_name' => 'Esike Chioma',
            'staff_email' => 'chioma.esike@vfdgroup.com',
            'department' => 'Executive Office',
            'grade' => 'Senior Associate 2 Level 3',
            'staff_id' => '16007',
            'employment_date' => 'June, 15 2016 00:00:00 +0100',
        ),
    41 =>
        array (
            'staff_name' => 'Osueke Christian',
            'staff_email' => 'christian.osueke@vfdgroup.com',
            'department' => 'Information Technology',
            'grade' => 'Experienced Analyst',
            'staff_id' => '16010',
            'employment_date' => 'October, 24 2016 00:00:00 +0100',
        ),
    42 =>
        array (
            'staff_name' => 'Okoye Chigozie Kate',
            'staff_email' => 'chigozie.okoye@vfdgroup.com',
            'department' => 'Human Resources',
            'grade' => 'Associate 3,Level 2',
            'staff_id' => '16006',
            'employment_date' => 'March, 16 2016 00:00:00 +0100',
        ),
    43 =>
        array (
            'staff_name' => 'Anigbogu Kodichinma',
            'staff_email' => 'kodichinma.anigbogu@vfdgroup.com',
            'department' => 'Legal',
            'grade' => 'Analyst',
            'staff_id' => 'VF0024',
            'employment_date' => NULL,
        ),
    44 =>
        array (
            'staff_name' => 'Imeigha Phidelia',
            'staff_email' => 'phidelia.imiegha@vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Associate 1 Level 1',
            'staff_id' => 'VF0034',
            'employment_date' => NULL,
        ),
    45 =>
        array (
            'staff_name' => 'Ogunbiyi David',
            'staff_email' => 'ogunbiyidavid@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 2',
            'staff_id' => 'VF0046',
            'employment_date' => NULL,
        ),
    46 =>
        array (
            'staff_name' => 'Adebukunola Oluwatosin',
            'staff_email' => 'oluwatosin.adebukunola@vfdgroup.com',
            'department' => 'Strategy',
            'grade' => 'Senior Associate 1',
            'staff_id' => '17004',
            'employment_date' => 'March, 01 2017 00:00:00 +0100',
        ),
    47 =>
        array (
            'staff_name' => 'Abel Oluwatoyin',
            'staff_email' => 'Oluwatoyin.Abel@Everdonbdc.com',
            'department' => 'Sales',
            'grade' => 'Assistant Manager 2',
            'staff_id' => '16011',
            'employment_date' => 'December, 01 2016 00:00:00 +0100',
        ),
    48 =>
        array (
            'staff_name' => 'Adenrele Omolara',
            'staff_email' => 'omolara.adenrele@vfdgroup.com',
            'department' => 'Sales',
            'grade' => 'Senior Associate 2 Level 3',
            'staff_id' => 'VF0025',
            'employment_date' => NULL,
        ),
    49 =>
        array (
            'staff_name' => 'Oladele Oladoyin Solomon',
            'staff_email' => 'oladelesolomon@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Associate Supervisor 1',
            'staff_id' => '16002',
            'employment_date' => 'January, 04 2016 00:00:00 +0100',
        ),
    50 =>
        array (
            'staff_name' => 'Ude Chidimma Ogechukwu',
            'staff_email' => 'chidimma.ude@vfdgroup.com',
            'department' => 'Legal',
            'grade' => 'Associate 2 Level 2',
            'staff_id' => '17012',
            'employment_date' => 'October, 03 2017 00:00:00 +0100',
        ),
    51 =>
        array (
            'staff_name' => 'Umar Khalifah Aminu',
            'staff_email' => 'khalifah.umar@vfdgroup.com',
            'department' => 'Executive Office',
            'grade' => 'Associate 2',
            'staff_id' => '17014',
            'employment_date' => 'October, 30 2017 00:00:00 +0100',
        ),
    52 =>
        array (
            'staff_name' => 'Maxwell Anthony',
            'staff_email' => 'maxwellanthony@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 3',
            'staff_id' => '17001',
            'employment_date' => 'January, 02 2017 00:00:00 +0100',
        ),
    53 =>
        array (
            'staff_name' => 'Nwoye Ebuka',
            'staff_email' => 'ebuka.nwoye@vfdgroup.com',
            'department' => 'Administration',
            'grade' => 'Analyst',
            'staff_id' => '17015',
            'employment_date' => 'November, 20 2017 00:00:00 +0100',
        ),
    54 =>
        array (
            'staff_name' => 'Gana-Mahmoud Fatimah',
            'staff_email' => 'Fatimah.Gana-Mahmoud@anchoriaam.com',
            'department' => 'Sales',
            'grade' => 'Senior Associate 1 Level 1',
            'staff_id' => '18003',
            'employment_date' => 'January, 02 2018 00:00:00 +0100',
        ),
    55 =>
        array (
            'staff_name' => 'Osunleke Damilare',
            'staff_email' => 'damilare.osunleke@vfdgroup.com',
            'department' => 'Information Technology',
            'grade' => 'Analyst',
            'staff_id' => 'VF0047',
            'employment_date' => NULL,
        ),
    56 =>
        array (
            'staff_name' => 'Oluwabusola Oluwaseun',
            'staff_email' => 'oluwaseun.oluwabusola@vfdgroup.com',
            'department' => 'Strategy',
            'grade' => 'Associate 2',
            'staff_id' => '18005',
            'employment_date' => 'January, 02 2018 00:00:00 +0100',
        ),
    57 =>
        array (
            'staff_name' => 'Ogungbaro Jerry',
            'staff_email' => 'jerry.ogungbaro@vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Associate 2 Level 2',
            'staff_id' => 'VF0039',
            'employment_date' => NULL,
        ),
    58 =>
        array (
            'staff_name' => 'Daniel Babatunde Moses',
            'staff_email' => 'daniel.babatunde@vfdgroup.com',
            'department' => 'Information Technology',
            'grade' => 'Associate 3',
            'staff_id' => '17016',
            'employment_date' => 'December, 11 2017 00:00:00 +0100',
        ),
    59 =>
        array (
            'staff_name' => 'Emodi Azubuike',
            'staff_email' => 'azubike.emodi@vfd-mfb.com',
            'department' => 'Executive Office',
            'grade' => 'Director',
            'staff_id' => '16004',
            'employment_date' => 'February, 01 2016 00:00:00 +0100',
        ),
    60 =>
        array (
            'staff_name' => 'Agbanusi Chidi',
            'staff_email' => 'chidi.agbanusi@vfd-mfb.com',
            'department' => 'Finance',
            'grade' => 'Associate 3, Level 3',
            'staff_id' => 'VM0002',
            'employment_date' => NULL,
        ),
    61 =>
        array (
            'staff_name' => 'Ugochukwu Samuel',
            'staff_email' => 'samuel.ugochukwu@vfd-mfb.com',
            'department' => 'Recovery',
            'grade' => 'Associate 1 Level 2',
            'staff_id' => '15008',
            'employment_date' => 'July, 01 2015 00:00:00 +0100',
        ),
    62 =>
        array (
            'staff_name' => 'Ogunmodede Seun',
            'staff_email' => 'seun.ogunmodede@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Associate 1 Level 1',
            'staff_id' => '13002',
            'employment_date' => 'October, 14 2013 00:00:00 +0100',
        ),
    63 =>
        array (
            'staff_name' => 'Adesorioye Mercy',
            'staff_email' => 'mercy.adesorioye@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Associate 1 Level 2',
            'staff_id' => '16009',
            'employment_date' => 'October, 01 2016 00:00:00 +0100',
        ),
    64 =>
        array (
            'staff_name' => 'Oyero Kolawole',
            'staff_email' => 'oyerokolawole@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 3',
            'staff_id' => '14004',
            'employment_date' => 'September, 22 2014 00:00:00 +0100',
        ),
    65 =>
        array (
            'staff_name' => 'Sulaiman Abdulahi',
            'staff_email' => 'sulaimanabdulahi@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 3',
            'staff_id' => '15011',
            'employment_date' => 'November, 02 2015 00:00:00 +0100',
        ),
    66 =>
        array (
            'staff_name' => 'Olaitan Bosun Esther',
            'staff_email' => 'olaitanesther@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 1',
            'staff_id' => 'VM0023',
            'employment_date' => NULL,
        ),
    67 =>
        array (
            'staff_name' => 'Onuoha Chukwudi Andrew',
            'staff_email' => 'chukwudi.onuoha@vfd-mfb.com',
            'department' => 'Internal Control',
            'grade' => 'Associate 2 Level 3',
            'staff_id' => '17005',
            'employment_date' => NULL,
        ),
    68 =>
        array (
            'staff_name' => 'Alade Abiodun Emmanuel',
            'staff_email' => 'abiodun.alade@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Experienced Analyst Level 3',
            'staff_id' => 'VF0032',
            'employment_date' => NULL,
        ),
    69 =>
        array (
            'staff_name' => 'Lawal Olalekan',
            'staff_email' => 'olalekan.lawal@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Experienced Analyst Level 2',
            'staff_id' => '15004',
            'employment_date' => 'February, 02 2015 00:00:00 +0100',
        ),
    70 =>
        array (
            'staff_name' => 'Majekodunmi David',
            'staff_email' => 'david.majekodunmi@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Associate 2 Level 2',
            'staff_id' => '15002',
            'employment_date' => 'January, 23 2015 00:00:00 +0100',
        ),
    71 =>
        array (
            'staff_name' => 'Mokum Samuel',
            'staff_email' => 'samuel.mokum@vfdgroup.com',
            'department' => 'Recovery',
            'grade' => 'Associate 2 Level 2',
            'staff_id' => '15003',
            'employment_date' => 'January, 23 2015 00:00:00 +0100',
        ),
    72 =>
        array (
            'staff_name' => 'Abia Kingsley',
            'staff_email' => 'kingsley.abia@vfd-mfb.com',
            'department' => 'Information Technology',
            'grade' => 'Experienced Analyst',
            'staff_id' => '18001',
            'employment_date' => 'January, 02 2018 00:00:00 +0100',
        ),
    73 =>
        array (
            'staff_name' => 'Nwaka Victor',
            'staff_email' => 'victor.nwaka@vfd-mfb.com',
            'department' => 'Information Technology',
            'grade' => 'Experienced Analyst',
            'staff_id' => '18004',
            'employment_date' => 'January, 02 2018 00:00:00 +0100',
        ),
    74 =>
        array (
            'staff_name' => 'Ajibuwa Opeyemi',
            'staff_email' => 'opeyemi.ajibuwa@vfd-mfb.com',
            'department' => 'Information Technology',
            'grade' => 'Analyst',
            'staff_id' => 'VM0033',
            'employment_date' => NULL,
        ),
    75 =>
        array (
            'staff_name' => 'Ighure Ojiyovbi',
            'staff_email' => 'ojiyovbi.ighure@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Analyst',
            'staff_id' => '18007',
            'employment_date' => 'January, 03 2018 00:00:00 +0100',
        ),
    76 =>
        array (
            'staff_name' => 'Kalesanwo Bayowa',
            'staff_email' => 'bayowa.kalesanwo@vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Experienced Analyst',
            'staff_id' => 'VF0053',
            'employment_date' => NULL,
        ),
    77 =>
        array (
            'staff_name' => 'Okoduwa Ehimare Philip',
            'staff_email' => 'Philip.Okoduwa@vfdgroup.com',
            'department' => 'Information Technology',
            'grade' => 'Analyst',
            'staff_id' => '18012',
            'employment_date' => 'January, 31 2018 00:00:00 +0100',
        ),
    78 =>
        array (
            'staff_name' => 'Olajide Williams Abidemi',
            'staff_email' => 'olajide.abidemi@vfdgroup.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 2',
            'staff_id' => '18011',
            'employment_date' => 'January, 29 2018 00:00:00 +0100',
        ),
    79 =>
        array (
            'staff_name' => 'Olaofe Adekunle Rotimi',
            'staff_email' => 'adekunle.olaofe@vfdgroup.com',
            'department' => 'Legal',
            'grade' => 'Associate 2 Level 3',
            'staff_id' => 'VF0054',
            'employment_date' => NULL,
        ),
    80 =>
        array (
            'staff_name' => 'Adeleye Folajimi Oluwole',
            'staff_email' => 'Folajimi.Adeleye@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Deputy Vice President Level 2',
            'staff_id' => '18016',
            'employment_date' => 'April, 16 2018 00:00:00 +0100',
        ),
    81 =>
        array (
            'staff_name' => 'Omolokun Gbenga',
            'staff_email' => 'gbenga.omolokun@vfdgroup.com',
            'department' => 'Executive Office',
            'grade' => 'Director',
            'staff_id' => '15010',
            'employment_date' => 'August, 17 2015 00:00:00 +0100',
        ),
    82 =>
        array (
            'staff_name' => 'Adenubi Adeniyi',
            'staff_email' => 'niyi.adenubi@vfdgroup.com',
            'department' => 'Executive Office',
            'grade' => 'Director',
            'staff_id' => '18044',
            'employment_date' => 'October, 01 2018 00:00:00 +0100',
        ),
    83 =>
        array (
            'staff_name' => 'Iwuoha Chidozie Kelechi',
            'staff_email' => 'chidozie.iwuoha@everdonbdc.com',
            'department' => 'Sales',
            'grade' => 'Analyst Level 3',
            'staff_id' => '18009',
            'employment_date' => 'January, 15 2018 00:00:00 +0100',
        ),
    84 =>
        array (
            'staff_name' => 'Nwachukwu Felicia Chinyere',
            'staff_email' => 'Felicia.Nwachukwu@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Experienced Analyst Level 2',
            'staff_id' => '18018',
            'employment_date' => 'May, 03 2018 00:00:00 +0100',
        ),
    85 =>
        array (
            'staff_name' => 'Adewuyi Abolaji Simbiat',
            'staff_email' => 'abolaji.adewuyi@anchoriaam.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => 'IT0002',
            'employment_date' => NULL,
        ),
    86 =>
        array (
            'staff_name' => 'Egbeyemi Joseph Kehinde',
            'staff_email' => 'joseph.egbeyemi@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Intern',
            'staff_id' => 'IT0003',
            'employment_date' => NULL,
        ),
    87 =>
        array (
            'staff_name' => 'Ige Mayowa',
            'staff_email' => 'mayowa.ige@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Analyst',
            'staff_id' => 'IT0001',
            'employment_date' => 'July, 20 2017 00:00:00 +0100',
        ),
    88 =>
        array (
            'staff_name' => 'Aderibigbe Adeoluwa',
            'staff_email' => 'adeoluwa.aderibigbe@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Experienced Analyst',
            'staff_id' => '18061',
            'employment_date' => 'November, 01 2018 00:00:00 +0100',
        ),
    89 =>
        array (
            'staff_name' => 'Adeoti Tobi Elijah',
            'staff_email' => 'tobi.adeoti@anchoriaam.com',
            'department' => 'Research',
            'grade' => 'Intern',
            'staff_id' => 'IT0004',
            'employment_date' => NULL,
        ),
    90 =>
        array (
            'staff_name' => 'Agbalaya Abosede Odunayo',
            'staff_email' => 'Abosede.Agbalaya@everdonbdc.com',
            'department' => 'FX Operations',
            'grade' => 'Analyst',
            'staff_id' => 'EB0032',
            'employment_date' => NULL,
        ),
    91 =>
        array (
            'staff_name' => 'Ekpoh Michael Ikoro',
            'staff_email' => 'Michael.Ekpo@sictl.com.ng',
            'department' => 'Project',
            'grade' => NULL,
            'staff_id' => '18015',
            'employment_date' => 'February, 23 2018 00:00:00 +0100',
        ),
    92 =>
        array (
            'staff_name' => 'Olatunji Olusoji Ayooluwa',
            'staff_email' => 'Olusoji.Olatunji@sictl.com.ng',
            'department' => 'Project',
            'grade' => NULL,
            'staff_id' => '18017',
            'employment_date' => 'April, 30 2018 00:00:00 +0100',
        ),
    93 =>
        array (
            'staff_name' => 'Umeh Monica Ogechukwu',
            'staff_email' => 'Monica.Umeh@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Experienced Analyst Level 3',
            'staff_id' => '18022',
            'employment_date' => 'May, 21 2018 00:00:00 +0100',
        ),
    94 =>
        array (
            'staff_name' => 'Adeoye Oladipupo',
            'staff_email' => 'dipo.adeoye@vfdgroup.com',
            'department' => 'Group Treasury',
            'grade' => 'Deputy Vice President Level 2',
            'staff_id' => '18020',
            'employment_date' => 'May, 14 2018 00:00:00 +0100',
        ),
    95 =>
        array (
            'staff_name' => 'Azeez Jinodu Babatunde',
            'staff_email' => 'Azeez.j@yahoo.com',
            'department' => 'Administraion',
            'grade' => 'Supervisor Level 2',
            'staff_id' => '18019',
            'employment_date' => 'May, 07 2018 00:00:00 +0100',
        ),
    96 =>
        array (
            'staff_name' => 'Adesalu Oluwabunmi',
            'staff_email' => 'bunmi.adesalu@vfdgroup.com',
            'department' => 'Client Experience',
            'grade' => 'Senior Vice President',
            'staff_id' => 'VF0058',
            'employment_date' => NULL,
        ),
    97 =>
        array (
            'staff_name' => 'Obiji Chikodi Akuadi',
            'staff_email' => 'Chikodi.Obiji@anchoriaam.com',
            'department' => 'Research',
            'grade' => 'Analyst',
            'staff_id' => '18023',
            'employment_date' => 'June, 04 2018 00:00:00 +0100',
        ),
    98 =>
        array (
            'staff_name' => 'Mebube Konyinsola',
            'staff_email' => 'Mebude.konyinsola@vfd-mfb.com',
            'department' => 'Finance',
            'grade' => 'Intern',
            'staff_id' => 'IT0007',
            'employment_date' => 'June, 11 2018 00:00:00 +0100',
        ),
    99 =>
        array (
            'staff_name' => 'Onishemo Damilola',
            'staff_email' => 'Damilola.Onishemo@anchoriaam.com',
            'department' => 'Sales',
            'grade' => 'Associate 2',
            'staff_id' => '18025',
            'employment_date' => 'June, 19 2018 00:00:00 +0100',
        ),
    100 =>
        array (
            'staff_name' => 'Umeh Ugochukwu',
            'staff_email' => 'ugo.umeh@vfdgroup.com',
            'department' => 'Information Technology',
            'grade' => NULL,
            'staff_id' => '18021',
            'employment_date' => 'May, 14 2018 00:00:00 +0100',
        ),
    101 =>
        array (
            'staff_name' => 'Ukaegbu Chukwuemeka',
            'staff_email' => 'Chukwuemeka.Ukaegbu@vfd-mfb.com',
            'department' => 'Recovery',
            'grade' => 'Analyst',
            'staff_id' => '18024',
            'employment_date' => 'June, 04 2018 00:00:00 +0100',
        ),
    102 =>
        array (
            'staff_name' => 'Uchenna Tochi Donald',
            'staff_email' => 'uchenna.tochi@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Intern',
            'staff_id' => 'IT0006',
            'employment_date' => 'May, 25 2018 00:00:00 +0100',
        ),
    103 =>
        array (
            'staff_name' => 'Balogun Ibrahim Abiola',
            'staff_email' => 'Ibrahim.Balogun@vfd-mfb.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => '18026',
            'employment_date' => 'June, 25 2018 00:00:00 +0100',
        ),
    104 =>
        array (
            'staff_name' => 'Ogba Christopher',
            'staff_email' => 'Christopher.Ogba@Everdonbdc.com',
            'department' => 'Finance',
            'grade' => 'Associate 3, Level 3',
            'staff_id' => '18027',
            'employment_date' => 'June, 25 2018 00:00:00 +0100',
        ),
    105 =>
        array (
            'staff_name' => 'Eziamaka Obiageli Jennifer',
            'staff_email' => 'Obiageli.Eziamaka@Everdonbdc.com',
            'department' => 'FX Operations',
            'grade' => 'Analyst',
            'staff_id' => '18028',
            'employment_date' => NULL,
        ),
    106 =>
        array (
            'staff_name' => 'Uzoagu Chinonso Ndubueze',
            'staff_email' => 'chinonso.uzoagu@vfdgroup.com',
            'department' => 'Information Technology',
            'grade' => 'Intern',
            'staff_id' => 'IT0008',
            'employment_date' => NULL,
        ),
    107 =>
        array (
            'staff_name' => 'Adedoyin Bukola',
            'staff_email' => 'Bukola.Adedoyin@vfdgroup.com',
            'department' => 'Operations',
            'grade' => 'Intern',
            'staff_id' => 'IT0009',
            'employment_date' => 'November, 13 2017 00:00:00 +0100',
        ),
    108 =>
        array (
            'staff_name' => 'Akata Favour',
            'staff_email' => 'Favour.Akata@Vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Senior Associate 1 Level 1',
            'staff_id' => 'VF0060',
            'employment_date' => NULL,
        ),
    109 =>
        array (
            'staff_name' => 'Ogheneme Damilola Ufuoma',
            'staff_email' => 'Damilola.ogheneme@vfdgroup.com',
            'department' => 'Client Experience',
            'grade' => 'Analyst',
            'staff_id' => '18032',
            'employment_date' => 'July, 23 2018 00:00:00 +0100',
        ),
    110 =>
        array (
            'staff_name' => 'Henry - Toochukwu Sylvia',
            'staff_email' => 'Sylvia.Henry-Toochukwu@Vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Assistant Vice President',
            'staff_id' => '18030',
            'employment_date' => 'July, 16 2018 00:00:00 +0100',
        ),
    111 =>
        array (
            'staff_name' => 'Dimieari Dabobelema',
            'staff_email' => 'dabobelema.dimieari@vfdgroup.com',
            'department' => 'Client Experience',
            'grade' => 'Contract',
            'staff_id' => '18031',
            'employment_date' => 'July, 23 2018 00:00:00 +0100',
        ),
    112 =>
        array (
            'staff_name' => 'Maduka Chinesom Maduka',
            'staff_email' => 'chinesom.maduka@anchoriaam.com',
            'department' => 'Executive Office',
            'grade' => 'Associate 1 Level 2',
            'staff_id' => '18035',
            'employment_date' => 'August, 27 2018 00:00:00 +0100',
        ),
    113 =>
        array (
            'staff_name' => 'Doumu Sarah Olaere',
            'staff_email' => 'Sarah.Doumu@Vfdgroup.com',
            'department' => 'Finance',
            'grade' => NULL,
            'staff_id' => 'IT0013',
            'employment_date' => NULL,
        ),
    114 =>
        array (
            'staff_name' => 'Olatunji David',
            'staff_email' => 'David.Olatunji@vfd-mfb.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => '18062',
            'employment_date' => 'November, 01 2018 00:00:00 +0100',
        ),
    115 =>
        array (
            'staff_name' => 'Adegoke Idowu Victor',
            'staff_email' => 'Victor.Adegoke@Everdonbdc.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => '19035',
            'employment_date' => 'August, 06 2018 00:00:00 +0100',
        ),
    116 =>
        array (
            'staff_name' => 'Buhari Surat Temitope',
            'staff_email' => 'Surat.Buhari@Everdonbdc.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => '19036',
            'employment_date' => 'August, 08 2018 00:00:00 +0100',
        ),
    117 =>
        array (
            'staff_name' => 'Emmanuel Eno John',
            'staff_email' => 'eno.emmanuel@vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Associate 2,Level 1',
            'staff_id' => '18034',
            'employment_date' => 'August, 17 2018 00:00:00 +0100',
        ),
    118 =>
        array (
            'staff_name' => 'Akindele Oyinkansola Moyo',
            'staff_email' => 'Oyinkansola.Akindele@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Intern',
            'staff_id' => 'IT0014',
            'employment_date' => 'October, 15 2018 00:00:00 +0100',
        ),
    119 =>
        array (
            'staff_name' => 'Okeke Kelechi',
            'staff_email' => 'Kelechi.Okeke@drelugs.com',
            'department' => 'Executive Office',
            'grade' => NULL,
            'staff_id' => '18029',
            'employment_date' => 'July, 01 2018 00:00:00 +0100',
        ),
    120 =>
        array (
            'staff_name' => 'Chibuike Godfrey Kingsley',
            'staff_email' => 'Kingsley.Godfrey@drelugs.com',
            'department' => 'Sales',
            'grade' => NULL,
            'staff_id' => 'DL0003',
            'employment_date' => NULL,
        ),
    121 =>
        array (
            'staff_name' => 'Ozulorah Chioma Gloria',
            'staff_email' => 'Chioma.Ozulorah@drelugs.com',
            'department' => 'Sales',
            'grade' => NULL,
            'staff_id' => 'DL0002',
            'employment_date' => NULL,
        ),
    122 =>
        array (
            'staff_name' => 'Ugwu Chibuike Friday',
            'staff_email' => 'chibuike.ugwu@vfdgroup.com',
            'department' => 'Information Technology',
            'grade' => 'Intern',
            'staff_id' => 'IT0015',
            'employment_date' => 'September, 03 2018 00:00:00 +0100',
        ),
    123 =>
        array (
            'staff_name' => 'Igboekwe Juliet Uche',
            'staff_email' => 'Juliet.Igboekwe@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Associate 1 Level 2',
            'staff_id' => '18039',
            'employment_date' => 'September, 03 2018 00:00:00 +0100',
        ),
    124 =>
        array (
            'staff_name' => 'Chukwurah Patrick',
            'staff_email' => 'Patrick.Chukwurah@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Experienced Analyst Level 3',
            'staff_id' => '18038',
            'employment_date' => 'September, 03 2018 00:00:00 +0100',
        ),
    125 =>
        array (
            'staff_name' => 'Awo Mabel Adaobi',
            'staff_email' => 'Mabel.Awo@Everdonbdc.com',
            'department' => 'Client Experience',
            'grade' => 'Associate Supervisor',
            'staff_id' => '18036',
            'employment_date' => 'September, 03 2018 00:00:00 +0100',
        ),
    126 =>
        array (
            'staff_name' => 'Oyedokun Muyiwa Damilare',
            'staff_email' => 'muyiwa.oyedokun@vfd-mfb.com',
            'department' => 'Internal Control',
            'grade' => 'Experienced Analyst Level 2',
            'staff_id' => '18043',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    127 =>
        array (
            'staff_name' => 'Hassan Oyindamola Sadiat',
            'staff_email' => 'Oyindamola.Hassan@vfd-mfb.com',
            'department' => 'Client Experience',
            'grade' => 'Associate Supervisor',
            'staff_id' => '18040',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    128 =>
        array (
            'staff_name' => 'Chidoka Samuel',
            'staff_email' => 'sam.chidoka@kairoscapitalng.com',
            'department' => 'Executive Office',
            'grade' => 'MD/CEO',
            'staff_id' => '18037',
            'employment_date' => 'September, 03 2018 00:00:00 +0100',
        ),
    129 =>
        array (
            'staff_name' => 'Odujoko Adebola',
            'staff_email' => 'adebola.odujoko@vfdgroup.com',
            'department' => 'Client Experience',
            'grade' => 'Analyst Level 2',
            'staff_id' => '18042',
            'employment_date' => NULL,
        ),
    130 =>
        array (
            'staff_name' => 'Aikay-Unegbu Perpetua',
            'staff_email' => 'pep.aikay-unegbu@kairoscapitalng.com',
            'department' => 'Project',
            'grade' => 'Assistant Vice President',
            'staff_id' => '18041',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    131 =>
        array (
            'staff_name' => 'Mbaeze Testimony Ijeoma',
            'staff_email' => 'testimony.mbaeze@vfd-mfb.com',
            'department' => 'Legal',
            'grade' => NULL,
            'staff_id' => 'IT00016',
            'employment_date' => NULL,
        ),
    132 =>
        array (
            'staff_name' => 'Olarele Adedamilola',
            'staff_email' => 'Adedamilola.Olarele@Everdonbdc.com',
            'department' => 'FX Operations',
            'grade' => 'Analyst',
            'staff_id' => '19001',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    133 =>
        array (
            'staff_name' => 'Ogbeide Osemudiamen Oghenetega',
            'staff_email' => 'Osemudiamen.Ogbeide@Vfdgroup.com',
            'department' => 'Executive Office',
            'grade' => 'Analyst',
            'staff_id' => '19002',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    134 =>
        array (
            'staff_name' => 'Otokpa Innocent Inalegwu',
            'staff_email' => 'Innocent.Otokpa@Vfdgroup.com',
            'department' => 'GTP',
            'grade' => NULL,
            'staff_id' => 'VF0072',
            'employment_date' => NULL,
        ),
    135 =>
        array (
            'staff_name' => 'Moses Pinna',
            'staff_email' => 'Pinna.Moses@Vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => '19003',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    136 =>
        array (
            'staff_name' => 'Benaiah Chukwuemeka Liberation',
            'staff_email' => 'Chukwuemeka.Benaiah@Everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Analyst',
            'staff_id' => '19005',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    137 =>
        array (
            'staff_name' => 'Tiamiyu-musa Zainab',
            'staff_email' => 'Zainab.Tiamiyu-musa@Everdonbdc.com',
            'department' => 'FX Operations',
            'grade' => 'Analyst',
            'staff_id' => '19006',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    138 =>
        array (
            'staff_name' => 'Akpowaye Rukevwe Laura',
            'staff_email' => 'Rukevwe.Akpowaye@anchoriaam.com',
            'department' => 'Operations',
            'grade' => 'Analyst',
            'staff_id' => '19007',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    139 =>
        array (
            'staff_name' => 'Abel Anna Ogechi',
            'staff_email' => 'Anna.Abel@Kairoscapitalng.com',
            'department' => 'Operations',
            'grade' => 'Analyst',
            'staff_id' => '19008',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    140 =>
        array (
            'staff_name' => 'Adediran Oluwamayowa',
            'staff_email' => 'Oluwamayowa.Adediran@Vfdgroup.com',
            'department' => 'GTP',
            'grade' => 'Management Trainee',
            'staff_id' => 'VF0079',
            'employment_date' => NULL,
        ),
    141 =>
        array (
            'staff_name' => 'Ogbeche Lilian Ogbene',
            'staff_email' => 'Lilian.Ogbeche@Everdonbdc.com',
            'department' => 'FX Operations',
            'grade' => 'Analyst',
            'staff_id' => '19009',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    142 =>
        array (
            'staff_name' => 'Olaosebikan Emmanuel Opeyemi',
            'staff_email' => 'Emmanuel.Olaosebikan@Vfdgroup.com',
            'department' => 'GTP',
            'grade' => 'Management Trainee',
            'staff_id' => 'VF0081',
            'employment_date' => NULL,
        ),
    143 =>
        array (
            'staff_name' => 'Onodenalore Victor Irabor',
            'staff_email' => 'Victor.Onodenalore@Everdonbdc.com',
            'department' => 'FX Operations',
            'grade' => 'Analyst',
            'staff_id' => '19010',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    144 =>
        array (
            'staff_name' => 'Igbinoba Osarieme Georgina',
            'staff_email' => 'Osarieme.Igbinoba@anchoriaam.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Analyst',
            'staff_id' => '19011',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    145 =>
        array (
            'staff_name' => 'Adetula Abimbola Christie',
            'staff_email' => 'Abimbola.Adetula@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Analyst',
            'staff_id' => '19012',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    146 =>
        array (
            'staff_name' => 'Daramola Tofarati Usifo',
            'staff_email' => 'Tofarati.Daramola@vfd-mfb.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => '19013',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    147 =>
        array (
            'staff_name' => 'Muraina Shakiru Adebare',
            'staff_email' => 'Shakiru.Muraina@Vfdgroup.com',
            'department' => 'Strategy',
            'grade' => 'Analyst',
            'staff_id' => '19014',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    148 =>
        array (
            'staff_name' => 'Usifoh Kingsley',
            'staff_email' => 'Kingsley.Usifoh@Vfdgroup.com',
            'department' => 'GTP',
            'grade' => 'Management Trainee',
            'staff_id' => 'VF0087',
            'employment_date' => NULL,
        ),
    149 =>
        array (
            'staff_name' => 'Alonge Tomide Toluwalemi',
            'staff_email' => 'Tomide.Alonge@anchoriaam.com',
            'department' => 'Operations',
            'grade' => 'Analyst',
            'staff_id' => '19015',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    150 =>
        array (
            'staff_name' => 'NnamdI Obinna Louis',
            'staff_email' => 'Obinna.Nnamdi@Everdonbdc.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Analyst',
            'staff_id' => '19016',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    151 =>
        array (
            'staff_name' => 'Adesoyin Fehintola',
            'staff_email' => 'Fehintola.Adesoyin@Kairoscapitalng.com',
            'department' => 'Investment Management',
            'grade' => 'Assistant Vice President Level 1',
            'staff_id' => '18045',
            'employment_date' => 'October, 02 2018 00:00:00 +0100',
        ),
    152 =>
        array (
            'staff_name' => 'Matthew Oluwatoyin',
            'staff_email' => 'Oluwatoyin.Matthew@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Analyst',
            'staff_id' => '19004',
            'employment_date' => 'September, 10 2018 00:00:00 +0100',
        ),
    153 =>
        array (
            'staff_name' => 'Makinde Damilola Bunmi',
            'staff_email' => 'damilola.makinde@vfdgroup.com',
            'department' => 'Human Resources',
            'grade' => 'Asst. Manager 2 Level 2',
            'staff_id' => '18046',
            'employment_date' => 'October, 02 2018 00:00:00 +0100',
        ),
    154 =>
        array (
            'staff_name' => 'Oamen Lucky Iyere',
            'staff_email' => 'lucky.oamen@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Senior Associate 1 Level 1',
            'staff_id' => '18047',
            'employment_date' => NULL,
        ),
    155 =>
        array (
            'staff_name' => 'Eni-Ikeh Kanayo Anthony',
            'staff_email' => 'kanayo.Eni-ikeh@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Assistant Manager 2',
            'staff_id' => '18050',
            'employment_date' => 'October, 08 2018 00:00:00 +0100',
        ),
    156 =>
        array (
            'staff_name' => 'Okomowho Bryan Omovwera',
            'staff_email' => 'Bryan.Okomowho@Kairoscapitalng.com',
            'department' => 'Investment Management',
            'grade' => 'Associate 1 Level 1',
            'staff_id' => '18048',
            'employment_date' => 'October, 02 2018 00:00:00 +0100',
        ),
    157 =>
        array (
            'staff_name' => 'Orok Richard Etim',
            'staff_email' => 'orok.etim@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 2',
            'staff_id' => '18049',
            'employment_date' => 'October, 02 2018 00:00:00 +0100',
        ),
    158 =>
        array (
            'staff_name' => 'Onyemere Patrick',
            'staff_email' => 'onyemerepatrick@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Contract',
            'staff_id' => '17007',
            'employment_date' => 'June, 01 2017 00:00:00 +0100',
        ),
    159 =>
        array (
            'staff_name' => 'Onuegbu Tony Chinedum',
            'staff_email' => 'tony.onuegbu@anchoriaam.com',
            'department' => 'Operations',
            'grade' => 'Contract',
            'staff_id' => '18033',
            'employment_date' => 'July, 30 2018 00:00:00 +0100',
        ),
    160 =>
        array (
            'staff_name' => 'Akerele Temitayo Victoria',
            'staff_email' => 'Temitayo.Akerele@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Intern',
            'staff_id' => 'LO0001',
            'employment_date' => 'September, 25 2018 00:00:00 +0100',
        ),
    161 =>
        array (
            'staff_name' => 'Uduigwomen Efearue',
            'staff_email' => 'Efearue.Uduigwomen@Vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Associate 2,Level 1',
            'staff_id' => '18053',
            'employment_date' => 'October, 22 2018 00:00:00 +0100',
        ),
    162 =>
        array (
            'staff_name' => 'Ezekewem Emeka',
            'staff_email' => 'ezekwememmaaz345@gmail.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 2',
            'staff_id' => '18051',
            'employment_date' => 'October, 08 2018 00:00:00 +0100',
        ),
    163 =>
        array (
            'staff_name' => 'Cobbina Lucia',
            'staff_email' => 'Lucia.Cobbina@anchoriaam.com',
            'department' => 'Sales',
            'grade' => 'Senior Associate 1 Level 1',
            'staff_id' => '18052',
            'employment_date' => 'October, 22 2018 00:00:00 +0100',
        ),
    164 =>
        array (
            'staff_name' => 'Sado Sandra',
            'staff_email' => 'sandra.sado@vfdgroup.com',
            'department' => 'Legal',
            'grade' => 'Associate 2 Level 2',
            'staff_id' => '18054',
            'employment_date' => 'November, 01 2018 00:00:00 +0100',
        ),
    165 =>
        array (
            'staff_name' => 'Olawale Kehinde',
            'staff_email' => 'olawale@yahoo.com',
            'department' => 'Administraion',
            'grade' => 'Supervisor Level 2',
            'staff_id' => '18055',
            'employment_date' => 'November, 05 2018 00:00:00 +0100',
        ),
    166 =>
        array (
            'staff_name' => 'Ogun Ete',
            'staff_email' => 'ete.ogun@anchoriaam.com',
            'department' => 'Executive Office',
            'grade' => NULL,
            'staff_id' => '18056',
            'employment_date' => 'November, 12 2018 00:00:00 +0100',
        ),
    167 =>
        array (
            'staff_name' => 'Balogun Biodun Olawale',
            'staff_email' => 'biodun.balogun@yahoo.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 2',
            'staff_id' => '18057',
            'employment_date' => 'November, 15 2018 00:00:00 +0100',
        ),
    168 =>
        array (
            'staff_name' => 'Akinde Ona Ukwuoma',
            'staff_email' => 'ona.akinde@vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Intern',
            'staff_id' => 'IT0018',
            'employment_date' => 'October, 21 2018 00:00:00 +0100',
        ),
    169 =>
        array (
            'staff_name' => 'Nweke Obinna Emmanuel',
            'staff_email' => 'Obinna.Nweke@dre.ng',
            'department' => 'Executive Office',
            'grade' => 'Assistant Manager 1 Level 2',
            'staff_id' => '18058',
            'employment_date' => 'November, 19 2018 00:00:00 +0100',
        ),
    170 =>
        array (
            'staff_name' => 'Afolabi Wasiu Olayemi',
            'staff_email' => 'wasiu.afolabi@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Contract',
            'staff_id' => '18059',
            'employment_date' => NULL,
        ),
    171 =>
        array (
            'staff_name' => 'Test Staff',
            'staff_email' => 'osueke.christian@yahoo.com',
            'department' => 'Information Technology',
            'grade' => 'Experienced Analyst',
            'staff_id' => 'VFD12345',
            'employment_date' => NULL,
        ),
    172 =>
        array (
            'staff_name' => 'Test Finance',
            'staff_email' => 'test.finance@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Associate 3, Level 3',
            'staff_id' => 'VFD23456',
            'employment_date' => NULL,
        ),
    173 =>
        array (
            'staff_name' => 'Ezeh Lucy Ugochi',
            'staff_email' => 'lucy.ezeh@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Analyst',
            'staff_id' => '18060',
            'employment_date' => 'December, 03 2018 00:00:00 +0100',
        ),
    174 =>
        array (
            'staff_name' => 'Ekara Dennis Mfah',
            'staff_email' => 'Dennis.Ekara@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Intern',
            'staff_id' => 'IT0019',
            'employment_date' => 'December, 03 2018 00:00:00 +0100',
        ),
    175 =>
        array (
            'staff_name' => 'Nwankwo Paul',
            'staff_email' => 'paul.nwankwo@vfdgroup.com',
            'department' => 'Executive Office',
            'grade' => 'Intern',
            'staff_id' => 'IT0020',
            'employment_date' => 'December, 01 2018 00:00:00 +0100',
        ),
    176 =>
        array (
            'staff_name' => 'Brambaifa Jefferson Ebisindeh',
            'staff_email' => 'Jefferson.brambaifa@everdonbdc.com',
            'department' => 'Treasury',
            'grade' => 'Experienced Analyst Level 3',
            'staff_id' => '19017',
            'employment_date' => 'January, 02 2019 00:00:00 +0100',
        ),
    177 =>
        array (
            'staff_name' => 'Chukwuemeka Cletus',
            'staff_email' => 'cletus.chukwuemeka@yahoo.com',
            'department' => 'Executive Office',
            'grade' => NULL,
            'staff_id' => '18063',
            'employment_date' => 'December, 01 2018 00:00:00 +0100',
        ),
    178 =>
        array (
            'staff_name' => 'Ekwo Romanus Okpe',
            'staff_email' => 'romanus400@gmail.com',
            'department' => 'Administraion',
            'grade' => 'Supervisor Level 2',
            'staff_id' => '19022',
            'employment_date' => 'January, 07 2019 00:00:00 +0100',
        ),
    179 =>
        array (
            'staff_name' => 'Emina Benedict',
            'staff_email' => 'Benedict.Emina@anchoriaam.com',
            'department' => 'Client Experience',
            'grade' => 'Senior Associate 1',
            'staff_id' => '19018',
            'employment_date' => 'January, 21 2019 00:00:00 +0100',
        ),
    180 =>
        array (
            'staff_name' => 'Ameh Simon',
            'staff_email' => 'simon.Amehm@yahoo.com',
            'department' => 'Administraion',
            'grade' => 'Snr Ass. Supervisor 2',
            'staff_id' => '19023',
            'employment_date' => 'January, 21 2019 00:00:00 +0100',
        ),
    181 =>
        array (
            'staff_name' => 'Dabiri Oluwatosin',
            'staff_email' => 'tosin.dabiri@vfdgroup.com',
            'department' => 'Sales',
            'grade' => 'Asst. Manager 2 Level 2',
            'staff_id' => '19020',
            'employment_date' => 'January, 28 2019 00:00:00 +0100',
        ),
    182 =>
        array (
            'staff_name' => 'Williams Eguaikhide',
            'staff_email' => 'williams.eguaikhide@everdonbdc.com',
            'department' => 'Operations',
            'grade' => 'Assistant Manager 1 Level 3',
            'staff_id' => '19021',
            'employment_date' => 'February, 01 2019 00:00:00 +0100',
        ),
    183 =>
        array (
            'staff_name' => 'Husayn Kilani',
            'staff_email' => 'kilani.husayn@drelugs.com',
            'department' => 'Executive Assistant',
            'grade' => 'Analyst',
            'staff_id' => '19024',
            'employment_date' => 'January, 21 2019 00:00:00 +0100',
        ),
    184 =>
        array (
            'staff_name' => 'Boyim Emmanuel',
            'staff_email' => 'emmanuelboyim@gmail.com',
            'department' => 'Administration',
            'grade' => 'Supervisor Level 3',
            'staff_id' => '19025',
            'employment_date' => 'January, 15 2019 00:00:00 +0100',
        ),
    185 =>
        array (
            'staff_name' => 'Orji Onyekachukwu Sebastian',
            'staff_email' => 'sebastian.orji@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Analyst',
            'staff_id' => '19026',
            'employment_date' => 'February, 11 2019 00:00:00 +0100',
        ),
    186 =>
        array (
            'staff_name' => 'Noiki Oluwatobi Olarewaju',
            'staff_email' => 'tobi.noiki@vfdgroup.com',
            'department' => 'Human Resources',
            'grade' => 'Intern',
            'staff_id' => 'IT0021',
            'employment_date' => 'February, 05 2019 00:00:00 +0100',
        ),
    187 =>
        array (
            'staff_name' => 'Ige Oluwagbotemi',
            'staff_email' => 'igeskye@gmail.com',
            'department' => 'Operations',
            'grade' => 'Intern',
            'staff_id' => 'IT0022',
            'employment_date' => 'January, 28 2019 00:00:00 +0100',
        ),
    188 =>
        array (
            'staff_name' => 'Adesina Olumuyiwa',
            'staff_email' => 'Olumuyiwa.Adesina@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Analyst',
            'staff_id' => '19027',
            'employment_date' => 'February, 11 2019 00:00:00 +0100',
        ),
    189 =>
        array (
            'staff_name' => 'Osakuade Ifeoluwa Ayomide',
            'staff_email' => 'ifeoluwa.osakuade@vfdgroup.com',
            'department' => 'Investment Management',
            'grade' => 'Analyst Level 3',
            'staff_id' => '19028',
            'employment_date' => 'February, 11 2019 00:00:00 +0100',
        ),
    190 =>
        array (
            'staff_name' => 'George Gloria Iyanuoluwa',
            'staff_email' => 'gloria.george@vfdgroup.com',
            'department' => 'Human Resources',
            'grade' => 'Associate 3 Level 1',
            'staff_id' => '19019',
            'employment_date' => 'February, 12 2019 00:00:00 +0100',
        ),
    191 =>
        array (
            'staff_name' => 'Awaiye Adebimpe Alaba',
            'staff_email' => 'Adebimpe.awaiye@vfdgroup.com',
            'department' => 'Operations',
            'grade' => 'Senior Associate 1, Level 3',
            'staff_id' => '19029',
            'employment_date' => 'February, 25 2019 00:00:00 +0100',
        ),
    192 =>
        array (
            'staff_name' => 'Adewumi Mobolaji',
            'staff_email' => 'M.A@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Director',
            'staff_id' => '19030',
            'employment_date' => 'March, 07 2019 00:00:00 +0100',
        ),
    193 =>
        array (
            'staff_name' => 'Ogundahunsi Adeyemi Olumuyiwa',
            'staff_email' => 'Adeyemi.ogundahunsi@vfd-mfb.com',
            'department' => 'Finance',
            'grade' => 'Senior Associate 2 Level 2',
            'staff_id' => '19031',
            'employment_date' => 'March, 01 2019 00:00:00 +0100',
        ),
    194 =>
        array (
            'staff_name' => 'Awofisibe Rotimi',
            'staff_email' => 'rotimi.awofisibe@vfdgroup.com',
            'department' => 'Finance',
            'grade' => 'Assistant Manager 1 Level 2',
            'staff_id' => '19032',
            'employment_date' => 'March, 04 2019 00:00:00 +0100',
        ),
    195 =>
        array (
            'staff_name' => 'Uloko Ogwuche Maxwell',
            'staff_email' => 'maxwell.uloko@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Analyst',
            'staff_id' => '19033',
            'employment_date' => 'March, 04 2019 00:00:00 +0100',
        ),
    196 =>
        array (
            'staff_name' => 'Ololade Pelumi',
            'staff_email' => 'pelumi.ololade@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => 'Analyst',
            'staff_id' => '19034',
            'employment_date' => 'March, 04 2019 00:00:00 +0100',
        ),
    197 =>
        array (
            'staff_name' => 'Ekeabu Uchenna',
            'staff_email' => 'Uchenna.Ekeabu@Everdonbdc.com',
            'department' => 'Operations',
            'grade' => NULL,
            'staff_id' => '19038',
            'employment_date' => 'March, 11 2019 00:00:00 +0100',
        ),
    198 =>
        array (
            'staff_name' => 'Adeyemi Oluwatosin Bamidele',
            'staff_email' => 'Oluwatosin.Adeyemi@vfdgroup.com',
            'department' => 'Project',
            'grade' => 'Senior Associate 1',
            'staff_id' => '19039',
            'employment_date' => 'March, 18 2019 00:00:00 +0100',
        ),
    199 =>
        array (
            'staff_name' => 'Daniel Friday Etuk',
            'staff_email' => 'daniel.friday@vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Associate 3,Level 2',
            'staff_id' => '19037',
            'employment_date' => 'March, 11 2019 00:00:00 +0100',
        ),
    200 =>
        array (
            'staff_name' => 'Umeilechukwu Chibuzo',
            'staff_email' => 'chibuzo.umeilechukwu@vfdgroup.com',
            'department' => 'Information Technology',
            'grade' => 'Analyst',
            'staff_id' => '19041',
            'employment_date' => 'October, 29 2018 00:00:00 +0100',
        ),
    201 =>
        array (
            'staff_name' => 'Eruotor Mudiaga',
            'staff_email' => 'Mudiaga.Eruotor@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Analyst',
            'staff_id' => '19042',
            'employment_date' => 'April, 01 2019 00:00:00 +0100',
        ),
    202 =>
        array (
            'staff_name' => 'Ajibo Kennedy',
            'staff_email' => 'Kennedy.Ajibo@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Analyst',
            'staff_id' => '19043',
            'employment_date' => 'April, 01 2019 00:00:00 +0100',
        ),
    203 =>
        array (
            'staff_name' => 'Orji Jennifer',
            'staff_email' => 'Jennifer.Orji@vfd-mfb.com',
            'department' => 'Sales',
            'grade' => 'Analyst',
            'staff_id' => '19044',
            'employment_date' => 'April, 01 2019 00:00:00 +0100',
        ),
    204 =>
        array (
            'staff_name' => 'Oke Olufemi',
            'staff_email' => 'Olufemi.Oke@vfdgroup.com',
            'department' => 'Marketing and Corporate Communication',
            'grade' => 'Asst. Manager 2 Level 2',
            'staff_id' => '19045',
            'employment_date' => 'April, 01 2019 00:00:00 +0100',
        ),
    205 =>
        array (
            'staff_name' => 'Adelaja Adekunle',
            'staff_email' => 'Adekunle.Adelaja@vfdgroup.com',
            'department' => 'Internal Control',
            'grade' => 'Assistant Manager 1 Level 2',
            'staff_id' => '19040',
            'employment_date' => 'April, 15 2019 00:00:00 +0100',
        ),
    206 =>
        array (
            'staff_name' => 'Talabi Ebenezar Akinwale',
            'staff_email' => 'Akiinwale.talabi@vfdgroup.com',
            'department' => 'Human Resources',
            'grade' => NULL,
            'staff_id' => '19046',
            'employment_date' => 'April, 29 2019 00:00:00 +0100',
        ),
    207 =>
        array (
            'staff_name' => 'Ifeajuna Olisaeloka',
            'staff_email' => 'Olisa.ifeajuna@vfd-mfb.com',
            'department' => 'Operations',
            'grade' => NULL,
            'staff_id' => '19047',
            'employment_date' => 'May, 02 2019 00:00:00 +0100',
        ),
    208 =>
        array (
            'staff_name' => 'Nnorom Ugochi Ngozi',
            'staff_email' => 'ugochi.nnorom@vfdgroup.com',
            'department' => 'Investment Management',
            'grade' => 'Intern',
            'staff_id' => 'IT0023',
            'employment_date' => 'May, 02 2019 00:00:00 +0100',
        ),
    209 =>
        array (
            'staff_name' => 'Dike Theophilus Uzoma',
            'staff_email' => 'Theophilus.Dike@Vfdgroup.com',
            'department' => 'Operations',
            'grade' => 'Contract',
            'staff_id' => '19048',
            'employment_date' => 'April, 29 2019 00:00:00 +0100',
        ),
    210 =>
        array (
            'staff_name' => 'Akanmu Kunle Razaq',
            'staff_email' => 'Kunle.Akanmu@Vfdgroup.com',
            'department' => 'Operations',
            'grade' => 'Contract',
            'staff_id' => '19049',
            'employment_date' => 'April, 29 2019 00:00:00 +0100',
        ),
    211 =>
        array (
            'staff_name' => 'Okpala Azubike Paul',
            'staff_email' => 'azubike.okpala@vfdgroup.com',
            'department' => 'Operations',
            'grade' => 'Contract',
            'staff_id' => '19050',
            'employment_date' => 'April, 02 2019 00:00:00 +0100',
        ),
    212 =>
        array (
            'staff_name' => 'Okonkwo Egobudine Anthony',
            'staff_email' => 'anthony.okonkwo@dre.ng',
            'department' => 'Operations',
            'grade' => 'Contract',
            'staff_id' => '19051',
            'employment_date' => 'May, 02 2019 00:00:00 +0100',
        ),
    213 =>
        array (
            'staff_name' => 'Akinola Olumide',
            'staff_email' => 'olumide.akinola@Everdonbdc.com',
            'department' => 'Finance',
            'grade' => 'Intern',
            'staff_id' => 'IT0024',
            'employment_date' => 'May, 02 2019 00:00:00 +0100',
        ),
    214 =>
        array (
            'staff_name' => 'Nwafor Mitchell',
            'staff_email' => 'Mitchell.Nwafor@vfdgroup.com',
            'department' => 'Operations',
            'grade' => 'Contract',
            'staff_id' => '19052',
            'employment_date' => 'May, 02 2019 00:00:00 +0100',
        ),
    215 =>
        array (
            'staff_name' => 'Jipreze Somkene',
            'staff_email' => 'somkene.jipreze@anchoriaam.com',
            'department' => 'Investment Management',
            'grade' => 'Associate 2 Level 3',
            'staff_id' => '19053',
            'employment_date' => 'May, 13 2019 00:00:00 +0100',
        ),
    216 =>
        array (
            'staff_name' => 'Ohu Valentine',
            'staff_email' => 'val@streamlinemedia.ng',
            'department' => 'Operations',
            'grade' => NULL,
            'staff_id' => '19054',
            'employment_date' => 'April, 01 2019 00:00:00 +0100',
        ),
    217 =>
        array (
            'staff_name' => 'Bassey Mfon Oluwaseun',
            'staff_email' => 'seun.bassey@gmail.com',
            'department' => 'Operations',
            'grade' => NULL,
            'staff_id' => '19055',
            'employment_date' => 'April, 01 2019 00:00:00 +0100',
        ),
    218 =>
        array (
            'staff_name' => 'Mbaebie Chinazor Julia',
            'staff_email' => 'mbaebiejulia@gmail.com',
            'department' => 'Operations',
            'grade' => NULL,
            'staff_id' => '19056',
            'employment_date' => 'April, 01 2019 00:00:00 +0100',
        ),
);