<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Course;
use App\CourseUser;
use App\Topic;

Route::get('scrum-player', function () {

    return view("scum.index");
    //return view('test');
    //return view('certificate');
    //return view("index.html");

})->name('test');


Route::any("scrum-content/{course}/{topic}", function (Course $course, Topic $topic) {

    $checkAssign = CourseUser::where([
        ['course_id', '=', $course->id],
        ['user_id', '=', Auth::id()]
    ])->first();

    if (is_null($checkAssign)) {
        return redirect()->back();
    }

    switch (strtolower($course->course_name)) {
        case 'car policy' :
            return redirect()->to("scrum/courses/car_policy/res/");
            break;
        case "clean desk policy" :
            return redirect()->to("scrum/courses/clean_desk_policy/res/");
            break;
        case "performance policy" :
            return redirect()->to("scrum/courses/performance_policy/res/");
            break;
        case "whistle blowing policy" :
            return redirect()->to("scrum/courses/whistle_blowing_policy/res/");
            break;
        default:
            break;

    }

})->middleware("auth")->name("scrum_content");

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@welcome')->name('hm');
Route::get('signin', 'HomeController@signin')->name('signin');

Route::get("anything", "HomeController@anything");

Auth::routes();

Route::get('/home', 'HomeController@index');

//Users Routes
Route::get('dashboard', 'UserController@profile')->name('dashboard');
Route::get('dashboard', 'UserController@profile')->name('profile');
Route::get('profile', 'UserController@profile');
Route::get('edit_profile', 'UserController@editProfile')->name('edit_profile');
Route::post('edit_profile/{id}', 'UserController@updateProfile')->name('edit_pro');


Route::get('all-available-courses', 'UserController@allAvailableCourse')->name('all_available_crs');
Route::post('all-available-course', 'UserController@searchCourse')->name('search_course');
Route::get('request-courses', 'UserController@staffRequestCourse')->name('staff_req_crs');
Route::get('completed-courses', 'UserController@completedCourses')->name('completed_courses');
Route::get('requested-courses', 'UserController@requestedCourses')->name('requested_courses');
Route::any('request-course/{cId}', 'UserController@requestCourse')->name('req_crs');
Route::get('assigned-courses', 'UserController@staffAssignedCourse')->name('staff_assigned_crs');
Route::get('chat-mess', 'UserController@getChatMess')->name('get_chat');
Route::get('take-course/{id}', 'UserController@takeCourse')->name('tak_crs')->middleware('chk_dur');
Route::get('end-of-session-quiz/{cId}/{mId}', 'UserController@endOfModuleQuiz')->name('mod_quiz');
Route::get('take-quiz/{course}', 'UserController@takeQuiz')->name('crs_quiz');
Route::post('process-quiz/{cid}/{mId}', 'UserController@proQuizModule')->name('pro_quiz_mod');
Route::get('results', 'UserController@userResults')->name('get_us_result');
Route::get('course-leaderboard/{cId}', 'LeaderBoardController@index')->name('lead_bd');
Route::get('download-certificate/{cId}', 'UserController@downloadCertificate')->name('down_cert');
Route::get('view-news/{article}', 'UserController@viewNews')->name('view_news');

//Staff Exam
Route::get('course-exam/{cId}', 'UserController@courseExam')->name('crs_exam');
Route::post('course-exam/{cId}', 'UserController@courseExamPro')->name('pro_crs_exam');


Route::get('my_courses', 'UserController@myCourses')->name('my_crs');
Route::get('all_courses', 'HomeController@allCourses')->name('all_crs');
Route::get('lms_courses_all', 'UserController@userListCourses')->name('my_all_crs');
Route::get('course/{id}', 'HomeController@course')->name('crs');
Route::any('load_pdf', 'UserController@loadPdf')->name('load_pdf');

//User Search Route
Route::any('course_search', 'HomeController@courseSearch')->name('crs_search');
Route::any('home_search', 'HomeController@homeSearch')->name('hm_search');

Route::any('beginner_course', 'HomeController@beginnerSearch')->name('bg_search');
Route::any('intermediate_course', 'HomeController@intermediateSearch')->name('it_search');
Route::any('advanced_course', 'HomeController@advancedSearch')->name('ad_search');


//User Password Functionality
Route::get('user_forgot_password', 'HomeController@getForgotPasswordForm')->name('user_fg_pass');
Route::post('user_forgot_password', 'HomeController@getForgotPasswordPro')->name('user_fg_p');

//Route::get('user_change_password/{email}', 'HomeController@staffChangePasswordForm')->name('us_fg_f');
Route::get('user_change_password', 'HomeController@staffChangePasswordForm')->name('us_fg_f');
Route::post('user_change_password', 'HomeController@staffChangePasswordPro')->name('us_fg_p');
Route::get('staff_login', 'HomeController@customLogin')->name('cus_login');


Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');


//ADMIN ROUTES

Route::prefix('myadmin')/*->middleware("auth")*/
->group(function () {

    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::get('/sign-in', 'Auth\AdminLoginController@showSignInForm')->name('admin.sign_in');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::any('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::any('forgot', 'Auth\AdminLoginController@AdminChangePasswordPro')->name('admin.login.forgot');
    Route::get('settings', 'AdminController@settings')->name('admin.settings');

    // Password reset routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');


    //Users Functionaliies
    Route::get('list_users', 'AdminController@allUsers')->name('all_users');
    Route::get('suspend_user/{id}', 'AdminController@suspendUser')->name('suspend_user');

    Route::get('create_user', 'AdminController@newUser')->name('create_user');
    Route::post('create_user', 'AdminController@storeUser')->name('store_user')->middleware('chk_user');
    Route::get('download-staff-upload-template', 'AdminController@downloadUserUploadTemplate')->name('staff_template');
    Route::get('upload_users', 'AdminController@uploadUsers')->name('upload_users');
    Route::post('upload_users', 'AdminController@storeUploadUsers')->name('store_upload_users')->middleware('chk_user');

    Route::get('view_users/{id}', 'AdminController@viewUser')->name('view_user');
    Route::get('edit_users/{id}', 'AdminController@editUser')->name('edit_user');
    Route::get('user_courses/{id}', 'AdminController@userCourses')->name('us_crses');
    Route::get('remove_user_course/{cId}/{uId}', 'AdminController@removeUserCourse')->name('rm_ass_cr');
    Route::post('update_users/{id}', 'AdminController@updateUser')->name('upd_user');
    Route::any('delete_users/{id}', 'AdminController@deleteUser')->name('del_user');

    Route::get('view_user_course_request/{uId}/{cId}', 'AdminController@viewUserRequest')->name('us_crs_req');


    //Admin Users Functionality
    Route::get('my_profile', 'AdminController@myProfile')->name('admin_pro');
    Route::get('edit_profile', 'AdminController@adminEditProfile')->name('admin_edit_pro');
    Route::post('edit_profile', 'AdminController@adminUpdateProfile')->name('admin_upd_pro');


    //Department Functionalities
    Route::get('departments', 'DepartmentController@index')->name('depts');
    Route::get('department_create', 'DepartmentController@create')->name('dept_form');
    Route::post('department_create', 'DepartmentController@store')->name('dept_crt');
    Route::get('department_edit/{id}', 'DepartmentController@edit')->name('dept_edt');
    Route::post('department_update/{id}', 'DepartmentController@update')->name('dept_upd');
    Route::any('department_delete/{id}', 'DepartmentController@destroy')->name('dept_del');


    //Role Functionalities
    Route::get('roles', 'RoleController@index')->name('roles');
    Route::get('role_create', 'RoleController@create')->name('role_form');
    Route::post('role_create', 'RoleController@store')->name('role_crt');
    Route::get('role_edit/{id}', 'RoleController@edit')->name('role_edt');
    Route::post('role_update/{id}', 'RoleController@update')->name('role_upd');
    Route::any('role_delete/{id}', 'RoleController@destroy')->name('role_del');


    //Category Functionalities
    Route::get('categories', 'CategoryController@index')->name('cats');
    Route::get('create_category', 'CategoryController@create')->name('cat_form');
    Route::post('create_category', 'CategoryController@store')->name('cat_crt');
    Route::get('edit_category/{id}', 'CategoryController@edit')->name('cat_edt');
    Route::post('update_category/{id}', 'CategoryController@update')->name('cat_upd');
    Route::any('delete_category/{id}', 'CategoryController@delete')->name('cat_del');


    //Logo Functionalities
    Route::get('logos', 'LogoController@index')->name('logos');
    Route::get('logo_create', 'LogoController@create')->name('logo_form');
    Route::post('logo_create', 'LogoController@store')->name('logo_crt');
    Route::get('logo_edit/{id}', 'LogoController@edit')->name('logo_edt');
    Route::post('logo_update/{id}', 'LogoController@update')->name('logo_upd');
    Route::any('logo_delete/{id}', 'LogoController@delete')->name('logo_del');
    Route::any('logo_set/{id}', 'LogoController@changeLogo')->name('logo_chg');


    //Courses Functionalities
    Route::get('all_courses', 'CourseController@index')->name('all_courses')->middleware('chk_cat');
    Route::get('req_courses', 'CourseController@reqCourses')->name('req_courses')->middleware('chk_cat');
    Route::get('create_course', 'CourseController@createCourse')->name('crt_course')->middleware('chk_cat');
    Route::post('create_course', 'CourseController@storeCourse')->name('store_course');

    Route::get('view_course/{id}', 'CourseController@viewCourse')->name('view_course');
    Route::get('view_course_students/{id}', 'CourseController@viewCourseStudents')->name('view_crs_std');
    Route::get('edit_course/{id}', 'CourseController@editCourse')->name('edit_course');
    Route::post('update_course/{id}', 'CourseController@updateCourse')->name('upd_course');
    Route::any('delete_course/{id}', 'CourseController@deleteCourse')->name('del_course');

    Route::get('course_users/{crsId}', 'CourseController@courseUsers')->name('crs_users');
    Route::get('view_course_request/{cId}', 'CourseController@viewCourseRequest')->name('view_crs_req');
    Route::post('course-request-decline', 'CourseController@courseRequestDecline')->name('course_request_decline');

    Route::get('mail_staff/{crsId}', 'CourseController@mailStaff')->name('mail_staff');


    //Course_User Functionalities
    Route::get('assign_course/{cId}', 'CourseUserController@userAssignCourse')->name('ass_cr');
    Route::any('assign_course/{uId}/{cId}', 'CourseUserController@assignCourse')->name('ass_crs');

    Route::any('new_assign_course', 'CourseUserController@newAssignCourse')->name('new_ass_crs');

    Route::post('mass_assign_course/{cId}', 'CourseUserController@massAssignCourse')->name('mass_ass_crs');
    Route::any('delete_course_user/{uId}/{cId}', 'CourseUserController@deleteCourseUser')->name('del_crs_us');


    //Quiz Functionalities
    Route::get('quizes', 'QuizController@index')->name('quizes');
    Route::get('quiz_create', 'QuizController@create')->name('quiz_form');
    Route::post('quiz_create', 'QuizController@store')->name('quiz_crt');
    Route::get('quiz_edit/{id}', 'QuizController@edit')->name('quiz_edt');
    Route::post('quiz_update/{id}', 'QuizController@update')->name('quiz_upd');
    Route::any('quiz_delete/{id}', 'QuizController@destroy')->name('quiz_del');
    Route::get('import_quiz', 'QuizController@importQuizForm')->name('quiz_import_form');
    Route::get('download-quiz-upload-template', 'QuizController@downloadQuizUploadTemplate')->name('quiz_template');
    //Route::post('import_quiz', 'QuizController@importQuiz')->name('quiz_import_form');
    Route::post('import_quiz', 'QuizController@importQuestions')->name('quiz_import_form');

    /*Course Certificate*/
    Route::prefix("certificate")->as("certificate.")->group(function () {
        Route::get("", "CourseCertificateController@index")->name("all");
        Route::get("create", "CourseCertificateController@create")->name("create");
        Route::post("create", "CourseCertificateController@store");
        Route::get("edit/{courseCertificate}", "CourseCertificateController@edit")->name("edit");
        Route::get("show/{courseCertificate}", "CourseCertificateController@show")->name("show");
        Route::post("edit/{courseCertificate}", "CourseCertificateController@update");
        Route::any("delete/{courseCertificate}", "CourseCertificateController@destroy")->name("delete");

    });


    /*User Group*/
    Route::prefix("user-group")->as("group.")->group(function () {
        Route::get("", "GroupController@index")->name("all");
        Route::get("create", "GroupController@create")->name("create");
        Route::post("create", "GroupController@store");
        Route::get("edit/{group}", "GroupController@edit")->name("edit");
        Route::post("edit/{group}", "GroupController@update");
        Route::get("users/{group}", "GroupController@showUsers")->name("show");
        Route::any("delete/{group}", "GroupController@destroy")->name("delete");
        Route::any("delete-user/{group}/{user}", "GroupController@deleteUser")->name("delete_user");
        Route::get("assign-staff/{group}", "GroupController@assignStaffForm")->name("assign_staff");
        Route::any("process-assign-staff/{group}/{user?}", "GroupController@assignStaff")->name("process_assign_staff");
        Route::post("process-assign-staff-multiple/{group}", "GroupController@assignStaffMultiple")->name("process_assign_staff_m");
        Route::get("assign-course-to-group", "GroupController@assignCourseToGroup")->name("assign_course_group");
        Route::post("assign-course-to-group", "GroupController@processAssignCourseToGroup");

    });


    //Module Functionalities
    Route::get('modules', 'ModuleController@index')->name('mods');
    Route::get('create_module', 'ModuleController@create')->name('mod_form');
    Route::post('create_module', 'ModuleController@store')->name('mod_crt');
    Route::get('edit_module/{id}', 'ModuleController@edit')->name('mod_edt');
    Route::post('update_module/{id}', 'ModuleController@update')->name('mod_upd');
    Route::any('delete_module/{id}', 'ModuleController@delete')->name('mod_del');


    //Topic Functionalities
    Route::get('topics', 'TopicController@index')->name('topics')->middleware('chk_top');
    Route::get('create_topic', 'TopicController@create')->name('topic_form')->middleware('chk_top');
    Route::post('create_topic', 'TopicController@store')->name('topic_crt');
    Route::get('edit_topic/{id}', 'TopicController@edit')->name('topic_edt');
    Route::post('update_topic/{id}', 'TopicController@update')->name('topic_upd');
    Route::any('delete_topic/{id}', 'TopicController@delete')->name('topic_del');


    //Facilitator Functionalities
    Route::get('facilitators', 'FacilitatorController@index')->name('facs');
    Route::get('create_facilitator', 'FacilitatorController@create')->name('fac_form');
    Route::post('create_facilitator', 'FacilitatorController@store')->name('fac_crt');
    Route::get('edit_facilitator/{id}', 'FacilitatorController@edit')->name('fac_edt');
    Route::post('update_facilitator/{id}', 'FacilitatorController@update')->name('fac_upd');
    Route::any('delete_facilitator/{id}', 'FacilitatorController@delete')->name('fac_del');


    //Admin Utility Search
    Route::get('search_staff', 'AdminController@searchStaffByName')->name('st_search');

    //CLIENTS
    Route::get('clients', 'AdminController@all_client')->name('all_client');
    Route::get('addclient', 'AdminController@add_client')->name('add_client');
    Route::post('addclient', 'AdminController@create_client')->name('create_client');
    Route::get('editclient/{id}', 'AdminController@edit_client')->name('edit_client');
    Route::post('editclient/{id}', 'AdminController@update_client')->name('update_client');
    Route::any('delclient/{id}', 'AdminController@del_client')->name('del_client');


    //ADMINS
    Route::get('all_admin', 'AdminController@all_admin')->name('all_admin');
    Route::get('addadmin', 'AdminController@add_admin')->name('add_admin');
    Route::post('addadmin', 'AdminController@create_admin')->name('create_admin');
    Route::get('editadmin/{id}', 'AdminController@edit_admin')->name('edit_admin');
    Route::post('editadmin/{id}', 'AdminController@update_admin')->name('update_admin');
    Route::any('deladmin/{id}', 'AdminController@del_admin')->name('del_admin');


    //STYLES
    Route::get('clients/{client_id}', 'AdminController@getStyle')->name('style_layout');
    Route::get('clients/{client_id}/{style}/{param}', 'AdminController@applyStyle')->name('apply_client');
    Route::get('admin/{client_id}', 'AdminController@getStyle')->name('style_layout_admin');
    Route::get('admin/{client_id}/{style}/{param}', 'AdminController@applyStyle')->name('apply_admin');

    // LIENCES
    Route::get('licences', 'AdminController@Licences')->name('licences');
    Route::get('addlicence', 'AdminController@AddLicence')->name('add_licence');
    Route::post('addlicence', 'AdminController@CreateLicence')->name('crt_licence');
    Route::get('editlicence/{id}', 'AdminController@EditLicence')->name('edit_licence');
    Route::post('editlicence/{id}', 'AdminController@UpdateLicence')->name('up_licence');
    Route::get('dellicence/{id}', 'AdminController@DelLicence')->name('del_licence');
    Route::get('hr_licences', 'AdminController@HrLicences')->name('hr_licences');

    //REPORT
    Route::prefix("report")->group( function () {
       Route::any("course", "ReportController@courseReport")->name("course_report");
       Route::any("course_users", "ReportController@courseStudentsReport")->name("course_users_report");
    });


    Route::prefix("news")->group( function () {
       Route::get("", "ArticleController@index")->name("news_all");
       Route::get("create", "ArticleController@create")->name("news_create");
       Route::post("create", "ArticleController@store");
       Route::get("edit/{article}", "ArticleController@edit")->name("news_edit");
       Route::post("update/{article}", "ArticleController@update")->name("news_update");
       Route::any("delete/{article}", "ArticleController@destroy")->name("news_delete");
    });

});


Route::get('myadmin/style/{client_id}', 'AdminController@getStyle');
//Custom Routes
Route::get('course_modules', 'UtilityController@getModulesByCourse')->name('get_mods');
Route::get('get_topic_video', 'UtilityController@getVideoByTopicId')->name('get_topic_video');

Route::post('get_users', 'UtilityController@get_users')->name('get_users');
Route::post('get_comments', 'UtilityController@get_comments')->name('get_comments');
Route::post('post_comment', 'UtilityController@post_comment')->name('post_comment');
Route::post('put_comment', 'UtilityController@put_comment')->name('put_comment');
Route::post('del_comment', 'UtilityController@del_comment')->name('del_comment');
// Route::get('course_modules', 'UtilityController@getModulesByCourse')->name('get_mods');
