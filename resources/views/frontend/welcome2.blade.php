<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LMS</title>
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
</head>

<body class="body-bg">
    <div class="bg-overlay">
        <nav class="navbar navbar-transparent" style="background-color: transparent;">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand navbar-link" href="#"> <img src="{{ asset('assets/img/logo-alt.png')}}"></a>
                    <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown"><a class="dropdown-toggle white link-hover" data-toggle="dropdown" aria-expanded="false" href="#">Login </a>
                        
                        @include('flash')
                        
                            <ul class="dropdown-menu dropdown-menu-cus" role="menu">
                                <div class="login-dropdown">
                                    
                                    <form class="login-formm" action="{{ route('login')}}" method="POST">
                                        {{ csrf_field() }}
                                      <div class="form-group">
                                        <label for="exampleInputEmail1" class="form-links">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" value="{{ old('email') }}" name="email">
                                      </div>
                                      <div class="form-group">
                                        <label for="exampleInputPassword1" class="form-links">Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                                      </div>
                                      <button type="submit" class="btn btn-blue btn-block">Submit</button>
                                      <br>
                                      <a href="#" class="form-links" id="forgot-password">Forgot Password?</a>
                                    </form>
                                    <form class="forgot-div" action="{{ route('us_fg_p')}}" method="POST">
                                        {{ csrf_field() }}
                                      <div class="form-group">
                                        <label for="exampleInputEmail1" class="form-links">Enter your Email Address</label>
                                        <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                      </div>
                                      <button type="submit" class="btn btn-blue btn-block">Submit</button>
                                      <br>
                                      <a href="#" class="form-links" id="sign-link-back">Go back to Sign In</a>
                                    </form>
                                </div>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container move-down">
            <div class="row">
                <div class="col-md-4 text-center"><img src="{{ asset('assets/img/co1.png')}}">
                    <p class="white-desc">Learn from Experts</p>
                </div>
                <div class="col-md-4 text-center"><img src="{{ asset('assets/img/co2.png')}}">
                    <p class="white-desc">Career Growth</p>
                </div>
                <div class="col-md-4 text-center"><img src="{{ asset('assets/img/co3.png')}}">
                    <p class="white-desc">Quality Resources</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-custom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Copyright © 2018 - All Rights Reserved -&nbsp;<a href="#">Leadway Pensure PFA</a> </p>
                </div>
                <div class="col-md-3"><a href="#" class="white"><i class="glyphicon glyphicon-envelope" id="ad-right"></i>Send Us An Email</a></div>
                <div class="col-md-3">
                    <p class="links-icon"><a href="#" class="socio-link"><i class="fa fa-facebook-square"></i></a><a href="#" class="socio-link"><i class="fa fa-twitter-square"></i></a><a href="#" class="socio-link"><i class="fa fa-instagram"></i></a></p>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".forgot-div").hide();
            $("#forgot-password").click(function(){
                $(".forgot-div").slideToggle("slow");
                $(".login-formm").slideToggle("slow");
            });
            $("#sign-link-back").click(function(){
                $(".forgot-div").slideToggle("slow");
                $(".login-formm").slideToggle("slow");
            });
        });
    </script>
</body>
