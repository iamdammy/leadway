<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Courses List - Universum</title>

	<!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/style.css')}}" />
	<!-- My Custom Stylesheet -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('user/css/profile.css')}}" /> -->

    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/responsive.css')}}" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('user/images/assets/favicon.png')}}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/ie9/html5shiv.min.js"></script>
      <script src="js/ie9/respond.min.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="left-sidebar">
	@include('layouts.user.preloader')
	

	<div id="wrapper">
		@include('layouts.user.header')
		

		<!-- Login Modal -->
		@include('layouts.user.login_modal')
		
		<!-- End of Login Modal -->

		<div id="content" class="site-content courses-content">
			<div class="page-title parallax-window" data-parallax="scroll" data-image-src="{{ asset('user/images/placeholder/ct.jpg')}}">
				<div class="container">
					<h1>Courses List</h1>
					<div class="breadcrumb">
						<ul>
							<li><a href="#">Home</a></li>
							<li><span class="current">Courses</span></li>
						</ul>
					</div><!-- .breadcrumb -->
				</div><!-- .container -->
			</div><!-- .page-title -->

			<div class="search-by-cat">
				<form action="{{ route('crs_search') }}" method="post" />
				{{ csrf_field() }}
				@include('flash')
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<div class="selectbox">
									<select name="cat">
										<option value="">Select Course Categories</option>
										<option value="Beginner">Beginner</option>
										<option value="Intermediate">Intermediate</option>
										<option value="Advanced">Advanced</option>
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<input name="name" type="text" placeholder="Enter Keyword" />
							</div>

							<div class="col-md-3">
								<input type="submit" value="Find Course" class="button primary rounded large full" />
							</div>
						</div>
					</div><!-- .container -->
				</form>
			</div><!-- .search-by-cat -->

			<div class="container">
				<div class="row">
					<main id="main" class="site-main col-md-9">
						<div class="sort clearfix">
							<!-- <form action="#" class="ordering pull-left" />
								<div class="selectbox">
									<select class="orderby" name="orderby">
										<option value="menu_order" />Default sorting
										<option value="popularity" />popularity
										<option value="rating" />average rating
										<option value="date" />newness
										<option value="price" />price: low to high
										<option value="price-desc" />price: high to low
									</select>
								</div>
							</form> -->

							<div class="style-switch pull-right clearfix">								
								<a href="#" data-view="grid"><i class="fa fa-th"></i></a>
								<a class="active" href="#" data-view="list"><i class="fa fa-th-list"></i></a>
							</div>
						</div><!-- .sort -->

						<div class="courses layout list column-3">
							@foreach($courses as $course)
							<div class="course">
								<div class="course-inner shadow">
									<div class="course-thumb">
										<a class="mini-zoom" href="{{ route('crs', ['id'=>$course->id])}}">
											<img src="{{ asset('uploads/images/'.$course->course_img)}}" alt="" />
											<img class="img-list" src="{{ asset('uploads/images/'.$course->course_img)}}" alt="" />
										</a>
									</div><!-- .course-thumb -->

									<div class="course-info">
										<span class="course-cat"><a href="#">{{$course->course_cat}}</a></span>	

										<h3 class="course-title"><a href="{{ route('crs', ['id'=>$course->id])}}">{{$course->course_name}}</a></h3>

										<div class="course-desc">
											<p>{{$course->course_description}}</p>
										</div><!-- .course-desc -->

										<!-- <div class="course-author">
											<a href="#"><img src="{{ asset('user/images/placeholder/user6.jpg')}}" alt="" /></a>
											<span>by <a href="#">Sandra Pearson</a></span>
										</div> --><!-- .course-author -->

										<div class="course-meta">
											<!-- <ul>
												<li>
													<div class="star-rating">
														<span style="width:80%"></span>
													</div>
												</li>
												<li><i class="fa fa-users"></i> 2</li>
												<li><a href="#"><i class="fa fa-comment"></i> 5</a></li>
											</ul> -->
										</div><!-- .course-meta -->
									</div><!-- .course-info -->
								</div><!-- .course-inner -->
							</div><!-- .course -->
							@endforeach

							{{-- @if(count($courses))
								<h4>No Result</h4>
							@endif --}}

							@unless (count($courses))
    							<center><h4>Unfortunately, NO RESULT</h4></center>
							@endunless

							

							
						</div><!-- .courses -->

						<nav class="pagination">
							<ul>
								{{ $courses->links() }}
								<!-- <li class="prev"><a href="#">Previous</a></li>
								<li><span class="current">1</span></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><span>...</span></li>
								<li><a href="#">68</a></li>
								<li class="next"><a href="#">Next</a></li>
 -->
							</ul>

						</nav>
					</main><!-- .site-main -->

					<div id="sidebar" class="sidebar col-md-3">
						<aside class="widget">
							<h3 class="widget-title">Course Categories</h3>
							<ul>
								<li><a href="{{ route('bg_search') }}">Beginner</a></li>
								<li><a href="{{ route('it_search') }}">Intermediate</a></li>
								<li><a href="{{ route('ad_search') }}">Advanced</a></li>
								<!-- <li><a href="#">Language Books</a></li>
								<li><a href="#">Biological Books</a></li>
								<li><a href="#">Architecture Books</a></li> -->
							</ul>
						</aside><!-- .widget -->

						<aside class="widget courses-widget">
							<h3 class="widget-title">Latest Courses</h3>
							<div class="courses">
								<ul>
									@foreach($latest as $course)
									<li>
										<a class="thumb" href="#"><img src="{{ asset('uploads/images/'.$course->course_img)}}" alt="" /></a>
										<div class="info">
											<h4><a href="#">{{$course->course_name}}</a></h4>
											<span><i class="fa fa-users"></i> 2289</span>
										</div>
									</li>
									@endforeach

									<!-- <li>
										<a class="thumb" href="#"><img src="{{ asset('user/images/placeholder/course-wd2.jpg')}}" alt="" /></a>
										<div class="info">
											<h4><a href="#">Landscape Architecture, and Urban Planning</a></h4>
											<span><i class="fa fa-users"></i> 1762</span>
										</div>
									</li>

									<li>
										<a class="thumb" href="#"><img src="{{ asset('user/images/placeholder/course-wd3.jpg')}}" alt="" /></a>
										<div class="info">
											<h4><a href="#">Lesson english how to write a paragraph (writing)</a></h4>
											<span><i class="fa fa-users"></i> 1226</span>
										</div>
									</li> -->
								</ul>
							</div><!-- .widget -->
						</aside><!-- .widget -->

					</div><!-- .sidebar -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .site-content -->

		@include('layouts.user.base')
		

		@include('layouts.user.footer')<!-- .site-footer -->
		
	</div><!-- #wrapper -->

	<!-- jQuery -->    
    <script src="{{ asset('user/js/jquery-1.11.3.js')}}"></script>
    <!-- Boostrap -->
    <script src="{{ asset('user/js/vendo/bootstrap.min.js')}}"></script>
    <!-- Jquery Parallax -->
    <script src="{{ asset('user/js/vendo/parallax.min.js')}}"></script>
    <!-- jQuery UI -->
	<script src="{{ asset('user/js/vendo/jquery-ui.min.js')}}"></script>
	<!-- jQuery Sticky -->
	<script src="{{ asset('user/js/vendo/jquery.sticky.js')}}"></script>
	<!-- OWL CAROUSEL Slider -->    
    <script src="{{ asset('user/js/vendo/owl.carousel.js')}}"></script>
    <!-- PrettyPhoto -->   
    <script src="{{ asset('user/js/vendo/jquery.prettyPhoto.js')}}"></script>
    <!-- Jquery Isotope -->
    <script src="{{ asset('user/js/vendo/isotope.pkgd.min.js')}}"></script>
    <!-- Main -->    
    <script src="{{ asset('user/js/main.js')}}"></script>
</body>
</html>
