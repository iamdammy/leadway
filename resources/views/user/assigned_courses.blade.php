<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Courses Detail Version 2 - Universum</title>

	<!-- Style CSS -->
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/style.css')}}" />
	<!-- My Custom Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/profile.css')}}" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/responsive.css')}}" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('user/images/assets/favicon.png')}}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/ie9/html5shiv.min.js"></script>
      <script src="js/ie9/respond.min.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="single-course v2">
	@include('layouts.user.preloader')
	

	<div id="wrapper">
		@include('layouts.user.header')
		<!-- .site-header -->

		<!-- Login Modal -->
		@include('layouts.user.login_modal')
		<!-- End of Login Modal -->

		<div id="content" class="site-content">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="sidebar">
							<div class="profile-img">
								<img src="{{ asset('user/images/placeholder/team1.png')}}" class="img-responsive profileimage" alt="Image">
							</div>
							@include('user.profile_menu')
							
						</div>
					</div>
					@include('flash')
					<div class="col-md-9">
						<div class="profilee">
							<h2 class="main-title">Assigned Courses</h2>

							<!-- Assigned Course Table -->
							<div class="table-responsive courses-table">
								<table class="table">
									<thead>
										<tr>
											<th>Course Image</th>
											<th>Course Name</th>
											<th>Categories</th>
											<th>Quiz</th>
											<th>Score</th>
											<th>Action</th>
											<!-- <th>Start Date <i class="fa fa-long-arrow-up"></i></th> -->
										</tr>
									</thead>
									<tbody>
										@foreach($courses as $course)
										<tr>
											<td>
												<a href="#"><img width="40px" height="40px" src="{{ asset('uploads/images/'.$course->course_img.'')}}">
												</a>
											</td>
											<td><a href="#">{{$course->course_name}}</a></td>
											<td>{{$course->course_cat}}</td>
											<td> <a href="{{ route('crs_quiz', ['crs'=> $course->id]) }}"> Take Quiz </a></td>

											<td>
												{{ App\QuizResult::where([
													['user_id', '=', Auth::id()],
													['course_id', '=', $course->id],
													])->first()->score ?? '-'
												 }}
											</td>

											<td> <a href="{{ route('tak_crs', ['id'=>$course->id])}}"> Take Course</a></td>
											
										</tr>
										@endforeach

										
									</tbody>
								</table>
							</div>



							<!-- End of Assigned Course Table -->
						</div>
					</div>
				</div>
			</div>
		</div><!-- .site-content -->

		@include('layouts.user.base')

		@include('layouts.user.footer')
	</div><!-- #wrapper -->

	<!-- jQuery -->    
    <script src="{{ asset('user/js/jquery-1.11.3.js')}}"></script>
    <!-- Boostrap -->
    <script src="{{ asset('user/js/vendo/bootstrap.min.js')}}"></script>
    <!-- Jquery Parallax -->
    <script src="{{ asset('user/js/vendo/parallax.min.js')}}"></script>
    <!-- jQuery UI -->
	<script src="{{ asset('user/js/vendo/jquery-ui.min.js')}}"></script>
	<!-- jQuery Sticky -->
	<script src="{{ asset('user/js/vendo/jquery.sticky.js')}}"></script>
	<!-- OWL CAROUSEL Slider -->    
    <script src="{{ asset('user/js/vendo/owl.carousel.js')}}"></script>
    <!-- PrettyPhoto -->   
    <script src="{{ asset('user/js/vendo/jquery.prettyPhoto.js')}}"></script>
    <!-- Jquery Isotope -->
    <script src="{{ asset('user/js/vendo/isotope.pkgd.min.js')}}"></script>
    <!-- Main -->    
    <script src="{{ asset('user/js/main.js')}}"></script>
</body>
</html>
