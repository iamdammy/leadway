<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Courses Detail Version 2 - Universum</title>

	<!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/style.css')}}" />
	<!-- My Custom Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/profile.css')}}" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/responsive.css')}}" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('user/images/assets/favicon.png')}}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/ie9/html5shiv.min.js"></script>
      <script src="js/ie9/respond.min.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="single-course v2">
	@include('layouts.user.preloader')
	

	<div id="wrapper">
		@include('layouts.user.header')
		<!-- .site-header -->

		<!-- Login Modal -->
		@include('layouts.user.login_modal')
		
		<!-- End of Login Modal -->

		<!-- .site-content -->
		<form method="POST" action="{{ route('pro_quiz', ['crs'=>$course->id]) }}">
			{{ csrf_field() }}
			@php $i =1; @endphp

			@foreach($questions as $que)
				@php
					$options = [$que->choice_1, $que->choice_2, $que->choice_3, $que->answer ];
					shuffle($options);
				@endphp
				<h3>{{ $i }}. {{ $que->question }}</h3>
				<input type="radio" name="quizId{{ $que->id }}" value="{{ $options[0] }}"> {{ $options[0] }} <br>
				<input type="radio" name="quizId{{ $que->id }}" value="{{ $options[1] }}"> {{ $options[1] }} <br>
				<input type="radio" name="quizId{{ $que->id }}" value="{{ $options[2] }}"> {{ $options[2] }} <br>
				<input type="radio" name="quizId{{ $que->id }}" value="{{ $options[3] }}"> {{ $options[3] }} <br>
				@php $i++; @endphp
				<br>
			@endforeach
			<button type="submit">Submit</button>
		</form>


		@include('layouts.user.base')
		

		@include('layouts.user.footer')<!-- .site-footer -->
	</div><!-- #wrapper -->

	<!-- jQuery -->    
    <script src="{{ asset('user/js/jquery-1.11.3.js')}}"></script>
    <!-- Boostrap -->
    <script src="{{ asset('user/js/vendo/bootstrap.min.js')}}"></script>
    <!-- Jquery Parallax -->
    <script src="{{ asset('user/js/vendo/parallax.min.js')}}"></script>
    <!-- jQuery UI -->
	<script src="{{ asset('user/js/vendo/jquery-ui.min.js')}}"></script>
	<!-- jQuery Sticky -->
	<script src="{{ asset('user/js/vendo/jquery.sticky.js')}}"></script>
	<!-- OWL CAROUSEL Slider -->    
    <script src="{{ asset('user/js/vendo/owl.carousel.js')}}"></script>
    <!-- PrettyPhoto -->   
    <script src="{{ asset('user/js/vendo/jquery.prettyPhoto.js')}}"></script>
    <!-- Jquery Isotope -->
    <script src="{{ asset('user/js/vendo/isotope.pkgd.min.js')}}"></script>
    <!-- Main -->    
    <script src="{{ asset('user/js/main.js')}}"></script>
</body>
</html>
