<ul class="nav sidebar-cont">
	<li>
		<a href="{{ route('profile') }}" class="{{ active('profile') }}"><span class="fa fa-user link-icon"></span>Profile</a>
	</li>
	<li>
		<a href="{{ route('my_crs') }}" class="{{ active('my_crs') }}"><span class="fa fa-graduation-cap link-icon"></span>Assigned Course</a>
	</li>
	<li>
		<a href="{{ route('my_all_crs') }}" class="{{ active('my_all_crs') }}"><span class="fa fa-tasks link-icon"></span>All Courses</a>
	</li>
	<li>
		<a href="{{ route('lead_bd') }}"><span class="fa fa-clipboard link-icon"></span>Leaderboard</a>
	</li>
	<li>
		<a href="{{ route('edit_profile') }}"><span class="fa fa-pencil link-icon"></span>Edit Profile</a>
	</li>
	{{-- <li class="logout">
		<a href=""><span class="fa fa-sign-out link-icon"></span>Log out</a>
	</li> --}}

	<li class="logout">
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
</ul>




