<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>LMS | Edit Profile</title>

	<!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/style.css') }}" />
	<!-- My Custom Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/profile.css') }}" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/responsive.css') }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('user/images/assets/favicon.png') }}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/ie9/html5shiv.min.js"></script>
      <script src="js/ie9/respond.min.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="single-course v2">
	@include('layouts.user.preloader')

	<div id="wrapper">
		@include('layouts.user.header')
		

		<!-- Login Modal -->
		@include('layouts.user.login_modal')
		
		<!-- End of Login Modal -->

		<div id="content" class="site-content">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="sidebar">
							<div class="profile-img">
								<img src="{{ asset('uploads/images/'.$user->photo) }} " class="img-responsive profileimage" alt="No Image">

								{{-- @if(empty($user->photo) || is_null($user->photo))
								<img src="{{ asset('uploads/images/default.png') }} " class="img-responsive profileimage" alt="Image">
								@endif --}}
							</div>
							@include('user.profile_menu')
							
						</div>
					</div>
					<form method="post" action="{{ route('edit_pro', ['id'=>$user->id]) }}" enctype="multipart/form-data">
						<div class="col-md-9">
							<div class="profilee">
								@include('flash')
								<h2 class="main-title">My Profile</h2>
							</div>
								{{ csrf_field() }}
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										    <label for="exampleInputEmail1" style="margin-bottom: 7px;">First Name</label>
										    <input name="firstName" value="{{ $user->firstName }}" type="text" class="form-control" id="" aria-describedby="" placeholder="First Name">
										</div>

										<div class="form-group">
										    <label for="exampleInputEmail1" style="margin-bottom: 7px;">Last Name</label>
										    <input name="lastName" value="{{ $user->lastName }}" type="text" class="form-control" id="" aria-describedby="" placeholder="Last Name">
										</div>

										<div class="form-group">
										    <label for="exampleInputEmail1" style="margin-bottom: 7px;">Email address</label>
										    <input name="email" value="{{ $user->email }}" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="" placeholder="Email Address">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										    <label for="exampleInputEmail1" style="margin-bottom: 7px;">Department</label>
										    <input name="department" value="{{ $user->department }}" type="text" class="form-control" id="" aria-describedby="" placeholder="Department">
										</div>

										<div class="form-group">
										    <label for="exampleInputEmail1" style="margin-bottom: 7px;">Password</label>
										    <input name="password" value="" type="password" class="form-control" id="" aria-describedby="" placeholder="Change your password">
										</div>

										<div class="form-group">
										    <label for="exampleInputEmail1" style="margin-bottom: 7px;">Picture</label>
										    <input name="photo" type="file" class="form-control" id="" aria-describedby="" >
										</div>
									</div>
								</div>
								<button type="submit" class="btn btn-primary editp">Edit Profile</button>
						</div>
					</form>

				</div>
			</div>
		</div><!-- .site-content -->

		@include('layouts.user.base')
		

		@include('layouts.user.footer')
		
	</div><!-- #wrapper -->

	<!-- jQuery -->    
    <script src="{{ asset('user/js/jquery-1.11.3.js')}}"></script>
    <!-- Boostrap -->
    <script src="{{ asset('user/js/vendo/bootstrap.min.js')}}"></script>
    <!-- Jquery Parallax -->
    <script src="{{ asset('user/js/vendo/parallax.min.js')}}"></script>
    <!-- jQuery UI -->
	<script src="{{ asset('user/js/vendo/jquery-ui.min.js')}}"></script>
	<!-- jQuery Sticky -->
	<script src="{{ asset('user/js/vendo/jquery.sticky.js')}}"></script>
	<!-- OWL CAROUSEL Slider -->    
    <script src="{{ asset('user/js/vendo/owl.carousel.js')}}"></script>
    <!-- PrettyPhoto -->   
    <script src="{{ asset('user/js/vendo/jquery.prettyPhoto.js')}}"></script>
    <!-- Jquery Isotope -->
    <script src="{{ asset('user/js/vendo/isotope.pkgd.min.js')}}"></script>
    <!-- Main -->    
    <script src="{{ asset('user/js/main.js')}}"></script>
</body>
</html>
