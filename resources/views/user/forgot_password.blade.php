<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>LMS</title>

	<!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/style.css')}}" />
	<!-- My Custom Stylesheet -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('user/css/profile.css')}}" /> -->

    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/responsive.css')}}" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('user/images/assets/favicon.png')}}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/ie9/html5shiv.min.js"></script>
      <script src="js/ie9/respond.min.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="left-sidebar">
	@include('layouts.user.preloader')
	

	<div id="wrapper">
		@include('layouts.user.header')
		

		<!-- Login Modal -->
		@include('layouts.user.login_modal')
		
		<!-- End of Login Modal -->

		<div id="content" class="site-content courses-content">
			<div class="page-title parallax-window" data-parallax="scroll" data-image-src="{{ asset('user/images/placeholder/ct.jpg')}}">
				<div class="container">
					<h1>Courses List</h1>
					<div class="breadcrumb">
						<ul>
							<li><a href="#">Home</a></li>
							<li><span class="current">Courses</span></li>
						</ul>
					</div><!-- .breadcrumb -->
				</div><!-- .container -->
			</div><!-- .page-title -->

			<div class="search-by-cat">
				<form action="{{ route('crs_search') }}" method="post" />
				{{ csrf_field() }}
				@include('flash')
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<div class="selectbox">
									<select name="cat">
										<option value="">Select Course Categories</option>
										<option value="Beginner">Beginner</option>
										<option value="Intermediate">Intermediate</option>
										<option value="Advanced">Advanced</option>
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<input name="name" type="text" placeholder="Enter Keyword" />
							</div>

							<div class="col-md-3">
								<input type="submit" value="Find Course" class="button primary rounded large full" />
							</div>
						</div>
					</div><!-- .container -->
				</form>
			</div><!-- .search-by-cat -->

			<div class="container">
				<div class="row">
					<main id="main" class="site-main col-md-9">
						<div class="sort clearfix">
							<!-- <form action="#" class="ordering pull-left" />
								<div class="selectbox">
									<select class="orderby" name="orderby">
										<option value="menu_order" />Default sorting
										<option value="popularity" />popularity
										<option value="rating" />average rating
										<option value="date" />newness
										<option value="price" />price: low to high
										<option value="price-desc" />price: high to low
									</select>
								</div>
							</form> -->

							<div class="style-switch pull-right clearfix">								
								<a href="#" data-view="grid"><i class="fa fa-th"></i></a>
								<a class="active" href="#" data-view="list"><i class="fa fa-th-list"></i></a>
							</div>
						</div><!-- .sort -->

						

						
					</main><!-- .site-main -->

					<div id="sidebar" class="sidebar col-md-3">
						

						

					</div><!-- .sidebar -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .site-content -->

		@include('layouts.user.base')
		

		@include('layouts.user.footer')<!-- .site-footer -->
		
	</div><!-- #wrapper -->

	<!-- jQuery -->    
    <script src="{{ asset('user/js/jquery-1.11.3.js')}}"></script>
    <!-- Boostrap -->
    <script src="{{ asset('user/js/vendo/bootstrap.min.js')}}"></script>
    <!-- Jquery Parallax -->
    <script src="{{ asset('user/js/vendo/parallax.min.js')}}"></script>
    <!-- jQuery UI -->
	<script src="{{ asset('user/js/vendo/jquery-ui.min.js')}}"></script>
	<!-- jQuery Sticky -->
	<script src="{{ asset('user/js/vendo/jquery.sticky.js')}}"></script>
	<!-- OWL CAROUSEL Slider -->    
    <script src="{{ asset('user/js/vendo/owl.carousel.js')}}"></script>
    <!-- PrettyPhoto -->   
    <script src="{{ asset('user/js/vendo/jquery.prettyPhoto.js')}}"></script>
    <!-- Jquery Isotope -->
    <script src="{{ asset('user/js/vendo/isotope.pkgd.min.js')}}"></script>
    <!-- Main -->    
    <script src="{{ asset('user/js/main.js')}}"></script>
</body>
</html>
