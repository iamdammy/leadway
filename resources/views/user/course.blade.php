<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Course</title>

	<!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/style.css')}}" />
	<!-- My Custom Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/profile.css')}}" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/responsive.css')}}" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('user/images/assets/favicon.png')}}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/ie9/html5shiv.min.js"></script>
      <script src="js/ie9/respond.min.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="single-course v2">
	@include('layouts.user.preloader')
	

	<div id="wrapper">
		@include('layouts.user.header')
		<!-- .site-header -->

		<!-- Login Modal -->
		@include('layouts.user.login_modal')
		
		<!-- End of Login Modal -->
		@include('flash')

		<div id="content" class="site-content">
			<div class="single-course-title">
				<div class="container">
					<div class="row">
						<div class="thumb">
							<img width="300px" height="300px" src="{{ asset('uploads/images/'.$course->course_img)}}" alt="" />
						</div><!-- .thumb -->

						<div class="info">
							<h1>NAME : {{$course->course_name }}</h1>
							<div class="course-meta">
								<ul>
									<li>
										<div class="star-rating">
											<span style="width:80%"></span>
										</div>
										<span>(102 reviews)</span>
									</li>
									<li><i class="fa fa-users"></i> 212</li>
									<li><a href="#"><i class="fa fa-comment"></i> 539</a></li>
								</ul>
							</div><!-- .course-meta -->

							<div class="course-desc">
								<p>{{$course->course_desc }}</p>
							</div>

							<div class="course-detail">
								<ul>
									<!-- <li>Category: {{$course->course_cat }} </li> -->
									<li>Duration: 6 weeks</li>
									<!-- <li>ID: BM 598</li> -->
									<li>Category: <a href="#">{{$course->course_cat }}</a></li>
									<li>Published BY: {{ env('APP_NAME')}}</li>
								</ul>
							</div><!-- .course-detail -->
							<!-- <a href="#" class="button large primary rounded apply-button">Start now</a> -->
						</div><!-- .info -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .single-course-title -->

			<div class="container">
				<div id="accordion" class="panel-group course-accordion accordion">

					<div class="panel v2">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#section3">
							Video</a>
							</h4>
						</div>
						<div id="section3" class="panel-collapse collapse in">
							<div class="panel-body">
								<p> Aenean elementum odio ut lorem cursus, eu auctor magna pellentesque.  Cras facilisis quam sed rhoncus dapibus.</p>

								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<a data-gal="prettyPhoto" href="https://www.youtube.com/watch?v=cKl-6u8Dcb0" class="video">
											<img src="{{ asset('user/images/placeholder/video1.jpg')}}" alt="" />
										</a><!-- .video -->
									</div>

									<div class="col-md-6 col-sm-6 col-xs-6">
										<a  href="{{ route('load_pdf')}}">
											<img src="{{ asset('user/images/placeholder/video2.jpg')}}" alt="" />
										</a><!-- .video -->
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="panel v2">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#section1">
							About the course</a>
							</h4>
						</div>
						<div id="section1" class="panel-collapse collapse ">
							<div class="panel-body">
								<p>Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat id. Vivamus interdum urna at sapien varius elementum. Suspendisse ut mi felis et interdum libero lacinia vel. Aenean elementum odio ut lorem cursus, eu auctor magna pellentesque.  Cras facilisis quam sed rhoncus dapibus. Quisque lorem enim, dictum at magna eu, hendrerit hendrerit arcu. Etiam vulputate ac tortor ac gravida. Proin accumsan placerat rutrum. Praesent ut eros ac nisi ultrices rhoncus et nec nunc</p>

								<p>Nulla fermentum eros vitae est finibus dapibus. Aliquam porta nulla a elit varius efficitur. In in magna sed turpis venenatis tristique eu eget neque. Duis condimentum libero ornare quam tincidunt interdum. Phasellus porttitor nisi ut lectus pellentesque, ut fringilla leo convallis. </p>

								<blockquote>
									<p>Creativity is just connecting things. When you ask creative people how they did something, they feel a little guilty because they didn't really do it, they just saw something. It seemed obvious to them after a while.</p>
									<p><strong>- Steve Job’s</strong></p>
								</blockquote>
							</div>
						</div>
					</div>

					<div class="panel v2">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#section2">
							Instructor</a>
							</h4>
						</div>
						<div id="section2" class="panel-collapse collapse">
							<div class="panel-body">
								<p> Aenean elementum odio ut lorem cursus, eu auctor magna pellentesque.  Cras facilisis quam sed rhoncus dapibus.</p>

								<div class="row">
									<div class="col-md-6">
										<div class="speaker">
											<div class="speaker-thumb">
												<img src="images/placeholder/user12.jpg" alt="" />

												<div class="socials">
													<ul>
														<li><a href="#" target="_blank"><i class="fa fa-envelope"></i></a></li>
														<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" target="_blank"><i class="fa fa-linkedin "></i></a></li>
													</ul>
												</div>
											</div><!-- .speaker-thumb -->

											<div class="speaker-info">
												<span class="name">Peter Hart</span>
												<span class="job">Economic professor</span>
												<p>Nam tempus ipsum non magna varius imperdiet. Etiam non sapien id diam cursus rutrum. Nulla nec leo ultrices, porttitor dui vel ornare massa.</p>
											</div><!-- .speaker-info -->
										</div><!-- .speaker -->
									</div>

									<div class="col-md-6">
										<div class="speaker">
											<div class="speaker-thumb">
												<img src="{{ asset('user/images/placeholder/user13.jpg')}}" alt="" />

												<div class="socials">
													<ul>
														<li><a href="#" target="_blank"><i class="fa fa-envelope"></i></a></li>
														<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" target="_blank"><i class="fa fa-linkedin "></i></a></li>
													</ul>
												</div>
											</div><!-- .speaker-thumb -->

											<div class="speaker-info">
												<span class="name">Betty Lane</span>
												<span class="job">Biological Professor</span>
												<p>Curabitur laoreet imperdiet eros, condimentum feugiat quam. Maecenas elementum metus leo, vitae pellentesque justo. Nunc iaculis, velit ac posuere</p>
											</div><!-- .speaker-info -->
										</div><!-- .speaker -->
									</div>
								</div>
							</div>
						</div>
					</div>

					
					

					<div class="panel v2">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#section4">
							Gallery</a>
							</h4>
						</div>
						<div id="section4" class="panel-collapse collapse">
							<div class="panel-body">
								<p>Aenean elementum odio ut lorem cursus, eu auctor magna pellentesque.  Cras facilisis quam sed rhoncus dapibus.</p>

								<div class="gallery clearfix col-3">
									<a data-gal="prettyPhoto[gal]" href="{{ asset('user/images/placeholder/gallery1.jpg')}}" class="gallery-item">
										<img src="{{ asset('user/images/placeholder/gallery1.jpg')}}" alt="" />
									</a>

									<a data-gal="prettyPhoto[gal]" href="{{ asset('user/images/placeholder/gallery2.jpg')}}" class="gallery-item">
										<img src="{{ asset('user/images/placeholder/gallery2.jpg')}}" alt="" />
									</a>

									<a data-gal="prettyPhoto[gal]" href="{{ asset('user/images/placeholder/gallery3.jpg')}}" class="gallery-item">
										<img src="{{ asset('user/images/placeholder/gallery3.jpg')}}" alt="" />
									</a>

									<a data-gal="prettyPhoto[gal]" href="{{ asset('user/images/placeholder/gallery4.jpg')}}" class="gallery-item">
										<img src="{{ asset('user/images/placeholder/gallery4.jpg')}}" alt="" />
									</a>

									<a data-gal="prettyPhoto[gal]" href="{{ asset('user/images/placeholder/gallery5.jpg')}}" class="gallery-item">
										<img src="{{ asset('user/images/placeholder/gallery5.jpg')}}" alt="" />
									</a>

									<a data-gal="prettyPhoto[gal]" href="{{ asset('user/images/placeholder/gallery6.jpg')}}" class="gallery-item">
										<img src="{{ asset('user/images/placeholder/gallery6.jpg')}}" alt="" />
									</a>
								</div><!-- .gallery -->
							</div>
						</div>
					</div>

					<!-- <div class="panel v2">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#section5">
							FAQs</a>
							</h4>
						</div>
						<div id="section5" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<div class="panel-group">
											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse1">
													Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh?</a>
													</h4>
												</div>
												<div id="collapse1" class="panel-collapse collapse in">
													<div class="panel-body">Suspendisse libero augue, commodo non sodales vel, pretium vel felis. Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh. In gravida sed elit ac dignissim. Cras eu leo vitae dui eleifend interdum quis nec tellus.</div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse2">
													Etiam eget leo consectetur, accumsan velit quis, pellentesque ante?</a>
													</h4>
												</div>
												<div id="collapse2" class="panel-collapse collapse in">
													<div class="panel-body">Donec dapibus bibendum eros, varius cursus massa. Maecenas a diam et leo ullamcorper maximus. Suspendisse tincidunt viverra ligula eget viverra. Phasellus iaculis porta sapien, at ultrices nulla aliquam sed. Phasellus ut dignissim dolor.</div>
												</div>
											</div>
											
											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse3">
													Curabitur sollicitudin imperdiet ex, eu finibus?</a>
													</h4>
												</div>
												<div id="collapse3" class="panel-collapse collapse in">
													<div class="panel-body">Sed ullamcorper ligula urna, eget euismod mauris cursus eget. Quisque venenatis tincidunt lacus, at finibus augue dignissim eu. Nulla et congue nisl. Nullam leo felis, convallis vel egestas ut, ultrices et orci.</div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse4">
													Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh?</a>
													</h4>
												</div>
												<div id="collapse4" class="panel-collapse collapse in">
													<div class="panel-body">Suspendisse libero augue, commodo non sodales vel, pretium vel felis. Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh. In gravida sed elit ac dignissim. Cras eu leo vitae dui eleifend interdum quis nec tellus. </div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse4">
													Donec eget elit eu turpis varius ultricies ut id tortor?</a>
													</h4>
												</div>
												<div id="collapse5" class="panel-collapse collapse in">
													<div class="panel-body">Donec eget elit eu turpis varius ultricies ut id tortor. Nunc vitae lectus magna. Sed viverra urna quis sapien dignissim, sed interdum arcu pharetra. Sed ut lectus in justo consectetur venenatis. Aenean orci metus, rhoncus suscipit nulla sit amet, rhoncus scelerisque lacus. Morbi vitae imperdiet massa. Nullam ligula orci, sagittis sit amet accumsan posuere, placerat in nulla. </div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="panel-group accordion" id="accordion-gr2">
											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion1">
													Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh?</a>
													</h4>
												</div>
												<div id="accordion1" class="panel-collapse collapse in">
													<div class="panel-body">Suspendisse libero augue, commodo non sodales vel, pretium vel felis. Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh. In gravida sed elit ac dignissim. Cras eu leo vitae dui eleifend interdum quis nec tellus.</div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion2">
													Etiam eget leo consectetur, accumsan velit quis, pellentesque ante?</a>
													</h4>
												</div>
												<div id="accordion2" class="panel-collapse collapse">
													<div class="panel-body">Donec dapibus bibendum eros, varius cursus massa. Maecenas a diam et leo ullamcorper maximus. Suspendisse tincidunt viverra ligula eget viverra. Phasellus iaculis porta sapien, at ultrices nulla aliquam sed. Phasellus ut dignissim dolor. </div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion3">
													Curabitur sollicitudin imperdiet ex, eu finibus?</a>
													</h4>
												</div>
												<div id="accordion3" class="panel-collapse collapse">
													<div class="panel-body">Sed ullamcorper ligula urna, eget euismod mauris cursus eget. Quisque venenatis tincidunt lacus, at finibus augue dignissim eu. Nulla et congue nisl. Nullam leo felis, convallis vel egestas ut, ultrices et orci.</div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion4">
													Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh?</a>
													</h4>
												</div>
												<div id="accordion4" class="panel-collapse collapse">
													<div class="panel-body">Suspendisse libero augue, commodo non sodales vel, pretium vel felis. Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh. In gravida sed elit ac dignissim. Cras eu leo vitae dui eleifend interdum quis nec tellus. </div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion5">
													Donec eget elit eu turpis varius ultricies ut id tortor?</a>
													</h4>
												</div>
												<div id="accordion5" class="panel-collapse collapse">
													<div class="panel-body">Donec eget elit eu turpis varius ultricies ut id tortor. Nunc vitae lectus magna. Sed viverra urna quis sapien dignissim, sed interdum arcu pharetra. Sed ut lectus in justo consectetur venenatis. Aenean orci metus, rhoncus suscipit nulla sit amet, rhoncus scelerisque lacus. Morbi vitae imperdiet massa. Nullam ligula orci, sagittis sit amet accumsan posuere, placerat in nulla. </div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion6">
													Etiam eget leo consectetur, accumsan velit quis, pellentesque ante?</a>
													</h4>
												</div>
												<div id="accordion6" class="panel-collapse collapse">
													<div class="panel-body">Donec eget elit eu turpis varius ultricies ut id tortor. Nunc vitae lectus magna. Sed viverra urna quis sapien dignissim, sed interdum arcu pharetra. Sed ut lectus in justo consectetur venenatis. Aenean orci metus, rhoncus suscipit nulla sit amet, rhoncus scelerisque lacus. Morbi vitae imperdiet massa. Nullam ligula orci, sagittis sit amet accumsan posuere, placerat in nulla. </div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion7">
													Curabitur sollicitudin imperdiet ex, eu finibus?</a>
													</h4>
												</div>
												<div id="accordion7" class="panel-collapse collapse">
													<div class="panel-body">Donec eget elit eu turpis varius ultricies ut id tortor. Nunc vitae lectus magna. Sed viverra urna quis sapien dignissim, sed interdum arcu pharetra. Sed ut lectus in justo consectetur venenatis. Aenean orci metus, rhoncus suscipit nulla sit amet, rhoncus scelerisque lacus. Morbi vitae imperdiet massa. Nullam ligula orci, sagittis sit amet accumsan posuere, placerat in nulla. </div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion8">
													Praesent lorem mi, semper vitae tincidunt non, iaculis et nibh?</a>
													</h4>
												</div>
												<div id="accordion8" class="panel-collapse collapse">
													<div class="panel-body">Donec eget elit eu turpis varius ultricies ut id tortor. Nunc vitae lectus magna. Sed viverra urna quis sapien dignissim, sed interdum arcu pharetra. Sed ut lectus in justo consectetur venenatis. Aenean orci metus, rhoncus suscipit nulla sit amet, rhoncus scelerisque lacus. Morbi vitae imperdiet massa. Nullam ligula orci, sagittis sit amet accumsan posuere, placerat in nulla. </div>
												</div>
											</div>

											<div class="panel">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="accordion-gr2" href="#accordion9">
													Donec eget elit eu turpis varius ultricies ut id tortor?</a>
													</h4>
												</div>
												<div id="accordion9" class="panel-collapse collapse">
													<div class="panel-body">Donec eget elit eu turpis varius ultricies ut id tortor. Nunc vitae lectus magna. Sed viverra urna quis sapien dignissim, sed interdum arcu pharetra. Sed ut lectus in justo consectetur venenatis. Aenean orci metus, rhoncus suscipit nulla sit amet, rhoncus scelerisque lacus. Morbi vitae imperdiet massa. Nullam ligula orci, sagittis sit amet accumsan posuere, placerat in nulla. </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> -->

					<div class="panel v2">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#section6">
							Course reviews</a>
							</h4>
						</div>
						<div id="section6" class="panel-collapse collapse">
							<div class="panel-body">
								<div id="reviews">
									<div class="ratings-counter">
										<div class="avegare">
											<div class="star-rating normal">
												<span style="width:90%"></span>
											</div>
											<span>4.5 average based on 2 ratings</span>
										</div><!-- .avegare -->

										<div class="star-progress">
											<div class="star-item star5">
												<label>5 Star</label>
												<div class="progress">
													<div class="progress-bar" style="width:70%">
														<span>7</span>
													</div>
												</div>
											</div><!-- .star5 -->
											
											<div class="star-item star4">
												<label>4 Star</label>
												<div class="progress">
													<div class="progress-bar" style="width:30%">
														<span>3</span>
													</div>
												</div>
											</div><!-- .star4 -->

											<div class="star-item star3">
												<label>3 Star</label>
												<div class="progress">
													<div class="progress-bar" style="width:0%">
														<span>0</span>
													</div>
												</div>
											</div><!-- .star3 -->

											<div class="star-item star2">
												<label>2 Star</label>
												<div class="progress">
													<div class="progress-bar" style="width:0%">
														<span>0</span>
													</div>
												</div>
											</div><!-- .star2 -->

											<div class="star-item star1">
												<label>1 Star</label>
												<div class="progress">
													<div class="progress-bar" style="width:0%">
														<span>0</span>
													</div>
												</div>
											</div><!-- .star1 -->
										</div><!-- .star-progress -->
									</div><!-- .ratings-counter -->

									<div class="comments-area" id="comments">
										<ol class="comment-list">
											<li class="comment">
												<div class="comment-body">
													<div class="comment-avatar">
														<img class="avatar" src="{{ asset('user/images/placeholder/user1.jpg')}}" alt="" />
													</div><!-- .comment-avatar -->
													<header class="comment-meta">
														<cite class="comment-author"><a href="#">Julia Anna</a></cite>
														<span class="comment-date">- Feb 25, 2016</span>
													</header><!-- .comment-meta -->
													<div class="comment-content comment">
														<p>Nunc risus ex, tempus quis purus ac, tempor consequat ex. Vivamus sem magna, maximus at est id, maximus aliquet nunc.</p>
													</div> <!-- .comment-content -->
												</div><!-- .comment-body -->
											</li><!-- .comment -->

											<li class="comment">
												<div class="comment-body">
													<div class="comment-avatar">
														<img class="avatar" src="{{ asset('user/images/placeholder/user2.jpg')}}" alt="" />
													</div><!-- .comment-avatar -->
													<header class="comment-meta">
														<cite class="comment-author"><a href="#">John Doe</a></cite>
														<span class="comment-date">- Feb 22, 2016</span>
													</header><!-- .comment-meta -->
													<div class="comment-content comment">
														<p>Quisque rhoncus turpis sit amet turpis faucibus, at condimentum ipsum aliquam. Morbi pulvinar lorem vitae magna dapibus ultrices.</p>
													</div> <!-- .comment-content -->
												</div><!-- .comment-body -->
											</li><!-- .comment -->
										</ol><!-- .comment-list -->

										<div class="comment-respond" id="respond">
											<h3 class="comment-reply-title" id="reply-title">Add a review</h3>
											<form novalidate="" class="comment-form" id="commentform" method="post" action="#" />
												<p class="comment-form-rating">
													<label>Your Rating:</label>
													<span class="comment-rating">
														<a class="one-star" href="#" data-rating="1"></a>
														<a class="two-star" href="#" data-rating="2"></a>
														<a class="three-star" href="#" data-rating="3"></a>
														<a class="four-star" href="#" data-rating="4"></a>
														<a class="five-star" href="#" data-rating="5"></a>
														<input type="hidden" name="comment-rating" />
													</span><!-- .comment-rating -->
												</p>
												<p class="comment-form-comment">
													<label for="comment">Your Comment:</label>
													<textarea id="comment" name="comment" cols="45" rows="2" aria-required="true"></textarea>
												</p>
												<p class="form-submit">
													<label for="submit">Submit</label>
													<input name="submit" type="submit" id="submit" class="button medium primary rounded" value="Submit" />
												</p>
											</form>
										</div> <!-- #respond -->									
									</div><!-- .comments-area -->
								</div><!-- #reviews -->
							</div>
						</div>
					</div>
				</div><!-- .course-accordion -->
			</div><!-- .container -->
		</div><!-- .site-content -->

		@include('layouts.user.base')
		

		@include('layouts.user.footer')<!-- .site-footer -->
	</div><!-- #wrapper -->

	<!-- jQuery -->    
    <script src="{{ asset('user/js/jquery-1.11.3.js')}}"></script>
    <!-- Boostrap -->
    <script src="{{ asset('user/js/vendo/bootstrap.min.js')}}"></script>
    <!-- Jquery Parallax -->
    <script src="{{ asset('user/js/vendo/parallax.min.js')}}"></script>
    <!-- jQuery UI -->
	<script src="{{ asset('user/js/vendo/jquery-ui.min.js')}}"></script>
	<!-- jQuery Sticky -->
	<script src="{{ asset('user/js/vendo/jquery.sticky.js')}}"></script>
	<!-- OWL CAROUSEL Slider -->    
    <script src="{{ asset('user/js/vendo/owl.carousel.js')}}"></script>
    <!-- PrettyPhoto -->   
    <script src="{{ asset('user/js/vendo/jquery.prettyPhoto.js')}}"></script>
    <!-- Jquery Isotope -->
    <script src="{{ asset('user/js/vendo/isotope.pkgd.min.js')}}"></script>
    <!-- Main -->    
    <script src="{{ asset('user/js/main.js')}}"></script>
</body>
</html>
