<!DOCTYPE html>
<html>
<head>
<title>{{ env('APP_NAME')}}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
</head>
<body id="top">


<div>
  
<table cellpadding="8" cellspacing="0" style="padding:0;width:100%!important;background:#ffffff;margin:0;background-color:#ffffff" border="0"><tbody><tr><td valign="top">
  
<table cellpadding="0" cellspacing="0" style="border-radius:4px; max-width:600;  border:1px #20467e solid" border="0" align="center">

<tbody>
<tr><td colspan="3" height="6"></td></tr>
<tr style="line-height:0px ">
  <td width="100%" style="font-size:0px;padding-top: 28px;" align="left" height="1">
    {{--<img alt="" src="{{ asset('admin/img/logo.png')}}" class="CToWUd" style="width: 150px">--}}
    <img alt="" src="{{ asset('admin/img/logo.png')}}" class="CToWUd" style="width: 150px">
  </td>
</tr>
<tr style="width:100%;">
  <td>
    <table cellpadding="0" cellspacing="0" style="line-height:25px" border="0" align="center">
      <tbody>
      <tr>
        <td colspan="3" height="30"></td>
      </tr>
      <tr>
        <!-- <td>
          <img src="line.jpg" width="100%">
        </td> -->
      </tr>
      <tr>
        <td style="width:100%; border-top: 4px solid #cc9933;">
          <img src="{{ asset('admin/img/logo.png')}}" style="width:100%;">
        </td>

      </tr>
      <tr>
        <td width="454" align="left" style="color:#444444;border-collapse:collapse;font-size:11pt;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';max-width:454px; padding:21px;"  valign="top">
          Hello<span style="color:#234980;"> <b>{{$email}},</b></span><br><br>

          <p>You were successfully registered on Fliplms Learning Portal.</p>
            <p>Registration details below<br>
              Email: {{$email}}<br>Password: {{$password}}

          </p>
              <p>
                Please click or copy and paste the below link into your browser url to login to your account
                
              </p>
        </td>
      </tr>
      <tr>
        <td>
          <center>
            <a  href="{{ route('hm') }}" style="text-decoration: none;">
          <span target="_blank" style="border-radius:3px;font-size:15px;color:white;border:1px #cc9933 solid;text-decoration:none;padding:14px 7px 14px 7px;width:210px;max-width:210px;font-family:proxima_nova,'Open Sans','lucida grande','Segoe UI',arial,verdana,'lucida sans unicode',tahoma,sans-serif;margin:6px auto;display:block;background-color:#cc9933;text-align:center" >
             Login
          
        </span>
        </a>
          <br><br>
         {{ route('login') }}</center>

          <p> </p>
         
<!-- 
          <img src="line.jpg" width="100%"> -->
        </td>
      </tr>
      <tr>
        <td width="454" align="left" style="color:#444444;border-collapse:collapse;font-size:11pt;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';max-width:454px; padding:21px;"  valign="top" >
          Thank you.<br>- <span class="il"> Leadway Pensure PFA</span>
        </td>
          <!-- <td width="36"></td> -->
          </tr><tr><td colspan="3" height="36"></td></tr></tbody></table></td></tr></tbody></table>
        </td>
      </tr>

      <div class="footer-custom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Copyright © 2018 - All Rights Reserved -&nbsp;<a href="#">Leadway Pensure PFA</a> </p>
                </div>
                <div class="col-md-3"><a href="#" class="white"><i class="glyphicon glyphicon-envelope" id="ad-right"></i>Send Us An Email</a></div>
                <div class="col-md-3">
                    <p class="links-icon"><a href="#" class="socio-link"><i class="fa fa-facebook-square"></i></a><a href="#" class="socio-link"><i class="fa fa-twitter-square"></i></a><a href="#" class="socio-link"><i class="fa fa-instagram"></i></a></p>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>


