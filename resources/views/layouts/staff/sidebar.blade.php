<div id="sidebar" class="sidebar" style="background-color: #004d3e!important;">
	<div class="tabbable-panel">
		<div class="tabbable-line">
			<ul class="nav nav-tabs nav-justified dammy">
				<!-- <li id="tab_menu_a" class="active">
                    <a href="#tab_menu_1" data-toggle="tab">
                        <i class="fa fa-reorder"></i>
                    </a>
                </li> -->
				<!-- <li id="contact-tab">
                    <a href="#tab_contact_2" data-toggle="tab">
                        <i class="fa fa-user"></i>
                    </a>
                </li>
                <li id="report-tab">
                    <a href="#tab_report_3" data-toggle="tab">
                        <i class="fa fa-pie-chart"></i>
                    </a>
                </li> -->
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_menu_1">
					<form class="search-menu-form" >
						<div class="">
							{{-- <input id="menu-list-search" placeholder="Search Menu..." type="text" class="form-control search-menu"> --}}
						</div>
					</form>

					<!-- sidebar Menu -->
					<div id="MainMenu" class="" style="background-color: #006d58 !important;">

						<ul id="menu-list" class="nav nav-list" style="background-color: #006d58 !important;">
							<li class="separate-menu"><span>Staff Menu</span></li>
							<li class="{{ active('profile') }}">
								<a href="{{ route('profile') }}">
									<i class="menu-icon fa fa-tachometer"></i>
									<span class="menu-text"> Dashboard </span>
								</a>
								<b class="arrow"></b>
							</li>

							<li class="{{ active('staff_req_crs') }}">
								<a href="{{ route('staff_req_crs') }}">
									<i class="menu-icon fa fa-graduation-cap"></i>
									<span class="menu-text"> Request Course </span>
								</a>
								<b class="arrow"></b>
							</li>

							<li class="{{ active('all_client') }}">
								<a href="{{ route('staff_assigned_crs')}}" >
									<i class="menu-icon fa fa-graduation-cap"></i>

									<span class="menu-text">Assigned Courses</span>
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="{{route('completed_courses')}}">
									<i class="menu-icon fa fa-graduation-cap"></i>
									<span class="menu-text">Completed Courses</span>
								</a>

								<b class="arrow"></b>
							</li>


							{{--<li class="dropdown-toggle">
								<a href="#" class="dropdown-toggle">
									<i class="menu-icon fa fa-graduation-cap"></i>
									<span class="menu-text"> Courses </span>

									<b class="arrow fa fa-angle-down"></b>
								</a>

								<b class="arrow"></b>
								<ul class="submenu nav-show"  >
									<li class="{{ active('all_client') }}">
										<a href="{{ route('staff_assigned_crs')}}" >
											<i class="menu-icon fa fa-caret-right"></i>
											<span class="menu-text">Assigned Courses</span>
										</a>

										<b class="arrow"></b>
									</li>
									<li class="">
										<a href="{{route('add_client')}}">
											<i class="menu-icon fa fa-caret-right"></i>
											<span class="menu-text">Completed Courses</span>
										</a>

										<b class="arrow"></b>
									</li>
								</ul>
							</li>--}}


							{{--<li class="{{ active('staff_assigned_crs') }}">


                                <a href="{{ route('staff_assigned_crs') }}">
                                    <i class="menu-icon fa fa-graduation-cap"></i>
                                    <span class="menu-text"> Assigned Courses </span>
                                </a>
                                <b class="arrow"></b>
                            </li>--}}

							<li class="{{ active('edit_profile') }}">
								<a href="{{ route('edit_profile') }}">
									<i class="menu-icon fa fa-refresh"></i>
									<span class="menu-text"> Update Profile </span>
								</a>
								<b class="arrow"></b>
							</li>

							<li class="{{ active('get_us_result') }}">
								<a href="{{ route('get_us_result') }}">
									<i class="menu-icon fa fa-list-alt"></i>
									<span class="menu-text"> My Result </span>
								</a>
								<b class="arrow"></b>
							</li>



							@if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
							<li class="{{ active('get_us_result') }}">
								<a href="{{ route('admin.sign_in') }}" target="_blank">
									<i class="menu-icon fa fa-sign-in"></i>
									<span class="menu-text"> Admin Login </span>
								</a>
								<b class="arrow"></b>
							</li>
							@endif

							<!--<li>-->
							<!--	<a href="logout.html">-->
							<!--		<i class="menu-icon fa fa-sign-out"></i>-->
							<!--		<span class="menu-text"> Logout </span>-->
							<!--	</a>-->
							<!--	<b class="arrow"></b>-->
							<!--</li>-->

						</ul>


						<a class="sidebar-collapse" id="sidebar-collapse" data-toggle="collapse" data-target="#test">
							<i id="icon-sw-s-b" class="fa fa-angle-double-left"></i>
						</a>
					</div>
				</div>
			</div><!-- end tab-content-->
		</div><!-- end tabbable-line -->
	</div><!-- end tabbable-panel -->
</div>


