<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300i,400,600" rel="stylesheet">
<style>
	html,body,h1,h2,h3,h4,h5,h6,p,span,a{
	  font-family: 'Nunito Sans', sans-serif!important;
	 }
</style>
<header id="header">
				
				<nav class="navbar navbar-default nopadding" >
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>						
						<button type="button" id="menu-open" class="navbar-toggle menu-toggler pull-left">
							<span class="sr-only">Toggle sidebar</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						

						@php
							$logo = \App\Logo::first();
							if (is_null($logo)) {
								$myLogo = false;
							} else {
								$myLogo = true;
							}

							// dd($logo);
						@endphp
						<a class="navbar-brand" href="#" id="logo-panel">
							@if($myLogo)
							<img width="50px" class="my-logo-ad" height="50px" src="{{ asset('uploads/images/logos/'.$logo->logo)}}" alt="Golabi Admin">
							@endif

							@if(!$myLogo)
							<img src="{{ asset('admin/img/logo.png')}}" class="my-logo-ad" alt="Lead Way">
							@endif
							<!-- <i class="fa fa-slack"></i> Golabi Admin -->
						</a>						
						
						
					</div>
					<form action="#" class="form-search-mobile pull-right">
						<input id="search-fld" class="search-mobile" type="text" name="param" placeholder="Search ...">
						<button id="submit-search-mobile" type="submit">
							<i class="fa fa-search"></i>
						</button>
						<a href="#" id="cancel-search-mobile" title="Cancel Search"><i class="fa fa-times"></i></a>
					</form>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						
						
						<ul class="nav navbar-nav navbar-right">
							<li id="search-show-li" class="dropdown">
								<a href="#" id="search-mobile-show" class="dropdown-toggle" >
									<i class="fa fa-search"></i>
								</a>
							</li>
							
							<li class="dropdown">
								
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									{{-- <img alt="Golabi Avatar Admin" src="{{ asset('admin/img/avatars/avatar.png') }}" height="50" width="50" class="img-circle" /> --}}

									<img src="{{ asset('uploads/images/'.Auth::user()->photo) }}" height="50" width="50" class="img-circle" />


									{{ Auth::user()->fullname() }}
									<strong class="caret"></strong>
								</a>
								<ul class="dropdown-menu">
									{{-- <li>
										<a href="#">Profile<span class="fa fa-user pull-right"></span></a>
									</li> --}}
									<li>
										<a href="{{ route('edit_profile') }}">Setting<span class="fa fa-cog pull-right"></span></a>
									</li>
									<!-- <li>
										<a href="#">Messages<span class="badge pull-right"> 42 </span></a>
									</li> -->
									<li class="divider">
									</li>
									{{-- <li>
										<a href="#">Sign out<span class="fa fa-power-off pull-right"></span></a>
									</li> --}}

									<li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" >
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

								</ul>
							</li>

						</ul>
						
						<ul class="nav navbar-nav navbar-right">

							<li id="fullscreen-li">
								<a href="#" id="fullscreen" class="dropdown-toggle" >
									<i class="fa fa-arrows-alt"></i>
								</a>
							</li>
							
							<li id="side-hide-li" class="dropdown">
								<a href="#" id="side-hide" class="dropdown-toggle" >
									<i class="fa fa-reorder"></i>
								</a>
							</li>

							
						</ul>
						<!-- search form in header -->
						{{-- <form class="navbar-form navbar-right" >
							<div class="form-group">
								<input type="text" class="form-control search-header" placeholder="Enter Keyword" />
								<button type="submit" class="btn btn-link search-header-btn" >
								<i class="fa fa-search"></i>
								</button>
							</div>
						</form> --}}
						
						
					</div>
					
				</nav>
			</header>