<div class="page-footer">				
				<div class="col-xs-12 col-sm-12 text-center">
					<strong class=""><a href="#">{{ env("APP_NAME") }} © {{ \Carbon\Carbon::today()->year }}</a> </strong>
					<!--<a href="#">-->
					<!--	<i class="fa fa-twitter-square bigger-120"></i>-->
					<!--</a>-->
					<!--<a href="#">-->
					<!--	<i class="fa fa-facebook-square bigger-120"></i>-->
					<!--</a>-->
					<!--<a href="#">-->
					<!--	<i class="fa fa-rss-square orange bigger-120"></i>-->
					<!--</a>-->
				</div>			
			</div>