<div class="modal fade" id="modal-add-task" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<form class="form-horizontal">
							<div class="modal-header default">
								 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel1">
									Add Task
								</h4>
							</div>

							<div class="modal-body">
								<!-- Text input-->
								<div class="control-group">
								  	<label class="control-label" for="task-name">Task Name</label>
								  	<div class="controls">
								    	<input id="task-name" name="task-name" type="text" placeholder="" class="form-control">
								  	</div>
								</div>

								<!-- Textarea -->
								<div class="control-group">
								  	<label class="control-label" for="Description">Description</label>
								  	<div class="controls">                     
								    	<textarea id="Description" name="Description" class="form-control"></textarea>
								  	</div>
								</div>

								<!-- Text input-->
								<div class="control-group">
								  	<label class="control-label" for="owner">Owner</label>
								  	<div class="controls">
								    	<input id="owner" name="owner" type="text" placeholder="" class="form-control">
								    
								  	</div>
								</div>
							</div>
							<div class="modal-footer">
								 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button>
							</div>
						</form>
					</div>										
				</div>									
			</div>