<!DOCTYPE html>
<html>
	<head>
		<title>{{ env("APP_NAME") }}</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('admin/vendo/font-awesome/css/font-awesome.min.css') }}">
		
		<!-- Related css to this page -->	
		<link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
		
		<!-- Yeptemplate css --><!-- Please use *.min.css in production -->
		<link rel="stylesheet" href="{{ asset('admin/css/yep-style.css') }}">
		<link rel="stylesheet" href="{{ asset('admin/css/yep-vendors.css') }}">

			<!-- Related css to this page -->
			<link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.bootstrap.min.css') }}">
			<link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.responsive.min.css') }}">
			<link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.tableTools.min.css') }}">
			<link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.colVis.min.css') }}">

		<!-- favicon -->
		<link rel="shortcut icon" href="{{ asset("ft/img/favicon.png") }}" type="image/x-icon">
		<link rel="icon" href="{{ asset("ft/img/favicon.png") }}" type="image/x-icon">
		<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300i,400,600" rel="stylesheet">
	    <style>
	        html,body,h1,h2,h3,h4,h5,h6,p,span,a{
	            font-family: 'Nunito Sans', sans-serif!important;
	        }
	    </style>
	</head>

	<body id="mainbody" style="">
		<div id="container" class="container-fluid skin-3">
			<!-- Add Task in sidebar list modal -->
			@include('layouts.staff.task_modal')
			<!--./ Add Task in sidebar list modal -->
			
			<!-- Add Contact in sidebar list modal -->
			@include('layouts.staff.contact_modal')
			<!--./ Add Contact in sidebar list modal -->

			<!-- Header -->
			@include('layouts.staff.header')
			<!-- /end Header -->
			
			<!-- sidebar menu -->			
			@include('layouts.staff.sidebar')
			<!-- /end #sidebar -->
			
			<!-- main content  -->
			@yield('content')
			<!-- ./end #main  -->
			
			<!-- footer -->
			@include('layouts.staff.footer')
			<!-- /footer -->
		</div>
		<!-- end #container -->		

		<!-- General JS script library-->
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js') }}"></script>	
		<script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js') }}"></script>						
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js') }}"></script>									
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js') }}"></script>																		
		
	
		<!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
		<!--<script type="text/javascript" src="{{ asset('admin/js/yep-script.js') }}"></script>-->
		<!--<script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js') }}"></script>-->
		<!--<script type="text/javascript" src="{{ asset('admin/js/yep-demo.js') }}"></script> -->


		<!-- Related JavaScript Library to This Pagee -->
			<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js') }}"></script>


			<!-- Plugin-Scripts -->
			<script type="text/javascript">

				// Default Data Table Script
				$('#example1').dataTable({
					responsive: true
				});

				// Default Data Table Script
					$('#request').dataTable({
						responsive: true
					});
				// Default Data Table Script
					$('#video-logs').dataTable({
						responsive: true
					});
				
				// Default Data Table Script
					$('#staff').dataTable({
						responsive: true
					});

				
				// Default Data Table Script
					$('#instructor').dataTable({
						responsive: true
					});

				// Table Tools Data Tables
				$('#example2').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
						"t" +
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
					"oTableTools": {
						"aButtons": [
							"copy",
							"csv",
							"xls",
							{
								"sExtends": "pdf",
								"sTitle": "Yeptemplate_PDF",
								"sPdfMessage": "Yeptemplate PDF Export",
								"sPdfSize": "letter"
							},
							{
								"sExtends": "print",
								"sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
							}
						],

					},
					"autoWidth": true,
					responsive: true

				});

				// Column show & hide
				$('#example3').DataTable({
					"dom": '<"pull-left"f><"pull-right"l>tip',
					"dom": 'C<"clear">lfrtip',

					"bLengthChange": false,
					responsive: true
				});


			</script>
<style>
 		.dash-color1 { background-color: #223e70; }
 		.dash-color2 { background-color: #ff0000; }
 		.dash-color3 { background-color: #ffb81c;}
 		.dash-color4 { background-color: #0079c1; }
 		.icon-dash { color: white; margin-right: 20px; font-size: 36px; margin-top: 8px;}
		.icon-dash > span{ color: white;}
 		.card-body-dash { padding: 15px 15px 10px; }
 		.media-body { -ms-flex: 1; flex: 1; }
 		.media-head { font-size: 36px;  margin-top: 0px;}
 		.media { display: -ms-flexbox; display: flex; -ms-flex-align: start; align-items: flex-start; }
 		.no-margin { margin: 0px !important; }
 		.dash-img{ width: 44px;}
	</style>
		<!-- Google Analytics Script -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-67070957-1', 'auto');
			ga('send', 'pageview');
		</script>

	</body>
</html>

<style>
	.dammy{
		background-color: #004d3e !important;
		border-color: #004d3e !important;
	}

	div ul.breadcrumb {
		background-color: #004d3e !important;

	}
</style>