<header id="header" class="site-header">
			<div class="mid-header">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-8 col-xs-9">
							<div class="site-brand">
								<a class="logo" href="{{ route('hm')}}">
									<img src="{{ asset('user/images/assets/logo2.png')}}" alt="Universum" />
								</a>
							</div><!-- .site-brand  current-menu-item -->
						</div>

						<div class="col-md-8 col-sm-4 col-xs-3">
							<nav class="main-menu">
								<span class="mobile-btn"><i class="fa fa-bars"></i></span>
								<ul>
									<li class=""><a href="{{ route('hm') }}" class="{{ active('hm') }}">Home</a></li>
									<li><a href="{{ route('all_crs') }}" class="{{ active('all_crs') }}">Courses</a></li>
									@if(Auth::check())
									<li><a href="{{ route('profile') }}" class="{{ active('profile') }}">Dashboard</a></li>
									<li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
									@endif
									@if(Auth::guest())
									<li><a href="#loginModal" data-toggle="modal" data-target="#loginModal">Login</a></li>
									@endif
									@auth
									<li><a href="{{ route('logout') }}" data-toggle="modal">Logout</a></li>

									@endauth
								</ul>
							</nav>
						</div>
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .mid-header -->
		</header><!-- .site-header -->



		