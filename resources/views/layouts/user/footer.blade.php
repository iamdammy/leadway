<footer id="footer" class="site-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="copyright">
							<p><strong class=""><a href="#">{{ env("APP_NAME") }}</a></strong></p>
						</div><!-- .copyright -->
					</div>

					<div class="col-md-6">
						<nav class="nav-footer">
							<ul>
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Disclaimer</a></li>
								<li><a href="#">Feedback</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div><!-- .container -->
		</footer><!-- .site-footer -->