<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>{{ env("APP_NAME") }}</title>

	<!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/style.css')}}" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/responsive.css')}}" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('user/images/assets/favicon.png')}}" />
    
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300i,400,600" rel="stylesheet">
	    <style>
	        html,body,h1,h2,h3,h4,h5,h6,p,span,a{
	            font-family: 'Nunito Sans', sans-serif!important;
	        }
	    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/ie9/html5shiv.min.js"></script>
      <script src="js/ie9/respond.min.js"></script>
    <![endif]-->
    <!-- CUSTOM LINKS -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('user/css/profile.css')}}" /> -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="home6 header6">
	@include('layouts.user.preloader')

	<div id="wrapper">
		@include('layouts.user.header')

		<!-- Login Modal -->
		@include('layouts.user.login_modal')
		<!-- End of Login Modal -->

		@yield('content')

		@include('layouts.user.base')

		@include('layouts.user.footer')
	</div><!-- #wrapper -->

	<!-- jQuery -->    
    <script src="{{ asset('user/js/jquery-1.11.3.js')}}"></script>
    <!-- Boostrap -->
    <script src="{{ asset('user/js/vendo/bootstrap.min.js')}}"></script>
    <!-- Jquery Parallax -->
    <script src="{{ asset('user/js/vendo/parallax.min.js')}}"></script>
    <!-- jQuery UI -->
	<script src="{{ asset('user/js/vendo/jquery-ui.min.js')}}"></script>
	<!-- jQuery Sticky -->
	<script src="{{ asset('user/js/vendo/jquery.sticky.js')}}"></script>
	<!-- OWL CAROUSEL Slider -->    
    <script src="{{ asset('user/js/vendo/owl.carousel.js')}}"></script>
    <!-- PrettyPhoto -->   
    <script src="{{ asset('user/js/vendo/jquery.prettyPhoto.js')}}"></script>
    <!-- Jquery Isotope -->
    <script src="{{ asset('user/js/vendo/isotope.pkgd.min.js')}}"></script>
    <!-- Main -->    
    <script src="{{ asset('user/js/main.js')}}"></script>
</body>
</html>
