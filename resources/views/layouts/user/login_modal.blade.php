<div class="modal fade" id="loginModal">
			<div class="modal-dialog" style="max-width: 500px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" style="text-align: center;">LOGIN</h4>
					</div>
					<div class="container">
						<div class="modal-body" style="max-width: 472px;">
							<form method="POST" action="{{ route('login')}}">
								{{ csrf_field() }}
							  <div class="form-group">
							    <label for="exampleInputEmail1">Email address</label>
							    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Password</label>
							    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
							  </div>
							  <button type="submit" class="btn btn-primary">Submit</button>
							  <small><a href="{{ route('user_fg_pass') }}" class="pull-right" data-toggle="modal">Forgot Password?</a></small>
							</form>
						</div>
					</div>
					<!-- <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div> -->
				</div>
			</div>
		</div>


		<div class="modal fade" id="passwordModal">
			<div class="modal-dialog" style="max-width: 500px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" style="text-align: center;">FORGOT PASSWORD</h4>
						<h5>Enter Email Associated with your Account</h5>
					</div>
					<div class="container">
						<div class="modal-body" style="max-width: 472px;">
							<form method="POST" action="{{ route('login')}}">
								{{ csrf_field() }}
							  <div class="form-group">
							    <label for="exampleInputEmail1">Email address</label>
							    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email Associated with your Account">
							  </div>
							  
							  <button type="submit" class="btn btn-primary">Reset Password</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>