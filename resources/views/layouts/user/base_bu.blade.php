<div id="bottom" class="site-bottom">
			<div class="container">
				<div class="footer-widget bottom1">
					<div class="row">
						<div class="col-md-3 col-md-6">
							<div class="widget">
								<h3 class="widget-title">About Universum</h3>
								<div class="text-widget">
									<p>Universum design for education themes,  university, event and online education Vestibulum at consectetur ligula. Morbi facilisis vestibulum tellus, id lacinia purus blandit quis. Ut fermentum, ipsum non sagittis viverra, nisl nibh tincidunt libero, a efficitur nibh magna et magna. Vestibulum vel vulputate turpis. </p>
								</div>
							</div><!-- .widget -->
						</div>

						<div class="col-md-3 col-md-6">
							<div class="widget">
								<h3 class="widget-title">All Courses</h3>
								<ul>
									<li><a href="#">Accounting Technologies</a></li>
									<li><a href="#">Business Law</a></li>
									<li><a href="#">Entrepreneurship</a></li>
									<li><a href="#">Marketing Online</a></li>
									<li><a href="#">Business Economics</a></li>
									<li><a href="#">Public Health</a></li>
									<li><a href="#">Health Policy</a></li>
									<li><a href="#">History of Science</a></li>
								</ul>
							</div><!-- .widget -->
						</div>

						<div class="col-md-3 col-md-6">
							<div class="widget">
								<h3 class="widget-title">Quick links</h3>
								<ul>
									<li><a href="#">Studream Notices</a></li>
									<li><a href="#">Future Students</a></li>
									<li><a href="#">Why Studream?</a></li>
									<li><a href="#">Admissions, fees & Applications</a></li>
									<li><a href="#">Research Centres</a></li>
									<li><a href="#">International Students</a></li>
									<li><a href="#">Single subjects & Short courses</a></li>
									<li><a href="#">Faculties & Graduate Schools</a></li>
								</ul>
							</div><!-- .widget -->
						</div>

						<div class="col-md-3 col-md-6">
							<div class="widget contact-widget">
								<h3 class="widget-title">Contact Us</h3>
								<ul>
									<li class="address">197 Grand Poin Street, Suite 7S New York, NY 10013</li>
									<li class="mobile">+(112) 345 7689<br />+(112) 345 7689</li>
									<li class="email"><a href="mailto:contact@university.com">contact@university.com</a></li>
									<li class="time-work">Mon - Fri 8.00 - 18.00<br />Sat 9.00 - 16.00</li>
								</ul>
							</div><!-- .widget -->
						</div>
					</div>
				</div><!-- .bottom1 -->

				<div class="footer-widget bottom2">
					<div class="row">
						<div class="col-md-3 col-md-6">
							<div class="widget">
								<h3 class="widget-title">Our Socials</h3>
								<p>Follow our sosials sagittis viverra, nisl nibh tincidunt libero </p>
								<div class="socials">
									<ul>
										<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
										<li><a href="#" target="_blank"><i class="fa fa-behance"></i></a></li>
									</ul>
								</div>
							</div><!-- .widget -->
						</div>

						<div class="col-md-3 col-md-6">
							<div class="widget">
								<h3 class="widget-title">Tags</h3>
								<div class="tags-cloud">
									<a href="#">education</a>
									<a href="#">event</a>
									<a href="#">university</a>
									<a href="#">course</a>
									<a href="#">campus</a>
									<a href="#">book</a>
									<a href="#">college</a>
									<a href="#">scholl</a>
									<a href="#">gallery</a>
									<a href="#">blog</a>
									<a href="#">new</a>
									<a href="#">student</a>
									<a href="#">design</a>
									<a href="#">theme</a>
								</div><!-- .tags-cloud -->
							</div><!-- .widget -->
						</div>

						<div class="col-md-3 col-md-6">
							<div class="widget newsletter-widget">
								<h3 class="widget-title">Newsletter</h3>
								<p>Sign up for our mailing ist to get new course and course updates.</p>
								<form action="#" method="get" />
									<input type="email" placeholder="Enter email address" />
									<input type="submit" value="Subscribe" />
								</form>
							</div><!-- .widget -->
						</div>

						<div class="col-md-3 col-md-6">
							<div class="widget instargram-widget">
								<h3 class="widget-title">Instargram</h3>
								<div class="instargram">
									<ul>
										<li><a href="#" target="_blank">
											<img src="{{ asset('user/images/placeholder/instargram1.jpg')}}" alt="" />
										</a></li>

										<li><a href="#" target="_blank">
											<img src="{{ asset('user/images/placeholder/instargram2.jpg')}}" alt="" />
										</a></li>

										<li><a href="#" target="_blank">
											<img src="{{ asset('user/images/placeholder/instargram3.jpg')}}" alt="" />
										</a></li>

										<li><a href="#" target="_blank">
											<img src="{{ asset('user/images/placeholder/instargram4.jpg')}}" alt="" />
										</a></li>

										<li><a href="#" target="_blank">
											<img src="{{ asset('user/images/placeholder/instargram5.jpg')}}" alt="" />
										</a></li>

										<li><a href="#" target="_blank">
											<img src="{{ asset('user/images/placeholder/instargram6.jpg')}}" alt="" />
										</a></li>
									</ul>
								</div><!-- .instargram -->
							</div><!-- .widget -->
						</div>
					</div>
				</div><!-- .bottom2 -->
			</div><!-- .container -->
		</div><!-- .site-bottom -->