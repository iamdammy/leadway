<!DOCTYPE html>
<html>
	@include('layouts.admin.head_script')

	<body id="mainbody">
		<div id="container" class="container-fluid skin-3">
			<!-- Add Task in sidebar list modal -->
			
			<!-- Add Task in sidebar list modal -->
			
			<!-- Add Contact in sidebar list modal -->
			
			<!-- Add Contact in sidebar list modal -->

			<!-- Header -->
			@include('layouts.admin.header')
			<!-- /end Header -->
			
			<!-- sidebar menu -->			
			@include('layouts.admin.sidebar')
			<!-- /end #sidebar -->
			
			<!-- main content  -->
			<div id="main" class="main">
				<div class="row">
					<!-- breadcrumb section -->
					<div class="ribbon">
						<ul class="breadcrumb" style="background: #004d3e;">
							<li>
								<i class="fa fa-home"></i>
								<a href="#">Website</a>
							</li>
							<!-- <li>
								<a href="#">Library</a>
							</li>
							<li>
								<a href="#">Data</a>
							</li> -->
						</ul>
					</div>
					
					<!-- main content -->
					@yield('content')
					<!-- end #content -->
				</div><!-- end .row -->
			</div>
			<!-- ./end #main  -->
			
			<!-- footer -->
			@include('layouts.admin.footer')
			<!-- /footer -->
		</div>
		<!-- end #container -->		

		@include('layouts.admin.footer_script')


		


		<!-- Plugins Script -->
		<script type="text/javascript">
			
			
		</script>

		<!-- Google Analytics Script -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-67070957-1', 'auto');
			ga('send', 'pageview');
		</script>

	</body>
</html>




<style>
	.dammy{
		background-color: #004d3e !important;
		border-color: #004d3e !important;
	}
</style>