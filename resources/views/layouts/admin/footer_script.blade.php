<!-- General JS script library-->
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js')}}"></script>	
		<script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js')}}"></script>						
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js')}}"></script>									
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js')}}"></script>																		
		
	
		<!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
		<script type="text/javascript" src="{{ asset('admin/js/yep-script.js')}}"></script>
		<script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js')}}"></script>
		<script type="text/javascript" src="{{ asset('admin/js/yep-demo.js')}}"></script> 


		<!-- Related JavaScript Library to This Pagee -->
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js')}}"></script>