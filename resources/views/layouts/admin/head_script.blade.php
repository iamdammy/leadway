<head>
    <title>{{ env("APP_NAME") }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/font-awesome/css/font-awesome.min.css')}}">


    <!-- Yeptemplate css --><!-- Please use *.min.css in production -->
    <link rel="stylesheet" href="{{ asset('admin/css/yep-style.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/css/yep-vendors.css')}}">

    <!--<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">-->


    <!-- favicon -->
    {{--<link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico')}}" type="image/x-icon">--}}

    <link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{ asset('admin/img/favicon/favicon.png')}}" type="image/x-icon">


    <!-- Related css to this page -->
    <link rel="stylesheet"
          href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.bootstrap.min.css')}}vendors/jquery-datatables/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.responsive.min.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.tableTools.min.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.colVis.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300i,400,600" rel="stylesheet">
    <style>
        html, body, h1, h2, h3, h4, h5, h6, p, span, a {
            font-family: 'Nunito Sans', sans-serif;
        }
    </style>

    <style>
        .dash-color1 {
            background-color: #223e70;
        }

        .dash-color2 {
            background-color: #ff0000;
        }

        .dash-color3 {
            background-color: #ffb81c;
        }

        .dash-color4 {
            background-color: #0079c1;
        }

        .icon-dash {
            color: white;
            margin-right: 20px;
            font-size: 36px;
            margin-top: 8px;
        }

        .icon-dash > span {
            color: white;
        }

        .card-body-dash {
            padding: 15px 15px 10px;
        }

        .media-body {
            -ms-flex: 1;
            flex: 1;
        }

        .media-head {
            font-size: 36px;
            margin-top: 0px;
        }

        .media {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: start;
            align-items: flex-start;
        }

        .no-margin {
            margin: 0px !important;
        }

        .dash-img {
            width: 44px;
        }

        .breadcrumb {
            background: #004d3e
        }

        div ul .dammy {
            background: #004d3e

        }


    </style>

</head>