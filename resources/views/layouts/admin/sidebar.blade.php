<style>
	#MainMenu .nav-list > li > a:hover{
		color: red;
	}

	.menu-list:hover{
		color: red;
	}
</style>
<div id="sidebar" class="sidebar" style="background: white;">
				<div class="tabbable-panel">
					<div class="tabbable-line">
						<ul class="nav nav-tabs nav-justified">
							
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_menu_1">
								<!--<form class="search-menu-form" >-->
								<!--	<div class="">-->
								<!--		<input id="menu-list-search" placeholder="Search Menu..." type="text" class="form-control search-menu">										-->
								<!--	</div>-->
								<!--</form>-->
								
								<!-- sidebar Menu -->
								<div id="MainMenu" class="" style="background-color: #006d58 !important;">
									
									<ul id="menu-list" class="nav nav-list" style="background-color: #006d58 !important;">
										<!--<li class="separate-menu"><span>Main Menu</span></li>-->
										<li class="{{ active('admin.login') }}">
											<a href="{{route('admin.login')}}" >
												<i class="menu-icon fa fa-tachometer"></i>
												<span class="menu-text"> Dashboard </span>
											</a>

											<b class="arrow"></b>
										</li>

										@if(adminRole() == "SuperAdmin")
										{{--<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Company </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>											
											<ul class="submenu nav-show"  >

												<li class="">
													<a href="{{route('add_client')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Company</span>
													</a>

													<b class="arrow"></b>
												</li>

												<li class="{{ active('all_client') }}">
													<a href="{{ route('all_client')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Companies</span>
													</a>

													<b class="arrow"></b>													
												</li>

											</ul>
										</li>--}}
										@endif


										@if(adminRole() == "SuperAdmin")
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Roles </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>											
											<ul class="submenu nav-show"  >

												{{--<li class="">
													<a href="{{route('role_form')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Role</span>
													</a>

													<b class="arrow"></b>
												</li>--}}
												<li class="">
													<a href="{{ route('roles')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Roles</span>
													</a>

													<b class="arrow"></b>													
												</li>

											</ul>
										</li>
										@endif


										@if(adminRole() == "SuperAdmin")
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Department </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>											
											<ul class="submenu nav-show"  >

												<li class="">
													<a href="{{route('dept_form')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Department</span>
													</a>

													<b class="arrow"></b>
												</li>
												<li class="{{ active('depts') }}">
													<a href="{{ route('depts')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Departments</span>
													</a>

													<b class="arrow"></b>													
												</li>

											</ul>
										</li>
										@endif


										@if(adminRole() == "SuperAdmin")
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Admins </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>
											<ul class="submenu nav-show"  >
												<li class="">
													{{-- <a href="{{ route('logos')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">List Logos</span>
													</a> --}}

													<b class="arrow"></b>
												</li>
												<li class="">
													<a href="{{route('add_admin')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Add Admin</span>
													</a>

													<b class="arrow"></b>
												</li>

												<li class="">
													<a href="{{route('all_admin')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Admin</span>
													</a>

													<b class="arrow"></b>
												</li>
											</ul>
										</li>
										@endif




										@if(adminRole() == "SuperAdmin" or adminRoleId() == 2)
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Staff </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>											
											<ul class="submenu nav-show"  >

												<li class="">
													<a href="{{route('create_user')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Staff</span>
													</a>

													<b class="arrow"></b>
												</li>

												<li class="">
													<a href="{{ route('all_users')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Staff</span>
													</a>

													<b class="arrow"></b>													
												</li>

												<li class="">
													<a href="{{ route('upload_users')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Upload Staff</span>
													</a>
												
													<b class="arrow"></b>
												</li>
											</ul>
										</li>
										@endif



										@if(adminRole() == "SuperAdmin" or adminRoleId() == 2)
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Staff Group </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>
											<ul class="submenu nav-show"  >

												<li class="">
													<a href="{{route('group.create')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Group</span>
													</a>

													<b class="arrow"></b>
												</li>

												<li class="{{ active('group.all') }}">
													<a href="{{ route('group.all')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Groups</span>
													</a>

													<b class="arrow"></b>
												</li>


												<li class="">
													<a href="{{route('group.assign_course_group')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Assign Course to Group</span>
													</a>

													<b class="arrow"></b>
												</li>
											</ul>
										</li>
										@endif



										@if(adminRole() == "SuperAdmin")
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Facilitators </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>											
											<ul class="submenu nav-show"  >

												<li class="">
													<a href="{{route('fac_form')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Facilitator</span>
													</a>

													<b class="arrow"></b>
												</li>

												<li class="">
													<a href="{{ route('facs')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Facilitators</span>
													</a>

													<b class="arrow"></b>													
												</li>

											</ul>
										</li>
										@endif



										@if(adminRole() == "SuperAdmin" or adminRoleId() == 3)
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Categories </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>											
											<ul class="submenu nav-show"  >

												<li class="">
													<a href="{{route('cat_form')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Category</span>
													</a>

													<b class="arrow"></b>
												</li>

												<li class="">
													<a href="{{ route('cats')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Categories</span>
													</a>

													<b class="arrow"></b>													
												</li>

											</ul>
										</li>
										@endif



										@if(adminRole() == "SuperAdmin" or adminRoleId() == 3)
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-graduation-cap"></i>
												<span class="menu-text"> Courses </span>
										
												<b class="arrow fa fa-angle-down"></b>
											</a>
										
											<b class="arrow"></b>
											<ul class="submenu nav-show">
												
												<li class="">
													<a href="{{ route('crt_course')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Courses</span>
													</a>
										
													<b class="arrow"></b>
												</li>
												<li class="">
													<a href="{{ route('all_courses')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Courses</span>
													</a>
										
													<b class="arrow"></b>
												</li>
												<li class="">
													<a href="{{ route('req_courses')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Requested Courses</span>
													</a>
										
													<b class="arrow"></b>
												</li>
												
											</ul>
										</li>
										@endif



										@if(adminRole() == "SuperAdmin")
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-user"></i>
												<span class="menu-text"> Certificate </span>

												<b class="arrow fa fa-angle-down"></b>
											</a>

											<b class="arrow"></b>
											<ul class="submenu nav-show"  >
												<li class="">
													<a href="{{route('certificate.create')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Certificate</span>
													</a>

													<b class="arrow"></b>
												</li>



												<li class="">
													<a href="{{ route('certificate.all')}}" >
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Certificates</span>
													</a>

													<b class="arrow"></b>
												</li>

											</ul>
										</li>
										@endif




										@if(adminRole() == "SuperAdmin" or adminRoleId() == 3)
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-graduation-cap"></i>
												<span class="menu-text"> Modules </span>
										
												<b class="arrow fa fa-angle-down"></b>
											</a>
										
											<b class="arrow"></b>
											<ul class="submenu nav-show">
												<li class="">
													<a href="{{ route('mod_form')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Module</span>
													</a>

													<b class="arrow"></b>
												</li>


												<li class="">
													<a href="{{ route('mods')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Modules</span>
													</a>
										
													<b class="arrow"></b>
												</li>

											</ul>
										</li>
										@endif



										@if(adminRole() == "SuperAdmin" or adminRoleId() == 3)
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-graduation-cap"></i>
												<span class="menu-text"> Topics </span>
										
												<b class="arrow fa fa-angle-down"></b>
											</a>
										
											<b class="arrow"></b>
											<ul class="submenu nav-show">

												<li class="">
													<a href="{{ route('topic_form')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Topic</span>
													</a>

													<b class="arrow"></b>
												</li>


												<li class="">
													<a href="{{ route('topics')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Topics</span>
													</a>
										
													<b class="arrow"></b>
												</li>

											</ul>
										</li>
										@endif



										@if(adminRole() == "SuperAdmin" or adminRoleId() == 3)
										<li class="">
											<a href="#" class="dropdown-toggle">
												<i class="menu-icon fa fa-question-circle"></i>
												<span class="menu-text"> Quizzes </span>
										
												<b class="arrow fa fa-angle-down"></b>
											</a>
										
											<b class="arrow"></b>
											<ul class="submenu nav-show">

												<li class="">
													<a href="{{ route('quiz_form')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Create Quiz</span>
													</a>

													<b class="arrow"></b>
												</li>


												<li class="">
													<a href="{{ route('quizes')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">View Quizes</span>
													</a>
										
													<b class="arrow"></b>
												</li>


												<li class="">
													<a href="{{ route('quiz_import_form')}}">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">Import Quiz / Import Course Exam</span>
													</a>
										
													<b class="arrow"></b>
												</li>
											</ul>
										</li>
										@endif




										@if(adminRole() == "SuperAdmin")
											<li class="">
												<a href="#" class="dropdown-toggle">
													<i class="menu-icon fa fa-question-circle"></i>
													<span class="menu-text"> Reports </span>

													<b class="arrow fa fa-angle-down"></b>
												</a>

												<b class="arrow"></b>
												<ul class="submenu nav-show">

													<li class="">
														<a href="{{ route('course_users_report')}}">
															<i class="menu-icon fa fa-caret-right"></i>
															<span class="menu-text">Course Report</span>
														</a>

														<b class="arrow"></b>
													</li>



												</ul>
											</li>
										@endif


										@if(adminRole() == "SuperAdmin" or adminRoleId() == 2)
											<li class="">
												<a href="#" class="dropdown-toggle">
													<i class="menu-icon fa fa-user"></i>
													<span class="menu-text"> News </span>

													<b class="arrow fa fa-angle-down"></b>
												</a>

												<b class="arrow"></b>
												<ul class="submenu nav-show"  >
													<li class="">
														{{-- <a href="{{ route('logos')}}" >
                                                            <i class="menu-icon fa fa-caret-right"></i>
                                                            <span class="menu-text">List Logos</span>
                                                        </a> --}}

														<b class="arrow"></b>
													</li>
													<li class="">
														<a href="{{route('news_create')}}">
															<i class="menu-icon fa fa-caret-right"></i>
															<span class="menu-text">Add News</span>
														</a>

														<b class="arrow"></b>
													</li>

													<li class="">
														<a href="{{route('news_all')}}">
															<i class="menu-icon fa fa-caret-right"></i>
															<span class="menu-text">View News</span>
														</a>

														<b class="arrow"></b>
													</li>
												</ul>
											</li>
										@endif




										@if(adminRole() == "SuperAdmin")
										<li>
											<a target="_blank" href="/settings"><i class="menu-icon fa fa-cog"></i>Settings</a>
										</li>
										@endif

										{{-- <li class="">
											<a href="assignment.html" class="dropdown-toggle">
												<i class="menu-icon fa fa-tasks"></i>
												<span class="menu-text"> Batch Assignment </span>
										
												<b class="arrow fa fa-angle-down"></b>
											</a>
										
											<b class="arrow"></b>
											<ul class="submenu nav-show">
												<li class="">
													<a href="listbatch.html">
														<i class="menu-icon fa fa-caret-right"></i>
														<span class="menu-text">List Users</span>
													</a>
										
													<b class="arrow"></b>
												</li>
											</ul>
										</li> --}}

										{{-- <li>
											<a href="context.html">
												<i class="menu-icon fa fa-plus"></i>
												<span class="menu-text"> Context Creation </span>
											</a>
										
											<b class="arrow"></b>
										</li>

										<li>
											<a href="quiz.html">
												<i class="menu-icon fa fa-question-circle"></i>
												<span class="menu-text"> Quiz </span>
											</a>
										
											<b class="arrow"></b>
										</li> --}}

									</ul>


									<a class="sidebar-collapse" id="sidebar-collapse" data-toggle="collapse" data-target="#test">
										<i id="icon-sw-s-b" class="fa fa-angle-double-left"></i>
									</a>
								</div>
							</div>
						</div><!-- end tab-content-->
					</div><!-- end tabbable-line -->
				</div><!-- end tabbable-panel -->
			</div>


