<!DOCTYPE html>
<html>
	<head>
		<title>Golabi Admin - Dashboard</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{ asset('admin/vendo/font-awesome/css/font-awesome.min.css')}}">
		
		<!-- RTL version css -->
		<!-- <link rel="stylesheet" href="../assets/css/yep-rtl.css"> -->
		
		<!-- Related css to this page -->	
		<link rel="stylesheet" href="{{ asset('admin/vendo/morrisjs/css/morris.css')}}">
		<link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap-daterangepicker/css/daterangepicker.min.css')}}">
		<link rel="stylesheet" href="{{ asset('admin/vendo/fullcalendar/css/fullcalendar.min.css')}}">
		<link rel="stylesheet" href="{{ asset('admin/vendo/fullcalendar/css/fullcalendar.print.min.css')}}" media="print">

		<!-- Yeptemplate css --><!-- Please use *.min.css in production --><!-- Please use *.min.css in production -->
		<link rel="stylesheet" href="{{ asset('admin/css/yep-style.css')}}">
		<link rel="stylesheet" href="{{ asset('admin/css/yep-vendors.css')}}">

		<!-- favicon -->
		<link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico')}}" type="image/x-icon">
		<link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico')}}" type="image/x-icon">
	</head>
	<!-- you can add .rtl class for change direction and add lang attribute for change font style ex: lang='ar' -->
	<body id="mainbody" >
		<!-- Available Classes
			* 'container'         	- boxed layout mode (non-responsive: will not work with fixed-sidebar & fixed-ribbon)
			* 'container-fluid'		- Fullwidth layout mode
			* 'fixed-header'		- Fixed header mode
			* 'fixed-ribbon'		- Fixed breadcrumb bar
			* 'fixed-footer'		- Fixed footer
			* 'fixed-sidebar'		- Fixed sidebar mode
			* 'skin-1'				- Blue Skin
			* 'skin-2'				- Black Skin
			* 'skin-3'				- Default Skin
			* 'top-menu'			- Top menu layout menu
			* 'hover-active'		- open submenu in hover mode if you use .top-menu
		-->
		<div id="container" class="container-fluid skin-3" >
			<!-- Add Task in sidebar list modal -->
			@include('layouts.admin.sidebar_modal')
			<!--./ Add Task in sidebar list modal -->
			
			<!-- Add Contact in sidebar list modal -->
			@include('layouts.admin.sidebar_contact')
			<!--./ Add Contact in sidebar list modal -->
			@include('layouts.admin.header')

			<!-- sidebar menu -->			
			@include('layouts.admin.sidebar')
			<!-- /end #sidebar -->
			<!-- main content  -->
			<div id="main" class="main">
				<div class="row">
					<!-- breadcrumb section -->
					@include('layouts.admin.breadcrumb')
					
					<!-- main content -->
					@yield('content')
					<!-- end #content -->

				</div><!-- end .row -->
			</div>
			<!-- ./end #main  -->
			
			<!-- footer -->
			@include('layouts.admin.footer')
			<!-- /footer -->
		</div>

		

		<!-- General JS script library-->
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js')}}"></script>	
		<script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js')}}"></script>						
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js')}}"></script>									
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js')}}"></script>																		
			
		<!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
		<script type="text/javascript" src="{{ asset('admin/js/yep-script.js')}}"></script>
		<script type="text/javascript" src="{{ asset('admin/js/yep-demo.js')}}"></script>

		<!-- Related JavaScript Library to This Pagee -->
		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-sparkline/js/jquery.sparkline.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('admin/vendo/morrisjs/js/raphael.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('admin/vendo/morrisjs/js/morris.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('admin/vendo/easy-pie-chart/js/jquery.easypiechart.min.js')}}"></script>												
		<script type="text/javascript" src="{{ asset('admin/vendo/momentjs/js/moment.min.js')}}"></script>					
		<script type="text/javascript" src="{{ asset('admin/vendo/bootstrap-daterangepicker/js/daterangepicker.min.js')}}"></script>

		<script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.custom.min.js')}}"></script>																	
		<script type="text/javascript" src="{{ asset('admin/vendo/fullcalendar/js/fullcalendar.min.js')}}"></script>					

		<!-- Plugins Script -->
		<script type="text/javascript">
			$(function(){	

				// daterange picker
				function cb(start, end) {
			        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			    }
			    cb(moment().subtract(29, 'days'), moment());

			    $('#reportrange').daterangepicker({
			        ranges: {
			           'Today': [moment(), moment()],
			           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			           'This Month': [moment().startOf('month'), moment().endOf('month')],
			           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			        }
			    }, cb);

				// easy pie chart
				$('.piechart').easyPieChart({
			        size: 60

			    })

				/* Morris Area Chart */
				var data = [
					{ y: '2014', a: 4, b: 1},
					{ y: '2015', a: 8,  b: 0},
					{ y: '2016', a: 5,  b: 2},
					{ y: '2017', a: 10,  b: 0},
					{ y: '2018', a: 4,  b: 1},
					{ y: '2019', a: 16,  b: 3},
					{ y: '2020', a: 5, b: 1},
					{ y: '2021', a: 11, b: 5},
					{ y: '2022', a: 6, b: 2},
					{ y: '2023', a: 11, b: 3},
					{ y: '2024', a: 30, b: 2},
					{ y: '2025', a: 13, b: 0},
					{ y: '2026', a: 4, b: 2},
					{ y: '2027', a: 3, b: 8},
					{ y: '2028', a: 3, b: 0},
					{ y: '2029', a: 6, b: 0},
				],
				config = {
					data: data,
					xkey: 'y',
					ykeys: ['a', 'b'],
					labels: ['Total Income', 'Total Outcome'],
					fillOpacity: 0.6,
					hideHover: 'auto',
					behaveLikeLine: true,
					resize: true,
					pointFillColors:['#ffffff'],
					pointStrokeColors: ['black'],
					lineColors:['blue'],
					
					pointFillColors: ['#00ff00'],
					lineWidth:[1],
					pointSize:[.5],
					hideHover:'always'
				};
				config.element = 'areaChart';
				Morris.Area(config);
				


				$("#pie1").sparkline('html', {
				    type: 'pie',
				    width: '45%',
				    height:'78px'
				});
				$("#pie2").sparkline('html', {
				    type: 'pie',
				    width: '45%',
				    height:'78px'
				});

				

				

				$('#line-processorload').sparkline('html', {
				     fillColor: false, 
				     							    
			    	height:'50px',
			    	width:'78px'
				});

				$('#line-memory').sparkline('html', {
				     fillColor: false, 
				     						    
			    	height:'50px',
			    	width:'78px'
				});

				$('#line-bandwidth').sparkline('html', {
				    fillColor: false, 
				    						    
			    	height:'50px',
			    	width:'78px'
				});

				$('#line-disk').sparkline('html', {
				     fillColor: false, 
				     						    
			    	height:'50px',
			    	width:'78px'
				});


				/* initialize the calendar
					-----------------------------------------------------------------*/

				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();

				
				var calendar = $('#calendar').fullCalendar({
					//isRTL: true,
					 buttonHtml: {
						prev: '<i class="         fa fa-chevron-left"></i>',
						next: '<i class="         fa fa-chevron-right"></i>'
					},
				
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,agendaDay'
					},
					events: [
					  {
						title: 'All Day Event',
						start: new Date(y, m, 1),
						className: 'label-important'
					  },
					  {
						title: 'Long Event',
						start: new Date(y, m, d-5),
						end: new Date(y, m, d-2),
						className: 'label-success'
					  },
					  {
						title: 'Some Event',
						start: new Date(y, m, d-3, 16, 0),
						allDay: false
					  }
					]
					,
					editable: true,
					droppable: true, // this allows things to be dropped onto the calendar !!!
					drop: function(date, allDay) { // this function is called when something is dropped
					
						// retrieve the dropped element's stored Event Object
						var originalevent = $(this).data('event');
						var $extraEventClass = $(this).attr('data-class');
						
						
						// we need to copy it, so that multiple events don't have a reference to the same object
						// var copiedevent = $.extend({}, originalevent);
						
						// assign it the date that was reported
						copiedevent.start = date;
						copiedevent.allDay = allDay;
						if($extraEventClass) copiedevent['className'] = [$extraEventClass];
						
						// render the event on the calendar
						// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
						$('#calendar').fullCalendar('renderEvent', copiedevent, true);
						
						// is the "remove after drop" checkbox checked?
						if ($('#drop-remove').is(':checked')) {
							// if so, remove the element from the "Draggable Events" list
							$(this).remove();
						}
						
					}
					,
					selectable: true,
					selectHelper: true,
					select: function(start, end, allDay) {
						
						bootbox.prompt("New Event Title:", function(title) {
							if (title !== null) {
								calendar.fullCalendar('renderEvent',
									{
										title: title,
										start: start,
										end: end,
										allDay: allDay
									},
									true // make the event "stick"
								);
							}
						});
						

						calendar.fullCalendar('unselect');
					}
					,
					eventClick: function(calEvent, jsEvent, view) {

						//display a modal
						var modal = 
						'<div class="modal fade">\
						  <div class="modal-dialog">\
						   <div class="modal-content">\
							 <div class="modal-body">\
							   <button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button>\
							   <form class="no-margin">\
								  <label>Change event name &nbsp;</label>\
								  <input class="middle" autocomplete="off" type="text" value="' + calEvent.title + '" />\
								 <button type="submit" class="btn btn-sm btn-success"><i class="         fa fa-check"></i> Save</button>\
							   </form>\
							 </div>\
							 <div class="modal-footer">\
								<button type="button" class="btn btn-sm btn-danger" data-action="delete"><i class="         fa fa-trash-o"></i> Delete Event</button>\
								<button type="button" class="btn btn-sm" data-dismiss="modal"><i class="         fa fa-times"></i> Cancel</button>\
							 </div>\
						  </div>\
						 </div>\
						</div>';
					
					
						var modal = $(modal).appendTo('body');
						modal.find('form').on('submit', function(ev){
							ev.preventDefault();

							calEvent.title = $(this).find("input[type=text]").val();
							calendar.fullCalendar('updateEvent', calEvent);
							modal.modal("hide");
						});
						modal.find('button[data-action=delete]').on('click', function() {
							calendar.fullCalendar('removeEvents' , function(ev){
								return (ev._id == calEvent._id);
							})
							modal.modal("hide");
						});
						
						modal.modal('show').on('hidden', function(){
							modal.remove();
						});
				
					}
					
				});

			});
			
		</script>

		<!-- Google Analytics Script -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-67070957-1', 'auto');
			ga('send', 'pageview');
		</script>

	</body>
</html>