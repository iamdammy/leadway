<div class="modal fade" id="modal-add-contact" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header default">
							 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title" id="myModalLabel2">
								Add Contact
							</h4>
						</div>
						<div class="modal-body">
							<!-- Text input-->
							<div class="control-group">
							  	<label class="control-label" for="name">Name</label>
							  	<div class="controls">
							    	<input id="name" name="name" type="text" placeholder="" class="form-control">
							  	</div>
							</div>

							<!-- Textarea -->
							<div class="control-group">
							  	<label class="control-label" for="Address">Address</label>
							  	<div class="controls">                     
							    	<textarea id="Address" name="Address" class="form-control"></textarea>
							  	</div>
							</div>

							<div class="control-group">
							  	<label class="control-label" for="Phone">Phone</label>
							  	<div class="controls">                     
							    	<input id="Phone" name="Phone" type="number" placeholder="" class="form-control">
							  	</div>
							</div>

							<!-- Text input-->
							<div class="control-group">
							  	<label class="control-label" for="owner">Email</label>
							  	<div class="controls">
							    	<input id="Email" name="Email" type="text" placeholder="" class="form-control">
							    
							  	</div>
							</div>
						</div>
						<div class="modal-footer">
							 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>										
				</div>									
			</div>