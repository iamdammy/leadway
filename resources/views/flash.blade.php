@if(!empty($errors->all()))
	@foreach($errors->all() as $error)
		<div class="alert alert-danger">
  			<strong>{{ $error }}</strong>
		</div>
	@endforeach
@endif


@if (!empty(session('csv_error')))
		<div class="alert alert-danger">
  			<strong>{{ session('csv_error') }}</strong>
		</div>
@endif

@if (!empty(session('user_created')))
		<div class="alert alert-success">
  			<strong>{{ session('user_created') }}</strong>
		</div>
@endif

@if (!empty(session('quiz_crtd')))
		<div class="alert alert-success">
  			<strong>{{ session('quiz_crtd') }}</strong>
		</div>
@endif


@if (!empty(session('csv_success1')))
		<div class="alert alert-success">
  			<strong>{{ session('csv_success1') }}</strong>
		</div>
@endif

@if (!empty(session('user_updated')))
		<div class="alert alert-success">
  			<strong>{{ session('user_updated') }}</strong>
		</div>
@endif

@if (!empty(session('user_deleted')))
		<div class="alert alert-danger">
  			<strong>{{ session('user_deleted') }}</strong>
		</div>
@endif

@if (!empty(session('course_created')))
		<div class="alert alert-success">
  			<strong>{{ session('course_created') }}</strong>
		</div>
@endif

@if (!empty(session('crs_updated')))
		<div class="alert alert-success">
  			<strong>{{ session('crs_updated') }}</strong>
		</div>
@endif


@if (!empty(session('user_exist')))
		<div class="alert alert-danger">
  			<strong>{{ session('user_exist') }}</strong>
		</div>
@endif

@if (!empty(session('user_assigned')))
		<div class="alert alert-success">
  			<strong>{{ session('user_assigned') }}</strong>
		</div>
@endif

@if (!empty(session('dept_crtd')))
		<div class="alert alert-success">
  			<strong>{{ session('dept_crtd') }}</strong>
		</div>
@endif

@if (!empty(session('dept_crtd_error')))
		<div class="alert alert-danger">
  			<strong>{{ session('dept_crtd_error') }}</strong>
		</div>
@endif


@if (!empty(session('dept_updtd')))
		<div class="alert alert-success">
  			<strong>{{ session('dept_updtd') }}</strong>
		</div>
@endif


@if (!empty(session('dept_del')))
		<div class="alert alert-danger">
  			<strong>{{ session('dept_del') }}</strong>
		</div>
@endif


@if (!empty(session('role_crtd')))
		<div class="alert alert-success">
  			<strong>{{ session('role_crtd') }}</strong>
		</div>
@endif


@if (!empty(session('role_updtd')))
		<div class="alert alert-success">
  			<strong>{{ session('role_updtd') }}</strong>
		</div>
@endif


@if (!empty(session('role_deleted')))
		<div class="alert alert-danger">
  			<strong>{{ session('role_deleted') }}</strong>
		</div>
@endif

@if (!empty(session('crs_deleted')))
		<div class="alert alert-danger">
  			<strong>{{ session('crs_deleted') }}</strong>
		</div>
@endif



@if (!empty(session('req_sub')))
		<div class="alert alert-success">
  			<strong>{{ session('req_sub') }}</strong>
		</div>
@endif


@if (!empty(session('no_user_se')))
		<div class="alert alert-danger">
  			<strong>{{ session('no_user_se') }}</strong>
		</div>
@endif

@if (!empty(session('us_crs_del')))
		<div class="alert alert-danger">
  			<strong>{{ session('us_crs_del') }}</strong>
		</div>
@endif

@if (!empty(session('mass_assn')))
		<div class="alert alert-success">
  			<strong>{{ session('mass_assn') }}</strong>
		</div>
@endif




@if (!empty(session('attempted')))
		<div class="alert alert-danger">
  			<strong>{{ session('attempted') }}</strong>
		</div>
@endif


@if (!empty(session('pro_edtd')))
		<div class="alert alert-success">
  			<strong>{{ session('pro_edtd') }}</strong>
		</div>
@endif



@if (!empty(session('u_av_crs')))
		<div class="alert alert-danger">
  			<strong>{{ session('u_av_crs') }}</strong>
		</div>
@endif



@if (!empty(session('admin_updated')))
		<div class="alert alert-success">
  			<strong>{{ session('admin_updated') }}</strong>
		</div>
@endif




@if (!empty(session('mod_crtd')))
		<div class="alert alert-success">
  			<strong>{{ session('mod_crtd') }}</strong>
		</div>
@endif



@if (!empty(session('mod_uptd')))
		<div class="alert alert-success">
  			<strong>{{ session('mod_uptd') }}</strong>
		</div>
@endif




@if (!empty(session('mod_del')))
		<div class="alert alert-danger">
  			<strong>{{ session('mod_del') }}</strong>
		</div>
@endif


@if (!empty(session('topic_crtd')))
		<div class="alert alert-success">
  			<strong>{{ session('topic_crtd') }}</strong>
		</div>
@endif



@if (!empty(session('topic_uptd')))
		<div class="alert alert-success">
  			<strong>{{ session('topic_uptd') }}</strong>
		</div>
@endif




@if (!empty(session('topic_del')))
		<div class="alert alert-success">
  			<strong>{{ session('topic_del') }}</strong>
		</div>
@endif






@if (!empty(session('fac_crtd')))
		<div class="alert alert-success">
  			<strong>{{ session('fac_crtd') }}</strong>
		</div>
@endif



@if (!empty(session('fac_uptd')))
		<div class="alert alert-success">
  			<strong>{{ session('fac_uptd') }}</strong>
		</div>
@endif

@if (!empty(session('fac_del')))
		<div class="alert alert-danger">
  			<strong>{{ session('fac_del') }}</strong>
		</div>
@endif

{{-- Logo --}}
@if (!empty(session('fac_del')))
		<div class="alert alert-danger">
  			<strong>{{ session('fac_del') }}</strong>
		</div>
@endif


@if (!empty(session('logo_crtd')))
		<div class="alert alert-success">
  			<strong>{{ session('logo_crtd') }}</strong>
		</div>
@endif


@if (!empty(session('logo_upd')))
		<div class="alert alert-success">
  			<strong>{{ session('logo_upd') }}</strong>
		</div>
@endif

@if (!empty(session('logo_del')))
		<div class="alert alert-danger">
  			<strong>{{ session('logo_del') }}</strong>
		</div>
@endif

@if (!empty(session('quiz_imptd')))
		<div class="alert alert-success">
  			<strong>{{ session('quiz_imptd') }}</strong>
		</div>
@endif


@if (!empty(session('no_imptd')))
		<div class="alert alert-danger">
  			<strong>{{ session('no_imptd') }}</strong>
		</div>
@endif



@if (!empty(session('u_nt_fd')))
		<div class="alert alert-danger">
  			<strong>{{ session('u_nt_fd') }}</strong>
		</div>
@endif


@if (!empty(session('pas_link')))
		<div class="alert alert-success">
  			<strong>{{ session('pas_link') }}</strong>
		</div>
@endif


@if (!empty(session('u_cant_take')))
		<div class="alert alert-danger">
  			<strong>{{ session('u_cant_take') }}</strong>
		</div>
@endif


@if (!empty(session('pre_nt_dn')))
	<div class="alert alert-danger">
  		<strong>{{ session('pre_nt_dn') }}</strong>
	</div>
@endif


@if (!empty(session('no_cat')))
	<div class="alert alert-danger">
  		<strong>{{ session('no_cat') }}</strong>
	</div>
@endif

@if (!empty(session('no_cc')))
	<div class="alert alert-danger">
  		<strong>{{ session('no_cc') }}</strong>
	</div>
@endif

@if (!empty(session('us_crs_dele')))
	<div class="alert alert-danger">
  		<strong>{{ session('us_crs_dele') }}</strong>
	</div>
@endif



@if (!empty(session('max_rh')))
	<div class="alert alert-danger">
  		<strong>{{ session('max_rh') }}</strong>
	</div>
@endif


@if (!empty(session('crs_xpire')))
	<div class="alert alert-danger">
		<strong>{{ session('crs_xpire') }}</strong>
	</div>
@endif

@if (!empty(session('course_cert_crt')))
	<div class="alert alert-success">
		<strong>{{ session('course_cert_crt') }}</strong>
	</div>
@endif

@if (!empty(session('course_cert_upd')))
	<div class="alert alert-success">
		<strong>{{ session('course_cert_upd') }}</strong>
	</div>
@endif

@if (!empty(session('course_cert_del')))
	<div class="alert alert-danger">
		<strong>{{ session('course_cert_del') }}</strong>
	</div>
@endif


@if (!empty(session('group_created')))
	<div class="alert alert-success">
		<strong>{{ session('group_created') }}</strong>
	</div>
@endif

@if (!empty(session('group_updated')))
	<div class="alert alert-success">
		<strong>{{ session('group_updated') }}</strong>
	</div>
@endif

@if (!empty(session('group_deleted')))
	<div class="alert alert-danger">
		<strong>{{ session('group_deleted') }}</strong>
	</div>
@endif

@if (!empty(session('user_group_deleted')))
	<div class="alert alert-danger">
		<strong>{{ session('user_group_deleted') }}</strong>
	</div>
@endif

@if (!empty(session('user_assigned_to_group')))
	<div class="alert alert-success">
		<strong>{{ session('user_assigned_to_group') }}</strong>
	</div>
@endif

@if (!empty(session('group_assigned')))
	<div class="alert alert-success">
		<strong>{{ session('group_assigned') }}</strong>
	</div>
@endif

@if (!empty(session('no_staff_in_group')))
	<div class="alert alert-danger">
		<strong>{{ session('no_staff_in_group') }}</strong>
	</div>
@endif

@if (!empty(session('cat_crtd')))
	<div class="alert alert-success">
		<strong>{{ session('cat_crtd') }}</strong>
	</div>
@endif


@if (!empty(session('cat_updtd')))
	<div class="alert alert-success">
		<strong>{{ session('cat_updtd') }}</strong>
	</div>
@endif

@if (!empty(session('cat_deleted')))
	<div class="alert alert-danger">
		<strong>{{ session('cat_deleted') }}</strong>
	</div>
@endif

@if (!empty(session('dept_del_no')))
	<div class="alert alert-danger">
		<strong>{{ session('dept_del_no') }}</strong>
	</div>
@endif


@if (!empty(session('article_created')))
	<div class="alert alert-success">
		<strong>{{ session('article_created') }}</strong>
	</div>
@endif


@if (!empty(session('article_nt_created')))
	<div class="alert alert-danger">
		<strong>{{ session('article_nt_created') }}</strong>
	</div>
@endif



@if (!empty(session('article_updated')))
	<div class="alert alert-success">
		<strong>{{ session('article_updated') }}</strong>
	</div>
@endif


@if (!empty(session('article_nt_updated')))
	<div class="alert alert-danger">
		<strong>{{ session('article_nt_updated') }}</strong>
	</div>
@endif

@if (!empty(session('article_deleted')))
	<div class="alert alert-danger">
		<strong>{{ session('article_deleted') }}</strong>
	</div>
@endif




