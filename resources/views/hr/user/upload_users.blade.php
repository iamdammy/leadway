@extends('layouts.admin.app')
@section('content')
 <div id="content">
                    <input type="file" name="file">
                    <!-- end col-md-12 -->
                </div>
@stop

<script type="text/javascript">


                    jQuery(function ($) {
                        Dropzone.autoDiscover = false;
                        try {
                            var myDropzone = new Dropzone("#dropzone", {
                                paramName: "file", // The name that will be used to transfer the file
                                maxFilesize: .5, // MB

                                addRemoveLinks: true,
                                dictDefaultMessage:
                                    '<span class="bigger-150 bolder"><i class=" fa fa-caret-right red"></i> Drop files</span> to upload \
                					<span class="smaller-80 grey">(or click)</span> <br /> \
                					<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>'
                                ,
                                dictResponseError: 'Error while uploading file!',

                                //change the previewTemplate to use Bootstrap progress bars

                            });
                        } catch (e) {
                            alert('Dropzone.js does not support older browsers!');
                        }

                    });

                    jQuery(function ($) {
                        Dropzone.autoDiscover = false;
                        try {
                            var myDropzone = new Dropzone("#avatar-dropzone", {
                                url: "../dummy.html",
                                paramName: "file", // The name that will be used to transfer the file
                                maxFilesize: 50, // MB	               
                                maxFiles: 1,
                                maxfilesexceeded: function (file) {
                                    this.removeAllFiles();
                                    this.addFile(file);
                                },
                                addRemoveLinks: true,
                                dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
                                dictResponseError: 'Error while uploading file!',

                                //change the previewTemplate to use Bootstrap progress bars

                            });
                        } catch (e) {
                            alert('Dropzone.js does not support older browsers!');
                        }

                    });


                    jQuery(function ($) {
                        Dropzone.autoDiscover = false;
                        try {
                            var myDropzone = new Dropzone("#gmail-dropzone", {
                                url: "../dummy.html",
                                paramName: "file", // The name that will be used to transfer the file
                                maxFilesize: .5, // MB	               
                                maxFiles: 1,
                                clickable: '#upload',
                                createImageThumbnails: false,
                                maxfilesexceeded: function (file) {
                                    this.removeAllFiles();
                                    this.addFile(file);
                                },
                                addRemoveLinks: true,
                                dictDefaultMessage: '',
                                dictRemoveFile: 'x',
                                dictCancelUpload: 'x',
                                dictResponseError: 'Error while uploading file!',
                                previewTemplate: '<div class="dz-preview dz-file-preview">\
                										    <div class="dz-image"><img data-dz-thumbnail=""></div>\
                										    <div class="dz-details">\
                										        <div class="dz-size"><span data-dz-size=""></span></div>\
                										        <div class="dz-filename">\
                										            <span data-dz-name=""></span>\
                										        </div>\
                										    </div>\
                										    <div class="dz-progress">\
                										        <span class="dz-upload" data-dz-uploadprogress=""></span>\
                										    </div>\
                										    <div class="dz-error-message">\
                										        <span data-dz-errormessage=""></span>\
                										    </div>\
                										    <div class="dz-success-mark"><i class="fa fa-check success"></i></div>\
                										    <div class="dz-error-mark"><i class="fa fa-exclamation-triangle red"></i></div>\
                										</div>'
                                //change the previewTemplate to use Bootstrap progress bars

                            });
                        } catch (e) {
                            alert('Dropzone.js does not support older browsers!');
                        }
                    });


                </script>