<!DOCTYPE html>
<html>

<head>
    <title>{{ env("APP_NAME") }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/font-awesome/css/font-awesome.min.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">

    <!-- Yeptemplate css -->
    <!-- Please use *.min.css in production -->
    <link rel="stylesheet" href="{{ asset('admin/css/yep-style.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/yep-vendors.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.responsive.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.colVis.min.css') }}">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
</head>

<body id="mainbody">
    <div id="container" class="container-fluid skin-3">
        <!-- Add Task in sidebar list modal -->
			@include('layouts.staff.task_modal')
       
        <!--./ Add Task in sidebar list modal -->

        <!-- Add Contact in sidebar list modal -->
			@include('layouts.staff.contact_modal')
        
        <!--./ Add Contact in sidebar list modal -->

        <!-- Header -->
       
			@include('layouts.staff.header')
       
        <!-- /end Header -->

        <!-- sidebar menu -->
			@include('layouts.staff.sidebar')
       
        <!-- /end #sidebar -->

        <!-- main content  -->
        <div id="main" class="main">
            <div class="row">
                <!-- breadcrumb section -->
                <div class="ribbon">
                    <ul class="">
                        {{-- <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="#">Library</a>
                        </li>
                        <li>
                            <a href="#">Data</a>
                        </li> --}}
                    </ul>
                </div>

                <!-- main content -->
                <div id="content">
                    <div id="sortable-panel" class="ui-sortable">
                    	@include('flash')
                        <div id="titr-content" class="col-md-12">
                            <h2>Browse Through our Courses</h2>
                        </div>
                        <!-- Course Table -->
                        <!-- Admin over view .col-md-12 -->
                        <div class="col-md-12">
                           


                            <!-- Courses Taking -->
                            <div class="row">

                                <div class="col-md-9">
                                    <div class="row">
                                    	@foreach($courses as $course)
                                    	@if($course->userCourse(Auth::id(), $course->id) != 0 )
                                                    @continue
                                                    @endif
                                        <div class="col-md-4">
                                            <div class="panel panel-default">
                                                <div class="course-container">
                                                    
                                                   @if($course->course_img!="")
                                            <img src="{{ asset('uploads/images/'.$course->course_img)}}" alt="Avatar" class="image img-responsive">
												@else
													<img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar" class="image img-responsive">
												@endif
                                                    <div class="overlay">
                                                        <div class="text">
                                                            <span class="fa fa-graduation-cap hover-cap"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    
                                                        {{-- $course->course_name --}}
                                                        <legend class="staff-legend">
												@php $course_trim=$course->course_name; @endphp
													{{ str_limit($course_trim, $limit = 20, $end = '...') }}
												
                                                    </legend>
                                        
                                        			@if($course->userCourse(Auth::id(), $course->id) == 0 )
                                                   {{-- <a onclick="startDate()" href="{{ route('req_crs',['id'=>$course->id]) }}" class="btn btn-primary btn-sm">

                                                        Request

                                                    </a>--}}

                                                        @if($course->usCrsReq(Auth::id(), $course->id) == 0 )
                                                            {{--Send Course Request--}}
                                                            <form method="post" action="{{ route("req_crs", $course->id) }}">
                                                                {{ csrf_field() }}
                                                                <label>Start Date (Optional)</label>
                                                                <input type="date" name="start_date" class="form-control"> <br/>
                                                                <button type="submit" class="btn btn-primary btn-sm">Request</button>
                                                            </form>
                                                        @endif


                                                        @if($course->usCrsReq(Auth::id(), $course->id) != 0 )
                                                            <button class="btn btn-primary btn-sm">Your Request Awaits Confirmation</button>
                                                        @endif

                                                    @endif

                                                    @if($course->userCourse(Auth::id(), $course->id) != 0 )
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-danger" style="color: red;">
                                                    	Course Assigned
                                                    </a>
                                                    @endif
                                                        
                                                    <p class="req-info">
												@php $course_trim=$course->course_description; @endphp
													{{ str_limit($course_trim, $limit = 20, $end = '...') }}
												
                                                    {{-- $course->course_description --}}.</p>
                                        
                                                    <div class="media v-middle">
                                                        <div class="media-left">
                                                            <img src="http://demo.fliplms.com/images/demo.jpg " alt="People" class="img-circle req-img">
                                                        </div>
                                                        <div class="media-body">
                                                            <h4>
                                                            	@foreach($course->facs as $fac)
                                                                <a href="">{{ $fac->name ?? 'hdhdh'  }} </a>
                                                                @endforeach
                                                                <br>
                                                            </h4>
                                                            Instructor
                                        
                                                        </div>
                                                    </div>
                                        
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                            <div class="col-md-12"
                                                 style="margin-bottom: 40px!important; margin-top: 40px!important;">
                                                <div class="row">

                                                    <div class="col-md-3">
                                                        <a href="{{ route("completed_courses") }}" class="btn btn-primary dammy"
                                                           style="width: 100%!important;">Completed Courses</a>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <a href="{{ route("requested_courses") }}" class="btn btn-primary dammy"
                                                           style="width: 100%!important;">Requested Courses</a>

                                                    </div>

                                                    <div class="col-md-3">
                                                        <a href="{{ route("all_available_crs") }}" class="btn btn-primary dammy"
                                                           style="width: 100%!important;">All Courses</a>

                                                    </div>

                                                    @if(count($user->courses) != 0 )

                                                        <div class="col-md-3">
                                                            <a style="background-color: #004d3e!important;" href="{{route('staff_assigned_crs')}}" class="btn btn-primary ">
                                                                View All Assigned Courses
                                                            </a>
                                                        </div>
                                                    @endif

                                                    {{--<div class="col-md-3">
                                                        <button>TEST</button>

                                                    </div>--}}

                                                </div>
                                            </div>
                                            <br><br>

                                       {{-- <center> {{ $courses->links() }} </center>--}}




































                                        {{-- <div class="col-md-4">
                                            <div class="panel panel-default">
                                                <div class="course-container">
                                                    <img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar" class="image img-responsive">
                                                    <div class="overlay">
                                                        <div class="text">
                                                            <span class="fa fa-graduation-cap hover-cap"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <legend>
                                                        Computer Science 101
                                                    </legend>
                                        
                                        
                                                    <a href="javascript:void(0);" class="btn btn-primary btn-sm">Send Course Request</a>
                                        
                                                    <p class="req-info">It is a long established fact that a reader will be distracted by the readable content of a page when looking
                                                        at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distrib...</p>
                                        
                                                    <div class="media v-middle">
                                                        <div class="media-left">
                                                            <img src="http://demo.fliplms.com/images/demo.jpg " alt="People" class="img-circle req-img">
                                                        </div>
                                                        <div class="media-body">
                                                            <h4>
                                                                <a href="">Wale Akanbi</a>
                                                                <br>
                                                            </h4>
                                                            Instructor
                                        
                                                        </div>
                                                    </div>
                                        
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="panel panel-default">
                                                <div class="course-container">
                                                    <img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar" class="image img-responsive">
                                                    <div class="overlay">
                                                        <div class="text">
                                                            <span class="fa fa-graduation-cap hover-cap"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <legend>
                                                        Computer Science 101
                                                    </legend>
                                        
                                        
                                                    <a href="javascript:void(0);" class="btn btn-primary btn-sm">Send Course Request</a>
                                        
                                                    <p class="req-info">It is a long established fact that a reader will be distracted by the readable content of a page when looking
                                                        at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distrib...</p>
                                        
                                                    <div class="media v-middle">
                                                        <div class="media-left">
                                                            <img src="http://demo.fliplms.com/images/demo.jpg " alt="People" class="img-circle req-img">
                                                        </div>
                                                        <div class="media-body">
                                                            <h4>
                                                                <a href="">Wale Akanbi</a>
                                                                <br>
                                                            </h4>
                                                            Instructor
                                        
                                                        </div>
                                                    </div>
                                        
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <!-- Course Category -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle">
                                                    <i class="indicator fa fa-lg fa-angle-up pull-right"></i>
                                                    Course Category
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                @foreach($cats as $cat)
                                                <p>{{ $cat->cat_name }}
                                                    <span class="badge badge-default pull-right"> </span>
                                                </p>
                                                <hr>
                                                @endforeach
                                                @if(count($cats) == 0 )
                                                    <h2 style="color: red;">No Category</h2>
                                                @endif
                                                {{-- <p>Management
                                                    <span class="badge badge-default pull-right"> 30</span>
                                                </p>
                                                <hr>
                                                <p>L &amp; M
                                                    <span class="badge badge-default pull-right"> 30</span>
                                                </p>
                                                <hr> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <!-- end col-md-12 -->
                </div>
                <!-- end #content -->
            </div>
            <!-- end .row -->
        </div>
        <!-- ./end #main  -->

        <!-- footer -->
			@include('layouts.staff.footer')
        
        <!-- /footer -->
    </div>

    <!-- end #container -->

    <!-- General JS script library-->
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js') }}"></script>


    <!-- Yeptemplate JS Script -->
    <!-- Please use *.min.js in production -->
    <script type="text/javascript" src="{{ asset('admin/js/yep-script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/yep-demo.js') }}"></script>


    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js') }}"></script>


    <style>
        .dammy{
            background-color: #004d3e !important;
            border-color: #004d3e !important;
        }
    </style>


    <!-- Plugin-Scripts -->
    <script type="text/javascript">

        function startDate(e) {
            e.preventDefault();
            alert("YES");
        }

        // Default Data Table Script
        $('#example1').dataTable({
            responsive: true
        });

        // Default Data Table Script
        $('#request').dataTable({
            responsive: true
        });
        // Default Data Table Script
        $('#video-logs').dataTable({
            responsive: true
        });

        // Default Data Table Script
        $('#staff').dataTable({
            responsive: true
        });


        // Default Data Table Script
        $('#instructor').dataTable({
            responsive: true
        });

        // Table Tools Data Tables
        $('#example2').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "Yeptemplate_PDF",
                        "sPdfMessage": "Yeptemplate PDF Export",
                        "sPdfSize": "letter"
                    },
                    {
                        "sExtends": "print",
                        "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
                    }
                ],

            },
            "autoWidth": true,
            responsive: true

        });

        // Column show & hide
        $('#example3').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            "dom": 'C<"clear">lfrtip',

            "bLengthChange": false,
            responsive: true
        });


    </script>

    <!-- Google Analytics Script -->
    <script>
        (function (i, s, o, g, r, a, m) {        
i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67070957-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>

</html>