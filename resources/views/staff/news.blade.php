@extends('layouts.staff.app')
@section('content')
    <div id="main" class="main">
        <div class="row">
            <!-- breadcrumb section -->
            <div class="ribbon">
                <ul class="">
                    {{-- <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">Library</a>
                    </li>
                    <li>
                        <a href="#">Data</a>
                    </li> --}}
                </ul>
            </div>

            <!-- main content -->
            <div id="content">
                <div id="sortable-panel" class="ui-sortable">
                    <!-- Course Table -->
                    <!-- Admin over view .col-md-12 -->

                    <!-- main content -->
                    @include('flash')
                    <div id="content" class="col-md-12">
                        <div id="sortable-panel" class="">


                            <!-- Courses Taking -->
                            <hr>
                            <div id="titr-content" class="col-md-12">
                                <h1>Title : {{ $article->title }}</h1>
                            </div>
                            <br><br>
                            <div class="col-md-12">
                                <div class="row">

                                    {{ $article->body }}

                                </div>

                                <br><br>

                            </div>


                            <hr>

                            {{--<div id="titr-content" class="col-md-12">
                                <h2>Completed Courses</h2>
                            </div>--}}








                        </div>

                        <!-- end col-md-12 -->
                    </div>

                    <!-- end #content -->
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-8">
                                {{--<div class="panel panel-default">
                                    <div class="panel-body">
                                        <form class="form-horizontal" method="" enctype="">

                                            <div class="row">
                                                <div class="col-md-3 col-sm-5  col-xs-5" style="float: right">

                                                    <img src="{{ asset('uploads/images/'.$user->photo) }}" alt=""
                                                         style="width:160px;height:100%;margin:5px"
                                                         class="img-rectangle img-responsive">


                                                </div>
                                                <div class="col-md-9 col-sm-7  col-xs-7" style="float: left">
                                                    <div class="form-group">
                                                        <label for="" class="col-md-4 control-label">Full Name</label>
                                                        <div class="col-md-8">

                                                            <div class="">
                                                                <div class="input-group">
																			<span class="input-group-addon"
                                                                                  style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																				<i class="fa fa-user"></i>
																			</span>
                                                                    <input type="text" class="form-control" id=""
                                                                           name="firstname"
                                                                           value="{{ $user->fullname() }}"
                                                                           disabled="disabled"
                                                                           style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="form-group">
                                                            <label for="" class="col-md-4 control-label">Staff
                                                                Id</label>
                                                            <div class="col-md-8">
                                                                <div class="">
                                                                    <div class="input-group">
																				<span class="input-group-addon"
                                                                                      style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																					<i class="fa fa-th-list"></i>
																				</span>
                                                                        <input type="" class="form-control" id=""
                                                                               name=""
                                                                               value="{{ $user->staff_id ?? 'Not Set' }}"
                                                                               disabled=""
                                                                               style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="media-body">
                                                        <div class="form-group">
                                                            <label for="" class="col-md-4 control-label">Employment
                                                                Date</label>
                                                            <div class="col-md-8">
                                                                <div class="">
                                                                    <div class="input-group">
																				<span class="input-group-addon"
                                                                                      style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																					<i class="fa fa-th-list"></i>
																				</span>
                                                                        <input type="" class="form-control" id=""
                                                                               name=""
                                                                               value="{{ date('d-m-Y', strtotime($user->dob)  )?? 'Not Set' }}"
                                                                               disabled=""
                                                                               style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <br>
                                            <div class="media-body">
                                                <label for="" class="col-md-4 control-label">Email</label>
                                                <div class="col-md-8">
                                                    <div class="">
                                                        <div class="input-group">
																	<span class="input-group-addon"
                                                                          style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																		<i class="fa fa-envelope"></i>
																	</span>
                                                            <input type="" class="form-control" id="" name=""
                                                                   value="{{ $user->email }}" disabled=""
                                                                   style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="media-body">

                                                <div class="form-group">
                                                    <label for="" class="col-md-4 control-label">Mobile Number</label>
                                                    <div class="col-md-8">
                                                        <div class="">
                                                            <div class="input-group">
																		<span class="input-group-addon"
                                                                              style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																			<i class="fa fa-phone"></i>
																		</span>
                                                                <input type="" class="form-control" id="" name=""
                                                                       value="{{ $user->phone ?? '+234...' }}"
                                                                       disabled=""
                                                                       style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <br>

                                            <br>
                                        </form>
                                    </div>
                                </div>--}}
                                <!-- end panel -->
                            </div>
                            <div class="col-md-4">

                                <div class="panel panel-default">
                                    <div class="panel-heading ui-sortable-handle">
                                        <div class="panel-title">
                                            News
                                            <div class="bars pull-right">
                                                <div class="dropdown ">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu yep-panel-menu">
                                                        <li>
                                                            <a href="#">Daily
                                                                <span class="badge pull-right"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Monthly
                                                                <span class="badge pull-right"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Yearly
                                                                <span class="badge pull-right"> 42 </span>
                                                            </a>
                                                        </li>
                                                        <li class="divider">
                                                        </li>
                                                        <li>
                                                            <a href="#">All time
                                                                <span class="badge pull-right">56</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <a href="#">
                                                    <i class="maximum fa fa-expand" data-toggle="tooltip"
                                                       data-placement="bottom" title=""
                                                       data-original-title="Maximize"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="minimize fa fa-chevron-down" data-toggle="tooltip"
                                                       data-placement="bottom" title=""
                                                       data-original-title="Collapse"></i>
                                                </a>
                                                <a href="#">
                                                    <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip"
                                                       data-placement="bottom" title="" class="fa fa-times"
                                                       data-original-title="Close"></i>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div style="padding:10px">
                                            <marquee behavior="scroll" direction="up" style="height:220px;"
                                                     scrollamount="3">
                                                @foreach($news as $new)
                                                    <h4><a href="{{ route("view_news", $new->id) }}">{{ $new->title }}</a></h4>
                                                    <p></p><a href="{{ route("view_news", $new->id) }}"> {{ str_limit($new->body, 80) }}  </a><p></p>
                                                    <hr>
                                                @endforeach


                                            </marquee>
                                        </div>
                                    </div>
                                </div>
                                <!-- end panel -->
                            </div>
                        </div>


                    </div>

                </div><!-- end col-md-12 -->
            </div><!-- end #content -->
        </div><!-- end .row -->
    </div>
@stop