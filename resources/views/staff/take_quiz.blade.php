<!DOCTYPE html>
<html>

<head>
    <title>Leadway Pensure Online Academy</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/font-awesome/css/font-awesome.min.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">

    <!-- Yeptemplate css -->
    <!-- Please use *.min.css in production -->
    <link rel="stylesheet" href="{{ asset('admin/css/yep-style.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/yep-vendors.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.responsive.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.colVis.min.css') }}">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">


    <style>
        .skin-3 .list-group-item {
            background-color: white;
        }

        .skin-3 .list-group-item:hover {
            background-color: gainsboro;
        }
    </style>
</head>

<body id="mainbody">
    <div id="container" class="container-fluid skin-3">
        <!-- Add Task in sidebar list modal -->
        @include('layouts.staff.task_modal')
        <!--./ Add Task in sidebar list modal -->

        <!-- Add Contact in sidebar list modal -->
       @include('layouts.staff.contact_modal')
        <!--./ Add Contact in sidebar list modal -->

        <!-- Header -->
       @include('layouts.staff.header')
        <!-- /end Header -->

        <!-- sidebar menu -->
        @include('layouts.staff.sidebar')
        <!-- /end #sidebar -->

        <!-- main content  -->
        <div id="main" class="main">
            <div class="row">
                <!-- breadcrumb section -->
                <div class="ribbon">
                    <ul class="breadcrumb">
                        {{-- <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="#">Library</a>
                        </li>
                        <li>
                            <a href="#">Data</a>
                        </li> --}}
                    </ul>
                </div>

                <!-- main content -->
                <div id="content">
                    <div id="sortable-panel" class="ui-sortable">
                            @include('flash')
                            <div id="titr-content" class="col-md-12">
                            <h2>Course Name : {{ $course->course_name }}</h2>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <!-- Modules -->
                                   {{--  <div class="panel-group yep-accordion" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" aria-expanded="false">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle">
                                                        <i class="indicator fa fa-lg pull-right fa-angle-down"></i>
                                                        Project Management Framework
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <ul class="list-group">
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">1. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 1
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">2. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 2
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">3. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 3
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">3. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right fa fa-question"></span>
                                                                Quiz
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo" aria-expanded="false">
                                                <h4 class="panel-title">
                                                    <a class="collapsed">
                                                        <i class="fa fa-lg pull-right fa-angle-down"></i>
                                                        Project Integration Management
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <ul class="list-group">
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">1. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 1
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">2. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 2
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">3. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 3
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">3. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right fa fa-question"></span>
                                                                Quiz
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree" aria-expanded="false">
                                                <h4 class="panel-title">
                                                    <a class="collapsed">
                                                        <i class="fa fa-lg fa-angle-down pull-right"></i>
                                                        Project Scope Management
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false">
                                                <div class="panel-body">
                                                    <ul class="list-group">
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">1. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 1
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">2. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 2
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">3. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right ">5:18</span>
                                                                Module 3
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item  ">
                                                            <a class="list-group-link" href="#">
                                                                <span class=" pull-left">3. &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                <span class=" pull-right fa fa-question"></span>
                                                                Quiz
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <a href="{{ route('tak_crs', ['id'=>$course->id]) }}" class="btn btn-primary has-ripple" style="margin-bottom: 14px; width: 100%;">Back to Course
                                    </a>
                                </div>
                                <div class="col-md-10">
                                    <!-- Questions -->
                                    <form method="POST" action="{{ route('pro_quiz_mod', ['crs'=>$course->id,'mId'=>$mId]) }}">
                                        {{ csrf_field() }}
                                            @php $i = 1; @endphp
                                            @foreach($questions as $que)
                                            @php
                                                {{--$options = [$que->choice_1, $que->choice_2, $que->choice_3, $que->answer ];--}}
                                                $options = [$que->option1, $que->option2, $que->option3, $que->option4];
                                                shuffle($options);
                                            @endphp
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    Question {{ $i }}
                                                </div>
                                                <div class="panel-body">
                                                    {{ $que->question }}
                                                    <br><hr>
                                                    <div class="radio radio-inline">
                                                        <input type="radio" id="option1" value="{{ $options[0] }}" 
                                                        name="quizId{{ $que->id }}">
                                                        <label for="option1"> {{ $options[0] }}</label>
                                                    </div>
                                                    <div class="radio radio-inline">
                                                        <input type="radio" id="option2" value="opti{{ $options[1] }}on2" 
                                                        name="quizId{{ $que->id }}">
                                                        <label for="option2"> {{ $options[1] }}</label>
                                                    </div>
                                                    <div class="radio radio-inline">
                                                        <input type="radio" id="option3" value="{{ $options[2] }}" 
                                                        name="quizId{{ $que->id }}">
                                                        <label for="option3"> {{ $options[2] }}</label>
                                                    </div>
                                                    <div class="radio radio-inline">
                                                        <input type="radio" id="option4" value="{{ $options[3] }}" 
                                                        name="quizId{{ $que->id }}">
                                                        <label for="option4"> {{ $options[3] }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                            @php $i++; @endphp
                                            @endforeach
                                            @if(count($questions) != 0 )
                                            <center>
                                                <button class="btn btn-primary" type="submit">Submit</button>
                                            </center>
                                            @endif
                                </form>

                                    {{-- <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Question 1
                                        </div>
                                        <div class="panel-body">
                                            Who discovered Shaku Shaku?
                                            <br>
                                            <hr>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="q2-option1" value="option1" name="q2-option">
                                                <label for="q2-option1"> Don Jazzy</label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="q2-option2" value="option2" name="q2-option">
                                                <label for="q2-option2"> Olamide Badoo</label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="q2-option3" value="option3" name="q2-option">
                                                <label for="q2-option3"> Toons Hub</label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="q2-option4" value="option4" name="q2-option">
                                                <label for="q2-option4"> Adekunle Gold</label>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>

                                @if(count($questions) == 0)
                                    <h3>NO QUESTIONS FOR THE COURSE YET</h3>
                                @endif()
                            </div>

                        </div>

                    </div>
                    <!-- end col-md-12 -->
                </div>
                <!-- end #content -->
            </div>
            <!-- end .row -->
        </div>
        <!-- ./end #main  -->

        <!-- footer -->
        @include('layouts.staff.footer')
        <!-- /footer -->
    </div>
    <!-- end #container -->

    <!-- General JS script library-->
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js') }}"></script>


    <!-- Yeptemplate JS Script -->
    <!-- Please use *.min.js in production -->
    <script type="text/javascript" src="{{ asset('admin/js/yep-script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/yep-demo.js') }}"></script>


    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js') }}"></script>


    <!-- Plugin-Scripts -->
    <script type="text/javascript">

        // Default Data Table Script
        $('#example1').dataTable({
            responsive: true
        });

        // Default Data Table Script
        $('#request').dataTable({
            responsive: true
        });
        // Default Data Table Script
        $('#video-logs').dataTable({
            responsive: true
        });

        // Default Data Table Script
        $('#staff').dataTable({
            responsive: true
        });


        // Default Data Table Script
        $('#instructor').dataTable({
            responsive: true
        });

        // Table Tools Data Tables
        $('#example2').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "Yeptemplate_PDF",
                        "sPdfMessage": "Yeptemplate PDF Export",
                        "sPdfSize": "letter"
                    },
                    {
                        "sExtends": "print",
                        "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
                    }
                ],

            },
            "autoWidth": true,
            responsive: true

        });

        // Column show & hide
        $('#example3').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            "dom": 'C<"clear">lfrtip',

            "bLengthChange": false,
            responsive: true
        });


    </script>

    <!-- Google Analytics Script -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67070957-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>

</html>