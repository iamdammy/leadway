<!DOCTYPE html>
<html>

<head>
    <title>{{ env("APP_NAME") }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/font-awesome/css/font-awesome.min.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
    <!--JS CHAT-->
    <link rel="stylesheet" type="text/css" href="{{ asset('jcomment/css/jquery-comments.css')}}">
    <!--<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->

    <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->

    <!-- Yeptemplate css -->
    <!-- Please use *.min.css in production -->
    <link rel="stylesheet" href="{{ asset('admin/css/yep-style.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/yep-vendors.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.responsive.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.colVis.min.css') }}">
    <script src="{{ asset('admin/js/jquery.js') }}" type="text/javascript"></script>
    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">


    <style>
        .skin-3 .list-group-item {
            background-color: white;
        }

        .skin-3 .list-group-item:hover {
            background-color: gainsboro;
        }

        #chat_post li {
            list-style-type: none;
            list-style: none;

        }

        #comments-container .main {
            min-height: 5px;
        }

        .wrapper .actions {
            position: static !important;
            right: 0px;
        }
    </style>
</head>

<body id="mainbody">
<div id="container" class="container-fluid skin-3">
    <!-- Add Task in sidebar list modal -->
@include('layouts.staff.task_modal')
<!--./ Add Task in sidebar list modal -->

    <!-- Add Contact in sidebar list modal -->
@include('layouts.staff.contact_modal')
<!--./ Add Contact in sidebar list modal -->

    <!-- Header -->
@include('layouts.staff.header')
<!-- /end Header -->

    <!-- sidebar menu -->
@include('layouts.staff.sidebar')
<!-- /end #sidebar -->

    <!-- main content  -->
    <div id="main" class="main">

        <!--START COMMENTS-->

        <script type="text/javascript">
            $(document).ready(function () {

                $(function () {
                    var saveComment = function (data) {
                        //get all users info in relation to comments
                        var usersURL = "{{route('get_users')}}";
                        var v_token = "{{csrf_token()}}";
                        var userArray = function () {
                            var tmp = null;

                            $.ajax({
                                'async': false,
                                'type': "POST",
                                'global': false,
                                'dataType': 'json',
                                'url': usersURL,
                                'data': {'_token': v_token},
                                'success': function (data) {
                                    tmp = data;
                                }
                            });

                            return tmp;
                        }();

                        // Convert pings to human readable format
                        $(data.pings).each(function (index, id) {
                            var user = userArray.filter(function (user) {
                                return user.id == id
                            })[0];
                            data.content = data.content.replace('@' + id, '@' + user.fullname);
                        });

                        return data;

                    }


                    $('#comments-container').comments({

                        <?php if (is_file(asset('uploads/images') . '/' . Auth::user()->photo)) {
                            $chat_image = asset('uploads/images') . '/' . Auth::user()->photo;

                        } else {

                            $chat_image = asset('uploads/images/default.png');//site_url('public/img/avatar.jpg');
                        }
                            ?>

                        profilePictureURL: "{{$chat_image}}",
                        currentUserId: "<?=Auth::user()->id?>",
                        roundProfilePictures: true,
                        enablePinging: true,
                        enableAttachments: false,
                        enableReplying: false,
                        enableUpvoting: false,
                        getComments: function (success, error) {
                            var v_token = "{{csrf_token()}}";
                            $.post("{{route('get_comments')}}", {
                                    "course_id": "{{$course->id}}",
                                    "cid": "{{$course->id}}",
                                    '_token': v_token,
                                    'user_id':{{Auth::user()->id}},
                                    'company_id': 0
                                },
                                function (data) {
                                    // console.log(data);

                                    success(data);
                                }, "json");

                        },
                        postComment: function (data, success, error) {
                            var v_token = "{{csrf_token()}}";


                            var ping_data = data['content']; //capture raw data before it is treated
                            var url = "{{route('post_comment')}}";
                            var post = saveComment(data);

                            setTimeout(function () {

                                $.post(url,
                                    {
                                        'cid': "{{$course->id}}",
                                        'content': post.content,
                                        'content_pings': ping_data,
                                        'parent': data['parent'],
                                        'jqcid': data['id'],
                                        'parent_jqcid': data['parent'],
                                        'raw_created': data['created'],
                                        'user_id':{{Auth::user()->id}},
                                        '_token': v_token,
                                        'company_id': 0
                                    },

                                    success(post)
                                );

                            }, 500);


                        },

                        getUsers: function (success, error) {
                            var v_token = "{{csrf_token()}}";
                            var usersURL = "{{route('get_users')}}";
                            $.post(usersURL, {'_token': v_token}, function (userArray) {
                                // console.log(userArray);
                                success(userArray)
                            }, "json");

                        },
                        putComment: function (data, success, error) {
                            var v_token = "{{csrf_token()}}";

                            var url = "{{route('put_comment')}}";


                            var ping_data = data['content']; //capture raw data before it is treated

                            var put = saveComment(data);
                            var comment_id = data['id']
                            setTimeout(function () {
                                $.post(url,
                                    {
                                        'course_id': "{{$course->id}}",
                                        'comment_id': comment_id,
                                        'content': put.content,
                                        'content_pings': ping_data,
                                        'parent': data['parent'],
                                        'jqcid': data['id'],
                                        'parent_jqcid': data['parent'],
                                        'raw_created': data['created'],
                                        'user_id':{{Auth::user()->id}},
                                        '_token': v_token,
                                        'company_id': 0
                                    },

                                    success(put)
                                );
                            }, 500);
                        },
                        deleteComment: function (data, success, error) {
                            var comment_id = data['id'];

                            var delUrl = "{{route('del_comment')}}";

                            var v_token = "{{csrf_token()}}";

                            setTimeout(function () {
                                $.post(delUrl, {
                                        'course_id': "{{$course->id}}", 'comment_id': comment_id,
                                        '_token': v_token, 'jqcid': data['id'],
                                        'user_id':{{Auth::user()->id}}, 'company_id': 0
                                    },
                                    function (data) {
                                        success()
                                    }, "json");
                                success();
                            }, 500);
                        }


                    });

                });

//ready
            });
        </script>


        <div class="row">
            <!-- breadcrumb section -->
            <div class="ribbon">
                <ul class="breadcrumb" style="background: #004d3e">
                {{-- <li>
                    <i class="fa fa-home"></i>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Library</a>
                </li>
                <li>
                    <a href="#">Data</a>
                </li>
            </ul> --}}
            </div>

            <!-- main content -->
            <div id="content">
                <div id="sortable-panel" class="ui-sortable">
                    @include('flash')

                    <div id="titr-content" class="col-md-12">
                        <h2>Course Name : {{ $course->course_name }}</h2>
                    </div>
                    @if(count($course->modules) != 0 )
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- Modules -->
                                    <div class="panel-group yep-accordion" id="accordion">
                                        @foreach($course->modules as $mod)
                                            <div class="panel panel-default">
                                                <div class="panel-heading collapsed" data-toggle="collapse"
                                                     data-parent="#accordion" data-target="#{{ $mod->id }}"
                                                     aria-expanded="false">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle">
                                                            <i class="indicator fa fa-lg pull-right fa-angle-down"></i>
                                                            {{ $mod->name }}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="{{ $mod->id }}" class="panel-collapse collapse"
                                                     aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body panel-body-cus">
                                                        <ul class="list-group">
                                                            @php $item = 1; @endphp
                                                            @foreach($mod->topics as $topic)
                                                                @php $firstTopic = $topic->material; @endphp

                                                                @if(! is_null($topic->scrum_url))
                                                                    {{-- <a href="#">Play Scum</a>--}}

                                                                    <li class="list-group-item">
                                                                        <a class="list-group-link" target="_blank"
                                                                           href="{{ route("scrum_content", [$course->id, $topic->id]) }}">
                                                                <span class=" pull-left">{{ $item++ }} &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                            <span class=" pull-right">{{ strtolower($topic->format) }}</span>

                                                                            {{ $topic->name }} : Scrom content

                                                                        </a>
                                                                    </li>
                                                                @endif



                                                                @if(is_null($topic->scrum_url))
                                                                    <li class="list-group-item"
                                                                        onclick="clickMe('{{ $topic->id }}')">
                                                                        <a class="list-group-link">
                                                                <span class=" pull-left">{{ $item++ }} &nbsp;
                                                                    <i class=" "></i>
                                                                </span>
                                                                            <span class=" pull-right">{{ strtolower($topic->format) }}</span>

                                                                            {{ $topic->name }}

                                                                        </a>
                                                                    </li>
                                                                @endif

                                                            @endforeach

                                                            @if(empty($mod->topics) || count($mod->topics) == 0 )
                                                                <center><h3>No Lesson For this Session Yet</h3></center>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                @php
                                    if(empty($firstTopic)){$firstTopic='';}
                                @endphp

                                <div class="col-md-8">
                                    <div class="panel panel-default">
                                        @if(is_null($topic->scrum_url))
                                        <div class="panel-body" id="crsVideo">
                                            <iframe width="100%" height="490" src="{{ $firstTopic }}" frameborder="0"
                                                    allow="autoplay; encrypted-media"
                                                    allowfullscreen></iframe>
                                        </div>
                                        @endif

                                        @if(! is_null($topic->scrum_url))
                                            <h1>click on the drop down arrow, then click on the video</h1>
                                            @endif
                                        <!-- start chat -->
                                        <div class="panel-body">


                                            {{--  {{ csrf_field() }}
                                          <span data-id="{{$cid}}"></span>
                                          <div class="form-group">
                                         <div class="col-md-12">
                                             <label for="about" class=" control-label" >Comments</label>

                                          <ul id="chat_post">
                                              @foreach($chatPost as $post)
                                              <li> {{$post->mess}}</li>
                                              @endforeach
                                              <li id="new_post"> </li>
                                              </ul>
                                              </div>
                                              </div>
                                              <div class="form-group">
                                          <div class="col-md-6">
                                              <textarea class="form-control" rows="2" cols="10" name="chat" id="chat_mess"></textarea>
                                          </div>
                                          </div>
                                          <div class="form-group">
                                          <div class="col-md-6 ">
                                              <button class="btn btn-primary" id="chat_btn"> Submit </button>
                                          </div>
                                          </div>--}}


                                        </div>

                                    </div>
                                    <div id="comments-container"></div>
                                    <!-- end chat -->
                                </div>
                            </div>

                        </div>
                    @endif

                    @if(count($course->modules) == 0)
                        <center><h1> No Content for this Course</h1></center>
                    @endif

                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end #content -->
        </div>
        <!-- end .row -->
    </div>
    <!-- ./end #main  -->

    <!-- footer -->
@include('layouts.staff.footer')
<!-- /footer -->
</div>
<!-- end #container -->


<!-- General JS script library-->
<script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js') }}"></script>


<!-- Yeptemplate JS Script -->
<!-- Please use *.min.js in production -->
<script type="text/javascript" src="{{ asset('admin/js/yep-script.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/yep-demo.js') }}"></script>


<!-- Related JavaScript Library to This Pagee -->
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js') }}"></script>

{{-- Video Jquery --}}

<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.textcomplete/1.8.0/jquery.textcomplete.js"></script>

<script type="text/javascript" src="{{ asset('jcomment/js/jquery-comments.js')}}"></script>

<script type="text/javascript">

    function clickMe(id) {
        var id = id;
        var uid =<?php echo Auth::id();?>;
        $.ajax({
            type: 'GET',
            url: '{{ route('get_topic_video') }}',
            data: {'topicId': id, 'userId': uid},
            success: function (data) {
                //alert(data);
                $('#crsVideo').html(data);
                // alert(data);
            },
            error: function (data) {
                alert('BAD');
            },

        });

    }

    //     $(document).ready(function() {
    //         //option A
    //         $("#chat_mess").keyup(function(){
    //                     var chat_mess= $.trim($("#chat_mess").val()); 
    //                     $('#new_post').html(chat_mess);
    //                 });

    //             $('#comments-container').comments({
    //             profilePictureURL: 'https://app.viima.com/static/media/user_profiles/user-icon.png',
    //             currentUserId: 1,
    // 	roundProfilePictures: true,
    // 	textareaRows: 1,
    // 	getUsers: function(success, error) {
    // 		setTimeout(function() {
    // 			success(usersArray);
    // 		}, 500);
    // 	},
    // 	getComments: function(success, error) {
    // 	    var commentsArray = [{
    //                     id: 1,
    //                     created: '2015-10-01',
    //                     content: 'Lorem ipsum dolort sit amet',
    //                     fullname: 'Simon Powell',
    //                     upvote_count: 2,
    //                     user_has_upvoted: false
    //                 }];
    //                       $.ajax({
    //                     type : 'GET',
    //                     url : '{{ route('get_chat') }}',
    //                     data : {'cid': cid,'mess':chat_mess},
    //                     success : function(data) {
    //                         $('#chat_post').html(data);

    //                     },
    //                     error : function(data) {
    //                         // alert(data);
    //                     },

    //                 })

    // 		setTimeout(function() {
    // 			success(commentsArray);
    // 		}, 500);
    // 	}

    //         });
    //         $("#chat_btn").click(function(e){
    //             e.preventDefault(e);
    //                 var cid = <?php echo $cid;?>;
    //                 var chat_mess= $.trim($("#chat_mess").val()); 
    //                     $('#new_post').html(chat_mess);
    //                     $("#chat_mess").val('')
    //                         $.ajax({
    //                     type : 'GET',
    //                     url : '{{ route('get_chat') }}',
    //                     data : {'cid': cid,'mess':chat_mess},
    //                     success : function(data) {
    //                         $('#chat_post').html(data);

    //                     },
    //                     error : function(data) {
    //                         // alert(data);
    //                     },

    //                 })
    //         });
    //     });


    /*$('#csrTopic').click(function() {
        var topicId = $(this).data('tid');
            alert('OLOTU');
    });*/

</script>


<!-- Plugin-Scripts -->
<script type="text/javascript">

    // Default Data Table Script
    $('#example1').dataTable({
        responsive: true
    });

    // Default Data Table Script
    $('#request').dataTable({
        responsive: true
    });
    // Default Data Table Script
    $('#video-logs').dataTable({
        responsive: true
    });

    // Default Data Table Script
    $('#staff').dataTable({
        responsive: true
    });


    // Default Data Table Script
    $('#instructor').dataTable({
        responsive: true
    });

    // Table Tools Data Tables
    $('#example2').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "Yeptemplate_PDF",
                    "sPdfMessage": "Yeptemplate PDF Export",
                    "sPdfSize": "letter"
                },
                {
                    "sExtends": "print",
                    "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
                }
            ],

        },
        "autoWidth": true,
        responsive: true

    });

    // Column show & hide
    $('#example3').DataTable({
        "dom": '<"pull-left"f><"pull-right"l>tip',
        "dom": 'C<"clear">lfrtip',

        "bLengthChange": false,
        responsive: true
    });


</script>

<!-- Google Analytics Script -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-67070957-1', 'auto');
    ga('send', 'pageview');
</script>

</body>

</html>
