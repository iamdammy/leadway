<!DOCTYPE html>
<html>

<head>
    <title>{{ env("APP_NAME") }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/font-awesome/css/font-awesome.min.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">

    <!-- Yeptemplate css -->
    <!-- Please use *.min.css in production -->
    <link rel="stylesheet" href="{{ asset('admin/css/yep-style.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/yep-vendors.css') }}">

    <!-- Datatables -->
    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.responsive.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.colVis.min.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.responsive.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.colVis.min.css') }}">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
</head>

<body id="mainbody">
    <div id="container" class="container-fluid skin-3">
        <!-- Add Task in sidebar list modal -->
       @include('layouts.staff.task_modal')
        
        <!--./ Add Task in sidebar list modal -->

        <!-- Add Contact in sidebar list modal -->
       @include('layouts.staff.contact_modal')
       
        <!--./ Add Contact in sidebar list modal -->

        <!-- Header -->
       @include('layouts.staff.header')
       
        <!-- /end Header -->

        <!-- sidebar menu -->
       @include('layouts.staff.sidebar')
        
        <!-- /end #sidebar -->

        <!-- main content  -->
        <div id="main" class="main">
            <div class="row">
                <!-- breadcrumb section -->
                <div class="ribbon">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="#">Library</a>
                        </li>
                        <li>
                            <a href="#">Data</a>
                        </li>
                    </ul>
                </div>

                <!-- main content -->
                <div id="content">
                    <div id="sortable-panel" class="ui-sortable">
                        <!-- Course Table -->
                        <!-- Admin over view .col-md-12 -->
                        <div class="col-md-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <form class="form-horizontal" method="">
                                    
                                        <div class="row">
                                            <div class="col-md-3 col-sm-5  col-xs-5" style="float: right">
                                    
                                                <img src="{{ asset('uploads/images/'.$course->course_img) }}" alt="" style="width:160px;height:100%;margin:5px" class="img-rectangle img-responsive">
                                    
                                    
                                            </div>
                                            <div class="col-md-3">
                                                <a href="{{route('staff_assigned_crs')}}"><button class="btn btn-primary">Back to Course</button></a>
                                                {{-- <h1>hhh</h1> --}}
                                            </div>
                                            <div class="col-md-6 col-sm-7  col-xs-7" style="float: left">
                                                <div class="form-group">
                                                    <label for="" class="col-md-4 control-label"> Course Name</label>
                                                    <div class="col-md-8">
                                    
                                                        <div class="">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
                                                                    <i class="fa fa-user"></i>
                                                                </span>
                                                                <input type="text" class="form-control" id="" name="firstname" value="{{ $course->course_name }}" disabled="disabled" style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
                                                            </div>
                                                        </div>
                                    
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <div class="form-group">
                                                        <label for="" class="col-md-4 control-label">Category</label>
                                                        <div class="col-md-8">
                                                            <div class="">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon" style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
                                                                        <i class="fa fa-th-list"></i>
                                                                    </span>
                                                                    <input type="" class="form-control" id="" name="" value="{{ $course->course_cat}}" disabled="" style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                
                                    
                                            </div>
                                    
                                        </div>
                                        <br>
                                    
                                        <br>
                                    </form>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    {{$staff->links()}}
                                    <table id="example3" class="table table-striped table-bordered width-100 cellspace-0">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Department</th>
                                                <th>Email</th>
                                                <th>Score</th>
                                                <th>Video Watched</th>
                                            </tr>
                                        </thead>
                                    
                                        <tbody>
                                            @if(!is_null($staff) && !empty($staff))
                                                @php $i = 1; $pre_user=''; @endphp
                                                @foreach($staff as $user)
                                                @if($pre_user!="")
                                                @if($pre_user->email==$user->email)
                                                    @continue
                                                    @endif
                                                @endif
                                                @php

                                                 @endphp
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $user->firstName.' '. $user->lastName }}</td>
                                                    <td>{{ $user->department }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ $user->score }}</td>
                                                    <td>{{ $user->video_watched() }}</td>
                                                    {{-- <td>
                                                        <p>23 / 60</p>
                                                        <p>80 / 100</p>
                                                    </td>
                                                    <td>
                                                        <div class="progress progress-sm">
                                                            <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 60%"></div>
                                                        </div>
                                                    </td>
                                                    <td>65%</td> --}}
                                                </tr>
                                                @php $i++;$pre_user=$user; @endphp
                                                @endforeach
                                                
                                            @endif
                                        </tbody>
                                    </table>
                                    {{$staff->links()}}
                                    @if(empty($staff) || is_null($staff))
                                            <h1 style="color: red;">NO RECORD YET</h1>
                                        @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end col-md-12 -->
                </div>
                <!-- end #content -->
            </div>
            <!-- end .row -->
        </div>
        <!-- ./end #main  -->

        <!-- footer -->
       @include('layouts.staff.footer')
        
        <!-- /footer -->
    </div>
    <!-- end #container -->

    <!-- General JS script library-->
    <script type="text/javascript" src="../assets/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>


    <!-- Yeptemplate JS Script -->
    <!-- Please use *.min.js in production -->
    <script type="text/javascript" src="../assets/js/yep-script.js"></script>
    <script type="text/javascript" src="../assets/js/yep-vendors.js"></script>
    <script type="text/javascript" src="../assets/js/yep-demo.js"></script>


    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>


    <!-- Plugin-Scripts -->
    <script type="text/javascript">

        // Default Data Table Script
        $('#example1').dataTable({
            responsive: true
        });

        // Default Data Table Script
        $('#request').dataTable({
            responsive: true
        });
        // Default Data Table Script
        $('#video-logs').dataTable({
            responsive: true
        });

        // Default Data Table Script
        $('#staff').dataTable({
            responsive: true
        });


        // Default Data Table Script
        $('#instructor').dataTable({
            responsive: true
        });

        // Table Tools Data Tables
        $('#example2').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "Yeptemplate_PDF",
                        "sPdfMessage": "Yeptemplate PDF Export",
                        "sPdfSize": "letter"
                    },
                    {
                        "sExtends": "print",
                        "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
                    }
                ],

            },
            "autoWidth": true,
            responsive: true

        });

        // Column show & hide
        $('#example3').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            "dom": 'C<"clear">lfrtip',

            "bLengthChange": false,
            responsive: true
        });


    </script>

    <!-- Google Analytics Script -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67070957-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>

</html>