@extends('layouts.staff.app')
@section('content')
    <div id="main" class="main">
        <div class="row">
            <!-- breadcrumb section -->
            <div class="ribbon">
                <ul class="">

                </ul>
            </div>

            <!-- main content -->
            <div id="content">
                <div id="sortable-panel" class="ui-sortable">

                    <!-- main content -->
                    @include('flash')
                    <div id="content" class="col-md-12">
                        <div id="sortable-panel" class="">
                            <div id="titr-content" class="col-md-12">
                                <h2>Dashboard</h2>
                                @if($user->password_changed==0)
                                    <a href="{{route('edit_profile')}}">
                                        <div class="alert alert-danger">
                                            <strong>Change Default Password</strong>
                                        </div>
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default dash-bod-d">
                                    <div class="panel-body dash-bod-c">
                                        <div class="row">
                                            <a href="{{route('staff_assigned_crs')}}">
                                                <div class="col-sm-3">
                                                    <div class="card-body card-body-dash dash-color4">
                                                        <div class="media">
                                                            <div class="icon-dash">
                                                                <img src="{{ asset('uploads/lms-img/instructor-lecture-with-sceen-projection-tool.png')}}"
                                                                     class="img-responsive dash-img" alt="Image">
                                                            </div>
                                                            <div class="media-body white">
                                                                <p class="no-margin">Assigned Courses</p>
                                                                <h5 class="media-head">{{$total_assigned}}</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="col-sm-3">
                                                <div class="card-body card-body-dash dash-color3">
                                                    <div class="media">
                                                        <div class="icon-dash">
                                                            <img src="{{ asset('uploads/lms-img/online-course.png')}}"
                                                                 class="img-responsive dash-img" alt="Image">
                                                        </div>
                                                        <div class="media-body white">
                                                            <p class="no-margin">Quizes</p>
                                                            <h5 class="media-head">0</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="{{route('staff_req_crs')}}">
                                                <div class="col-sm-3">
                                                    <div class="card-body card-body-dash dash-color2">
                                                        <div class="media">
                                                            <div class="icon-dash">
                                                                <img src="{{ asset('uploads/lms-img/online-course.png')}}"
                                                                     class="img-responsive dash-img" alt="Image">
                                                            </div>
                                                            <div class="media-body white">
                                                                <p class="no-margin">Requested Courses</p>
                                                                <h5 class="media-head">{{$total_req}}</h5></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="col-sm-3">
                                                    <div class="card-body card-body-dash dash-color1">
                                                        <div class="media">
                                                            <div class="icon-dash">
                                                                <img src="{{ asset('uploads/lms-img/profile.png')}}"
                                                                     class="img-responsive dash-img" alt="Image">
                                                            </div>
                                                            <div class="media-body white">
                                                                <p class="no-margin">My Profile</p>
                                                                <h5 class="media-head"></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Courses Taking -->
                            <hr>
                            <div id="titr-content" class="col-md-12">
                                <h2>Assigned Courses</h2>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    @php $i=0; @endphp
                                    @foreach($user->courses as $course)
                                        @php $i=$i+1;  @endphp

                                        <div class="col-md-3">
                                            <div class="panel panel-default">
                                                <div class="course-container">
                                                    @if($course->courseName($course->course_id)!="")
                                                        <img src="{{ asset('uploads/images/'.$course->courseImg($course->course_id)) }}"
                                                             alt="Avatar" class="image img-responsive">
                                                    @else
                                                        <img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar"
                                                             class="image img-responsive">
                                                    @endif
                                                    <div class="overlay">
                                                        <div class="text"><span
                                                                    class="fa fa-graduation-cap hover-cap"></span></div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <legend class="staff-legend">
                                                        @php $course_trim=$course->courseName($course->course_id); @endphp
                                                        {{ str_limit($course_trim, $limit = 20, $end = '...') }}
                                                    </legend>
                                                    <p>Session 1
                                                        of {{ count($course->courseModules($course->course_id)) }} <span
                                                                class="pull-right">{{ $user->singleUserCrsStatus($user->id, $course->course_id) }}
                                                            %</span></p>
                                                    <div class="progress progress-sm">
                                                        <div class="progress-bar progress-bar-warning"
                                                             role="progressbar"
                                                             style="width: {{ $user->singleUserCrsStatus($user->id, $course->course_id) }}%"></div>
                                                    </div>
                                                    <a href="{{ route('lead_bd', ['id'=>$course->course_id]) }}"
                                                       class="btn btn-primary btn-sm dammy">Leaderboard</a>
                                                    <a href="{{ route('tak_crs', ['id'=>$course->course_id]) }}"
                                                       class="btn btn-default btn-sm has-ripple">Go to Course
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        @if($i==4) @break @endif
                                    @endforeach


                                </div>

                                <div class="col-md-12"
                                     style="margin-bottom: 40px!important; margin-top: 40px!important;">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <a href="{{ route("completed_courses") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">Completed Courses</a>
                                        </div>

                                        <div class="col-md-3">
                                            <a href="{{ route("requested_courses") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">Requested Courses</a>

                                        </div>

                                        <div class="col-md-3">
                                            <a href="{{ route("all_available_crs") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">All Courses</a>

                                        </div>

                                        @if(count($user->courses) != 0 )

                                            <div class="col-md-3">
                                                <a style="background-color: #004d3e!important;" href="{{route('staff_assigned_crs')}}" class="btn btn-primary ">
                                                    View All Assigned Courses
                                                </a>
                                            </div>
                                        @endif


                                    </div>
                                </div>
                                <br><br>


                            </div>


                            <hr>


                            <div class="row">

                            </div>

                            @if(count($user->courses) == 0 )
                                <center><h1 style="color:red;"><strong>No Course Assigned Yet </strong></h1>
                                    <strong><a href="{{ route('staff_req_crs') }}">CLICK HERE TO REQUEST
                                            NOW</a></strong>
                                </center>

                            @endif


                        </div>

                        <!-- end col-md-12 -->
                    </div>

                    <!-- end #content -->


                </div><!-- end col-md-12 -->
            </div><!-- end #content -->
        </div><!-- end .row -->
    </div>
@stop