<!DOCTYPE html>
<html>

<head>
    <title>{{ env("APP_NAME") }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('admin/vendo/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/font-awesome/css/font-awesome.min.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendo/dropzone/css/basic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/dropzone/css/dropzone.min.css') }}">


    <!-- Yeptemplate css -->
    <!-- Please use *.min.css in production -->
    <link rel="stylesheet" href="{{ asset('admin/css/yep-style.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/yep-vendors.css') }}">

    <!-- Related css to this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.responsive.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendo/jquery-datatables/css/dataTables.colVis.min.css') }}">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
</head>

<body id="mainbody">
<div id="container" class="container-fluid skin-3">
    <!-- Add Task in sidebar list modal -->
@include('layouts.staff.task_modal')
<!--./ Add Task in sidebar list modal -->

    <!-- Add Contact in sidebar list modal -->
@include('layouts.staff.contact_modal')
<!--./ Add Contact in sidebar list modal -->

    <!-- Header -->
@include('layouts.staff.header')
<!-- /end Header -->

    <!-- sidebar menu -->
@include('layouts.staff.sidebar')
<!-- /end #sidebar -->

    <!-- main content  -->
    <div id="main" class="main">
        <div class="row">
            <!-- breadcrumb section -->
            <div class="ribbon">
                <ul class="">
                    {{-- <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">Library</a>
                    </li>
                    <li>
                        <a href="#">Data</a>
                    </li> --}}
                </ul>
            </div>

            <!-- main content -->
            <div id="content">
                <div id="sortable-panel" class="ui-sortable">
                    @include('flash')
                    <div id="titr-content" class="col-md-12">
                        <h2>Update Information</h2>
                    </div>
                    <!-- Course Table -->
                    <!-- Admin over view .col-md-12 -->
                    <div class="col-md-8">

                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img width="70px" height="70px" alt="Profile Picyure"
                                             src="{{ asset('uploads/images/'.$user->photo) }}" class="img-circle">
                                    </div>
                                    <div class="col-md-12">
                                        <form method="POST" action="{{ route('edit_pro', ['id'=>$user->id]) }}"
                                              accept-charset="UTF-8" class="form-horizontal"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}


                                            <div class="form-group">
                                                <label for="name" class="col-md-4 control-label">Firstname</label>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="Name" name="firstName"
                                                           type="text" value="{{ $user->firstName }}" id="name" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-md-4 control-label">Lastname</label>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="Name" name="lastName"
                                                           type="text" value="{{ $user->lastName }}" id="name" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-md-4 control-label">Email</label>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="Name" name="email"
                                                           type="text" value="{{ $user->email }}" id="name" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="phone" class="col-md-4 control-label">Mobile Number</label>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="" name="phone" type="text"
                                                           value="{{ $user->phone ?? '+234....' }}" id="phone" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-md-4 control-label">Department</label>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="Name" name="department"
                                                           type="text" value="{{ $user->department }}" id="name"
                                                           readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="level" class="col-md-4 control-label">Level</label>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="" name="level" type="text"
                                                           value="{{ $user->level ?? 'Not Set'}}" id="level" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="dob" class="col-md-4 control-label">Employment Date</label>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="" name="dob" type="date"
                                                           value="{{ $user->dob ?? 'NILL' }}" id="dob" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" for="form-field-1">
                                                    Change Password
                                                </label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="password"
                                                           placeholder="Change User Password" id="form-field-1" value=""
                                                           class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" for="form-field-21">
                                                    Photo
                                                </label>
                                                <div class="col-sm-4">
                                                    <input type="file" name="photo" id="form-file-20"
                                                           class="form-control yep-file-input">

                                                </div>
                                            </div>


                                            {{-- <div class="form-group">
                                                <label for="about" class="col-md-4 control-label">About yourself</label>
                                                <div class="col-md-6">
                                                    <textarea class="form-control" rows="2" cols="10" name="about" id="about">fff</textarea>
                                                </div>
                                            </div> --}}


                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    {{-- <a class="btn btn-danger">Update</a> --}}
                                                    <input class="btn btn-primary" type="submit" value="Update">
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end #content -->
        </div>
        <!-- end .row -->
    </div>
    <!-- ./end #main  -->

    <!-- footer -->
@include('layouts.staff.footer')
<!-- /footer -->
</div>
<!-- end #container -->

<!-- General JS script library-->
<script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js') }}"></script>


<!-- Yeptemplate JS Script -->
<!-- Please use *.min.js in production -->
<script type="text/javascript" src="{{ asset('admin/js/yep-script.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/yep-demo.js') }}"></script>


<!-- Related JavaScript Library to This Pagee -->
<script type="text/javascript" src="{{ asset('admin/vendo/dropzone/js/dropzone.min.js') }}"></script>


<!-- Related JavaScript Library to This Pagee -->
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js') }}"></script>


<!-- Plugins Script -->

<script type="text/javascript">

    // Default Data Table Script
    $('#example1').dataTable({
        responsive: true
    });

    // Default Data Table Script
    $('#request').dataTable({
        responsive: true
    });
    // Default Data Table Script
    $('#video-logs').dataTable({
        responsive: true
    });

    // Default Data Table Script
    $('#staff').dataTable({
        responsive: true
    });


    // Default Data Table Script
    $('#instructor').dataTable({
        responsive: true
    });

    // Table Tools Data Tables
    $('#example2').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "Yeptemplate_PDF",
                    "sPdfMessage": "Yeptemplate PDF Export",
                    "sPdfSize": "letter"
                },
                {
                    "sExtends": "print",
                    "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
                }
            ],

        },
        "autoWidth": true,
        responsive: true

    });

    // Column show & hide
    $('#example3').DataTable({
        "dom": '<"pull-left"f><"pull-right"l>tip',
        "dom": 'C<"clear">lfrtip',

        "bLengthChange": false,
        responsive: true
    });


</script>

<!-- Google Analytics Script -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-67070957-1', 'auto');
    ga('send', 'pageview');
</script>

</body>

</html>