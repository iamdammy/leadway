@extends('layouts.staff.app')
@section('content')
    <div id="main" class="main">
        <div class="row">
            <!-- breadcrumb section -->
            <div class="ribbon">
                <ul class="">
                    {{-- <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">Library</a>
                    </li>
                    <li>
                        <a href="#">Data</a>
                    </li> --}}
                </ul>
            </div>

            <!-- main content -->
            <div id="content">
                <div id="sortable-panel" class="ui-sortable">
                    <!-- Course Table -->
                    <!-- Admin over view .col-md-12 -->

                    <!-- main content -->
                    @include('flash')
                    <div id="content" class="col-md-12">
                        <div id="sortable-panel" class="">



                            <!-- Courses Taking -->
                            <hr>
                            <div id="titr-content" class="col-md-12">
                                <h2>Completed Courses</h2>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    @if(count($completed) <= 0 )
                                        <h3 style="color: red; align-items: center;">No Course Completed</h3>
                                    @endif
                                    @php $i=0; @endphp
                                        @foreach($completed as $course)
                                            @php $i=$i+1;  @endphp

                                            <div class="col-md-3">
                                                <div class="panel panel-default">
                                                    <div class="course-container">
                                                        @if($course->course->course_name)
                                                            <img src="{{ asset('uploads/images/'.$course->course->course_img) }}"
                                                                 alt="Avatar" class="image img-responsive">
                                                        @else
                                                            <img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar"
                                                                 class="image img-responsive">
                                                        @endif
                                                        <div class="overlay">
                                                            <div class="text"><span
                                                                        class="fa fa-graduation-cap hover-cap"></span></div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-body">
                                                        <legend class="staff-legend">
                                                            {{ $course->course->course_name }}
                                                            @php $course_trim=$course->course->course_namw; @endphp
                                                            {{ str_limit($course_trim, $limit = 20, $end = '...') }}
                                                        </legend>
                                                        <a target="_blank" href="{{ route("down_cert", $course->course->id) }}" class="btn btn-primary dammy">Download Certificate</a>


                                                    </div>
                                                </div>
                                            </div>
                                            @if($i==4) @break @endif
                                        @endforeach


                                </div>
                                <div class="col-md-12"
                                     style="margin-bottom: 40px!important; margin-top: 40px!important;">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <a href="{{ route("completed_courses") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">Completed Courses</a>
                                        </div>

                                        <div class="col-md-3">
                                            <a href="{{ route("requested_courses") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">Requested Courses</a>

                                        </div>

                                        <div class="col-md-3">
                                            <a href="{{ route("all_available_crs") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">All Courses</a>

                                        </div>

                                        @if(count($user->courses) != 0 )

                                            <div class="col-md-3">
                                                <a style="background-color: #004d3e!important;" href="{{route('staff_assigned_crs')}}" class="btn btn-primary ">
                                                    View All Assigned Courses
                                                </a>
                                            </div>
                                        @endif

                                        {{--<div class="col-md-3">
                                            <button>TEST</button>

                                        </div>--}}

                                    </div>
                                </div>
                                <br><br>

                            </div>


                            <hr>

                            {{--<div id="titr-content" class="col-md-12">
                                <h2>Completed Courses</h2>
                            </div>--}}




                            <div class="row">

                            </div>


                            @if(count($user->courses) == 0 )
                                <center><h1 style="color:red;"><strong>No Course Assigned Yet </strong></h1>
                                    <strong><a href="{{ route('staff_req_crs') }}">CLICK HERE TO REQUEST
                                            NOW</a></strong>
                                </center>

                            @endif


                        </div>

                        <!-- end col-md-12 -->
                    </div>

                    <!-- end #content -->


                </div><!-- end col-md-12 -->
            </div><!-- end #content -->
        </div><!-- end .row -->
    </div>
@stop