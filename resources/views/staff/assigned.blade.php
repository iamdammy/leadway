@extends('layouts.staff.app')
@section('content')
<div id="main" class="main">
				<div class="row">
					<!-- breadcrumb section -->
					<div class="ribbon">
						<ul class="breadcrumb">
							{{-- <li>
								<i class="fa fa-home"></i>
								<a href="#">Home</a>
							</li>
							<li>
								<a href="#">Library</a>
							</li>
							<li>
								<a href="#">Data</a>
							</li> --}}
						</ul>
					</div>
					
					<!-- main content -->
					<div id="content">
						<div id="sortable-panel" class="ui-sortable">
							<!-- Course Table -->
							<!-- Admin over view .col-md-12 -->
							
				<!-- main content -->
				<div id="content" class="col-md-12">
					
						
								<!-- Courses Taking -->
								<hr>
								@include('flash')
								<div id="titr-content" class="col-md-12">
        							<h2>Assigned Courses</h2>
        						</div>
        						<div class="col-md-12">
        						    <div class="row">
										@foreach($user->courses as $course)
									@php 
									$courseDuration=$course->courseDuration($course->course_id);
									$timeLeft='';						
									@endphp
									@if($courseDuration !=null || $courseDuration!="")	
									@php $timeLeft=$courseDuration-$course->timeUsed(); @endphp								
										@if($timeLeft<=0)
										@continue
										@endif
									@endif
									<div class="col-md-3">
										<div class="panel panel-default">
											<div class="course-container">
												@if($course->courseName($course->course_id)!="")
											<img src="{{ asset('uploads/images/'.$course->courseImg($course->course_id)) }}" alt="Avatar" class="image img-responsive">
												@else
													<img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar" class="image img-responsive">
												@endif
												<div class="overlay">
													<div class="text"><span class="fa fa-graduation-cap hover-cap"></span></div>
												</div>
											</div>
											<div class="panel-body">
											{{--	<legend class="staff-legend">
													{{ $course->courseName($course->course_id) }}
												</legend>--}}
												<legend class="staff-legend">
												@php $course_trim=$course->courseName($course->course_id); @endphp
													{{ str_limit($course_trim, $limit = 20, $end = '...') }}
												</legend>
												<legend class="staff-legend"  style="text-transform: none;">
													{{ $timeLeft ?? 'Null' }} Day(s) left
												</legend>
												<p>Session 1 of {{ count($course->courseModules($course->course_id)) }} <span class="pull-right">{{ $user->singleUserCrsStatus($user->id, $course->course_id) }}%</span></p>
												<div class="progress progress-sm">
													<div class="progress-bar progress-bar-warning" role="progressbar" style="width: {{ $user->singleUserCrsStatus($user->id, $course->course_id) }}%"></div>
												</div>
												<a href="{{ route('lead_bd', ['id'=>$course->course_id]) }}" class="btn btn-primary btn-sm">Leaderboard</a>
												<a href="{{ route('tak_crs', ['id'=>$course->course_id]) }}" class="btn btn-default btn-sm has-ripple">Go to Course
												</a>
											</div>
										</div>
									</div>
									@endforeach

									@if(count($user->courses) == 0 )
										<center><h1 style="color:red;"> <strong>No Course Assigned Yet </strong></h1>
											<strong><a href="{{ route('staff_req_crs') }}">CLICK HERE TO REQUEST NOW</a></strong>
										</center>
									@endif

									
								</div>
        						</div>



					<div class="col-md-12"
						 style="margin-bottom: 40px!important; margin-top: 40px!important;">
						<div class="row">

							<div class="col-md-3">
								<a href="{{ route("completed_courses") }}" class="btn btn-primary dammy"
								   style="width: 100%!important;">Completed Courses</a>
							</div>

							<div class="col-md-3">
								<a href="{{ route("requested_courses") }}" class="btn btn-primary dammy"
								   style="width: 100%!important;">Requested Courses</a>

							</div>

							<div class="col-md-3">
								<a href="{{ route("all_available_crs") }}" class="btn btn-primary dammy"
								   style="width: 100%!important;">All Courses</a>

							</div>

							@if(count($user->courses) != 0 )

								<div class="col-md-3">
									<a style="background-color: #004d3e!important;" href="{{route('staff_assigned_crs')}}" class="btn btn-primary ">
										View All Assigned Courses
									</a>
								</div>
							@endif

							{{--<div class="col-md-3">
                                <button>TEST</button>

                            </div>--}}

						</div>
					</div>
					</div>
					<!-- end col-md-12 -->
				</div>
				
						</div><!-- end col-md-12 -->
					</div><!-- end #content -->
				</div><!-- end .row -->
			</div>
@stop