@extends('layouts.staff.app')
@section('content')
    <div id="main" class="main">
        <div class="row">
            <!-- breadcrumb section -->
            <div class="ribbon">
                <ul class="">
                    {{-- <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">Library</a>
                    </li>
                    <li>
                        <a href="#">Data</a>
                    </li> --}}
                </ul>
            </div>

            <!-- main content -->
            <div id="content">
                <div id="sortable-panel" class="ui-sortable">
                    <!-- Course Table -->
                    <!-- Admin over view .col-md-12 -->

                    <!-- main content -->
                    @include('flash')
                    <div id="content" class="col-md-12">
                        <div id="sortable-panel" class="">
                            <div id="titr-content" class="col-md-12">
                                <h2>Dashboard</h2>
                                @if($user->password_changed==0)
                                    <a href="{{route('edit_profile')}}">
                                        <div class="alert alert-danger">
                                            <strong>Change Default Password</strong>
                                        </div>
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default dash-bod-d">
                                    <div class="panel-body dash-bod-c">
                                        <div class="row">
                                            <a href="{{route('staff_assigned_crs')}}">
                                                <div class="col-sm-3">
                                                    <div class="card-body card-body-dash dash-color4">
                                                        <div class="media">
                                                            <div class="icon-dash">
                                                                <img src="{{ asset('uploads/lms-img/instructor-lecture-with-sceen-projection-tool.png')}}"
                                                                     class="img-responsive dash-img" alt="Image">
                                                            </div>
                                                            <div class="media-body white">
                                                                <p class="no-margin">Assigned Courses</p>
                                                                <h5 class="media-head">{{$total_assigned}}</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="col-sm-3">
                                                <div class="card-body card-body-dash dash-color3">
                                                    <div class="media">
                                                        <div class="icon-dash">
                                                            <img src="{{ asset('uploads/lms-img/online-course.png')}}"
                                                                 class="img-responsive dash-img" alt="Image">
                                                        </div>
                                                        <div class="media-body white">
                                                            <p class="no-margin">Quizes</p>
                                                            <h5 class="media-head">0</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="{{route('staff_req_crs')}}">
                                                <div class="col-sm-3">
                                                    <div class="card-body card-body-dash dash-color2">
                                                        <div class="media">
                                                            <div class="icon-dash">
                                                                <img src="{{ asset('uploads/lms-img/online-course.png')}}"
                                                                     class="img-responsive dash-img" alt="Image">
                                                            </div>
                                                            <div class="media-body white">
                                                                <p class="no-margin">Requested Courses</p>
                                                                <h5 class="media-head">{{$total_req}}</h5></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="{{ route("edit_profile") }}">
                                                <div class="col-sm-3">
                                                    <div class="card-body card-body-dash dash-color1">
                                                        <div class="media">
                                                            <div class="icon-dash">
                                                                <img src="{{ asset('uploads/lms-img/profile.png')}}"
                                                                     class="img-responsive dash-img" alt="Image">
                                                            </div>
                                                            <div class="media-body white">
                                                                <p class="no-margin">My Profile</p>
                                                                <h5 class="media-head"></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Courses Taking -->
                            <hr>
                            <div id="titr-content" class="col-md-12">
                                <h2>Assigned Courses</h2>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    @php $i=0; @endphp
                                    @foreach($user->courses as $course)
                                        @php $i=$i+1;  @endphp

                                        <div class="col-md-3">
                                            <div class="panel panel-default">
                                                <div class="course-container">
                                                    @if($course->courseName($course->course_id)!="")
                                                        <img src="{{ asset('uploads/images/'.$course->courseImg($course->course_id)) }}"
                                                             alt="Avatar" class="image img-responsive">
                                                    @else
                                                        <img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar"
                                                             class="image img-responsive">
                                                    @endif
                                                    <div class="overlay">
                                                        <div class="text"><span
                                                                    class="fa fa-graduation-cap hover-cap"></span></div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <h4>DAY(s) LEFT : {{ daysLeft($course->course, $user) }}</h4>
                                                    <legend class="staff-legend">
                                                        @php $course_trim=$course->courseName($course->course_id); @endphp
                                                        {{ str_limit($course_trim, $limit = 20, $end = '...') }}
                                                    </legend>
                                                    <p>Session 1
                                                        of {{ count($course->courseModules($course->course_id)) }} <span
                                                                class="pull-right">{{ $user->singleUserCrsStatus($user->id, $course->course_id) }}
                                                            %</span></p>
                                                    <div class="progress progress-sm">
                                                        <div class="progress-bar progress-bar-warning"
                                                             role="progressbar"
                                                             style="width: {{ $user->singleUserCrsStatus($user->id, $course->course_id) }}%"></div>
                                                    </div>
                                                    <a href="{{ route('lead_bd', ['id'=>$course->course_id]) }}"
                                                       class="btn btn-primary btn-sm dammy">Leaderboard</a>
                                                    <a href="{{ route('tak_crs', ['id'=>$course->course_id]) }}"
                                                       class="btn btn-default btn-sm has-ripple">Go to Course
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        {{--@if($i==4) @break @endif--}}
                                    @endforeach


                                </div>

                                <div class="col-md-12"
                                     style="margin-bottom: 40px!important; margin-top: 40px!important;">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <a href="{{ route("completed_courses") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">Completed Courses</a>
                                        </div>

                                        <div class="col-md-3">
                                            <a href="{{ route("requested_courses") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">Requested Courses</a>

                                        </div>

                                        <div class="col-md-3">
                                            <a href="{{ route("all_available_crs") }}" class="btn btn-primary dammy"
                                               style="width: 100%!important;">All Courses</a>

                                        </div>

                                        @if(count($user->courses) != 0 )

                                            <div class="col-md-3">
                                                <a style="background-color: #004d3e!important;" href="{{route('staff_assigned_crs')}}" class="btn btn-primary ">
                                                    View All Assigned Courses
                                                </a>
                                            </div>
                                        @endif

                                        {{--<div class="col-md-3">
                                            <button>TEST</button>

                                        </div>--}}

                                    </div>
                                </div>


                                <div class="col-md-12">

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <form class="form-horizontal" method="" enctype="">

                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-5  col-xs-5" style="float: right">

                                                                <img src="{{ asset('uploads/images/'.$user->photo) }}" alt=""
                                                                     style="width:160px;height:100%;margin:5px"
                                                                     class="img-rectangle img-responsive">


                                                            </div>
                                                            <div class="col-md-9 col-sm-7  col-xs-7" style="float: left">
                                                                <div class="form-group">
                                                                    <label for="" class="col-md-4 control-label">Full Name</label>
                                                                    <div class="col-md-8">

                                                                        <div class="">
                                                                            <div class="input-group">
																			<span class="input-group-addon"
                                                                                  style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																				<i class="fa fa-user"></i>
																			</span>
                                                                                <input type="text" class="form-control" id=""
                                                                                       name="firstname"
                                                                                       value="{{ $user->fullname() }}"
                                                                                       disabled="disabled"
                                                                                       style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="media-body">
                                                                    <div class="form-group">
                                                                        <label for="" class="col-md-4 control-label">Staff
                                                                            Id</label>
                                                                        <div class="col-md-8">
                                                                            <div class="">
                                                                                <div class="input-group">
																				<span class="input-group-addon"
                                                                                      style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																					<i class="fa fa-th-list"></i>
																				</span>
                                                                                    <input type="" class="form-control" id=""
                                                                                           name=""
                                                                                           value="{{ $user->staff_id ?? 'Not Set' }}"
                                                                                           disabled=""
                                                                                           style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="media-body">
                                                                    <div class="form-group">
                                                                        <label for="" class="col-md-4 control-label">Employment
                                                                            Date</label>
                                                                        <div class="col-md-8">
                                                                            <div class="">
                                                                                <div class="input-group">
																				<span class="input-group-addon"
                                                                                      style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																					<i class="fa fa-th-list"></i>
																				</span>
                                                                                    <input type="" class="form-control" id=""
                                                                                           name=""
                                                                                           value="{{ date('d-m-Y', strtotime($user->dob)  )?? 'Not Set' }}"
                                                                                           disabled=""
                                                                                           style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                        <br>
                                                        <div class="media-body">
                                                            <label for="" class="col-md-4 control-label">Email</label>
                                                            <div class="col-md-8">
                                                                <div class="">
                                                                    <div class="input-group">
																	<span class="input-group-addon"
                                                                          style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																		<i class="fa fa-envelope"></i>
																	</span>
                                                                        <input type="" class="form-control" id="" name=""
                                                                               value="{{ $user->email }}" disabled=""
                                                                               style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="media-body">

                                                            <div class="form-group">
                                                                <label for="" class="col-md-4 control-label">Mobile Number</label>
                                                                <div class="col-md-8">
                                                                    <div class="">
                                                                        <div class="input-group">
																		<span class="input-group-addon"
                                                                              style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">
																			<i class="fa fa-phone"></i>
																		</span>
                                                                            <input type="" class="form-control" id="" name=""
                                                                                   value="{{ $user->phone ?? '+234...' }}"
                                                                                   disabled=""
                                                                                   style="background-color:#fff; border:rgba(255, 255, 255, 0) 1px">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br>

                                                        <br>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- end panel -->
                                        </div>
                                        <div class="col-md-4">

                                            <div class="panel panel-default">
                                                <div class="panel-heading ui-sortable-handle">
                                                    <div class="panel-title">
                                                        News
                                                        <div class="bars pull-right">
                                                            <div class="dropdown ">
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                                <ul class="dropdown-menu yep-panel-menu">
                                                                    <li>
                                                                        <a href="#">Daily
                                                                            <span class="badge pull-right"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Monthly
                                                                            <span class="badge pull-right"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Yearly
                                                                            <span class="badge pull-right"> 42 </span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="divider">
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">All time
                                                                            <span class="badge pull-right">56</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <a href="#">
                                                                <i class="maximum fa fa-expand" data-toggle="tooltip"
                                                                   data-placement="bottom" title=""
                                                                   data-original-title="Maximize"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="minimize fa fa-chevron-down" data-toggle="tooltip"
                                                                   data-placement="bottom" title=""
                                                                   data-original-title="Collapse"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip"
                                                                   data-placement="bottom" title="" class="fa fa-times"
                                                                   data-original-title="Close"></i>
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div style="padding:10px">
                                                        <marquee behavior="scroll" direction="up" style="height:220px;"
                                                                 scrollamount="3">
                                                            @foreach($news as $new)
                                                                <h4><a href="{{ route("view_news", $new->id) }}">{{ $new->title }}</a></h4>
                                                                <p></p><a href="{{ route("view_news", $new->id) }}"> {{ str_limit($new->body, 80) }}  </a><p></p>
                                                                <hr>
                                                            @endforeach


                                                        </marquee>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end panel -->
                                        </div>
                                    </div>


                                </div>


                                <br><br>


                            </div>


                            <hr>

                            {{--<div id="titr-content" class="col-md-12">
                                <h2>Completed Courses</h2>
                            </div>--}}


                            <div class="row">

                            </div>


                            {{--  @if(count($completed) <= 0 )
                                  <h3 style="color: red; align-items: center;">No Course Completed</h3>
                              @endif
                              <div class="col-md-12">
                                  <div class="row">


                                      @php $i=0; @endphp
                                      @foreach($completed as $course)
                                          @php $i=$i+1;  @endphp

                                          <div class="col-md-3">
                                              <div class="panel panel-default">
                                                  <div class="course-container">
                                                      @if($course->courseName($course->course_id)!="")
                                                          <img src="{{ asset('uploads/images/'.$course->courseImg($course->course_id)) }}"
                                                               alt="Avatar" class="image img-responsive">
                                                      @else
                                                          <img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar"
                                                               class="image img-responsive">
                                                      @endif
                                                      <div class="overlay">
                                                          <div class="text"><span
                                                                      class="fa fa-graduation-cap hover-cap"></span></div>
                                                      </div>
                                                  </div>
                                                  <div class="panel-body">
                                                      <legend class="staff-legend">
                                                          @php $course_trim=$course->courseName($course->course_id); @endphp
                                                          {{ str_limit($course_trim, $limit = 20, $end = '...') }}
                                                      </legend>
                                                      <p>Session 1
                                                          of {{ count($course->courseModules($course->course_id)) }} <span
                                                                  class="pull-right">{{ $user->singleUserCrsStatus($user->id, $course->course_id) }}
                                                              %</span></p>
                                                      <div class="progress progress-sm">
                                                          <div class="progress-bar progress-bar-warning"
                                                               role="progressbar"
                                                               style="width: {{ $user->singleUserCrsStatus($user->id, $course->course_id) }}%"></div>
                                                      </div>
                                                      <a href="{{ route('lead_bd', ['id'=>$course->course_id]) }}"
                                                         class="btn btn-primary btn-sm">Leaderboard</a>
                                                      <a href="{{ route('tak_crs', ['id'=>$course->course_id]) }}"
                                                         class="btn btn-default btn-sm has-ripple">Go to Course
                                                      </a>
                                                  </div>
                                              </div>
                                          </div>
                                          @if($i==4) @break @endif
                                      @endforeach

                                  </div>
                              </div>


                              <hr>
                              <div id="titr-content" class="col-md-12">
                                  <h2>Requested Courses</h2>
                              </div>
                              @if(count($requested) <= 0 )
                                  <h3 style="color: red; align-items: center;">No Course Requested</h3>
                              @endif
                              <div class="col-md-12">
                                  <div class="row">
                                      @php $i=0; @endphp
                                      @foreach($requested as $course)
                                          @php $i=$i+1;  @endphp

                                          <div class="col-md-3">
                                              <div class="panel panel-default">
                                                  <div class="course-container">
                                                      @if($course->course->course_name)
                                                          <img src="{{ asset('uploads/images/'.$course->course->course_img) }}"
                                                               alt="Avatar" class="image img-responsive">
                                                      @else
                                                          <img src="{{ asset('admin/img/course-img.svg') }}" alt="Avatar"
                                                               class="image img-responsive">
                                                      @endif
                                                      <div class="overlay">
                                                          <div class="text"><span
                                                                      class="fa fa-graduation-cap hover-cap"></span></div>
                                                      </div>
                                                  </div>
                                                  <div class="panel-body">
                                                      <legend class="staff-legend">
                                                          {{ $course->course->course_name }}
                                                          @php $course_trim=$course->course->course_namw; @endphp
                                                          {{ str_limit($course_trim, $limit = 20, $end = '...') }}
                                                      </legend>


                                                  </div>
                                              </div>
                                          </div>
                                          @if($i==4) @break @endif
                                      @endforeach


                                  </div>
                              </div>--}}





                            {{--@foreach($leaderboard as $key => $leader)
                                <h4>Course : {{$key}}</h4>
                                @foreach($leader as $course)
                                    <ul>
                                        <li><h5>Name : {{ $course->user->firstName ?? "" }} : {{ $course->score }} </h5></li>
                                    </ul>
                                      <br/>
                                @endforeach
                            @endforeach--}}




                            @if(count($user->courses) == 0 )
                                <center><h1 style="color:red;"><strong>No Course Assigned Yet </strong></h1>
                                    <strong><a href="{{ route('staff_req_crs') }}">CLICK HERE TO REQUEST
                                            NOW</a></strong>
                                </center>

                            @endif


                        </div>

                        <!-- end col-md-12 -->
                    </div>

                    <!-- end #content -->


                </div><!-- end col-md-12 -->
            </div><!-- end #content -->
        </div><!-- end .row -->
    </div>
@stop