<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description"
          content="GE is the world's Digital Industrial Company, transforming industry with software-defined machines and solutions that are connected, responsive and predictive.">
    <meta name="keywords" content="GE.com, Elearning, LMS">
    <meta name="author" content="Odukpahie Oseleumese">
    <title>VFD GROUP</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ asset("ft/img/favicon.png") }}" type="image/x-icon"/>
    <link rel="apple-touch-icon" type="image/x-icon"
          href="{{ asset("ft/img/apple-touch-icon-57x57-precomposed.png") }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72"
          href="{{ asset("ft/img/apple-touch-icon-72x72-precomposed.png") }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
          href="{{ asset("ft/img/apple-touch-icon-114x114-precomposed.png") }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
          href="{{ asset("ft/img/apple-touch-icon-144x144-precomposed.png") }}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">

    <!-- CSS -->
    <link href="{{ asset("ft/css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("ft/css/style.css") }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css')}}">

    <link href="{{ asset("ft/fontello/css/fontello.css") }}" rel="stylesheet">
    <link href="{{ asset("ft/fontello/css/animation.css") }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="http://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div id="wrapper">
    <div id="main">
        <div class="container-fluid">

            <div class="row" style="margin-top: 0px;margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="pull-left" id="logo">
                        <img src="{{ asset("ft/img/logo-impose.png") }}" class="visible-md visible-lg" width="150"
                             height="150" alt="" data-retina="true" style="margin-left: -15px">
                        <img src="{{ asset("ft/img/vfd-logo.png") }}" class="visible-xs visible-sm " width="180"
                             height="46" alt="" data-retina="true" style="margin-left: -15px">
                    </div>
                    <div class="pull-right">
                        <div class="btn-group">

                            {{--<button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" style="padding: 20px">Login</button>
                            <ul class="dropdown-menu" role="menu">
                                <form role="form" class="stop-propagation" style="padding: 20px" action="md.html">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="text-dark">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="text-dark">Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                    </div>
                                    <button type="submit" class="btn btn-danger w-100">Submit</button>
                                    <!-- <a href="" class="text-dark">Forgot Password? </a> -->
                                </form>
                            </ul>--}}
                        </div>
                    </div>
                    <h1 style="clear: both;">VFD GROUP ADMIN LOGIN</h1>
                    <h2>Global Network. Worldwide Strength.</h2>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 text-center">

                    <img src="{{ asset("ft/img/co1.png") }}">
                    <p style="padding-top: 10px;font-size: 20px">Learn from Experts</p>
                </div>
                <div class="col-sm-4 text-center">
                    @include('flash')

                    <form role="form" method="post" class="stop-propagation" style="padding: 20px" action="{{ route("admin.login.submit") }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputEmail1" class="text-white">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="text-white">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                                   placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-danger w-100">Login</button>
                        {{--<a href="{{ route("us_fg_p") }}" class="text-white">Forgot Password? </a>--}}
                        <a href="{{ route("admin.login.forgot") }}" class="text-white">Forgot Password? </a>
                    </form>
                </div>
                <div class="col-sm-4 text-center">
                    <img src="{{ asset("ft/img/co3.png") }}">
                    <p style="padding-top: 10px;font-size: 20px">Quality Resources</p>
                </div>
            </div>

            <div id="social_footer">
                <ul>
                    <li><a href="#"><i class="icon-facebook"></i></a></li>
                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                    <li><a href="#"><i class="icon-google"></i></a></li>
                    <li><a href="#"><i class="icon-vimeo"></i></a></li>
                    <li><a href="#"><i class="icon-youtube-play"></i></a></li>
                </ul>
                <p>© VFD Group 2019</p>
            </div>
        </div><!-- End container -->
    </div><!-- End main -->
</div>

<div id="slides">
    <ul class="slides-container">
        <li><img src="{{ asset("ft/img/s1.jpg") }}" alt=""></li>
        <li><img src="{{ asset("ft/img/s2.jpg") }}" alt=""></li>
    </ul>
</div><!-- End background slider -->

<!-- JQUERY -->
<script src="{{ asset("ft/js/jquery-2.2.4.min.js") }}"></script>
<script src="{{ asset("ft/js/jquery.easing.1.3.min.js") }}"></script>
<script src="{{ asset("ft/js/jquery.animate-enhanced.min.js") }}"></script>
<script src="{{ asset("ft/js/jquery.superslides.min.js") }}"></script>
<script type="text/javascript">
    $('#slides').superslides({
        play: 6000,
        pagination: false,
        animation_speed: 800,
        animation: 'fade'
    });
    $('.stop-propagation').on('click', function (e) {
        e.stopPropagation();
    });
</script>
<!-- OTHER JS -->
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="{{ asset("ft/js/retina.min.js") }}"></script>
<script src="{{ asset("ft/js/functions.js") }}"></script>
<script src="{{ asset("ft/assets/validate.js") }}"></script>
</body>

</html>