@extends('layouts.admin.app')
@section('content')
 {{--<div id="content" class="row" style="margin-left: 200px;">
     <h1>Upload Staff</h1>
     <h5> Please, click on the "Download Upload Template" below and fill accordingly with correct information and upload the file back</h5>
     <div class="row">
         <a href="{{ route("staff_template") }}"> Download Upload Template</a>
     </div>
     <br/><br/>
     @include('flash')
                    <form action="{{ route('store_upload_users') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field()}}
                        <input type="file" name="user_csv">
                        <input type="submit" value="Upload">
                    </form>

 </div>--}}


 <div id="content">
     <div id="sortable-panel" class="ui-sortable">
         @include('flash')
         <div id="titr-content" class="col-md-12">
             <h2>Upload Staff</h2>
             <hr>
             <h5> Please, click on the "Download Upload Template" below and fill accordingly with correct information and upload the file back</h5>
             {{--<div class="row">
                 <a href="{{ route("staff_template") }}"> Download Upload Template</a>
             </div>--}}
         </div>

         <div class="col-md-6 ">
             <div class="panel panel-default">
                 <div class="panel-heading ui-sortable-handle">

                 </div>
                 <div class="panel-body">

                     <div class="row">
                         <a href="{{ route("staff_template") }}"> Download Upload Template</a>
                     </div>
                     <br><br>

                 <!--<hr>-->
                     <form action="{{ route('store_upload_users')}}" role="form" method="POST" id="form1" novalidate="novalidate" enctype="multipart/form-data">
                         {{ csrf_field()}}
                         <div class="row">

                             <div class="col-md-6">
                                 <div class="form-group">
                                     <label class="control-label">
                                         File
                                         <span style="color: red">*</span>
                                         <span class="symbol required" aria-required="true"></span>
                                     </label>
                                     <input type="file" placeholder="Insert Course Name" class="form-control" id="course_name" name="user_csv">
                                 </div>

                             </div>
                         </div>
                         <div class="row">
                             <div class="col-md-4">
                                 <button class="btn btn-success btn-block dammy" type="submit">
                                     Upload
                                     <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                             </div>
                         </div>
                     </form>


                 </div>
             </div>
             <!-- end panel -->
         </div>


         <!-- /end Admin over view .col-md-12 -->
     </div>
     <!-- end col-md-12 -->
 </div>
@stop

<!--<script-->
<!--			  src="https://code.jquery.com/jquery-3.3.1.min.js"-->
<!--			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="-->
<!--			  crossorigin="anonymous"></script>-->
			  
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/basic.css" />-->

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone-amd-module.js"></script>-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css" />-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>-->
<script type="text/javascript">


                    jQuery(function ($) {
                        Dropzone.autoDiscover = false;
                        try {
                            var myDropzone = new Dropzone("#dropzone", {
                                paramName: "file", // The name that will be used to transfer the file
                                maxFilesize: .5, // MB

                                addRemoveLinks: true,
                                dictDefaultMessage:
                                    '<span class="bigger-150 bolder"><i class=" fa fa-caret-right red"></i> Drop files</span> to upload \
                					<span class="smaller-80 grey">(or click)</span> <br /> \
                					<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>'
                                ,
                                dictResponseError: 'Error while uploading file!',

                                //change the previewTemplate to use Bootstrap progress bars

                            });
                        } catch (e) {
                            alert('Dropzone.js does not support older browsers!');
                        }

                    });

                    jQuery(function ($) {
                        Dropzone.autoDiscover = false;
                        try {
                            var myDropzone = new Dropzone("#avatar-dropzone", {
                                url: "../dummy.html",
                                paramName: "file", // The name that will be used to transfer the file
                                maxFilesize: 50, // MB	               
                                maxFiles: 1,
                                maxfilesexceeded: function (file) {
                                    this.removeAllFiles();
                                    this.addFile(file);
                                },
                                addRemoveLinks: true,
                                dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
                                dictResponseError: 'Error while uploading file!',

                                //change the previewTemplate to use Bootstrap progress bars

                            });
                        } catch (e) {
                            alert('Dropzone.js does not support older browsers!');
                        }

                    });


                    jQuery(function ($) {
                        Dropzone.autoDiscover = false;
                        try {
                            var myDropzone = new Dropzone("#gmail-dropzone", {
                                url: "../dummy.html",
                                paramName: "file", // The name that will be used to transfer the file
                                maxFilesize: .5, // MB	               
                                maxFiles: 1,
                                clickable: '#upload',
                                createImageThumbnails: false,
                                maxfilesexceeded: function (file) {
                                    this.removeAllFiles();
                                    this.addFile(file);
                                },
                                addRemoveLinks: true,
                                dictDefaultMessage: '',
                                dictRemoveFile: 'x',
                                dictCancelUpload: 'x',
                                dictResponseError: 'Error while uploading file!',
                                previewTemplate: '<div class="dz-preview dz-file-preview">\
                										    <div class="dz-image"><img data-dz-thumbnail=""></div>\
                										    <div class="dz-details">\
                										        <div class="dz-size"><span data-dz-size=""></span></div>\
                										        <div class="dz-filename">\
                										            <span data-dz-name=""></span>\
                										        </div>\
                										    </div>\
                										    <div class="dz-progress">\
                										        <span class="dz-upload" data-dz-uploadprogress=""></span>\
                										    </div>\
                										    <div class="dz-error-message">\
                										        <span data-dz-errormessage=""></span>\
                										    </div>\
                										    <div class="dz-success-mark"><i class="fa fa-check success"></i></div>\
                										    <div class="dz-error-mark"><i class="fa fa-exclamation-triangle red"></i></div>\
                										</div>'
                                //change the previewTemplate to use Bootstrap progress bars

                            });
                        } catch (e) {
                            alert('Dropzone.js does not support older browsers!');
                        }
                    });


                </script>