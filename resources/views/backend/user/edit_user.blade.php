@extends('layouts.admin.app')
@section('content')
<div id="titr-content" class="col-md-12">


								@include('flash')

							</div>

							<div class="col-md-6 ">
								<div  class="panel panel-default">
									<div class="panel-heading">
										<div class="panel-title">
											<i class="fa fa-check-square-o"></i>
											Edit {{ $user->firstName }} Details

										</div>
									</div>
									<div class="panel-body">
										<form  class="form-horizontal" action="{{ route('upd_user', ['id'=>$user->id])}}" method="POST" enctype="multipart/form-data">
											{{ csrf_field() }}
											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Firstname
												</label>
												<div class="col-sm-9">
													<input type="text" name="firstName" placeholder="First Name" value="{{ $user->firstName}}" id="form-field-1" class="form-control">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Lastname
												</label>
												<div class="col-sm-9">
													<input type="text" name="lastName" placeholder="Last Name" value="{{ $user->lastName}}" id="form-field-1" class="form-control">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Email
												</label>
												<div class="col-sm-9">
													<input type="email" name="email" placeholder="Email" id="form-field-1" value="{{ $user->email}}" class="form-control">
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Gender
												</label>
												<div class="col-sm-9">
													<select name="gender" class="form-control">
														<option value="{{ $user->gender }}">{{ $user->gender }}</option>
															<option value="Male">Male</option>
															<option value="Feale">Feale</option>
													</select>
												</div>
											</div>


											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Level
												</label>
												<div class="col-sm-9">
													<select name="level" class="form-control">
														<option value="{{ $user->level }}">{{ $user->level }}</option>
														@foreach(config("levels") as $level)
															<option value="{{ $level }}">{{ $level }}</option>
														@endforeach
													</select>
												</div>
											</div>


											{{--<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Employment Date
												</label>
												<div class="col-sm-9">
													<input type="date" name="employment_date" placeholder="Email" id="form-field-1" value="{{ $user->employment_date}}" class="form-control">
												</div>
											</div>--}}




											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Department
												</label>
												<div class="col-sm-9">
													<select name="department" class="form-control">
														<option value="{{ $user->department }}">{{ $user->department }}</option>
														@foreach($depts as $dept)
														<option value="{{ $dept->name }}">{{ $dept->name }}</option>
														@endforeach
													</select>
												</div>
											</div>


											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Role
												</label>
												<div class="col-sm-9">
													<select name="role" class="form-control">
														<option value="{{ $user->role }}">{{ $user->role }}</option>
														@foreach($roles as $role)
														<option value="{{$role->name}}">{{$role->name}}</option>
														@endforeach
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Change Password
												</label>
												<div class="col-sm-9">
													<input type="text" name="password" placeholder="Change Staff Password" id="form-field-1" value="" class="form-control">
												</div>
											</div>



											<fieldset>
												<legend></legend>
												<div class="form-group">
													<label class="col-sm-3 control-label" for="form-field-21">
														Photo
													</label>
													<div class="col-sm-9">
														<input type="file" name="photo"  id="form-file-20" class="form-control yep-file-input">
													</div>


												</div>




												<button type="submit" class="btn btn-primary btn-lg btn-block dammy">
													Update Staff
												</button>
											</fieldset>
										</form>

									</div>

								</div><!-- end panel -->
							</div>




							<div class="col-md-6 ">
                                <img style="margin-top: 10%; alignment: center; margin-right: 80%" class="yep-image-profile " src="{{ asset('uploads/images/'.$user->photo.'')}}" alt="Profile image example"/>


							</div>
@stop