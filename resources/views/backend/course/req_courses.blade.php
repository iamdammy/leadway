<!DOCTYPE html>
<html>
    @include('layouts.admin.head_script')

    <body id="mainbody">
        <div id="container" class="container-fluid skin-3">
            <!-- Add Task in sidebar list modal -->
            
            <!--./ Add Task in sidebar list modal -->
            
            <!-- Add Contact in sidebar list modal -->
            
            <!--./ Add Contact in sidebar list modal -->

            <!-- Header -->
            @include('layouts.admin.header')
            <!-- /end Header -->
            
            <!-- sidebar menu -->           
            @include('layouts.admin.sidebar')
            <!-- /end #sidebar -->
            
            <!-- main content  -->
            <div id="main" class="main">
                <div class="row">
                    <!-- breadcrumb section -->
                    <div class="ribbon">
                        <ul class="breadcrumb" style="background: #004d3e">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="#">Website</a>
                            </li>
                            <!-- <li>
                                <a href="#">Library</a>
                            </li>
                            <li>
                                <a href="#">Data</a>
                            </li> -->
                        </ul>
                    </div>
                    
                    <div class="col-md-12 ">
                                
                    </div>
                    @include('flash')
                    <!-- main content -->
                    <div id="content">
                        <div id="sortable-panel" class="ui-sortable">
                            <div id="titr-content" class="col-md-12">
                                <h2>Courses</h2>
                                <h5>All</h5>
                            </div>
                            <div class="col-md-12 ">
                                <div class="panel panel-default">
                                    
                                    <div class="panel-body">
                            
                                        <table id="example3" class="table table-striped table-bordered width-100 cellspace-0">
                                            <thead>
                                                <tr>
                                                    <th>Course Image</th>
                                                    <th>Course Name</th>
                                                    <th>Course Category</th>
                                                    <th>Duration</th>
                                                    {{-- <th>Course Video</th>
                                                    <th>Course Doc</th> --}}
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($courses as $course)
                                                <tr>
                                                    <td><img width="60px" height="60px" src="{{ asset('uploads/images/'.$course->course_img.'') }}"></td>
                                                    <td>{{$course->course_name ?? 'No Name'}}</td>
                                                   
                                                    <td>{{$course->myCategory($course->category_id) ?? 'No Category'}}</td>
                                                    <td>{{$course->duration ?? 'Null'}}</td>
                                                   {{-- <td>{{$course->cat_name ?? 'No Category'}}</td>--}}
                                                    {{-- <td>{{$course->course_video ?? 'No Video'}}</td>
                                                    <td>{{ $course->course_doc ?? 'No Doc'}}</td> --}}
                                                    <td>
                                                        <div class=" action-buttons">
                                                            <a class="blue" href="{{ route('view_course', ['id'=>$course->id])}}">
                                                                <i class=" fa fa-search-plus bigger-130"></i>
                                                            </a> |

                                                            <a class="green" href="{{route('edit_course', ['id'
                                                            =>$course->id] )}}">
                                                                <i class=" fa fa-pencil bigger-130"></i>
                                                            </a> |

                                                            <a class="red" href="{{ route('del_course', ['id'=>$course->id])}}">
                                                                <i class=" fa fa-trash-o bigger-130" onclick="confirmDelete()"></i>
                                                            </a> {{--|
                                                            <a href="{{ route('ass_cr', ['id'=>$course->id])}}" data-toggle="modal">
                                                                Assign Staff
                                                            </a> --}}|

                                                            <a href="{{ route('view_crs_std', ['id'=>$course->id])}}" data-toggle="modal">
                                                                View Staff
                                                            </a> |

                                                             <a href="{{ route('view_crs_req', ['id'=>$course->id])}}" data-toggle="modal">
                                                                View Requests
                                                            </a>

                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                        <!-- Modal For Assigned Courses -->
                                        <div id="assignModal" class="modal fade" role="dialog">
                                          <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Modal Header</h4>
                                              </div>
                                              <div class="modal-body">
                                                
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </div>
                                            </div>

                                          </div>
                                        </div>
                                        <!-- End of Modal for Assigned courses -->
                            
                                    </div>
                                </div>
                                <!-- end panel -->
                            </div>
                            

                            <!-- /end Admin over view .col-md-12 -->
                        </div><!-- end col-md-12 -->
                    </div><!-- end #content -->
                </div><!-- end .row -->
            </div>
            <!-- ./end #main  -->
            
            <!-- footer -->
            <div class="page-footer">               
                <div class="col-xs-12 col-sm-12 text-center">
                    <strong class=""><a href="#">{{ env("APP_NAME") }} © {{ \Carbon\Carbon::today()->year }}</a> </strong>
                    <a href="#">
                        <i class="fa fa-twitter-square bigger-120"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-facebook-square bigger-120"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-rss-square orange bigger-120"></i>
                    </a>
                </div>          
            </div>
            <!-- /footer -->
        </div>
        <!-- end #container -->     

        <!-- General JS script library-->
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js')}}"></script>    
        <script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js')}}"></script>                        
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js')}}"></script>                                    
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js')}}"></script>                                                                        
        
    
        <!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
        <script type="text/javascript" src="{{ asset('admin/js/yep-script.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/js/yep-demo.js')}}"></script> 


        <!-- Related JavaScript Library to This Pagee -->
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js')}}"></script>



         <script type="text/javascript">
            function confirmDelete()
            {
                var x = confirm("Are you sure you want to delete?");
                    if (x) {
                        return true;
                    }
                    else {

                        event.preventDefault();
                        return false;
                    }
            }
    
        </script>
        


        <!-- Plugins Script -->
        <script type="text/javascript">

            // Default Data Table Script
            $('#example1').dataTable({
                responsive: true
            });

            // Table Tools Data Tables
            // $('#example2').dataTable({
            //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
            //         "t" +
            //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            //     "oTableTools": {
            //         "aButtons": [
            //             "copy",
            //             "csv",
            //             "xls",
            //             {
            //                 "sExtends": "pdf",
            //                 "sTitle": "Yeptemplate_PDF",
            //                 "sPdfMessage": "Yeptemplate PDF Export",
            //                 "sPdfSize": "letter"
            //             },
            //             {
            //                 "sExtends": "print",
            //                 "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
            //             }
            //         ],

            //     },
            //     "autoWidth": true,
            //     responsive: true

            // });

            // Column show & hide
            $('#example3').DataTable({
                "dom": '<"pull-left"f><"pull-right"l>tip',
                "dom": 'C<"clear">lfrtip',

                "bLengthChange": false,
                responsive: true
            });


        </script>

        <!-- Google Analytics Script -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-67070957-1', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
</html>