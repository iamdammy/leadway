@extends('layouts.admin.app')
@section('content')
<div class="col-md-12 ">
								<div  class="panel panel-default">	
								@include('flash')										
									<div class="panel-body no-padding">											
										<div class="yep-profile ">
											<!-- <div class="statistic">
												<div class="padding-10">																
													<h4> <strong class="center">1,543</strong> <br> <small>Followers</small></h4>
													<h4><strong>419</strong> <br> <small>Connections</small></h4>																															
												</div>
											</div> -->
											<img class="yep-image-lg" src="{{ asset('admin/img/gallery/a9.jpg')}}" height="214" width="500" alt="Profile image example"/>
											<div class="yep-profile-text">
												<div class="col-sm-12 ">
												
													<div class="col-sm-3">
														<img  class="yep-image-profile " src="{{ asset('uploads/images/'.$course->course_img.'')}}" alt="Profile image example"/>																	
														
													</div>
													
													<div class="col-sm-6 no-padding ">	
														<h4>Name: {{$course->course_name}} 
															<br>
															<small> <strong>Category : </strong>{{$course->cat_name ?? 'No Category'}}
															</small>
														</h4>
															<br>
														<h4>Total Staff: {{$totalStaff}} 
															<br>
															<small> <a href="{{ route('mail_staff', ['id'=>$course->courseID]) }}"><strong>Mail All Staff</strong></a>
															</small>
														</h4>
			
			
														<br>
														<p class="font-md">
															<i>Description</i>
														</p>
														<p>

															{{ $course->course_description }}
			
														</p>
													</div>
													
													<div class="col-sm-3 bg-grey">
														<br>
														<a href="{{ route('edit_course', ['id'=>$course->courseID])}}">
															<button class="btn btn-xs btn-success btn-block"><i class="fa fa-pencil"> </i> 
																Edit course
															</button>	
														</a>	
														
														<a href="{{ route('all_courses')}}">
															<button class="btn btn-xs btn-primary btn-block"><i class="fa fa-link"> </i> All courses </button>	
														</a>															
																												
														
														<!-- <h4>Photos</h4>
														<ul class="list-inline friends-list">
															<li><img src="{{ asset('admin/img/avatars/male.png')}}" alt="friend-1">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar4.png')}}" alt="friend-2">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar3.png')}}" alt="friend-3">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar2.png')}}" alt="friend-4">
															</li>
															<li><img src="{{ asset('admin/img/avatars/male.png')}}" alt="friend-5">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar.png')}}" alt="friend-6">
															</li>
															
														</ul> -->
			
														<!-- <h4>Recent visitors</h4>
														<ul class="list-inline friends-list">
															<li><img src="{{ asset('admin/img/avatars/avatar4.png')}}" alt="friend-1">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar2.png')}}" alt="friend-2">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar3.png')}}" alt="friend-3">
															</li>
														</ul>			 -->																	
													</div>


												</div>
											</div>
										</div>

									</div>	
																					
								</div><!-- end panel -->									
							</div>
@stop