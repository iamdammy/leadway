@extends('layouts.admin.app')
@section('content')
<style type="text/css">
    body {margin-top: 0px;margin-left: 0px;}
    #page_1 {position:relative; overflow: hidden;margin: 0px 0px 0px 0px;padding: 0px;border: none;width: 1040px;height: 720px;margin: 0 auto;}
    #page_1 #p1dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:1040px;height:720px;}
    #page_1 #p1dimg1 #p1img1 {width:1040px;height:720px;}
    .dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}
    .ft0{font: 21px 'Helvetica';color: #ff0000;line-height: 24px;}
    .ft1{font: 24px 'Calibri';line-height: 24px;}
    .ft2{font: 64px 'Helvetica';color: #1d706b;line-height: 72px;}
    .ft3{font: 21px 'Calibri';line-height: 26px;}
    .p0{text-align: left;padding-left: 435px;margin-top: 226px;margin-bottom: 0px;}
    .p1{text-align: left;padding-left: 444px;margin-top: 49px;margin-bottom: 0px;}
    .p2{text-align: left;padding-left: 289px;margin-top: 3px;margin-bottom: 0px;}
    .p3{text-align: left;padding-left: 289px;margin-top: 7px;margin-bottom: 0px;}
</style>
<div id="content">
  <a href="{{ route("certificate.all") }}" class="btn btn-primary">Back</a>
  <div class="container">
      <div id="page_1">
      
        <div id="p1dimg1">
          <img src="{{ asset('assets/img/cert.jpg')  }}" id="p1img1">
        </div>

        <div class="dclr"></div>
        <p class="p0 ft0">O F C O M P L E T I O N</p>
        <p class="p1 ft1">This certifies that</p>
        <p class="p2 ft2"></p>
        <p class="p3 ft3">you have successfully completed the {{ $courseCertificate->title }} Training.</p>
      </div>
  </div>
</div>
@endsection

