@extends('layouts.admin.app')
@section('content')
<div id="content">
                    <div id="sortable-panel" class="ui-sortable">
                        <div id="titr-content" class="col-md-12">
                            <h2>Form Validation</h2>
                            <h5>You may also check the form validation by clicking on the form action button...</h5>
                            <div class="actions">
                                <button class="btn btn-success  has-ripple">Add new
                                    <span class="ripple ripple-animate" style="height: 81px; width: 81px; animation-duration: 0.4s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.2; top: -26.5px; left: -32.7656px;"></span>
                                </button>
                                <button class="btn btn-default  has-ripple">Options
                                    <span class="ripple ripple-animate" style="height: 76px; width: 76px; animation-duration: 0.4s; animation-timing-function: linear; background: rgb(51, 51, 51); opacity: 0.2; top: -27px; left: -26.7188px;"></span>
                                </button>
                            </div>
                        </div>

                        <div class="col-md-12 ">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-sortable-handle">
                                    <div class="panel-title">
                                        <i class="fa fa-edit"></i>
                                        Form Validation
                                        <div class="bars pull-right">
                                            <div class="dropdown ">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-cog"></i>
                                                </a>
                                                <ul class="dropdown-menu yep-panel-menu">
                                                    <li>
                                                        <a href="#">Daily
                                                            <span class="badge pull-right"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Monthly
                                                            <span class="badge pull-right"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Yearly
                                                            <span class="badge pull-right"> 42 </span>
                                                        </a>
                                                    </li>
                                                    <li class="divider">
                                                    </li>
                                                    <li>
                                                        <a href="#">All time
                                                            <span class="badge pull-right">56</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="#">
                                                <i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Maximize"></i>
                                            </a>
                                            <a href="#">
                                                <i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Collapse"></i>
                                            </a>
                                            <a href="#">
                                                <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title=""
                                                    class="fa fa-times" data-original-title="Close"></i>
                                            </a>
                        
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @include('flash')
                                    <hr>
                                    <form action="{{ route('store_user')}}" role="form" method="POST" id="form1" novalidate="novalidate">
                                        {{ csrf_field()}}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="errorHandler alert alert-danger no-display">
                                                    <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                </div>
                                                <div class="successHandler alert alert-success no-display">
                                                    <i class="fa fa-ok"></i> Your form validation is successful!
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        First Name
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="text" placeholder="Insert your First Name" class="form-control" id="firstname" name="firstName">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Last Name
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="text" placeholder="Insert your Last Name" class="form-control" id="lastname" name="lastName">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Email Address
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="email" placeholder="Text Field" class="form-control" id="email" name="email">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Password
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="password" class="form-control" name="password" id="password">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Department
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <select name="department" id="mm" class="form-control">
                                                        <option value="">Select Department</option>
                                                        @foreach($depts as $dept)
                                                        <option value="{{ $dept->name}}">{{ $dept->name}}</option>
                                                         @endforeach
                                                    </select>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <!-- <div class="form-group connected-group">
                                                    <label class="control-label">
                                                        Date of Birth
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <select name="dd" id="dd" class="form-control">
                                                                <option value="">DD</option>
                                                                <option value="01">1</option>
                                                                <option value="02">2</option>
                                                                <option value="03">3</option>
                                                                <option value="04">4</option>
                                                                <option value="05">5</option>
                                                                <option value="06">6</option>
                                                                <option value="07">7</option>
                                                                <option value="08">8</option>
                                                                <option value="09">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                                <option value="13">13</option>
                                                                <option value="14">14</option>
                                                                <option value="15">15</option>
                                                                <option value="16">16</option>
                                                                <option value="17">17</option>
                                                                <option value="18">18</option>
                                                                <option value="19">19</option>
                                                                <option value="20">20</option>
                                                                <option value="21">21</option>
                                                                <option value="22">22</option>
                                                                <option value="23">23</option>
                                                                <option value="24">24</option>
                                                                <option value="25">25</option>
                                                                <option value="26">26</option>
                                                                <option value="27">27</option>
                                                                <option value="28">28</option>
                                                                <option value="29">29</option>
                                                                <option value="30">30</option>
                                                                <option value="31">31</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select name="mm" id="mm" class="form-control">
                                                                <option value="">MM</option>
                                                                <option value="01">1</option>
                                                                <option value="02">2</option>
                                                                <option value="03">3</option>
                                                                <option value="04">4</option>
                                                                <option value="05">5</option>
                                                                <option value="06">6</option>
                                                                <option value="07">7</option>
                                                                <option value="08">8</option>
                                                                <option value="09">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" placeholder="YYYY" id="yyyy" name="yyyy" class="form-control">
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Gender
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div>
                        
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" value="Female" name="gender" id="gender_female">
                                                            <label for="gender_female">Female</label>
                                                        </div>
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" value="Male" name="gender" id="gender_male">
                                                            <label for="gender_male">male</label>
                                                        </div>
                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                    <label class="control-label">
                                                        Role
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <select name="role" id="mm" class="form-control">
                                                        <option value="">Select Role</option>
                                                        @foreach($roles as $role)
                                                        <option value="{{ $role->name}}">{{ $role->name}}</option>
                                                         @endforeach
                                                    </select>
                                                    
                                                </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label class="control-label">
                                                                Photo
                                                                <span class="symbol required" aria-required="true"></span>
                                                            </label>
                                                            <input type="file" name="photo" placeholder="Select a photo">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <hr>
                                                    <label class="control-label">
                                                        <strong>Signup for Clip-One Emails</strong>
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <p>
                                                        Would you like to review Clip-One emails?
                                                    </p>
                                                    <div>
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" value="" name="newsletter" id="no">
                                                            <label for="no">Female</label>
                                                        </div>
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" value="" name="newsletter" id="yes">
                                                            <label for="yes">male</label>
                                                        </div>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div>
                                                    <span class="symbol required" aria-required="true"></span>Required Fields *
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <p>
                                                    By clicking REGISTER, you are agreeing to the Policy and Terms &amp; Conditions.
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success btn-block" type="submit">
                                                    Register
                                                    <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                        
                        
                                </div>
                            </div>
                            <!-- end panel -->
                        </div>


                        <!-- /end Admin over view .col-md-12 -->
                    </div>
                    <!-- end col-md-12 -->
                </div>
@stop

<script type="text/javascript">
            $(function () {
                var form1 = $('#form1');
                var errorHandler1 = $('.errorHandler', form1);
                var successHandler1 = $('.successHandler', form1);
                $.validator.addMethod("FullDate", function () {
                    //if all values are selected
                    if ($("#dd").val() != "" && $("#mm").val() != "" && $("#yyyy").val() != "") {
                        return true;
                    } else {
                        return false;
                    }
                }, 'Please select a day, month, and year');
                form1.validate({
                    errorElement: "span", // contain the error msg in a span tag
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                            error.insertAfter($(element).closest('.form-group').children('div').children().last());
                        } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                            error.insertAfter($(element).closest('.form-group').children('div'));
                        } else {
                            error.insertAfter(element);
                            // for other inputs, just perform default behavior
                        }
                    },
                    ignore: "",
                    rules: {
                        firstname: {
                            minlength: 2,
                            required: true
                        },
                        lastname: {
                            minlength: 2,
                            required: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            minlength: 6,
                            required: true
                        },
                        password_again: {
                            required: true,
                            minlength: 5,
                            equalTo: "#password"
                        },
                        yyyy: "FullDate",
                        gender: {
                            required: true
                        },
                        zipcode: {
                            required: true,
                            number: true,
                            minlength: 5,
                            minlength: 5
                        },
                        city: {
                            required: true
                        },
                        newsletter: {
                            required: true
                        }
                    },
                    messages: {
                        firstname: "Please specify your first name",
                        lastname: "Please specify your last name",
                        email: {
                            required: "We need your email address to contact you",
                            email: "Your email address must be in the format of name@domain.com"
                        },
                        gender: "Please check a gender!"
                    },
                    groups: {
                        DateofBirth: "dd mm yyyy",
                    },
                    invalidHandler: function (event, validator) { //display error alert on form submit
                        successHandler1.hide();
                        errorHandler1.show();
                    },
                    highlight: function (element) {
                        $(element).closest('.help-block').removeClass('valid');
                        // display OK icon
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                        // add the Bootstrap error class to the control group
                    },
                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error');
                        // set error class to the control group
                    },
                    success: function (label, element) {
                        label.addClass('help-block valid');
                        // mark the current input as valid and display OK icon
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                    },
                    submitHandler: function (form) {
                        successHandler1.show();
                        errorHandler1.hide();
                        // submit form
                        //$('#form').submit();
                    }
                });
               
            });
        </script>

