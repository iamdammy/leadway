<!DOCTYPE html>
<html>
    @include('layouts.admin.head_script')

    <body id="mainbody">
        <div id="container" class="container-fluid skin-3">
            <!-- Add Task in sidebar list modal -->
            
            <!--./ Add Task in sidebar list modal -->
            
            <!-- Add Contact in sidebar list modal -->
            
            <!--./ Add Contact in sidebar list modal -->

            <!-- Header -->
            @include('layouts.admin.header')
            <!-- /end Header -->
            
            <!-- sidebar menu -->           
            @include('layouts.admin.sidebar')
            <!-- /end #sidebar -->
            
            <!-- main content  -->
            <div id="main" class="main">
                <div class="row">
                    <!-- breadcrumb section -->
                    <div class="ribbon">
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="#">Website</a>
                            </li>
                            <!-- <li>
                                <a href="#">Library</a>
                            </li>
                            <li>
                                <a href="#">Data</a>
                            </li> -->
                        </ul>
                    </div>
                    
                    <div class="col-md-12 ">
                                
                    </div>
                    
                    <!-- main content -->
                    <div id="content">
                        <div id="sortable-panel" class="ui-sortable">
                            <div id="titr-content" class="col-md-12">
                                <h2>Licences</h2>
                                
                            </div>
                            <div class="col-md-12 ">
                                <div class="panel panel-default">
                                
                                    <div class="panel-body">
                                        @include('flash')
                                        <table id="example3" class="table table-striped table-bordered width-80 cellspace-0">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>Company Name</th>                               
                                                    <th>Number of Staff</th>
                                                    <th>Licences</th>
                                                    <th>Action</th>
                                                    
                                                </tr>
                                            </thead>
                                            @foreach($licences as $client)
                                            <tbody>
                                                <tr>
                                                    
                                                    <td>{{$client->company_name ?? 'No Name'}}</td>                                                    
                                                    <td>{{$client->no_staff ?? 'Null'}}</td>
                                                    <td>{{$client->licences ?? 'Null'}}</td>
                                                    <td>
                                                        <div class=" action-buttons">
                                                              <div class='dmo'>
                                                            <a href="{{route('edit_licence',['id'=>$client->licence_id])}}"><i <i class=" fa fa-pencil"></i>
                                                            </a>
                                                            |
                                                            <a href="{{route('del_licence',['id'=>$client->licence_id])}}">

                                                            <i <i class=" fa fa-trash" onclick="confirmDelete()"></i>
                                                            </a>
                                                        </div>

                                                            
                                                            
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            @endforeach
                                        </table>
                            
                                    </div>
                                </div>
                                <!-- end panel -->
                            </div>
                            

                            <!-- /end Admin over view .col-md-12 -->
                        </div><!-- end col-md-12 -->
                    </div><!-- end #content -->
                </div><!-- end .row -->
            </div>
            <!-- ./end #main  -->
            
            <!-- footer -->
            @include('layouts.admin.footer')
            <!-- /footer -->
        </div>
        <!-- end #container -->     

        <!-- General JS script library-->
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js')}}"></script>    
        <script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js')}}"></script>                        
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js')}}"></script>                                    
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js')}}"></script>                                                                        
        
    
        <!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
        <script type="text/javascript" src="{{ asset('admin/js/yep-script.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/js/yep-demo.js')}}"></script> 


        <!-- Related JavaScript Library to This Pagee -->
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js')}}"></script>

        <script type="text/javascript">
            function confirmDelete()
            {
                var x = confirm("Are you sure you want to delete?");
                    if (x) {
                        return true;
                    }
                    else {
                        event.preventDefault();
                        return false;
                    }
            }
    
        </script>
        


        <!-- Plugins Script -->
        <script type="text/javascript">

            // Default Data Table Script
            $('#example1').dataTable({
                responsive: true
            });

            // Table Tools Data Tables
            // $('#example2').dataTable({
            //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
            //         "t" +
            //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            //     "oTableTools": {
            //         "aButtons": [
            //             "copy",
            //             "csv",
            //             "xls",
            //             {
            //                 "sExtends": "pdf",
            //                 "sTitle": "Yeptemplate_PDF",
            //                 "sPdfMessage": "Yeptemplate PDF Export",
            //                 "sPdfSize": "letter"
            //             },
            //             {
            //                 "sExtends": "print",
            //                 "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
            //             }
            //         ],

            //     },
            //     "autoWidth": true,
            //     responsive: true

            // });

            // Column show & hide
            $('#example3').DataTable({
                "dom": '<"pull-left"f><"pull-right"l>tip',
                "dom": 'C<"clear">lfrtip',

                "bLengthChange": false,
                responsive: true
            });


        </script>

        <!-- Google Analytics Script -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-67070957-1', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
</html>