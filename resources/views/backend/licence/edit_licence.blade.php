@extends('layouts.admin.app')
@section('content')
<div id="titr-content" class="col-md-12">
								
									
								@include('flash')
								<div class="actions">
									<a href="{{ route('add_licence')}}"><button class="btn btn-success ">Add new</button></a>
									<a href="{{ route('licences')}}"><button class="btn btn-primary dammy">All Licences</button></a>
								</div>
							</div>
							
							<div class="col-md-6 ">
								<div  class="panel panel-default">
									<div class="panel-heading">
										<div class="panel-title">
											<i class="fa fa-check-square-o"></i>
											
											<div class="bars pull-right">
												<!-- <div class="dropdown ">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">																	
														<i class="fa fa-cog"></i>
													</a>
													<ul class="dropdown-menu yep-panel-menu">
														<li>
															<a href="#">Daily<span class="badge pull-right"></span></a>
														</li>
														<li>
															<a href="#">Monthly<span class="badge pull-right"></span></a>
														</li>
														<li>
															<a href="#">Yearly<span class="badge pull-right"> 42 </span></a>
														</li>
														<li class="divider">
														</li>
														<li>
															<a href="#">All time<span class="badge pull-right">56</span></a>
														</li>
													</ul>
												</div> -->
												<a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
												<a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
												<a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
												
											</div>
										</div>
									</div>
									<div class="panel-body">
										<form  class="form-horizontal" action="{{ route('up_licence',['id'=>$licence->licence_id])}}" method="POST" enctype="multipart/form-data">
											{{ csrf_field() }}
											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													Company
												</label>
												<div class="col-sm-9">
													<select name="client_id" required="true" class="form-control">
														<option value="">Select Company</option>
														@foreach($clients as $client)
														<option value="{{ $client->clientId }}">{{ $client->company_name }}</option>
														@endforeach
													</select> 
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label" for="form-field-1">
													 Number of Licences
												</label>
												<div class="col-sm-9">
													<input type="number" name="licences" placeholder="number of Licences"  id="form-field-1" class="form-control" value="{{$licence->licences}}">
												</div>
											</div>

											<fieldset>
												

												<button type="submit" class="btn btn-primary btn-lg btn-block">
													Submit
												</button>
											</fieldset>
										</form>
										
									</div>	
																						
								</div><!-- end panel -->
							</div>




							<div class="col-md-6 ">
								<div  class="panel panel-default">
									<div class="panel-heading">
										<div class="panel-title">
											<i class="fa fa-check-square-o"></i>
											
											
										</div>
									</div>


									
																						
								</div><!-- end panel -->

								<div class="col-sm-3">
														{{--<img  class="yep-image-profile " src="{{ asset('uploads/images/'.$user->photo.'')}}" alt="Profile image example"/>								--}}									
														<!-- <div class="profile-info">
															<h4 ><small>User Role</small><br>Developers</h4>															
															<h4 ><small>Role</small><br>{{$user->role ?? 'No Role'}}</h4><br>	

															<h4 ><small>Department</small><br>{{$user->department ?? 'No Dep'}}</h4><br>

														</div>															 -->															
													</div>
							</div>
@stop