<!DOCTYPE html>
<html>
@include('layouts.admin.head_script')

<body id="mainbody">
    <div id="container" class="container-fluid skin-3">
        <!-- Add Task in sidebar list modal -->
        
        <!--./ Add Task in sidebar list modal -->

        <!-- Add Contact in sidebar list modal -->

        <!--./ Add Contact in sidebar list modal -->

        <!-- Header -->
        @include('layouts.admin.header')
        <!-- /end Header -->

        <!-- sidebar menu -->           
        @include('layouts.admin.sidebar')
        <!-- /end #sidebar -->

        <!-- main content  -->
        <div id="main" class="main">
            <div class="row">
                <!-- breadcrumb section -->
                <div class="ribbon">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Website</a>
                        </li>
                            <!-- <li>
                                <a href="#">Library</a>
                            </li>
                            <li>
                                <a href="#">Data</a>
                            </li> -->
                        </ul>
                    </div>
                    
                    <div class="col-md-12 ">

                    </div>
                    
                    <!-- main content -->
                    <div id="content">
                        <div id="sortable-panel" class="ui-sortable">
                            <div id="titr-content" class="col-md-12">
                                @include('flash')
                                <h2>Assign Users to {{ $group->name }} Group</h2>
                            </div>
                            <div class="col-md-12 ">
                                <div class="panel panel-default">
                                    <!-- <div class="panel-heading">
                                        <div class="panel-title">
                                            <i class="fa fa-table"></i>
                                            Show & Hide Data Tables Columns
                                            <div class="bars pull-right">
                                                <div class="dropdown ">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu yep-panel-menu">
                                                        <li>
                                                            <a href="#">Daily
                                                                <span class="badge pull-right"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Monthly
                                                                <span class="badge pull-right"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Yearly
                                                                <span class="badge pull-right"> 42 </span>
                                                            </a>
                                                        </li>
                                                        <li class="divider">
                                                        </li>
                                                        <li>
                                                            <a href="#">All time
                                                                <span class="badge pull-right">56</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <a href="#">
                                                    <i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i>
                                                </a>
                                                <a href="#">
                                                    <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close"
                                                        class="fa fa-times"></i>
                                                </a>
                            
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="panel-body">
                                        <form method="POST" action="{{ route('group.process_assign_staff_m',['id'=>$group->id]) }}">
                                            {{ csrf_field() }}
                                            <table id="example3" class="table table-striped table-bordered width-100 cellspace-0">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Photo</th>
                                                        <th>FirstName</th>
                                                        <th>LastName</th>
                                                        <th>Department</th>
                                                        <th>Gender</th>
                                                        <th>Role</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>


                                                    @foreach($users as $user)
                                                    <tr>
                                                        <td>
                                                            <div class="">
                                                                <input id="checkbox3" value="{{ $user->id }}" type="checkbox" name="users[]">
                                                                <label for="">

                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <img width="60px" height="60px" src="{{ asset('uploads/images/'.$user->photo.'')}}">
                                                        </td>
                                                        <td>{{$user->firstName ?? 'No Name'}}</td>
                                                        <td>{{$user->lastName ?? 'No NAme'}}</td>
                                                        <td>{{$user->department ?? 'No Dept'}}</td>
                                                        <td>{{ $user->gender ?? 'No Gender'}}</td>
                                                        <td>{{$user->role ?? 'No Role'}}</td>
                                                        <td>
                                                            {{--{{ route('ass_crs', ['uId'=>$user->id, 'cId'=>$course->id])}}--}}
                                                            <div class=" action-buttons">
                                                                <a href="{{ route("group.process_assign_staff", [$group->id, $user->id]) }}" id="pre">
                                                                    Assign to group
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>

                                        <button onclick="noSubmit()" class="btn btn-success" style="padding: 7px 39px; background-color: #004d3e !important; border-color: #004d3e!important;" data-toggle="modal" data-target="#assignDaysMass"> Mass Assign Selected Users</button>
                                    </form>

                                </div>
                            </div>
                            <!-- end panel -->
                        </div>


                        <!-- /end Admin over view .col-md-12 -->
                    </div><!-- end col-md-12 -->
                </div><!-- end #content -->
            </div><!-- end .row -->
        </div>


        <!-- Modal -->
        @foreach($users as $user)
            <div class="modal fade" id="assignDays{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Add Number Of Days</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="{{ route('new_ass_crs') }}">
                        <div class="modal-body">

                                {{ csrf_field() }}

                                <input type="hidden" id="myUId" name="user" value="{{ $user->id }}">
                                <input type="hidden" id="myCId" name="course" value="{{ $group->id }}">
                                <input type="text" name="days" placeholder="Enter Number of Days">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" {{--id="submitDays"--}} class="btn btn-primary">Submit</button>
                        </div>

                        </form>

                    </div>
                </div>
            </div>
        @endforeach



        {{--Mass assign course--}}
        <div class="modal fade" id="assignDaysMass4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Add Number Of Days</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{ route('mass_ass_crs',['id'=>$group->id]) }}">
                        <div class="modal-body">

                            {{ csrf_field() }}

                            {{--<input type="hidden" id="myUId" name="user" value="{{ $user->id }}">
                            <input type="hidden" id="myCId" name="course" value="{{ $course->id }}">--}}
                            <input type="text" name="days" placeholder="Enter Number of Days">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" {{--id="submitDays"--}} class="btn btn-primary">Submit</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>

        <!-- ./end #main  -->

        <!-- footer -->
        <div class="page-footer">               
            <div class="col-xs-12 col-sm-12 text-center">
                <strong class=""><a href="#">{{ env("APP_NAME") }} © {{ \Carbon\Carbon::today()->year }}</a> </strong>
                <a href="#">
                    <i class="fa fa-twitter-square bigger-120"></i>
                </a>
                <a href="#">
                    <i class="fa fa-facebook-square bigger-120"></i>
                </a>
                <a href="#">
                    <i class="fa fa-rss-square orange bigger-120"></i>
                </a>
            </div>          
        </div>
        <!-- /footer -->
    </div>
    <!-- end #container -->     

    <!-- General JS script library-->
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js')}}"></script>    
    <script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js')}}"></script>                        
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js')}}"></script>                                    
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js')}}"></script>                                                                        

    
    <!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
    <script type="text/javascript" src="{{ asset('admin/js/yep-script.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/yep-demo.js')}}"></script> 


    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js')}}"></script>



    <!-- Plugins Script -->
    <script type="text/javascript">

        function noSubmit2(e) {
            //e.preventDefault();

        }

        /*$('#pre').click(function (e) {
            console.log($(this)[0]['dataset']);

            var user_id = $(this)[0]['dataset']['uid'];
            var crs_id = $(this)[0]['dataset']['cid'];
            $('#myUId').val(user_id);
            $('#myCId').val(crs_id);
        });*/

            // Default Data Table Script
            $('#example1').dataTable({
                responsive: true
            });

            // Table Tools Data Tables
            // $('#example2').dataTable({
            //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
            //         "t" +
            //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            //     "oTableTools": {
            //         "aButtons": [
            //             "copy",
            //             "csv",
            //             "xls",
            //             {
            //                 "sExtends": "pdf",
            //                 "sTitle": "Yeptemplate_PDF",
            //                 "sPdfMessage": "Yeptemplate PDF Export",
            //                 "sPdfSize": "letter"
            //             },
            //             {
            //                 "sExtends": "print",
            //                 "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
            //             }
            //         ],

            //     },
            //     "autoWidth": true,
            //     responsive: true

            // });

            // Column show & hide
            $('#example3').DataTable({
                "dom": '<"pull-left"f><"pull-right"l>tip',
                "dom": 'C<"clear">lfrtip',

                "bLengthChange": false,
                responsive: true
            });


        </script>

        <!-- Google Analytics Script -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-67070957-1', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
    </html>