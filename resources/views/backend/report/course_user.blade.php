<!DOCTYPE html>
<html>
@include('layouts.admin.head_script')

<body id="mainbody">
<div id="container" class="container-fluid skin-3">
    <!-- Add Task in sidebar list modal -->

    <!--./ Add Task in sidebar list modal -->

    <!-- Add Contact in sidebar list modal -->

    <!--./ Add Contact in sidebar list modal -->

    <!-- Header -->
@include('layouts.admin.header')
<!-- /end Header -->

    <!-- sidebar menu -->
@include('layouts.admin.sidebar')
<!-- /end #sidebar -->

    <!-- main content  -->
    <div id="main" class="main">
        <div class="row">
            <!-- breadcrumb section -->
            <div class="ribbon">
                <ul class="breadcrumb" style="background: #004d3e">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Website</a>
                    </li>
                    <!-- <li>
                        <a href="#">Library</a>
                    </li>
                    <li>
                        <a href="#">Data</a>
                    </li> -->
                </ul>
            </div>

            <div class="col-md-12 ">

            </div>

            <!-- main content -->
            <div id="content">
                <div id="sortable-panel" class="ui-sortable">
                    <div id="titr-content" class="col-md-12">
                        <h2>Course User Report</h2>
                    </div>
                    <div class="col-md-12 ">
                        <div class="panel panel-default">
                            <h3>Filter</h3>
                            <form method="post" action="{{ route("course_users_report") }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-2">
                                        <select name="course_id" class="form-control chzn-select" tabindex="2" required>
                                            <option disabled selected> Select Course</option>
                                            @foreach($allCourses as $course)
                                                <option value="{{ $course->id }}">{{ $course->course_name }}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <input class="form-control" name="name" type="text" placeholder="Enter Staff Name">

                                    </div>

                                    <div class="col-sm-2">
                                        <input class="form-control" name="level" type="text" placeholder="Enter Level">

                                    </div>


                                    <div class="col-sm-2">
                                        <select name="department" class="form-control" >
                                            <option value="">Select Department</option>
                                            @foreach($depts as $dept)
                                                <option value="{{ $dept->name }}">{{ $dept->name }}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select name="group" class="form-control" >
                                            <option value="">Select Group</option>
                                            @foreach($groups as $group)
                                                <option value="{{ $group->id }}">{{ $group->name }}</option>
                                            @endforeach

                                        </select>
                                    </div>






                                    <div class="col-sm-2">
                                        <select name="completed" class="form-control chzn-select" tabindex="2">
                                            <option disabled selected> Status</option>
                                            <option value="Yes">Completed</option>
                                            <option value="No">Not Completed</option>
                                        </select>
                                    </div>


                                    {{--<div class="col-sm-2">
                                        <input class="form-control" name="bvn" type="text" placeholder="Enter BVN">

                                    </div>

                                    <div class="col-sm-2">
                                        <input class="form-control" name="phone" type="text" placeholder="Enter Phone">

                                    </div>

                                    <div class="col-sm-2">
                                        <input class="form-control" name="admission_no" type="text" placeholder="Enter Admission Number">
                                    </div>


                                     <div class="col-sm-3"></div>
--}}

                                    {{--<div class="col-sm-2">
                                        <button type="submit" class="btn btn-primary">Get Report</button>
                                    </div>--}}
                                </div>

                                    <button type="submit" class="btn btn-primary" style="background: #004d3e">Get Report</button>
                                    <a href="{{ route("course_users_report") }}?action=export" class="btn btn-primary" style="background: #004d3e">Export</a>


                            </form>
                            <div class="panel-body">
                                @include('flash')
                                <table id="example3" class="table table-striped table-bordered width-50 cellspace-0">
                                    <thead>
                                    <tr>

                                        <th>S/N</th>
                                        <th>Name</th>
                                        <th>Staff Id</th>
                                        <th>Course</th>
                                        <th>Progress</th>
                                        <th>Status</th>
                                        <th>Department</th>
                                        <th>Level</th>
                                        <th>Gender</th>
                                        <th>Attempt</th>
                                        <th>Score</th>
                                        <th>Start Date</th>
                                        <th>Expected End Date</th>

                                    </tr>
                                    </thead>
                                    @foreach($users as $user)
                                        <tbody>
                                        <tr>

                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{$user->user->full_name ?? 'No Name'}}</td>
                                            <td>{{$user->user->staff_id ?? 'No ID'}}</td>
                                            <td>{{$user->course->course_name }}</td>
                                            <td>{{  \App\Custom\Utility::userCourseProgress($user->user, $user->course) }}</td>
                                            <td>{{$user->completed }}</td>
                                            <td>{{$user->user->department }}</td>
                                            <td>{{$user->user->level }}</td>
                                            <td>{{$user->user->gender }}</td>
                                            <td>{{ quizAttempts($user->course, $user->user) }}</td>
                                            <td>{{ courseScore($user->course, $user->user) }}</td>
                                            <td>{{ \Carbon\Carbon::parse($user->created_at)->format("d-m-Y")  }}</td>
                                            <td>{{ \Carbon\Carbon::parse($user->created_at)->addDays($user->days)->format("d-m-Y")  }}</td>

                                        </tr>
                                        </tbody>
                                    @endforeach
                                </table>

                            </div>
                        </div>
                        <!-- end panel -->
                    </div>


                    <!-- /end Admin over view .col-md-12 -->
                </div><!-- end col-md-12 -->
            </div><!-- end #content -->
        </div><!-- end .row -->
    </div>
    <!-- ./end #main  -->

    <!-- footer -->
@include('layouts.admin.footer')
<!-- /footer -->
</div>
<!-- end #container -->

<!-- General JS script library-->
<script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js')}}"></script>


<!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
<script type="text/javascript" src="{{ asset('admin/js/yep-script.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/yep-demo.js')}}"></script>


<!-- Related JavaScript Library to This Pagee -->
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js')}}"></script>

<script type="text/javascript">
    function confirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x) {
            return true;
        }
        else {

            event.preventDefault();
            return false;
        }
    }

</script>



<!-- Plugins Script -->
<script type="text/javascript">

    // Default Data Table Script
    $('#example1').dataTable({
        responsive: true,
        "oTableTools": {
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "Yeptemplate_PDF",
                    "sPdfMessage": "Yeptemplate PDF Export",
                    "sPdfSize": "letter"
                },
                {
                    "sExtends": "print",
                    "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
                }
            ]
        },
    });

     $('#example2').dataTable({
         "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
             "t" +
             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
         "oTableTools": {
             "aButtons": [
                 "copy",
                 "csv",
                 "xls",
                 {
                     "sExtends": "pdf",
                     "sTitle": "Yeptemplate_PDF",
                     "sPdfMessage": "Yeptemplate PDF Export",
                     "sPdfSize": "letter"
                 },
                 {
                     "sExtends": "print",
                     "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
                 }
             ]
         },
         "autoWidth": true,
         responsive: true

     });

    // Column show & hide
    $('#example3').DataTable({
        "dom": '<"pull-left"f><"pull-right"l>tip',
        "dom": 'C<"clear">lfrtip',

        "bLengthChange": false,
        responsive: true,
        "oTableTools": {
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "Yeptemplate_PDF",
                    "sPdfMessage": "Yeptemplate PDF Export",
                    "sPdfSize": "letter"
                },
                {
                    "sExtends": "print",
                    "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
                }
            ]
        }
    });


</script>

<!-- Google Analytics Script -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-67070957-1', 'auto');
    ga('send', 'pageview');
</script>

</body>
</html>