<!DOCTYPE html>
<html>
    @include('layouts.admin.head_script')

    <body id="mainbody">
        <div id="container" class="container-fluid skin-3">
            <!-- Add Task in sidebar list modal -->
            
            <!--./ Add Task in sidebar list modal -->
            
            <!-- Add Contact in sidebar list modal -->
            
            <!--./ Add Contact in sidebar list modal -->

            <!-- Header -->
            @include('layouts.admin.header')
            <!-- /end Header -->
            
            <!-- sidebar menu -->           
            @include('layouts.admin.sidebar')
            <!-- /end #sidebar -->
            
            <!-- main content  -->
            <div id="main" class="main">
                <div class="row">
                    <!-- breadcrumb section -->
                    <div class="ribbon">
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="#">Website</a>
                            </li>
                            <!-- <li>
                                <a href="#">Library</a>
                            </li>
                            <li>
                                <a href="#">Data</a>
                            </li> -->
                        </ul>
                    </div>
                    
                    <div class="col-md-12 ">
                                
                    </div>
                    
                    <!-- main content -->
                    <div id="content">
                        <div id="sortable-panel" class="ui-sortable">
                            <div id="titr-content" class="col-md-12">
                                <h2>Course Report</h2>
                            </div>
                            <div class="col-md-12 ">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <i class="fa fa-table"></i>
                                            Filter
                                            {{--<form method="post" action="">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <select name="school_id" class="form-control chzn-select" tabindex="2" required>
                                                            <option disabled selected> Select Course</option>
                                                            @foreach($allCourses as $course)
                                                                <option value="{{ $course->id }}">{{ $course->course_name }}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>


                                                    <div class="col-sm-3">
                                                        <select name="year" class="form-control" required>
                                                            <option value="">Select Year</option>
                                                            @for($i = 2018; $i<= 2025; $i++)
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endfor

                                                        </select>
                                                    </div>



                                                    <div class="col-sm-3">
                                                        <select name="status" class="form-control chzn-select" tabindex="2">
                                                            <option disabled selected> Status</option>
                                                            <option value="Yes">Submitted</option>
                                                            <option value="No">Not Submitted</option>
                                                        </select>
                                                    </div>


                                                    --}}{{--<div class="col-sm-2">
                                                        <input class="form-control" name="bvn" type="text" placeholder="Enter BVN">

                                                    </div>

                                                    <div class="col-sm-2">
                                                        <input class="form-control" name="phone" type="text" placeholder="Enter Phone">

                                                    </div>

                                                    <div class="col-sm-2">
                                                        <input class="form-control" name="admission_no" type="text" placeholder="Enter Admission Number">
                                                    </div>--}}{{--


                                                    --}}{{-- <div class="col-sm-3"></div>--}}{{--


                                                    <div class="col-sm-2">
                                                        <button type="submit" class="btn btn-primary">Get Report</button>
                                                    </div>
                                                </div>

                                            </form>--}}

                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        @include('flash')
                                        <table id="example3" class="table table-striped table-bordered width-50 cellspace-0">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>S/N</th>
                                                    <th>Name</th>
                                                    <th>Categoty</th>
                                                    <th>Staff</th>
                                                    
                                                </tr>
                                            </thead>
                                            @foreach($courses as $course)
                                            <tbody>
                                                <tr>
                                                    
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{$course->course_name ?? 'No NAme'}}</td>
                                                    <td>{{$course->category->cat_name ?? 'No NAme'}}</td>
                                                    <td>{{$course->users()->count() ?? 'No NAme'}}</td>

                                                    {{--<td>
                                                        <div class=" action-buttons">
                                                            
                                                            <a class="green" href="{{ route('group.edit',['id'=>$course->id])}}">
                                                                <i class=" fa fa-pencil bigger-130"></i>
                                                            </a> |

                                                            <a class="red" href="{{ route('group.delete', ['id'=>$course->id])}}">
                                                                <i class=" fa fa-trash-o bigger-130" onclick="confirmDelete()"></i>
                                                            </a> |

                                                            <a href="{{ route("group.assign_staff", $course->id) }}">Assign Staff</a>|
                                                            <a href="{{ route("group.show", $course->id) }}">Show Staff</a>

                                                        </div>
                                                    </td>--}}
                                                </tr>
                                            </tbody>
                                            @endforeach
                                        </table>
                            
                                    </div>
                                </div>
                                <!-- end panel -->
                            </div>
                            

                            <!-- /end Admin over view .col-md-12 -->
                        </div><!-- end col-md-12 -->
                    </div><!-- end #content -->
                </div><!-- end .row -->
            </div>
            <!-- ./end #main  -->
            
            <!-- footer -->
            @include('layouts.admin.footer')
            <!-- /footer -->
        </div>
        <!-- end #container -->     

        <!-- General JS script library-->
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-ui/js/jquery-ui.min.js')}}"></script>    
        <script type="text/javascript" src="{{ asset('admin/vendo/bootstrap/js/bootstrap.min.js')}}"></script>                        
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-searchable/js/jquery.searchable.min.js')}}"></script>                                    
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-fullscreen/js/jquery.fullscreen.min.js')}}"></script>                                                                        
        
    
        <!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
        <script type="text/javascript" src="{{ asset('admin/js/yep-script.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/js/yep-vendors.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/js/yep-demo.js')}}"></script> 


        <!-- Related JavaScript Library to This Pagee -->
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/jquery.dataTables.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.responsive.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.tableTools.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('admin/vendo/jquery-datatables/js/dataTables.colVis.min.js')}}"></script>

        <script type="text/javascript">
            function confirmDelete()
            {
                var x = confirm("Are you sure you want to delete?");
                    if (x) {
                        return true;
                    }
                    else {

                        event.preventDefault();
                        return false;
                    }
            }
    
        </script>
        


        <!-- Plugins Script -->
        <script type="text/javascript">

            // Default Data Table Script
            $('#example1').dataTable({
                responsive: true
            });

            // Table Tools Data Tables
            // $('#example2').dataTable({
            //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>" +
            //         "t" +
            //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            //     "oTableTools": {
            //         "aButtons": [
            //             "copy",
            //             "csv",
            //             "xls",
            //             {
            //                 "sExtends": "pdf",
            //                 "sTitle": "Yeptemplate_PDF",
            //                 "sPdfMessage": "Yeptemplate PDF Export",
            //                 "sPdfSize": "letter"
            //             },
            //             {
            //                 "sExtends": "print",
            //                 "sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
            //             }
            //         ],

            //     },
            //     "autoWidth": true,
            //     responsive: true

            // });

            // Column show & hide
            $('#example3').DataTable({
                "dom": '<"pull-left"f><"pull-right"l>tip',
                "dom": 'C<"clear">lfrtip',

                "bLengthChange": false,
                responsive: true
            });


        </script>

        <!-- Google Analytics Script -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-67070957-1', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
</html>