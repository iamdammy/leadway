@extends('layouts.admin.app')
@section('content')
<div class="col-md-12 ">
								<div  class="panel panel-default">	
								@include('flash')										
									<div class="panel-body no-padding">											
										<div class="yep-profile ">
											<!-- <div class="statistic">
												<div class="padding-10">																
													<h4> <strong class="center">1,543</strong> <br> <small>Followers</small></h4>
													<h4><strong>419</strong> <br> <small>Connections</small></h4>																															
												</div>
											</div> -->
											<img class="yep-image-lg" src="{{ asset('admin/img/gallery/a9.jpg')}}" height="214" width="500" alt="Profile image example"/>
											<div class="yep-profile-text">
												<div class="col-sm-12 ">
												
													<div class="col-sm-3">
														<img  class="yep-image-profile " src="{{ asset('uploads/images/'.$user->photo.'')}}" alt="Profile image example"/>																	
														<!-- <div class="profile-info">
															<h4 ><small>User Role</small><br>Developers</h4>															
															<h4 ><small>Role</small><br>{{$user->role->name ?? 'No Role'}}</h4><br>

															<h4 ><small>Department</small><br>{{$user->department ?? 'No Dep'}}</h4><br>

														</div>															 -->															
													</div>
													
													<div class="col-sm-6 no-padding ">	
														<h4>{{$user->lastName}} <span class="semi-bold">{{$user->firstName}}</span>
														<br>
														<small> <strong>Role : </strong>{{$user->role->name ?? 'No Role'}}</small></h4>
			
														<ul class="list-unstyled">
															<li>
																<p class="text-muted">
																	<span><i class="fa fa-transgender"></i>&nbsp;&nbsp;{{$user->gender}}</span>
																</p>
															</li>
															<li>
																<p class="text-muted">
																	<i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="yep@yeptemplate.com">{{$user->email}}</a>
																</p>
															</li>
															<!-- <li>
																<p class="text-muted">
																	<span><i class="fa fa-skype"></i>&nbsp;&nbsp;Branch : {{ $user->branch}}</span>
																</p>
															</li> -->

															<li>
																<p class="text-muted">
																	<span><i class="fa fa-calendar"></i>&nbsp;&nbsp;Department : {{ $user->department}}</span>
																</p>
															</li>
															<!-- <li>
																<p class="text-muted">
																	<span><i class="fa fa-twitter"></i>&nbsp;&nbsp; twitter.com/malinda</span>
																</p>
															</li>
															<li>
																<p class="text-muted">
																	 <span><i class="fa fa-map-marker"></i>&nbsp;&nbsp; USA, CA</span>	
																</p>
															</li> -->
														</ul>
														<br>
														<!-- <p class="font-md">
															<i>About me...</i>
														</p>
														<p>

															Saperet inermis vulputate ex sit, nec sale putant definitiones et. Qui soleat platonem id, ea vim tritani utroque percipitur. Vim te meliore legendos, utroque nominati intellegam vis at. Ne eos utamur
			
														</p> -->
													</div>
													
													<div class="col-sm-3 bg-grey">
														<br>
														<a href="{{ route('admin_edit_pro')}}">
															<button class="btn btn-xs btn-success btn-block"><i class="fa fa-pencil"> </i> 
																Edit Profile
															</button>	
														</a>	
														
														{{-- <a href="{{ route('all_users')}}">
															<button class="btn btn-xs btn-primary btn-block"><i class="fa fa-link"> </i> All Users </button>	
														</a> --}}															
																												
														
														<!-- <h4>Photos</h4>
														<ul class="list-inline friends-list">
															<li><img src="{{ asset('admin/img/avatars/male.png')}}" alt="friend-1">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar4.png')}}" alt="friend-2">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar3.png')}}" alt="friend-3">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar2.png')}}" alt="friend-4">
															</li>
															<li><img src="{{ asset('admin/img/avatars/male.png')}}" alt="friend-5">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar.png')}}" alt="friend-6">
															</li>
															
														</ul> -->
			
														<!-- <h4>Recent visitors</h4>
														<ul class="list-inline friends-list">
															<li><img src="{{ asset('admin/img/avatars/avatar4.png')}}" alt="friend-1">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar2.png')}}" alt="friend-2">
															</li>
															<li><img src="{{ asset('admin/img/avatars/avatar3.png')}}" alt="friend-3">
															</li>
														</ul>			 -->																	
													</div>


												</div>
											</div>
										</div>

									</div>	
																					
								</div><!-- end panel -->									
							</div>
@stop