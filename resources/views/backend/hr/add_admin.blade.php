@extends('layouts.admin.app')
@section('content')
    <div id="titr-content" class="col-md-12">


        @include('flash')
        {{--<div class="actions">
            <a href="{{ route('add_admin')}}"><button class="btn btn-success ">Add new</button></a>
            <a href="{{ route('all_admin')}}"><button class="btn btn-primary dammy ">All Admins</button></a>
        </div>--}}
    </div>

    <div class="col-md-6 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-check-square-o"></i>
                    Add Admin Form
                    <div class="bars pull-right">
                        <!-- <div class="dropdown ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </a>
                            <ul class="dropdown-menu yep-panel-menu">
                                <li>
                                    <a href="#">Daily<span class="badge pull-right"></span></a>
                                </li>
                                <li>
                                    <a href="#">Monthly<span class="badge pull-right"></span></a>
                                </li>
                                <li>
                                    <a href="#">Yearly<span class="badge pull-right"> 42 </span></a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="#">All time<span class="badge pull-right">56</span></a>
                                </li>
                            </ul>
                        </div> -->


                        {{--<a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom"
                                       title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom"
                                       title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip"
                                       data-placement="bottom" title="Close" class="fa fa-times"></i></a>--}}

                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="{{ route('create_admin')}}" method="POST"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}


                    {{--<div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Company
                        </label>
                        <div class="col-sm-9">
                            <select name="client_id" required="true" class="form-control">
                                <option value="">Select Company</option>
                                @foreach($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->company_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>--}}


                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Staff
                        </label>
                        <div class="col-sm-9">
                            <select name="user_id" required="true" class="form-control">
                                <option value="">Select Staff</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>



                    {{--<div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Firstname
                            <span style="color: red">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" value="{{ old("firstName") }}" name="firstName" placeholder="First Name" id="form-field-1"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Lastname
                            <span style="color: red">*</span>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" value="{{ old("lastName") }}" style="color: red" name="lastName" placeholder="Last Name" id="form-field-1"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Email
                            <span style="color: red">*</span>

                        </label>
                        <div class="col-sm-9">
                            <input type="email" value="{{ old("email") }}" name="email" placeholder="Email" id="form-field-1" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Gender
                            <span style="color: red">*</span>

                        </label>
                        <div class="col-sm-9">
                            <select name="gender" class="form-control">
                                <option value="">Select a Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Department
                            <span style="color: red">*</span>

                        </label>
                        <div class="col-sm-9">
                            <select name="department" class="form-control">
                                <option value="">Select a department</option>
                                @foreach($depts as $dept)
                                    <option value="{{ $dept->name }}">{{ $dept->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>--}}


                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Role
                            <span style="color: red">*</span>

                        </label>
                        <div class="col-sm-9">
                            <select name="role_id" class="form-control" required="true">
                                <option value="">Select a role</option>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">
                            Change Password
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="password" placeholder="Change User Password" id="form-field-1" value="" class="form-control">
                        </div>
                    </div>

                     -->

                    <fieldset>
                        <legend></legend>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-21">
                                Photo
                            </label>
                            <div class="col-sm-9">
                                <input type="file" name="photo" id="form-file-20" class="form-control yep-file-input">
                            </div>


                        </div>


                        <button type="submit" class="btn btn-primary btn-lg btn-block dammy">
                            Submit
                        </button>
                    </fieldset>
                </form>

            </div>

        </div><!-- end panel -->
    </div>





@stop