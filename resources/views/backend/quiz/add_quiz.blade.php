@extends('layouts.admin.app')
@section('content')
<div id="content">
                    <div id="sortable-panel" class="ui-sortable">
                        <div id="titr-content" class="col-md-12">
                            <h2>Add a new Question</h2>
                            
                        </div>

                        <div class="col-md-12 ">
                            <div class="panel panel-default">
                                
                                <div class="panel-body">
                        			@include('flash')
                                    <hr>
                                    <form action="{{ route('quiz_crt')}}" method="POST" role="form" id="form1" novalidate="novalidate">
                                    	{{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="errorHandler alert alert-danger no-display">
                                                    <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                </div>
                                                <div class="successHandler alert alert-success no-display">
                                                    <i class="fa fa-ok"></i> Your form validation is successful!
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Course
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <select name="course_id" id="myCrs" class="form-control" required>
                                                        <option value="">Select Course</option>
                                                        @foreach($courses as $course)
                                                        <option value="{{ $course->id}}">{{ $course->course_name}}</option>
                                                         @endforeach
                                                    </select>
                                                    
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Module
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <select name="module_id" id="myMod" class="form-control">
                                                        {{-- <option value="">Select Course</option>
                                                        @foreach($mods as $mod)
                                                        <option value="{{ $mod->id}}">{{ $mod->course_name}}</option>
                                                         @endforeach --}}
                                                    </select>
                                                    
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Question
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="text" placeholder="Insert Question" class="form-control" id="firstname" name="question">
                                                </div>

                                                 <div class="form-group">
                                                    <label class="control-label">
                                                        Option One
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="text" placeholder="Insert Option One" class="form-control" id="firstname" name="option1">
                                                </div>

                                                 <div class="form-group">
                                                    <label class="control-label">
                                                        Option Two
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="text" placeholder="Insert Option Two" class="form-control" id="firstname" name="option2">
                                                </div>

                                                 <div class="form-group">
                                                    <label class="control-label">
                                                        Option Three
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="text" placeholder="Insert Option Three" class="form-control" id="firstname" name="option3">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        Option Four
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="text" placeholder="Insert Option Four" class="form-control" id="firstname" name="option4">
                                                </div>
                                                 <div class="form-group">
                                                    <label class="control-label">
                                                        Answer
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input type="text" placeholder="Insert Option One" class="form-control" id="firstname" name="answer">
                                                </div>
                                                
                                            <div class="col-md-4">
                                                <button class="btn btn-success btn-block dammy" type="submit">
                                                    Save
                                                    <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                        
                        
                                </div>
                            </div>
                            <!-- end panel -->
                        </div>


                        <!-- /end Admin over view .col-md-12 -->
                    </div>
                    <!-- end col-md-12 -->
                </div>
@stop
<script src="{{ asset('admin/js/jquery.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function (){
        $('#myCrs').change(function(){
            let crsId = $('#myCrs').val();
            let getUrl = '{{ route('get_mods') }}';

            $.ajax({
                type : 'GET',
                url : getUrl,
                data : {'id':crsId},
                success : function(data) {
                    $('#myMod').html(data);
                },
                error : function(data) {

                }

            });

        });

    });
    
</script>

<script type="text/javascript">
            $(function () {
                var form1 = $('#form1');
                var errorHandler1 = $('.errorHandler', form1);
                var successHandler1 = $('.successHandler', form1);
                $.validator.addMethod("FullDate", function () {
                    //if all values are selected
                    if ($("#dd").val() != "" && $("#mm").val() != "" && $("#yyyy").val() != "") {
                        return true;
                    } else {
                        return false;
                    }
                }, 'Please select a day, month, and year');
                form1.validate({
                    errorElement: "span", // contain the error msg in a span tag
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                            error.insertAfter($(element).closest('.form-group').children('div').children().last());
                        } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                            error.insertAfter($(element).closest('.form-group').children('div'));
                        } else {
                            error.insertAfter(element);
                            // for other inputs, just perform default behavior
                        }
                    },
                    ignore: "",
                    rules: {
                        firstname: {
                            minlength: 2,
                            required: true
                        },
                        lastname: {
                            minlength: 2,
                            required: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            minlength: 6,
                            required: true
                        },
                        password_again: {
                            required: true,
                            minlength: 5,
                            equalTo: "#password"
                        },
                        yyyy: "FullDate",
                        gender: {
                            required: true
                        },
                        zipcode: {
                            required: true,
                            number: true,
                            minlength: 5,
                            minlength: 5
                        },
                        city: {
                            required: true
                        },
                        newsletter: {
                            required: true
                        }
                    },
                    messages: {
                        firstname: "Please specify your first name",
                        lastname: "Please specify your last name",
                        email: {
                            required: "We need your email address to contact you",
                            email: "Your email address must be in the format of name@domain.com"
                        },
                        gender: "Please check a gender!"
                    },
                    groups: {
                        DateofBirth: "dd mm yyyy",
                    },
                    invalidHandler: function (event, validator) { //display error alert on form submit
                        successHandler1.hide();
                        errorHandler1.show();
                    },
                    highlight: function (element) {
                        $(element).closest('.help-block').removeClass('valid');
                        // display OK icon
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                        // add the Bootstrap error class to the control group
                    },
                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error');
                        // set error class to the control group
                    },
                    success: function (label, element) {
                        label.addClass('help-block valid');
                        // mark the current input as valid and display OK icon
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                    },
                    submitHandler: function (form) {
                        successHandler1.show();
                        errorHandler1.hide();
                        // submit form
                        //$('#form').submit();
                    }
                });
               
            });
        </script>

