@extends('layouts.admin.app')
@section('content')
    @php use Carbon\Carbon; @endphp


    {{--
    <div id="content">
                            <div id="sortable-panel" class="ui-sortable">
                                @include('flash')

                                <!-- Course Table -->
                                <!-- Admin over view .col-md-12 -->
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                Courses
                                                <div class="bars pull-right">
                                                    <div class="dropdown ">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                        <ul class="dropdown-menu yep-panel-menu">
                                                            <li>
                                                                <a href="#">Daily
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Monthly
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Yearly
                                                                    <span class="badge pull-right"> 42 </span>
                                                                </a>
                                                            </li>
                                                            <li class="divider">
                                                            </li>
                                                            <li>
                                                                <a href="#">All time
                                                                    <span class="badge pull-right">56</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <a href="#">
                                                        <i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">

                                            <table id="example1" class="table table-striped table-bordered width-100 cellspace-0">
                                                <thead>
                                                    <tr>
                                                        <th>S/N</th>
                                                        <th>Our Recent Courses</th>
                                                        <th>{{ count($courses) }}</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @php $i = 1; @endphp
                                                    @foreach($courses as $course)
                                                    <tr>
                                                        <td>{{ $i }}</td>
                                                        <td>{{ $course->course_name }}</td>
                                                        <td>{{ $course->totalStaff() }}</td>
                                                    </tr>
                                                    @php $i++; @endphp
                                                    @endforeach
                                                    <!-- <tr>
                                                        <td>2</td>
                                                        <td>Computer Science 102</td>
                                                        <td>5 Staffs</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Computer Science 102</td>
                                                        <td>5 Staffs</td>
                                                    </tr> -->
                                                </tbody>
                                            </table>
                                            <a href="{{ route('crt_course') }}" class="btn btn-info has-ripple">Create Course

                                            </a>
                                        </div>
                                    </div>
                                    <!-- end panel -->
                                </div>

                                <!-- Course Request -->
                                <!-- Admin over view .col-md-12 -->
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                Courses Request
                                                <div class="bars pull-right">
                                                    <div class="dropdown ">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                        <ul class="dropdown-menu yep-panel-menu">
                                                            <li>
                                                                <a href="#">Daily
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Monthly
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Yearly
                                                                    <span class="badge pull-right"> 42 </span>
                                                                </a>
                                                            </li>
                                                            <li class="divider">
                                                            </li>
                                                            <li>
                                                                <a href="#">All time
                                                                    <span class="badge pull-right">56</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <a href="#">
                                                        <i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">

                                            <table id="request" class="table table-striped table-bordered width-100 cellspace-0">
                                                <thead>
                                                    <tr>
                                                        <th>List of Students with Request</th>
                                                        <th>{{ count($reqs) }}</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach($reqs as $req)
                                                    <tr>
                                                        <td>
                                                            <p class="text-subhead">
                                                                <a href="#">
                                                                    <img width="40px" height="40px" src="http://demo.fliplms.com/images/demo.jpg " alt="" class="prf-img">
                                                                </a> &nbsp;
                                                                <a href="#">{{ $req->firstName }} {{ $req->lastName }}</a>
                                                                <span class="prf-date">{{ date('d-m-Y', strtotime($req->created_at)) }} ( {{ $req->mydate() }} )</span>
                                                            </p>
                                                            <p class="">
                                                                <span class="">Course:</span>
                                                                <a href="#">{{ $req->course_name }}</a>
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('us_crs_req', ['id'=>$req->user_id, 'cid'=>$req->course_id]) }}" class="btn btn-info has-ripple">
                                                                <i class="fa fa-reply"></i> View Details
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    <!-- <tr>
                                                        <td>
                                                            <p class="text-subhead">
                                                                <a href="#">
                                                                    <img width="40px" height="40px" src="http://demo.fliplms.com/images/demo.jpg " alt="" class="prf-img">
                                                                </a> &nbsp;
                                                                <a href="#">Semi Developer</a>
                                                                <span class="prf-date">1 year ago</span>
                                                            </p>
                                                            <p class="">
                                                                <span class="">Course:</span>
                                                                <a href="#">Computer Science Principles</a>
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" class="btn btn-info has-ripple">
                                                                <i class="fa fa-reply"></i> declined
                                                            </a>
                                                        </td>
                                                    </tr> -->
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- end panel -->
                                </div>

                                <!-- Video Logs -->
                                <!-- Admin over view .col-md-12 -->
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                Video Logs
                                                <div class="bars pull-right">
                                                    <div class="dropdown ">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                        <ul class="dropdown-menu yep-panel-menu">
                                                            <li>
                                                                <a href="#">Daily
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Monthly
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Yearly
                                                                    <span class="badge pull-right"> 42 </span>
                                                                </a>
                                                            </li>
                                                            <li class="divider">
                                                            </li>
                                                            <li>
                                                                <a href="#">All time
                                                                    <span class="badge pull-right">56</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <a href="#">
                                                        <i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">

                                            <table id="video-logs" class="table table-striped table-bordered width-100 cellspace-0">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 180px;">Latest course engagement</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-default"> 6 Aug 2016</span>
                                                        </td>
                                                        <td>
                                                             Wale Akanbi
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" class="btn btn-info has-ripple">
                                                                <i class="fa fa-play"></i> Watch Video
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-default"> 6 Aug 2016</span>
                                                        </td>
                                                        <td>
                                                            Wale Akanbi
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" class="btn btn-info has-ripple">
                                                                <i class="fa fa-play"></i> Watch Video
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-default"> 6 Aug 2016</span>
                                                        </td>
                                                        <td>
                                                            Wale Akanbi
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" class="btn btn-info has-ripple">
                                                                <i class="fa fa-play"></i> Watch Video
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- end panel -->
                                </div>

                                <!-- Staff -->
                                <!-- Admin over view .col-md-12 -->
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                Staff ({{ count($users) }})
                                                <div class="bars pull-right">
                                                    <div class="dropdown ">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                        <ul class="dropdown-menu yep-panel-menu">
                                                            <li>
                                                                <a href="#">Daily
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Monthly
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Yearly
                                                                    <span class="badge pull-right"> 42 </span>
                                                                </a>
                                                            </li>
                                                            <li class="divider">
                                                            </li>
                                                            <li>
                                                                <a href="#">All time
                                                                    <span class="badge pull-right">56</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <a href="#">
                                                        <i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">

                                            <table id="staff" class="table table-striped table-bordered width-100 cellspace-0">
                                                <thead>
                                                    <tr>
                                                        <th>Our recent Registered Staff members</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach($users as $user)
                                                    <tr>
                                                        <td>
                                                             {{ $user->fullname() }}
                                                        </td>
                                                        <td>

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    <!-- <tr>
                                                        <td>
                                                            Management-Ikeja
                                                        </td>
                                                        <td>

                                                        </td>
                                                    </tr> -->
                                                </tbody>
                                            </table>
                                            <a href="{{ route('create_user') }}" class="btn btn-info has-ripple">Add Staff</a>
                                        </div>
                                    </div>
                                    <!-- end panel -->
                                </div>

                                <!-- Instructor -->
                                <!-- Admin over view .col-md-12 -->
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                Instructor
                                                <div class="bars pull-right">
                                                    <div class="dropdown ">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                        <ul class="dropdown-menu yep-panel-menu">
                                                            <li>
                                                                <a href="#">Daily
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Monthly
                                                                    <span class="badge pull-right"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Yearly
                                                                    <span class="badge pull-right"> 42 </span>
                                                                </a>
                                                            </li>
                                                            <li class="divider">
                                                            </li>
                                                            <li>
                                                                <a href="#">All time
                                                                    <span class="badge pull-right">56</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <a href="#">
                                                        <i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i>
                                                    </a>
                                                    <a href="#">
                                                        <i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">

                                            <table id="instructor" class="table table-striped table-bordered width-100 cellspace-0">
                                                <thead>
                                                    <tr>
                                                        <th>Our recent Instructors</th>
                                                        <th>{{ count($facs) }}</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach($facs as $fac)
                                                    <tr>
                                                        <td>
                                                            {{ $fac->name }}
                                                        </td>
                                                        <td>

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <a href="{{ route('fac_form') }}" class="btn btn-info has-ripple">Add Instructor</a>
                                        </div>
                                    </div>
                                    <!-- end panel -->
                                </div>

                            </div><!-- end col-md-12 -->
                        </div>
                                    --}}


    <!-- main content  -->
    <div id="main" class="main">
        <div class="row">
            <!-- breadcrumb section -->
            <!--<div class="ribbon">-->
            <!--	<ul class="breadcrumb">-->
            <!--		<li>-->
            <!--			<i class="fa fa-home"></i>-->
            <!--			<a href="#">Home</a>-->
            <!--		</li>-->
            <!--		<li>-->
            <!--			<a href="#">Library</a>-->
            <!--		</li>-->
            <!--		<li>-->
            <!--			<a href="#">Data</a>-->
            <!--		</li>-->
            <!--	</ul>-->
            <!--</div>-->

            <!-- main content -->
            <div id="content" class="col-md-12">
                <div id="sortable-panel" class="">
                    <div id="titr-content" class="col-md-12">
                        <h2>Dashboard</h2>
                        <!--<h5>Responsive admin template dashboard...</h5>-->
                        {{--@if($admin->password_changed==0)
                        <div class="alert alert-danger">
                          <strong>Change Dafault Password</strong>
                        </div>
                        @endif--}}
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <a href="{{ route('all_users')}}">
                                        <div class="col-sm-3">
                                            <div class="card-body card-body-dash dash-color1">
                                                <div class="media">
                                                    <div class="icon-dash">
                                                        <img src="{{ asset('uploads/lms-img/company-workers.png')}}"
                                                             class="img-responsive dash-img" alt="Image">
                                                    </div>
                                                    <div class="media-body white">
                                                        <p class="no-margin">Number of Staff</p>
                                                        <h5 class="media-head">{{count($staffs)}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="{{ route('all_courses')}}">
                                        <div class="col-sm-3">
                                            <div class="card-body card-body-dash dash-color3">
                                                <div class="media">
                                                    <div class="icon-dash">
                                                        <img src="{{ asset('uploads/lms-img/online-course.png')}}"
                                                             class="img-responsive dash-img" alt="Image">
                                                    </div>
                                                    <div class="media-body white">
                                                        <p class="no-margin">Number of Courses</p>
                                                        <h5 class="media-head">{{count($courses)}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="{{ route('cats')}}">
                                        <div class="col-sm-3">
                                            <div class="card-body card-body-dash dash-color2">
                                                <div class="media">
                                                    <div class="icon-dash">
                                                        <img src="{{ asset('uploads/lms-img/online-course.png')}}"
                                                             class="img-responsive dash-img" alt="Image">
                                                    </div>
                                                    <div class="media-body white">
                                                        <p class="no-margin">Course Categories</p>
                                                        <h5 class="media-head">{{count($cats)}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="{{ route('depts')}}">
                                        <div class="col-sm-3">
                                            <div class="card-body card-body-dash dash-color4">
                                                <div class="media">
                                                    <div class="icon-dash">
                                                        <img src="{{ asset('uploads/lms-img/profile.png')}}"
                                                             class="img-responsive dash-img" alt="Image">
                                                    </div>
                                                    <div class="media-body white">
                                                        <p class="no-margin">Number of Departments</p>
                                                        <h5 class="media-head">{{count($depts)}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </a>
                                <hr>
                                {{--<div class="row">
                                    <a href="{{ route('facs')}}">
                                        <div class="col-sm-3">
                                            <div class="card-body card-body-dash dash-color4">
                                                <div class="media">
                                                    <div class="icon-dash">
                                                        <img src="{{ asset('uploads/lms-img/instructor-lecture-with-sceen-projection-tool.png')}}"
                                                             class="img-responsive dash-img" alt="Image">
                                                    </div>
                                                    <div class="media-body white">
                                                        <p class="no-margin">Number of Instructors</p>
                                                        <h5 class="media-head">{{count($facs)}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="{{ route('hr_licences')}}">
                                        <div class="col-sm-3">
                                            <div class="card-body card-body-dash dash-color2">
                                                <div class="media">
                                                    <div class="icon-dash">
                                                        <img src="{{ asset('uploads/lms-img/driving-license.png')}}"
                                                             class="img-responsive dash-img" alt="Image">
                                                    </div>
                                                    <div class="media-body white">
                                                        <p class="no-margin">Number of Licenses</p>
                                                        @php $total_licences=0; @endphp
                                                        @foreach($licences as $licence)
                                                            @php $total_licences=$total_licences+$licence->licences; @endphp
                                                        @endforeach
                                                        <h5 class="media-head">{{$total_licences}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="{{ route('req_courses')}}">
                                        <div class="col-sm-3">
                                            <div class="card-body card-body-dash dash-color3">
                                                <div class="media">
                                                    <div class="icon-dash">
                                                        <img src="{{ asset('uploads/lms-img/online-course.png')}}"
                                                             class="img-responsive dash-img" alt="Image">
                                                    </div>
                                                    <div class="media-body white">
                                                        <p class="no-margin">Requested Courses</p>
                                                        <h5 class="media-head">{{count($reqs)}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <div class="col-sm-3">
                                        <div class="card-body card-body-dash dash-color1">
                                            <div class="media">
                                                <div class="icon-dash">
                                                    <img src="{{ asset('uploads/lms-img/profile.png')}}"
                                                         class="img-responsive dash-img" alt="Image">
                                                </div>
                                                <div class="media-body white">
                                                    <p class="no-margin">News</p>
                                                    <h5 class="media-head"></h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3>Recently Registered Staff</h3>
                                <div class="table-responsive">
                                    <table class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Department</th>
                                            <!-- <th>No of Courses Enrolled</th> -->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($users as $user)
                                            @php $i=$i+1; @endphp
                                            <tr>
                                                <td>#{{$i}}</td>
                                                <td>{{$user->firstName}} {{$user->lastName}}</td>
                                                <td>{{$user->department}}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <hr>
                                    <a href="{{ route('all_users')}}">
                                        <button class="btn btn-link btn-block margin-b-0">View All</button>
                                    </a>

                                </div>
                            </div>
                        </div>


                        {{--<p>
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Button with data-target
                            </button>
                        </p>--}}


                        {{--<div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                            </div>
                        </div>
--}}


                    </div>
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end #content -->
        </div>
        <!-- end .row -->
    </div>
    <!-- ./end #main  -->
@stop