@extends('layouts.admin.app')
@section('content')
<div id="content">
						<div id="sortable-panel" class="ui-sortable">
							<div id="titr-content" class="col-md-12">
								<h2>Dashboard</h2>
								<h5>Responsive admin template dashboard...</h5>
							</div>
							<!-- Admin over view .col-md-12 -->
							<div class="col-md-12 ">
								<div class="panel panel-default">
									<div class="panel-body">
							
										<i class="glyphicon glyphicon-stats"></i>
										<b>Admin Overview</b>
										<div class="bars pull-right">
											<div id="reportrange" class="pull-right daterange hidden-xs">
												<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
												<span>November 12, 2017 - December 11, 2017</span>
												<b class="caret"></b>
											</div>
										</div>
										<hr>
										<div class="row">
											<!-- progress section -->
											<div class="col-sm-3">
												<div class="headprogress">
													<strong>Tasks</strong>
													<strong class="progress-value">60/100
														<i class="fa fa-tasks"></i>
													</strong>
												</div>
												<div class="progress progress-xs">
													<div class="progress-bar progress-bar-info" role="progressbar" style="width: 60%;">
													</div>
												</div>
							
												<div class="headprogress">
													<strong>Capacity</strong>
													<strong class="progress-value">40/100 GB
														<i class="fa fa-pie-chart"></i>
													</strong>
												</div>
												<div class="progress  progress-xs">
													<div class="progress-bar progress-bar-info" role="progressbar" style="width: 40%;">
													</div>
												</div>
							
												<div class="headprogress">
													<strong>Email</strong>
													<strong class="progress-value">70%
														<i class="fa fa-envelope"></i>
													</strong>
												</div>
												<div class="progress progress-xs">
													<div class="progress-bar progress-bar-info" role="progressbar" style="width: 70%;">
													</div>
												</div>
							
												<div class="headprogress">
													<strong>Close request</strong>
													<strong class="progress-value">90%
														<i class="fa fa-ticket"></i>
													</strong>
												</div>
												<div class="progress progress-xs">
													<div class="progress-bar progress-bar-info" role="progressbar" style="width: 90%;">
													</div>
												</div>
							
												<a class="btn btn-success btn-block">
													<i class="fa fa-download"></i> Download
													<strong>PDF</strong>
												</a>
												<a class="btn btn-default btn-block">
													<i class="fa fa-file-o"></i> Report a
													<strong>bug</strong>
												</a>
											</div>
											<!-- ./preogress section -->
											<!-- chart section -->
											<div class="col-sm-6">
												<div class="well well-sm well-light padding-10">
													<h4>
														Composite
														<strong>Chart</strong>
														<a href="javascript:void(0);" class="pull-right ">
															<i class="fa fa-refresh"></i>
														</a>
													</h4>
													<div id="areaChart" class="chart-dashboard" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
														<svg height="205" version="1.1" width="502" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;">
															<desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
															<defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
															<text x="36.171875" y="170" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;"
															 font-size="12px" font-family="sans-serif" font-weight="normal">
																<tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan>
															</text>
															<path fill="none" stroke="#aaaaaa" d="M48.671875,170H477" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
															<text x="36.171875" y="133.75" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;"
															 font-size="12px" font-family="sans-serif" font-weight="normal">
																<tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">7.5</tspan>
															</text>
															<path fill="none" stroke="#aaaaaa" d="M48.671875,133.75H477" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
															<text x="36.171875" y="97.5" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;"
															 font-size="12px" font-family="sans-serif" font-weight="normal">
																<tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">15</tspan>
															</text>
															<path fill="none" stroke="#aaaaaa" d="M48.671875,97.5H477" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
															<text x="36.171875" y="61.25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;"
															 font-size="12px" font-family="sans-serif" font-weight="normal">
																<tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">22.5</tspan>
															</text>
															<path fill="none" stroke="#aaaaaa" d="M48.671875,61.25H477" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
															<text x="36.171875" y="25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;"
															 font-size="12px" font-family="sans-serif" font-weight="normal">
																<tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30</tspan>
															</text>
															<path fill="none" stroke="#aaaaaa" d="M48.671875,25H477" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
															<text x="219.95621920058406" y="182.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;"
															 font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)">
																<tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2020</tspan>
															</text>
															<path fill="#3d3df4" stroke="none" d="M48.671875,150.66666666666666C55.80546532784267,145.83333333333331,70.07264598352802,131.9375,77.20623631137069,131.33333333333334C84.33982663921336,130.72916666666669,98.60700729489871,147.04001367989056,105.74059762274138,145.83333333333334C112.89373203367403,144.6233470132239,127.20000085553933,121.06167350661195,134.35313526647198,121.66666666666667C141.48672559431466,122.27000683994528,155.75390625,154.29166666666666,162.88749657784268,150.66666666666666C170.02108690568537,147.04166666666666,184.28826756137067,93.27083333333333,191.42185788921336,92.66666666666667C198.55544821705604,92.0625,212.82262887274138,142.81663246694026,219.95621920058406,145.83333333333334C227.10935361151672,148.85829913360695,241.415622433382,117.43832649338806,248.56875684431466,116.83333333333334C255.70234717215735,116.22999316005473,269.9695278278427,141,277.10311815568537,141C284.23670848352805,141,298.50388913921336,131.33333333333334,305.63747946705604,116.83333333333334C312.7710697948987,102.33333333333334,327.03825045058403,26.206680346557228,334.1718407784267,25C341.32497518935935,23.79001367989056,355.6312440112247,91.43684450524398,362.7843784221573,107.16666666666667C369.91796875,122.85351117191064,384.18514940568537,144.625,391.31873973352805,150.66666666666666C398.45233006137073,156.70833333333331,412.71951071705604,154.89583333333334,419.8531010448987,155.5C426.9866913727414,156.10416666666666,441.2538720284267,157.31002051983583,448.3874623562694,155.5C455.54059676720203,153.68502051983583,469.84686558906736,144.625,477,141L477,170L48.671875,170Z"
															 fill-opacity="0.6" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.6;"></path>
															<path fill="none" stroke="#0000ff" d="M48.671875,150.66666666666666C55.80546532784267,145.83333333333331,70.07264598352802,131.9375,77.20623631137069,131.33333333333334C84.33982663921336,130.72916666666669,98.60700729489871,147.04001367989056,105.74059762274138,145.83333333333334C112.89373203367403,144.6233470132239,127.20000085553933,121.06167350661195,134.35313526647198,121.66666666666667C141.48672559431466,122.27000683994528,155.75390625,154.29166666666666,162.88749657784268,150.66666666666666C170.02108690568537,147.04166666666666,184.28826756137067,93.27083333333333,191.42185788921336,92.66666666666667C198.55544821705604,92.0625,212.82262887274138,142.81663246694026,219.95621920058406,145.83333333333334C227.10935361151672,148.85829913360695,241.415622433382,117.43832649338806,248.56875684431466,116.83333333333334C255.70234717215735,116.22999316005473,269.9695278278427,141,277.10311815568537,141C284.23670848352805,141,298.50388913921336,131.33333333333334,305.63747946705604,116.83333333333334C312.7710697948987,102.33333333333334,327.03825045058403,26.206680346557228,334.1718407784267,25C341.32497518935935,23.79001367989056,355.6312440112247,91.43684450524398,362.7843784221573,107.16666666666667C369.91796875,122.85351117191064,384.18514940568537,144.625,391.31873973352805,150.66666666666666C398.45233006137073,156.70833333333331,412.71951071705604,154.89583333333334,419.8531010448987,155.5C426.9866913727414,156.10416666666666,441.2538720284267,157.31002051983583,448.3874623562694,155.5C455.54059676720203,153.68502051983583,469.84686558906736,144.625,477,141"
															 stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
															<circle cx="48.671875" cy="150.66666666666666" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="77.20623631137069" cy="131.33333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="105.74059762274138" cy="145.83333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="134.35313526647198" cy="121.66666666666667" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="162.88749657784268" cy="150.66666666666666" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="191.42185788921336" cy="92.66666666666667" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="219.95621920058406" cy="145.83333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="248.56875684431466" cy="116.83333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="277.10311815568537" cy="141" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="305.63747946705604" cy="116.83333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="334.1718407784267" cy="25" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="362.7843784221573" cy="107.16666666666667" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="391.31873973352805" cy="150.66666666666666" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="419.8531010448987" cy="155.5" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="448.3874623562694" cy="155.5" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="477" cy="141" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<path fill="#3d3df4" stroke="none" d="M48.671875,165.16666666666666C55.80546532784267,166.375,70.07264598352802,170,77.20623631137069,170C84.33982663921336,169.39583333333334,98.60700729489871,160.33333333333334,105.74059762274138,160.33333333333334C112.89373203367403,160.33333333333334,127.20000085553933,169.3950068399453,134.35313526647198,170C141.48672559431466,170,155.75390625,166.97916666666666,162.88749657784268,165.16666666666666C170.02108690568537,163.35416666666666,184.28826756137067,155.5,191.42185788921336,155.5C198.55544821705604,155.5,212.82262887274138,166.37334701322388,219.95621920058406,165.16666666666666C227.10935361151672,163.95668034655722,241.415622433382,146.43832649338805,248.56875684431466,145.83333333333334C255.70234717215735,145.22999316005473,269.9695278278427,159.125,277.10311815568537,160.33333333333334C284.23670848352805,161.54166666666669,298.50388913921336,155.5,305.63747946705604,155.5C312.7710697948987,155.5,327.03825045058403,158.5233128134975,334.1718407784267,160.33333333333334C341.32497518935935,162.1483128134975,355.6312440112247,170,362.7843784221573,170C369.91796875,170,384.18514940568537,165.16666666666669,391.31873973352805,160.33333333333334C398.45233006137073,155.50000000000003,412.71951071705604,130.125,419.8531010448987,131.33333333333334C426.9866913727414,132.54166666666669,441.2538720284267,165.1732786137711,448.3874623562694,170C455.54059676720203,170,469.84686558906736,170,477,170L477,170L48.671875,170Z"
															 fill-opacity="0.6" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.6;"></path>
															<path fill="none" stroke="#0000ff" d="M48.671875,165.16666666666666C55.80546532784267,166.375,70.07264598352802,170,77.20623631137069,170C84.33982663921336,169.39583333333334,98.60700729489871,160.33333333333334,105.74059762274138,160.33333333333334C112.89373203367403,160.33333333333334,127.20000085553933,169.3950068399453,134.35313526647198,170C141.48672559431466,170,155.75390625,166.97916666666666,162.88749657784268,165.16666666666666C170.02108690568537,163.35416666666666,184.28826756137067,155.5,191.42185788921336,155.5C198.55544821705604,155.5,212.82262887274138,166.37334701322388,219.95621920058406,165.16666666666666C227.10935361151672,163.95668034655722,241.415622433382,146.43832649338805,248.56875684431466,145.83333333333334C255.70234717215735,145.22999316005473,269.9695278278427,159.125,277.10311815568537,160.33333333333334C284.23670848352805,161.54166666666669,298.50388913921336,155.5,305.63747946705604,155.5C312.7710697948987,155.5,327.03825045058403,158.5233128134975,334.1718407784267,160.33333333333334C341.32497518935935,162.1483128134975,355.6312440112247,170,362.7843784221573,170C369.91796875,170,384.18514940568537,165.16666666666669,391.31873973352805,160.33333333333334C398.45233006137073,155.50000000000003,412.71951071705604,130.125,419.8531010448987,131.33333333333334C426.9866913727414,132.54166666666669,441.2538720284267,165.1732786137711,448.3874623562694,170C455.54059676720203,170,469.84686558906736,170,477,170"
															 stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
															<circle cx="48.671875" cy="165.16666666666666" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="77.20623631137069" cy="170" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="105.74059762274138" cy="160.33333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="134.35313526647198" cy="170" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="162.88749657784268" cy="165.16666666666666" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="191.42185788921336" cy="155.5" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="219.95621920058406" cy="165.16666666666666" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="248.56875684431466" cy="145.83333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="277.10311815568537" cy="160.33333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="305.63747946705604" cy="155.5" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="334.1718407784267" cy="160.33333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="362.7843784221573" cy="170" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="391.31873973352805" cy="160.33333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="419.8531010448987" cy="131.33333333333334" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1"
															 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="448.3874623562694" cy="170" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
															<circle cx="477" cy="170" r="0.5" fill="#00ff00" stroke="#000000" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
														</svg>
													</div>
												</div>
											</div>
											<!-- ./chart section -->
							
											<!-- pie chart -->
											<div class="col-sm-3 ">
												<div class="col-md-6 no-padd-left-right">
													<div class="well well-sm text-center">
														<span class="h1 shadow-block no-margin purple">752</span>
														<span class="text-center">
															<i class="fa fa-upload text-muted"></i>
														</span>
														<span class="text-muted">Uploads</span>
													</div>
												</div>
												<div class="col-md-6 no-padd-left-right">
													<div class="well well-sm text-center">
														<span class="h1 shadow-block no-margin green">563</span>
														<span class="text-center">
															<i class="fa fa-file text-muted"></i>
														</span>
														<span class="text-muted">New Item</span>
													</div>
												</div>
												<div class="col-md-6 no-padd-left-right">
													<div class="well well-sm text-center">
														<span class="h1 shadow-block no-margin orange">236</span>
														<span class="text-center">
															<i class="fa fa-user text-muted"></i>
														</span>
														<span class="text-muted">Users</span>
													</div>
												</div>
												<div class="col-sm-6 no-padd-left-right hidden-sm">
													<div class="well well-sm text-center">
														<span class="h1 shadow-block no-margin darkblue">471</span>
														<span class="text-center">
															<i class="fa fa-rss text-muted"></i>
														</span>
														<span class="text-muted">Feeds</span>
													</div>
												</div>
												<p class="hidden-sm limit-3-line">Lorem ipsum dolor sit amet, mei appareat consequuntur an, eam iudico consulatu laboramus ea, pri et soleat audire.</p>
											</div>
											<!-- ./pie chart -->
										</div>
									</div>
							
								</div>
								<!-- end panel -->
							</div>
							<!-- /end Admin over view .col-md-12 -->
						</div><!-- end col-md-12 -->
					</div>
@stop