<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Chat extends Model
{
    use Notifiable;
    
    protected $fillable = [
    	'chat_id',
		'user_id',
		'mess',	
		'course_id',
    ];

    
}
