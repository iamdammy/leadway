<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Comment extends Model
{
     

    protected $primaryKey = 'comment_id';

    protected $fillable = [
    	'course_id',
        'user_id', 
        'company_id',             
        'content',
        'pings',
        'raw_jqcid',
        'created_at',
		
    ];


    public function sluggable()
    {
        return [
           
        ];
    }

    
    
    public function fullname()
    {
        $data=User::where('id',$this->user_id)->first();
        // return $this->hasOne(Facilitator::class,'user_id');
        return $data->firstName." ".$data->lastName;
    }
    public function photo()
    {
        $data=User::where('id',$this->user_id)->first();
        // return $this->hasOne(Facilitator::class,'user_id');
        return $data->photo;
    }
  
}
