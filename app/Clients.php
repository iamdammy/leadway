<?php

namespace App;

use App\Styles;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Clients extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'company_name', 'company_logo', 'banner', 'website', 'contact', 'email','no_staff',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function style()
    {
        return $this->hasOne(Styles::class);
    }

    public function admins()
    {
        return $this->hasMany(Admin::class);
    }
}

