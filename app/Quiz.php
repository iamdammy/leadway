<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable = [
    	'course_id',
    	'module_id',
    	'course_exam',
		'question',
		'option1',
		'option2',
		'option3',
        'option4',
		'answer',
    ];

    public function module()
    {
    	return $this->belongsTo(Module::class);
    }

    public function exam($crsId)
    {

    }

    public function course()
    {
    	return $this->belongsTo(Course::class);
    }
}
