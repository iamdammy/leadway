<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseCertificate extends Model
{
    protected $fillable = [
        "course_id",
        "title",
        "body",
    ];


    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
