<?php

namespace App\Console;

use App\CourseUser;
use App\Notifications\CourseAssignmentReminderNotification;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {
            $days = setting("course_reminder");
            $courseUsers = CourseUser::where("reminder_status", 0)->get();
            $courseUsers->each( function (CourseUser $courseUser) {
                \Notification::route('mail', $courseUser->user->email)
                    ->notify(new CourseAssignmentReminderNotification($courseUser->user, $courseUser->course));
            });
        });
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
