<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 02/06/2019
 * Time: 9:20 AM
 */

use App\Course;
use App\CourseUser;
use App\QuizResult;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

if (!function_exists("adminRole")) {
    function adminRole()
    {
        return Auth::guard("admin")->user()->role->name;
    }
}


if (!function_exists("adminRoleId")) {
    function adminRoleId()
    {
        return Auth::guard("admin")->user()->role->id;
    }
}


if (!function_exists("quizAttempts")) {
    function quizAttempts(Course $course, User $user)
    {
        return $result = QuizResult::where("course_id", $course->id)
                        ->where("user_id", $user->id)
                        ->get()->sum("attempt");
    }
}


if (!function_exists("courseScore")) {
    function courseScore(Course $course, User $user)
    {
        return $result = QuizResult::where("course_id", $course->id)
            ->where("user_id", $user->id)
            ->get()->sum("score");
    }
}


if (!function_exists("daysLeft")) {
    function daysLeft(Course $course, User $user)
    {
        $courseUser = CourseUser::where("course_id", $course->id)
                            ->where("user_id", $user->id)
                            ->first();
        $endDate = Carbon::parse($courseUser->created_at)->addDays($courseUser->days);
        $today = Carbon::now();
        $diff = $today->diffInDays($endDate);
        $d = Carbon::now()->subDays($courseUser->days)->day;
        //return $d;
        return $diff;
    }
}