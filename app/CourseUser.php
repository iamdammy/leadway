<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class CourseUser extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id',
        'course_id',
        'days',
        'created_at',
        'status',
        'completed', //Yes or No
    ];

    public function courseName($id)
    {
        $crs = Course::findOrFail($id)->course_name;
        return $crs;
    }

    public function courseDuration($id)
    {
        $crs = Course::findOrFail($id)->duration;
        return $crs;
    }

    public function timeUsed()
    {
        $date_assign = $this->created_at;
        $created = new Carbon($date_assign);
        $now = Carbon::now();
        $difference = $created->diff($now)->days;//diffForHumans($now);
        // // $difference = ($created->diff($now)->days <= 1)
        // //             ? 'today'
        // //             : $created->diffForHumans($now);
        return $difference;

        // $crs = Course::findOrFail($id)->duration;
        // return $crs;
    }

    public function assignedDate()
    {
        $date_assign = $this->created_at;
    }

    public function courseImg($id)
    {
        $crs = Course::findOrFail($id)->course_img;
        return $crs;
    }

    public function courseDetails($id)
    {
        $crs = Course::where('courses.id', $id)
            ->leftJoin('categories', 'categories.id', '=', 'courses.id')
            ->first();
        return $crs;
    }

    public function courseModules($id)
    {
        $crsMod = Module::where('course_id', $id)->get();
        return $crsMod;
    }

    public function user()
    {
        // return User::where('id',$id)->first();
        return $this->belongsTo(User::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        static::updated(function (CourseUser $courseUser) {
            if ($courseUser->status == 1 or strtolower($courseUser->completed) == "yes") {
                //send certificate
                $certificate = $courseUser->course->certificate;
            }
        });
    }
}
