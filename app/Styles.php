<?php

namespace App;

use App\Clients;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Styles extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'header_style', 'sidebar_style', 'ribbon_style', 'layout_style', 'rtl_style', 'gender','topmenu_style','bg_style','skin_style',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function style()
    {
        return $this->hasOne(Clients::class);
    }
}

        
