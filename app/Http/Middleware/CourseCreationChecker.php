<?php

namespace App\Http\Middleware;

use App\Category;
use Closure;

class CourseCreationChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cats = Category::all();
        if (count($cats) == 0 ) {
            session()->flash('no_cat', 'A course must belong to a Category, please create a category and then return to the course creation page');
            return redirect()->route('cat_form');
        }
        return $next($request);
    }
}
