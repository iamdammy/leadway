<?php

namespace App\Http\Middleware;

use App\CourseUser;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class CourseDurationCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request->route('id'));
        $cId = $request->route('id');
        $uId = Auth::id();
        $crs = CourseUser::whereUserId($uId)
                            ->whereCourseId($cId)
                            ->first();

        $crsDays = (int)$crs['days'];
        //dd($crs->created_at);
        //$regDate = Carbon::createFromFormat('Y-m-d H:i:s', $crs->created_at);
        $regDate = Carbon::parse($crs['created_at'])->format("Y-m-d H:i:s");
        $regDate = Carbon::parse($regDate);
        $endDate = $regDate->addDays($crsDays);
        $today = Carbon::today();

        $daysLeft = $endDate->diffInDays($today);
        $daysLeft++;

        if ($daysLeft <= 0) {
            session()->flash('crs_xpire', 'Days given over');
            return redirect()->back();
        }

        return $next($request);
    }
}
