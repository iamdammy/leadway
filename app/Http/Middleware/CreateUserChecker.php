<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CreateUserChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $maxUser = env('MAX_USER', 100);
        $appUsers = count(User::all());
        if ($appUsers == $maxUser) {
            session()->flash('max_rh', 'Maximum number of users reached');
            return redirect()->route('admin.dashboard');
            // dd('YOU CANNOT ADD USER');
        }
        return $next($request);
    }
}
