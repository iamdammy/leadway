<?php

namespace App\Http\Middleware;

use App\Course;
use App\Module;
use Closure;

class TopicCreationChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $crs = Course::all();
        $mods = Module::all();
        if (count($crs) == 0 ) {
            session()->flash('no_cc', 'No Course Available, please create one and a module...');
            return redirect()->route('crt_course');
        }
        return $next($request);
    }
}
