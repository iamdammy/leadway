<?php

namespace App\Http\Controllers;

use App\Course;
use App\Facilitator;
use Illuminate\Http\Request;

class FacilitatorController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

    public function index()
    {
    	$facs = Facilitator::all();
    	return view('backend.facilitator.all_facs', compact('facs'));
    }

    public function create()
    {
    	$courses = Course::all();
    	return view('backend.facilitator.create_fac', compact('courses'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'course' => 'required',
    		'name' => 'required|min:6',
    		'email' => 'required|unique:facilitators|email'
    	]);

    	$input = $request->all();
    	$input = array_except($input, '_token');
    	$img = $request->file('photo');
    	if ($img) {
    		$name = str_random(10).$img->getClientOriginalName();
    		$path = public_path('uploads/images');
    		$img->move($path, $name);
    		$input['photo'] = $name;
    	}

    	Facilitator::firstOrCreate([
    		'course_id' => $input['course'],
    		'name' => $input['name'],
    		'email' => $input['email'],
    		'photo' => $input['photo'],
    	]);
    	session()->flash('fac_crtd', 'Facilitator Created Successfully');
    	return  redirect()->back();
    }

    public function edit($id)
    {
    	$fac = Facilitator::findOrFail($id);
    	$courses = Course::all();
    	return view('backend.facilitator.edit_fac', compact('fac','courses'));
    }

    public function update(Request $request, $id)
    {
    	$fac = Facilitator::findOrFail($id);
    	$this->validate($request, [
    		'course' => 'required',
    		'name' => 'required|min:6',
    		'email' => 'required|email'
    	]);

    	$input = $request->all();
    	$input = array_except($input, '_token');
    	$img = $request->file('photo');
    	if ($img) {
    		$name = str_random(10).$img->getClientOriginalName();
    		$path = public_path('uploads/images');
    		$img->move($path, $name);
    		$input['photo'] = $name;
    	}

    	if (! $img) {
    		$input['photo'] = $fac->photo;
    	}

    	// Facilitator::firstOrCreate($input);
    	$fac->update([
    		'course_id' => $input['course'],
    		'name' => $input['name'],
    		'email' => $input['email'],
    		'photo' => $input['photo'],
    	]);
    	session()->flash('fac_uptd', 'Facilitator Created Successfully');
    	return  redirect()->back();

    }

    public function delete($id)
    {
    	$fac = Facilitator::findOrFail($id);
    	$fac->delete();
    	session()->flash('fac_del', 'Facilitatot Deleted Successfully');
    	return redirect()->route('facs');
    	
    }
}
