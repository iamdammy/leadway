<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::where("name", "!=", "SuperAdmin")->orderBy("name", "asc")->get();
        return view('backend/role/all_roles', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/role/add_role');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name'
        ], [
            "name.unique" => "A role already created with same name"
        ]);
        $data['name'] = $request->name;
        Role::firstOrCreate($data);
        session()->flash('role_crtd', 'Role Successfully Created');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($role);
        // $role = $role;
        $role = Role::findOrFail($id);
        return view('backend/role/edit_role', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  =>  'required'
        ]);
        $role = Role::findOrFail($id);
        $role->name = $request->name;
        $role->save();
        session()->flash('role_updtd', 'Role Updated Successfully');
        return redirect()->route('roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($role)
    {
        $role = Role::findOrFail($role);
        $role->delete();
        session()->flash('role_deleted', 'You have Successfully Deleted a Role');
        return redirect()->route('roles');
    }
}
