<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseRequest;
use App\CourseUser;
use App\Notifications\CourseAssignment;
use App\Notifications\UserCourseAssignment;
use App\User;
use Facades\App\Custom\Utility;
use Illuminate\Http\Request;

class CourseUserController extends Controller
{
    /*load constructor*/
    public function __construct()
    {
        $this->middleware('auth:admin');
        
    }

    public function userAssignCourse($cId)
    {
        $course = Course::findOrFail($cId);
        // dd($course);
        $users = User::all();
        return view('backend.course.assign_users', compact('users','course'));
    }
    
    public function assignCourse(Request $request, $uId, $cId)
    {
    	$user = User::findOrFail($uId);
    	$course = Course::findOrFail($cId);

    	$data['user_id'] = $uId;
    	$data['course_id'] = $cId;

    	/*$courseUser = CourseUser::where([
    		['user_id', '=', $uId],
    		['course_id', '=', $cId]
    	])->first();*/

    	$courseUser = Utility::assignUserToCourse($uId, $cId);

    	if (! is_null($courseUser)) {
    		session()->flash('user_exist', $user->firstName.' already assigned to '. $course->course_name);
            //Delete from Course Request
            /*$reqCrs = CourseRequest::where([
                ['user_id', '=', $uId],
                ['course_id', '=', $cId],
            ])->first();
            $reqCrs->delete();*/
    		return redirect()->back();
    	}

    	if ($request->days) {
    	    $data["days"] = $request->days;
        } else {
    	    $data["days"] = 30;
        }

    	CourseUser::firstOrCreate($data);
        $user->notify(new UserCourseAssignment($user, $course));
        /*if (! empty($testSess) || !is_null($testSess) && $testSess == 'Admin') {
            $reqCrs = CourseRequest::where([
                ['user_id', '=', $uId],
                ['course_id', '=', $cId],
            ])->first();
            $reqCrs->delete();
        }*/

        $reqCrs = CourseRequest::where([
                ['user_id', '=', $uId],
                ['course_id', '=', $cId],
            ])->first();
        /*if (count($reqCrs) > 0) {
            $reqCrs->delete();
        }*/

        if (!is_null($reqCrs)) {
            $reqCrs->delete();
        }
            
    	session()->flash('user_assigned', $user->firstName.' has been assigned to '.$course->course_name);
        // return redirect()->back();
        if (session()->get('dash') == 'Dashboard Assign Action') {
            return redirect()->route('admin.dashboard');
        }
        
        return redirect()->back();
    	
    }

    public function newAssignCourse(Request $request)
    {
        //dd($request->all());
        $uId = $request->user;
        $cId = $request->course;
        $days = $request->days;


        $user = User::findOrFail($uId);
        $course = Course::findOrFail($cId);

        $data['user_id'] = $uId;
        $data['course_id'] = $cId;
        $data['days'] = $days;

        $courseUser = Utility::assignUserToCourse($uId, $cId);

        if (! is_null($courseUser)) {
            session()->flash('user_exist', $user->firstName.' already assigned to '. $course->course_name);
            return redirect()->back();
        }

        CourseUser::firstOrCreate($data);
        $user->notify(new UserCourseAssignment($user, $course, $days));

        $reqCrs = CourseRequest::where([
            ['user_id', '=', $uId],
            ['course_id', '=', $cId],
        ])->first();
        /*if (count($reqCrs) > 0) {
            $reqCrs->delete();
        }*/

        if (!is_null($reqCrs)) {
            $reqCrs->delete();
        }

        session()->flash('user_assigned', $user->firstName.' has been assigned to '.$course->course_name);
        // return redirect()->back();
        if (session()->get('dash') == 'Dashboard Assign Action') {
            return redirect()->route('admin.dashboard');
        }

        return redirect()->back();

    }


    public function assCourseRequest($uId, $cId)
    {

    }

    public function massAssignCourse(Request $request, $cId)
    {
        $this->validate($request, [
            "days" => "required|integer"
        ], [
            "days.required" => "Number of days to take the course is required",
            "days.integer" => "Days must be a number"
        ]);
        $course = Course::findOrFail($cId);
        $users = $request->users;
        $days = $request->days;

        //dd($request->all());

        if (is_null($users) || empty($users)) {
            session()->flash('no_user_se', 'No User Selected for the mass assign Action');
            return redirect()->back();
        }

        $count = 0;
    	foreach ($users as $userId) {
            $user = User::findOrFail($userId);
    		$userCourse = Utility::assignUserToCourse($userId, $cId);

    		if (is_null($userCourse)) {
    			$data['user_id'] = $userId;
    			$data['course_id'] = $cId;
                $data['days'] = $days;

                CourseUser::firstOrCreate([
                    "user_id" => $userId,
                    "course_id" => $cId
                ],$data);

                $reqCrs = CourseRequest::where([
                    ['user_id', '=', $userId],
                    ['course_id', '=', $cId],
                ])->first();
                /*if (count($reqCrs) > 0) {
                    $reqCrs->delete();
                }*/

                if (!is_null($reqCrs)) {
                    $reqCrs->delete();
                }

                //Notify user
                $user->notify(new UserCourseAssignment($user, $course, $days));
                $count++;
    		}

    	}

        session()->flash('mass_assn', $count.' users were Successfully assigned to '.$course->course_name );
        return redirect()->back();


    }

    public function deleteCourseUser($uId, $cId)
    {
        //Delete Course User
        $courseName = Course::findOrFail($cId)->course_name;
        $userName = User::findOrFail($uId)->firstName;
        $crsUser = CourseUser::where([
            ['user_id', '=', $uId],
            ['course_id', '=', $cId],
        ])->first();
        $crsUser->delete();
        session()->flash('us_crs_dele', $userName.' , has been removed from '.$courseName.' Course');
        return redirect()->back();
    }
}
