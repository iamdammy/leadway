<?php

namespace App\Http\Controllers;

use App\Course;
use App\QuizResult;
use Illuminate\Http\Request;

class LeaderBoardController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }


    public function index($cId)
    {
        $course = Course::findOrFail($cId);
    	$staff = QuizResult::where('course_id', $cId)
                            ->orderBy('score', 'desc')
                            ->leftJoin('users', 'users.id', '=', 'quiz_results.user_id')
    						->leftJoin('courses', 'courses.id', '=', 'quiz_results.course_id')
    						->simplePaginate(30);
                            // ->get()->toArray();
        return view('staff.leaderboard', compact('staff', 'course'));
    	// dd($staff);
    }
}
