<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use App\CourseRequest as CrsReq;
use App\CourseUser;
use App\Notifications\CourseRequest;
use App\Notifications\CourseRequestDeclinedNotification;
use App\Notifications\CourseUsersEmail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use DB;
class CourseController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
		
	}

    public function index()
    {
    	$courses = Course::orderBy("course_name", "asc")->get();
    // 	$courses=DB::table('courses')
    // 	->join('categories','categories.id','=','courses.category_id')
    // 	->select('courses.id AS courseID', 'courses.*')
    // 	->get();
    	
    	// dd($courses);
    	return view('backend/course/all_courses', compact('courses'));

    }
        public function reqCourses()
    {
    	//$courses = Course::all();
    	$courses = CrsReq::leftJoin('users', 'users.id', '=', 'course_requests.user_id')
												->leftJoin('courses', 'courses.id','=', 'course_requests.course_id')->get();
    	return view('backend/course/req_courses', compact('courses'));

    }

	public function createCourse()
	{
		$cats = Category::all();
		return view('backend/course/add_course', compact('cats'));

	}

	public function storeCourse(Request $request)
	{
		$this->validate($request, [
			'course_name' 			=> 'required',
			'category_id' 			=> 'required',
			'course_img' 			=> 'required',
			'course_description' 	=> 'required|min:5',
//			'duration' 			=> 'required|numeric',
            "reminder_in_days" => "sometimes",
            "pass_mark" => "sometimes"

		], [
		    "course_img.required" => "Please supply the course image"
        ]);
		$input = $request->all();
		$img = $request->file('course_img');
		// dd($img);
		if ($img) {
			$name = $img->getClientOriginalName();
			// dd($name);
			$path = public_path('uploads/images');
			$input['course_img'] = $name;
			$img->move($path, $name);
		}

		$doc = $request->file('course_doc');
		if ($doc) {
			$this->validate($request, [
				'course_doc' => 'mimes:pdf'
			]);

			$name = uniqid().$doc->getClientOriginalName();
			$ext = $doc->getClientOriginalExtension();
			$mime = $doc->getMimeType();
			$path = public_path('uploads/docs');
			$doc->move($path,$name);
			$myCourseDoc = $name;
		}


		$input['course_video'] = $request->course_video;
		$input['course_doc'] = $myCourseDoc ?? '';
		$input = array_except($input, '_token');

		if ($request->scrum_url) {
		    $input["scrum_url"] = $request->scrum_url;
        }
		// dd($input);
		Course::firstOrCreate($input);
		session()->flash('course_created', $input['course_name'].' Course was Successfully Created');
		return redirect()->back();
	}

	public function viewCourse($id)
	{
		// $course = Course::findOrFail($id);
		$course=DB::table('courses')
    	->join('categories','categories.id','=','courses.category_id')    	 
    	->select('courses.id AS courseID', 'courses.*')
    	->where('courses.id','=',$id)
    	->first();
		$totalStaff='';
		return view('backend/course/view_course', compact('course','totalStaff'));


	}

	public function editCourse($id)
	{
		$course = Course::findOrFail($id);
		$cats = Category::all();
		return view('backend/course/edit_course', compact('course', 'cats'));

	}

	public function updateCourse(Request $request, $id)
	{
		$course = Course::findOrFail($id);
		$this->validate($request, [
			'course_name' => 'required',
			'category_id' => 'required',
			'duration' 	=> 'required|numeric',
			// 'course_img' => 'required',
		]);
		$input = $request->all();
		$img = $request->file('course_img');
		if ($img) {
			// dd('oloyu');
			$name = $img->getClientOriginalName();
			$path = public_path('uploads/images');
			$input['course_img'] = $name;
			/*$course->course_img = $name;
			$course->save();*/
			$img->move($path, $name);
		}
		if (!$img) {
			$input['course_img'] = $course->course_img;
		}
		$course->update($input);
		session()->flash('crs_updated', $input['course_name'].' was Successfully Updated');
		return redirect()->route('all_courses');

	}
	public function deleteCourse($id)
	{
		$course = Course::findOrFail($id);
		if($course->course_img!=""){
		unlink(public_path('uploads/images/'.$course->course_img.''));
			}
		if($course->course_doc!=""){
		unlink(public_path('uploads/docs/'.$course->course_doc.''));
			}
		$course->delete();
		session()->flash('crs_deleted', $course->course_name.' was Successfully Deleted');
		return redirect()->route('all_courses');

	}

	public function courseUsers($crsId)
	{
		$users = User::leftJoin('course_users', 'users.id', '=', 'course_users.user_id')->get();
		return view('course/course_users', compact('users'));

	}

	public function viewCourseStudents($cId)
	{
		$course = Course::findOrFail($cId);
		/*foreach ($course->users as $user) {
			dd($user);
		}*/
// 	return	$course->users[0]->id;
		/*$course = CourseUser::where('course_id', $cId)->leftJoin('users', 'users.id', '=', 'course_users.user_id');*/
		return view('backend.course.course_students', compact('course'));
	}

	public function viewCourseRequest($id)
	{
		/*$users = CrsReq::where('course_id', $id)->leftJoin('users', 'users.id', '=', 'course_requests.user_id')
												->leftJoin('courses', 'courses.id','=', 'course_requests.course_id')->get();*/
		// dd($users);
        $users = \App\CourseRequest::all();
		session()->put('crs_r', 'Admin');
		return view('backend.user.users_req', compact('users'));
	}

	public function courseRequestDecline(Request $request)
    {
        $this->validate($request, [
           "comment" => "required"
        ]);
        $user = User::where("id", $request->user)->first();
        $course = Course::where("id", $request->course)->first();

        if ($user and $course) {
            $user->notify(new  CourseRequestDeclinedNotification($user, $course, $request->comment));
            //flash("Request Declined and staff notified")->success();
        }

        \App\CourseRequest::where("user_id", $user->id)
                            ->where("course_id", $course->id)
                            ->delete();


        return redirect()->back();

    }


	public function mailStaff($crsId)
	{
		$course = Course::findOrFail($crsId);
		return view('backend.course.send_email', compact('course'));
		/*$users = CourseUser::where('course_id', $crsId)->leftJoin('users','users.id', '=', 'course_users.user_id')->get()->toArray();
		$newUsers = (new User)->hydrate($users);
		// dd($r);
		foreach($newUsers as $user) {
			$emailUser[] = $user;
			$user->notify(new CourseUsersEmail());
		}*/
	}

	

}
