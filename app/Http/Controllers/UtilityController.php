<?php

namespace App\Http\Controllers;

use App\Module;
use App\Topic;
use Illuminate\Http\Request;
use App\VideoWatched;
use App\CourseUser;
use App\User;
use App\Comment;

class UtilityController extends Controller
{
    public function getModulesByCourse(Request $request)
    {
    	$crsId = $request->id;
    	$mods = Module::where('course_id',$crsId)->get();
    	// echo $mods;

    	/*if (empty($mods) || is_null($mods)) {
    		echo "<option>". "No Data"  ."</option>" ;
    	}*/

    	foreach ($mods as $mod) {
             echo "<option value=".$mod['id']." name=".'module_id'.">"  .$mod['name']   ."</option>" ;
        }
    }

    public function getVideoByTopicId(Request $request)
    {
        $newId = $request->topicId;
        $userId = $request->userId;
        
        $video = Topic::where('id', $newId)->get()->toArray();
        $mat = $video[0]['material'];
        $courseId = $video[0]['course_id'];

        //echo $mat;



        $testMat = str_contains($mat, 'youtube.com');



        if ($testMat) {
            $this->videoWatched($userId,$courseId,$newId);
            $xplodeMat = explode("=", $mat);
            $videoId = $xplodeMat[1];
            echo "<iframe width=\"100%\" height=\"490\" src=\"https://www.youtube.com/embed/$videoId\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>";

        } else {
            $this->videoWatched($userId,$courseId,$newId);

            echo " <iframe width=\"100%\" height=\"490\" src=$mat></iframe> ";

        }
        
        
        // $video = Topic::where('id', $newId)->get()->toArray();
        // $mat = $video[0]['material'];
        
        // echo $video[0]['material'];

        /*echo " <iframe width=\"100%\" height=\"490\" src=\"https://www.youtube.com/embed/1pKTAUhFuf8\" frameborder=\"0\" encrypted-media\" allowfullscreen></iframe> ";*/

        // echo " <iframe width=\"100%\" height=\"490\" src=\"https://www.youtube.com/embed/".$mat."\" frameborder=\"0\" encrypted-media\" allowfullscreen></iframe> ";

    }
    
    public function videoWatched($uid,$cid,$tid)
    {
        $data=VideoWatched::where([
        ['user_id','=',$uid],
        ['course_id','=',$cid],
        ['topic_id','=',$tid],
        ])->count();
       $course= CourseUser::where([
        ['course_id','=',$cid],
        ['user_id','=',$uid]
       ])->first();
        if($data==0){
            $new_data['user_id']=$uid;
            $new_data['course_id']=$cid;
            $new_data['topic_id']=$tid;
            VideoWatched::firstOrCreate($new_data);
            $c_data=VideoWatched::where([
            ['user_id','=',$uid],
            ['course_id','=',$cid],            
            ])->count();
            $status['status']=$c_data;
            $course->update($status);
        }
    }
    
    
    
// Start Comments COntrollers

public function get_users()
{    
    $data=User::get()->toArray();
    
    
    echo(json_encode($data));
}

    public function get_comments(Request $request)
    {
    $data = [];
    $cid=$request->cid;
    $userid=$request->user_id;
    $company_id=$request->company_id;
    $result=Comment::where([
        ['course_id','=',$cid],
        ['company_id','=',$company_id],
        ])->orderBy('comment_id','desc')->get();
        if(count($result) != 0)
        {
            $i = 0;   //"parent": 3,
            foreach ($result as $item) {
                $data[$i]['id'] = $item->comment_id;
                // $data[$i]['created'] = date('o-m-d', strtotime($item->created_at->toIso8601String()));
                $data[$i]['created'] = $item->created_at->toIso8601String();
                $data[$i]['content'] = $item->content;
                if($item->user_id ==$userid ){
                    $data[$i]['fullname'] = "You";  
                    $data[$i]['created_by_current_user'] = true;
                }else{
                    $data[$i]['fullname'] = $item->fullname();
                }

                $data[$i]['creator'] = $item->user_id;
                if(is_file(asset('uploads/images').'/'.$item->photo())){
                    $data[$i]['profile_picture_url'] =asset('uploads/images').'/'.$item->photo();
                } else {
                    $data[$i]['profile_picture_url'] = asset('uploads/images/default.png');
                }

                

                $pings_array = explode(',', $item->pings); 
                $data[$i]['pings'] = $pings_array;
                $i++;
            }
        }
          echo(json_encode($data));
    }

public function post_comment(Request $request)
{
        $input = $request->all(); 
        $content =  $input['content_pings'];
        preg_match_all('/(?<!\w)@\S+/', $content, $matches);
    $raw_jqcid= $input['jqcid'];

        $comment = Comment::create(array(
                    'course_id'=> $input['cid'],
                    'company_id'=> $input['company_id'],
                    'user_id' => $input['user_id'],
                    'content' => $input['content'],
                    'pings' => $input['content_pings'],
                    'raw_jqcid' => $raw_jqcid,
                ));
       
    }
    
    
    public function put_comment(Request $request)
{
        $input = $request->all(); 
        $content =  $input['content_pings'];
        $comment_id=$input['comment_id'];
        $company_id=$input['company_id'];
        $user_id=$input['user_id'];
        // $comment=Comment::where('comment_id',$comment_id)->first();
        $jqcid= $input['jqcid'];
        $course_id= $input['course_id'];
        $comment=null;
        if(!is_numeric($jqcid)){
            $comment=Comment::where([
                ['course_id','=',$course_id],
                ['raw_jqcid','=',$jqcid],
                ['user_id','=',$user_id],
                ['company_id','=',$company_id],
            ])->first();
        }
        else{
             $comment=Comment::where('comment_id',$comment_id)->first();   
            }
        preg_match_all('/(?<!\w)@\S+/', $content, $matches);
        $comment->update(
                  [  'content' => $input['content'],
                    'pings' => $input['content_pings'],
                    ]
                );
       
    }


     public function del_comment(Request $request)
{
        $input = $request->all(); 
        $course_id= $input['course_id'];
        $comment_id=$input['comment_id'];
        $company_id=$input['company_id'];
        $user_id=$input['user_id'];
        $jqcid= $input['jqcid'];
        
        $comment=null;
        if(!is_numeric($jqcid)){
            $comment=Comment::where([
                ['course_id','=',$course_id],
                ['raw_jqcid','=',$jqcid],
                ['user_id','=',$user_id],
                ['company_id','=',$company_id],
            ])->first();
        }
        else{
             $comment=Comment::where('comment_id',$comment_id)->first();   
            }
        $comment->delete();

       
    }


// END COMMENTS
    
    
}





