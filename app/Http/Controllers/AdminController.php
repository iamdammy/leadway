<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Course;
use App\CourseRequest;
use App\CourseUser;
use App\Category;
use App\Department;
use App\Events\UserCreatedEvent;
use App\Facilitator;
use App\Notifications\NewPasswordEmail;
use App\Role;
use App\User;
use App\Clients;
use App\Styles;
use Auth;
use Carbon\Carbon;
use Cyberduck\LaravelExcel\ExporterFacade;
use Cyberduck\LaravelExcel\ImporterFacade;
use Facades\App\Custom\Utility;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

//use Importer;
//use Response;
//use Mail;
//use DB;
use App\Licences;
use Illuminate\Notifications\ChannelManager;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use App\Notifications\NewUserWelcomeEmail;
use App\Notifications\AdminCreate;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    use Notifiable;
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index()
    {
        $courses = Course::all();
        $clients = Clients::all();
        $reqs = CourseRequest::leftJoin('users', 'users.id', '=', 'course_requests.user_id')
            ->leftJoin('courses', 'courses.id', '=', 'course_requests.course_id')->get();
        $users = User::latest()->take(5)->get();
        $facs = Facilitator::all();
        $staffs = User::all();
        $cats = Category::all();
        $depts = Department::all();
        $licences = Licences::all();
        $admin = Auth::guard('admin')->user();
        // if($admin->password_changed==0){
        // 	session()->flash('user_deleted', 'Change default password');

        return view('admin.dashboard', compact('courses', 'admin', 'reqs', 'users', 'facs', 'clients', 'staffs', 'cats', 'depts', 'licences'));

    }

    public function settings()
    {
        return view("backend.admin.settings");

    }

    public function allUsers()
    {
        $users = User::orderBy("firstName", "asc")->get();
        return view('backend/user/users', compact('users'));

    }

    public function suspendUser($id)
    {
        $user = User::where('id', $id)->first();

        $suspend = $user->suspend;

        if ($suspend == 0) {
            $user->update(['suspend' => 1]);
            $mess = $user->firstName . ' ' . $user->lastName . ' has been succesfully suspended';
        } else {
            $user->update(['suspend' => 0]);
            $mess = $user->firstName . ' ' . $user->lastName . ' has been succesfully unsuspended';
        }

        session()->flash('user_created', $mess);
        return redirect()->back();

    }

    public function newUser()
    {
        $depts = Department::all();
        $roles = Role::all();
        return view('backend/user/add_user', compact('depts', 'roles'));

    }

    public function storeUser(Request $request)
    {
        $input = $request->all();
        // dd($input);
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'department' => 'required',
            'phone' => 'required|min:11',
            'staff_id' => 'required',
            'email' => 'required|email|max:255|unique:users',
        ]);
        $this->UpdateClientLicences();
        //$input['password'] = str_random(5) . "leadway";
        $input['password'] = "password";
        $img = $request->file('photo');
        // dd($img->getClientOriginalExtension());
        if ($img) {
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/images');
            $input['photo'] = $name;
            $img->move($path, $name);
        }

        /*if ($img->getClientOriginalExtension() == 'MP4') {
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/videos');
            $img->move($path, $name);
        }*/
        $password = $input['password'];
        $input['password'] = bcrypt($input['password']);
        $input = array_except($input, '_token');
        $user = User::firstOrCreate($input);
        $input['password'] = $password;
        (new User)->forceFill([
            'email' => $user->email
        ])->notify(new NewUserWelcomeEmail($user, $password));
// 		Mail::send('email.staff_success',$input, function ($message) use ($user) {
// 		            $message->from('info@leadway.com','FlipLms');
// 		            $message->to($user->email);
// 		            $message->subject('Staff Registration Account');
        //  });
        session()->flash('user_created', 'Staff Succesfully Created');
        return redirect()->route('create_user');
    }

    public function UpdateClientLicences()
    {
        $licence = Licences::where('licences', '>', 0)->
        orderBy('licence_id', 'asc')->first();
        if (!is_null($licence) && count($licence) > 0) {
            $new_licences['licences'] = $licence->licences - 1;
            $new_licences['used_licence'] = $licence->used_licence + 1;
            $licence->update($new_licences);
        }
    }

    public function viewUser($id)
    {
        $user = User::findOrFail($id);
        return view('backend/user/view_user', compact('user'));
    }

    public function uploadUsers()
    {
        return view('backend/user/upload_users');

    }

    public function downloadUserUploadTemplate()
    {
        //dd("HHS");
        $collection = new Collection();
        $myData = ['firstname', 'lastname', 'department', 'role', 'email'];

        $collection->push($myData);

        $excel = ExporterFacade::make('Excel');
        $excel->load($collection);
        return $excel->stream("staff_upload_template.xlsx");
    }

    public function storeUploadUsers(Request $request)
    {
        $this->validate($request, [
            'user_csv' => 'required',
        ]);

        $file = $request->file('user_csv');
        $name = $file->getClientOriginalName();
        $path = $file->getRealPath();
        $explodeName = explode('.', $name);
        $fileExt = $explodeName[1];


        $excel = ImporterFacade::make('Excel');
        $excel->load($path);
        $collection = $excel->getCollection();
        $collection = array_except($collection, 0);

        foreach ($collection as $i => $staff) {
            $data['firstName'] = $staff[0];
            $data['lastName'] = $staff[1];
            $data['department'] = $staff[2];
            $data['role'] = $staff[3];
            $data['email'] = $staff[4];
            $password = 'password';
            $data['password'] = bcrypt($password);
            $myUser = User::firstOrCreate(['email' => $staff[4]], $data);
            event(new UserCreatedEvent($myUser, $password));
        }

        $totalUser = count($collection);
        $request->session()->flash('csv_success1', '' . $totalUser . ' Staffs Successfully Imported or Updated');
        return redirect()->route('upload_users');


    }

    public function editUser($id)
    {
        $user = User::findOrFail($id);
        $depts = Department::all();
        $roles = Role::all();
        return view('backend/user/edit_user', compact('user', 'depts', 'roles'));

    }

    public function updateUser(Request $request, $id)
    {
        $user = User::findOrFail($id);
        // dd($user->password);
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'department' => 'required',
            'gender' => 'required',
            'role' => 'required',
            'email' => 'required|email|max:255',
            // 'password'	=>	'required|confirmed'
        ]);

        $data = $request->all();
        $img = $request->file('photo');

        //if password is not empty
        $newpassword = $request->password;
        if (!empty($newpassword) || !is_null($newpassword)) {
            //email password to user
            $user->notify(new NewPasswordEmail($user, $newpassword));
            $data['password'] = bcrypt($newpassword);
        }

        if (is_null($data['password'])) {
            $data['password'] = $user->password;
        }

        if ($img) {
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/images');
            $data['photo'] = $name;
            $img->move($path, $name);
        }
        if (!$img) {
            $data['photo'] = $user->photo;
        }
        // dd($data);
        $user->update($data);
        session()->flash('user_updated', 'User Updated Successfully');
        return redirect()->route('view_user', ['id' => $user->id]);


    }

    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        if ($user->photo != "" && $user->photo != "default.png") {
            unlink(public_path('uploads/images/' . $user->photo . ''));
        }
        $user->delete();
        session()->flash('user_deleted', 'Staff Successfully Deleted');
        return redirect()->route('all_users');

    }

    public function userCourses($id)
    {
        $user = User::findOrFail($id);
        return view('backend.user.user_courses', compact('user'));

    }

    public function removeUserCourse($cId, $uId)
    {
        //dd($cId . ' '. $uId);
        $course = Course::findOrFail($cId);
        $user = User::findOrFail($uId);

        $userCourse = CourseUser::where([
            ['user_id', '=', $uId],
            ['course_id', '=', $cId],
        ])->first();

        // dd($userCourse);

        $userCourse->delete();
        session()->flash('us_crs_del', 'User Remove from this Course');
        return redirect()->back();
    }

    public function myProfile()
    {
        $user = Auth::guard('admin')->user();
        return view('backend.admin.view_admin', compact('user'));
    }

    public function adminEditProfile()
    {
        $depts = Department::all();
        $roles = Role::all();
        $user = Auth::guard('admin')->user();
        return view('backend.admin.edit_admin', compact('user', 'depts', 'roles'));
    }

    public function adminUpdateProfile(Request $request)
    {
        $user = Admin::findOrFail(Auth::guard('admin')->id());
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'department' => 'required',
            'gender' => 'required',
            'role' => 'required',
            'email' => 'required|email|max:255',
            // 'password'	=>	'required|confirmed'
        ]);

        $data = $request->all();
        $img = $request->file('photo');

        //if password is not empty
        $newpassword = $request->password;
        if (!empty($newpassword) || !is_null($newpassword)) {
            $data['password_changed'] = 1;
            $data['password'] = bcrypt($newpassword);
        }

        if (is_null($data['password'])) {
            $data['password'] = $user->password;
        }

        if ($img) {
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/images');
            $data['photo'] = $name;
            $img->move($path, $name);
        }
        if (!$img) {
            $data['photo'] = $user->photo;
        }
        $data = array_except($data, '_token');
        // dd($data);
        $user->update($data);
        session()->flash('admin_updated', 'Profile Updated Successfully');
        return redirect()->route('admin_pro');
    }

    public function viewUserRequest($uId, $cId)
    {
        // $user = User::findOrFail($uId);
        $user = CourseRequest::where([
            ['user_id', '=', $uId],
            ['course_id', '=', $cId],
        ])->leftJoin('users', 'users.id', '=', 'course_requests.user_id')
            ->leftJoin('courses', 'courses.id', '=', 'course_requests.course_id')
            // ->leftJoin('facilitators', 'facilitators.course_id', '=', 'course_users.course_id')
            ->first();

        // dd($user);
        session()->put('dash', 'Dashboard Assign Action');
        return view('backend.user.view_user_request', compact('user'));


    }

//CLIENTS

    public function searchStaffByName(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $input = $request->name;
        $users = User::where('firstName', 'LIKE', '%' . $input . '%')
            ->orWhere('lastName', 'LIKE', '%' . $input . '%')
            ->orWhere('email', 'LIKE', '%' . $input . '%')
            ->get();
        return view('backend/user/users', compact('users'));

        // dd($user);
    }

    public function add_client()
    {
        return view('backend/client/add_clients');
    }

    public function del_client($id)
    {
        $client = Clients::where('id', $id)->first();
        if ($client->company_logo != "")
            //unlink(public_path('uploads/images/' . $client->company_logo . ''));
        if ($client->banner != "" or ! is_null($client->banner)) {
            //unlink(public_path('uploads/images/' . $client->banner . ''));
        }

        $client->delete();
        session()->flash('user_deleted', 'Client Successfully Deleted');
        return redirect()->back();
    }

    public function edit_client($id)
    {
        $client = Clients::where('id', $id)->first();

        return view('backend/client/edit_clients', compact('client'));
    }

    public function update_client(Request $request, $id)
    {

        $client = Clients::where('id', $id)->first();

        $input = $request->all();
        // dd($input);
        $this->validate($request, [
            'company_name' => 'required',
            'contact' => 'required',
            'no_staff' => 'required',
            'website' => '',
            'email' => 'required|email|max:255|unique:users',
        ]);

        $logo = $request->file('company_logo');
        $banner = $request->file('banner');

        // dd($img->getClientOriginalExtension());
        if ($logo) {
            $name = $logo->getClientOriginalName();
            $path = public_path('uploads/images');
            $input['company_logo'] = $name;
            $logo->move($path, $name);
        } else {
            $input['company_logo'] = $client->company_logo;

        }

        if ($banner) {
            $name = $banner->getClientOriginalName();
            $path = public_path('uploads/images');
            $input['banner'] = $name;
            $banner->move($path, $name);
        } else {
            $input['company_logo'] = $client->banner;

        }
        /*if ($img->getClientOriginalExtension() == 'MP4') {
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/videos');
            $img->move($path, $name);
        }*/

        $input = array_except($input, '_token');
        $client->update($input);
        session()->flash('user_created', 'Client updated');

        return redirect()->back();
    }

    public function create_client(Request $request)
    {
        $input = $request->all();
        // dd($input);
        $this->validate($request, [
            'company_name' => 'required',
            'contact' => 'required',
            'no_staff' => 'required',
            'website' => '',
            'logo' => "required|file",
            'email' => 'required|email|max:255|unique:clients,email',
        ]);

        $logo = $request->file('company_logo');
        $banner = $request->file('banner');

        // dd($img->getClientOriginalExtension());
        if ($logo) {
            $name = $logo->getClientOriginalName();
            $path = public_path('uploads/images');
            $input['company_logo'] = $name;
            $logo->move($path, $name);
        } else {
            $input['company_logo'] = null;

            /*session()->flash('user_created', 'add logo');
            return redirect()->back();*/
        }
        if ($banner) {
            $name = $banner->getClientOriginalName();
            $path = public_path('uploads/images');
            $input['banner'] = $name;
            $banner->move($path, $name);
        } else {
            $input['banner'] = null;

            /*session()->flash('user_created', 'Add banner');
            return redirect()->back();*/
        }
        /*if ($img->getClientOriginalExtension() == 'MP4') {
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/videos');
            $img->move($path, $name);
        }*/


        $input = array_except($input, '_token');
        Clients::firstOrCreate($input);
        // Mail::send('emails.success',$data, function ($message) use ($user) {
        //          $message->from('info@wduonline.gq','wduonline.gq');
        //          $message->to($user->email);
        //          $message->subject('Admin Registration Account');
        //      });

        session()->flash('user_created', 'Client Succesfully Created');

        return redirect()->back();
    }


//ADMINS

    public function all_client()
    {
        $clients = Clients::all();

        return view('backend/client/all_clients', compact('clients'));
    }

    public function add_admin()
    {
        $depts = Department::all();
        $roles = Role::all();
        $clients = Clients::all();
        $users = User::all();
        return view('backend/hr/add_admin', compact('depts', 'roles', 'clients', 'users'));
    }

    public function del_admin($id)
    {
        $admin = Admin::where('id', $id)->first();

        if ($admin->photo != "")
            unlink(public_path('uploads/images/' . $admin->photo . ''));
        $admin->delete();
        session()->flash('user_deleted', 'Admin Successfully Deleted');
        return redirect()->back();
    }

    public function edit_admin($id)
    {
        /*$admin = DB::table('admins')
        ->join('clients', 'clients.id', '=', 'admins.client_id')
        ->where('admins.id',$id)
        ->first();*/

        $admin = Admin::where("id", $id)->first();
        //dd($admin->client);
        $depts = Department::all();
        $roles = Role::all();
        $clients = Clients::all();
        return view('backend/hr/edit_admin', compact('admin', 'depts', 'roles', 'clients'));
    }

    public function update_admin(Request $request, $id)
    {
        $input = $request->all();
        // dd($input);
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'department' => '',
            'role_id' => 'required|exists:roles,id',
            'gender' => '',
            'email' => 'required|email|max:255|unique:admins,email,' . $id,
        ]);

        $photo = $request->file('photo');

        // dd($img->getClientOriginalExtension());
        if ($photo) {
            $name = $photo->getClientOriginalName();
            $path = public_path('uploads/images');
            $input['photo'] = $name;
            $photo->move($path, $name);
        }

        /*if ($img->getClientOriginalExtension() == 'MP4') {
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/videos');
            $img->move($path, $name);
        }*/


        $admin = Admin::where('id', $id)->first();
        $input = array_except($input, '_token');

        //dd($input);

        $admin->update($input);
        session()->flash('user_created', 'Admin updated');

        return redirect()->back();
    }

    public function create_admin(Request $request)
    {
        $input = $request->all();
        // dd($input);
        /*$this->validate($request, [
            //'client_id' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'department' => '',
            'role_id' => 'required|exists:roles,id',
            'gender' => '',
            'email' => 'required|email|max:255|unique:admins,email',
        ]);*/


        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'role_id' => 'required|exists:roles,id',
        ]);

        $user = User::where("id", $request->user_id)->first();



        $key = str_random(10) . "vfdgroup";
        $input['password'] = $key;

        $photo = $request->file('photo');

        /*if ($photo) {
            $name = $photo->getClientOriginalName();
            $path = public_path('uploads/images');
            $input['photo'] = $name;
            $photo->move($path, $name);
        }*/



        $input = array_except($input, '_token');

        //dd($input);
        $admin = Admin::create([
            //'client_id' => $input['client_id'],
            'firstName' => $user->firstName,
            'lastName' => $user->lastName,
            'department' => $user->department,
            'role_id' => $request->role_id,
            'gender' => $user->gender ?? "Male",
            'email' => $user->email,
            'password' => bcrypt($input['password']),
        ]);


        $admin->notify(new AdminCreate($admin, $key));

        session()->flash('user_created', 'Admin Succesfully Created');

        return redirect()->back();
    }

    public function all_admin()
    {
        $admins = Admin::where('id', '!=', Auth::id())->orderBy("firstName", "asc")->get()->load("role");
        //dd($admins);

        return view('backend/hr/all_admin', compact('admins'));
    }

    public function applyStyle($client_id, $stylef = '', $param = '')
    {
        $style = Styles::where('client_id', $client_id)->first();
        if ($param == 'True') {
            $param = 1;
        } elseif ($param == 'False') {
            $param = 0;
        }

        if (count($style) > 0) {
            $style->update([$stylef => $param]);
        } else {
            $style = Styles::create([
                'client_id' => $client_id,
                $stylef => $param
            ]);
        }
        return redirect()->back();

    }

    public function getStyle($client_id)
    {
        $style = Styles::where('client_id', $client_id)->first();

        return Response::json($style);
    }

    public function Licences()
    {
        $licences = Licences::
        leftJoin('clients', 'clients.id', '=', 'licences.client_id')
            ->orderBy('licences.licence_id', 'desc')
            ->simplePaginate(20);

        return view('backend/licence/index', compact('licences'));
    }

    public function AddLicences()
    {
        $clients = Clients::all();

        return view('backend/licence/add_licence', compact('clients'));
    }

    public function CreateLicences(Request $request)
    {
        $input = $request->all();
        // dd($input);
        $this->validate($request, [
            'client_id' => 'required',
            'licences' => 'required',
        ]);
        Licences::create($input);
        session()->flash('user_created', 'Licence have been Succesfully Added');
        return redirect()->back();
    }

    public function EditLicences($id)
    {
        $licence = Licences::
        leftJoin('clients', 'clients.id', '=', 'licences.client_id')
            ->where('licences.licence_id', $id)
            ->first();

        return view('backend/licence/edit_licence', compact('licence'));
    }

    public function UpdateLicences(Request $request, $id)
    {
        $input = $request->all();
        // dd($input);
        $this->validate($request, [
            'client_id' => 'required',
            'licences' => 'required',
        ]);
        $licence = Licences::where('licence_id', $id);
        $licence->update($input);
        session()->flash('user_created', 'Licence have been Succesfully Updated');
        return redirect()->back();
    }

    public function DelLicence($id)
    {
        $licence = Licences::findOrFail($id);
        $licence->delete();
        session()->flash('user_deleted', 'Licence have been Succesfully Deleted');
        return redirect()->back();
    }

    public function HrLicences()
    {
        $licences = Licences::where('client_id', Auth::user()->client_id)
            ->get();

        return view('hr/licences', compact('licences'));
    }

}
