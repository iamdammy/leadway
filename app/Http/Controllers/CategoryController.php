<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = Category::orderBy("cat_name", "asc")->get();
        return view('backend/category/all_cats', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/category/add_cat');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $data['cat_name'] = $request->name;
        Category::firstOrCreate($data);
        session()->flash('cat_crtd', 'Category Successfully Created');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($role);
        // $role = $role;
        $cat = Category::findOrFail($id);
        return view('backend/category/edit_cat', compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  =>  'required'
        ]);
        $cat = Category::findOrFail($id);
        $cat->cat_name = $request->name;
        $cat->save();
        session()->flash('cat_updtd', 'Category Updated Successfully');
        return redirect()->route('cats');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function delete($cat)
    {
        $cat = Category::findOrFail($cat);
        $cat->delete();
        session()->flash('cat_deleted', 'You have Successfully Deleted a Category');
        return redirect()->route('cats');
    }
}
