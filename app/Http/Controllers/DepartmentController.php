<?php

namespace App\Http\Controllers;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $depts = Department::orderBy("name", "asc")->get();
        /*$depts = DB::table('departments')
            ->orderBy('name', 'desc')
            ->get();*/
        return view('backend/dept/all_depts',compact('depts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/dept/add_dept');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  =>  'required|min:3',
        ]);
        $name = $request->name;
        $data['name'] = $name;
        if(Department::firstOrCreate($data)) {
            session()->flash('dept_crtd', 'Department Successfully Created');
            return redirect()->back();
        }

        session()->flash('dept_crtd_error', 'Error Creating Department');
        return redirect()->back();

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dept = Department::findOrFail($id);
        return view('backend/dept/edit_dept', compact('dept'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd('update');
        $this->validate($request, [
            'name'  =>  'required'
        ]);

        $dept = Department::findOrFail($id);
        $name = $request->name;
        $dept->name = $name;
        $dept->save();
        session()->flash('dept_updtd', 'Department Updated Successfully');
        return redirect()->route('depts');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $dept = Department::findOrFail($id);
        $deptStaff = User::where("department", $dept->name)->get();
        if (! is_null($deptStaff)) {
            session()->flash('dept_del_no', 'You cannot delete a department with staff');
            return redirect()->back();
        }

        if (is_null($deptStaff)) {
            $dept->delete();
            session()->flash('dept_del', 'You have just Deleted a Department');
        }

        return redirect()->route('depts');
    }
}
