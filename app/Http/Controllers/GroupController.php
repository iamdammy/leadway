<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseRequest;
use App\Group;
use App\Notifications\UserCourseAssignment;
use App\User;
use Facades\App\Custom\Utility;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::orderBy("name", "asc")->get();
        return view("backend.group.index", compact("groups"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.group.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:groups,name"
        ]);

        $group = Group::create($request->all());
        $request->session()->flash("group_created", "User Group Created");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function showUsers(Group $group)
    {
        $users = $group->users;
        return view("backend.group.show", compact("group", "users"));
    }

    /**
     * Show the form for editing the specified resource.
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view("backend.group.edit", compact("group"));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $this->validate($request, [
            "name" => "required|unique:groups,name," . $group->id
        ]);

        $group->update($request->all());
        $request->session()->flash("group_updated", "User Group Updated");
        return redirect()->route("group.all");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Group $group)
    {
        $group->delete();
        $request->session()->flash("group_deleted", "Staff Group Deleted");
        return redirect()->route("group.all");
    }

    public function deleteUser(Request $request,Group $group, User $user)
    {

        $user->groups()->detach($group->id);
        $request->session()->flash("user_group_deleted", "Staff removed from {$group->name} group");
        return redirect()->back();
    }

    public function assignStaffForm(Group $group)
    {
        $users = User::all();
        return view("backend.group.assign_users", compact("group", "users"));
    }

    public function assignStaff(Request $request, Group $group, User $user = null)
    {
        //$userIds = [];
        $userIds[] = $user->id;
        //dd($userIds);
        $group->users()->sync($userIds);
        $request->session()->flash("user_assigned_to_group", "User(s) Assigned to group successfully");
        return redirect()->back();

    }

    public function assignStaffMultiple(Request $request, Group $group)
    {
        //dd($request->all());
        $users = $request->users;
        $users = array_except($users, "_token");

        $group->users()->syncWithoutDetaching($users);

        $request->session()->flash("user_assigned_to_group", "User(s) Assigned to group successfully");
        return redirect()->back();
    }

    public function assignCourseToGroup(Request $request)
    {
        $groups = Group::all();
        $courses = Course::all();
        return view("backend.group.assign_course", compact("groups", "courses"));

    }

    public function processAssignCourseToGroup(Request $request)
    {
        $this->validate($request, [
            "group_id" => "required|exists:groups,id",
            "course_id" => "required|exists:courses,id",
            "days" => "required|numeric"
        ]);

        $group = Group::where("id", $request->group_id)->first();
        $course = Course::where("id", $request->course_id)->first();
        $count = $group->users()->count();
        if ($count <= 0) {
            session()->flash('no_staff_in_group', "No staff in the group selected, add staff and try again");
            return redirect()->back();
        }

        $group->users->each(function (User $user) use ($request, $course, &$count) {
            //assign to course
            $courseUser = Utility::assignUserToCourse($user->id, $request->course_id);

            if (is_null($courseUser)) {
                //$count++;
                $courseUser = $user->courses()->firstOrCreate([
                    "course_id" => $request->course_id
                ],
                    [
                        "course_id" => $request->course_id,
                        "days" => $request->days
                    ]);

                $user->notify(new UserCourseAssignment($user, $course, $request->days));

                $reqCrs = CourseRequest::where([
                    ['user_id', '=', $user->id],
                    ['course_id', '=', $course->id],
                ])->first();

                if (!is_null($reqCrs)) {
                    $reqCrs->delete();
                }

            }


        });

        session()->flash('group_assigned', $count.' staff from  '. $group->name.' has been assigned to '.$course->course_name);
        return redirect()->back();



    }
}
