<?php

namespace App\Http\Controllers;

use App\Logo;
use Illuminate\Http\Request;

class LogoController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:admin');
    }

    public function index()
    {
    	$logos = Logo::all();
    	return view('backend.logo.all_logos', compact('logos'));
    }

    public function create()
    {
    	return view('backend.logo.create_logo');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'logo' => 'required',
    	]);
        $img = $request->file('logo');

    	$countLogo = count(Logo::all());
    	if ($countLogo == 1) {
            $firstLogo = Logo::first();
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/images/logos');
            $img->move($path, $name);
            $firstLogo->logo = $name;
            $firstLogo->save();
    		// session()->flash('logo_max', 'You Can Only Upload Once, Change the Uploaded Logo');
    		return redirect()->back();
    	}

    	$input = $request->all();
    	$input = array_except($input, '_token');
    	if ($img) {
    		$name = $img->getClientOriginalName();
    		$path = public_path('uploads/images/logos');
    		$input['logo'] = $name;
    		$img->move($path, $name);
    	}

        //Replace Logo
        /*$logo1 = Logo::findOrFail(1);
        $logo1->logo = $name;
        $logo1->save();*/
        
    	Logo::create($input);
    	session()->flash('logo_crtd', 'Success!! Logo Uploaded Successfully');
    	return redirect()->back();
    }

    public function edit($id)
    {
    	$logo = Logo::findOrFail($id);
    	return view('backend.logo.edit_logo', compact('logo'));
    }

    public function update(Request $request, $id)
    {
    	$logo = Logo::findOrFail($id);
    	$this->validate($request, [
    		'logo' => 'required',
    	]);
    	$input = $request->all();
    	$input = array_except($input, '_token');
    	$img = $request->file('logo');
    	if ($img) {
    		$name = $img->getClientOriginalName();
    		$path = public_path('uploads/images/logos');
    		$input['logo'] = $name;
    		$img->move($path, $name);
    	}

    	if (! $img) {
    		$input['logo'] = $logo->logo;
    	}
    	$logo->update($input);
    	session()->flash('logo_upd', 'Success!! Logo Uploaded Updated');
    	return redirect()->back();

    }


    public function delete($id)
    {
    	$logo = Logo::findOrFail($id);
    	$logo->delete();
    	session()->flash('logo_del', 'Success!! Logo Uploaded Deleted');
    	return redirect()->route('logos');

    }

    public function changeLogo($id)
    {
    	$logo = Logo::findOrFail($id);
    	$name = $logo->logo;
    	
    	$latest = Logo::findOrFail(1);
    	$latest->logo = $name;
    	$latest->save();
    	return redirect()->back();

    }
}
