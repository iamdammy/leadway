<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Notifications\AdminChangePassword;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
      return view('auth.admin-login');
    }

    public function showSignInForm()
    {
        return view("backend.registration");

    }

    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);

      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('admin.dashboard'));
      }
      session()->flash('u_nt_fd', 'Incorrect username or password');

      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        \Session::flush();
        return redirect()->route('admin.login');
    }
    
    
    public function AdminChangePasswordPro(Request $request)
    {   $this->validate($request, [
            'email' => 'required|email'
        ]);
        $email= $request->email;
        $admin = Admin::where('email', $email)->first();
        
        if (is_null($admin)) {
            session()->flash('u_nt_fd', 'No User Found with this Email');
            return redirect()->back();
        }
        $password=str_random(15);
        $admin->password = bcrypt($password);
        $admin->save();
        $admin->notify(new AdminChangePassword($admin,$password));
        session()->flash('user_created', 'Your new password has been sent to your email, please change password when logged in!');
        return redirect()->route('admin.login');

    }
}
