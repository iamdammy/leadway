<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use App\CourseUser;
use App\Department;
use App\Group;
use Cyberduck\LaravelExcel\ExporterFacade;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function courseReport(Request $request)
    {
        $allCourses = Course::all();
        $cats = Category::all();

        $courses = Course::where( function ($query) use ($request) {
            if ($cosId = $request->course) {
                $query->where("id", $cosId);
            }

            if ($catId = $request->category_id) {
                $query->where("catgegory_id", $catId);
            }
        })
            //->orWhere("created_at", null)
            ->get();


        return view("backend.report.courses", compact("courses", "allCourses", "cats"));
    }


    public function courseStudentsReport(Request $request)
    {
        $allCourses = Course::all();
        $cats = Category::all();
        $depts = Department::all();
        $groups = Group::all();

        $users = CourseUser::where( function ($query) use ($request) {
            if ($cosId = $request->course_id){
                $query->where("course_id", $cosId);
            }


            if ($name = $request->name) {
                $query->whereHas("user", function ($query) use ($name) {
                    $query->where("firstName", "LIKE", "%".$name."%")
                        ->orWhere("LastName", "LIKE", "%".$name."%");
                });
            }


            if ($level = $request->level) {
                $query->whereHas("user", function ($query) use ($level) {
                    $query->where("level", $level);
                });
            }


            if ($dept = $request->department) {
                $query->whereHas("user", function ($query) use ($dept) {
                    $query->where("department", $dept);
                });
            }

            if ($complete = $request->completed) {
                if ($complete == "Yes") {
                    $query->where("completed", "$complete");
                            //->orWhere("status", 1);
                }
            }

            if ($group = $request->group) {
                $query->whereHas("user.groups", function ($query) use ($group) {
                   $query->where("groups.id", $group);
                });
            }
        })
            //->toSql();
            ->get();
        //dd($users);

        if ($request->action == "export") {
            $collection = new Collection();
            $collection->push(["S/N", "Staff-ID", "Name", "Course", "Progress", "Status", "Department", "Level", "Attempts", "Score"]);

            $i = 1;
            $users->each( function (CourseUser $courseUser) use ($collection, &$i) {
                $data = [
                    "s/n" => $i,
                    "staff_id" => $courseUser->user->staff_id,
                    "name" => $courseUser->user->full_name,
                    "course" => $courseUser->course->course_name,
                    "progress" => \App\Custom\Utility::userCourseProgress($courseUser->user, $courseUser->course),
                    "status" => $courseUser->completed,
                    "department" => $courseUser->user->department,
                    "level" => $courseUser->user->level,
                    "attempt" => quizAttempts($courseUser->course, $courseUser->user),
                    "score" => courseScore($courseUser->course, $courseUser->user)
                ];

                $collection->push($data);
                $i++;
            });

            $excel = ExporterFacade::make('Excel');
            $excel->load($collection);
            $excel->setChunk(1000);
            $date = date('Y-m-d-H:i:s');
            return $excel->stream("Course User Report.xlsx");

        }

        return view("backend.report.course_user", compact("users", "allCourses", "cats", "depts", "groups"));
    }
}
