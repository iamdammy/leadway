<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Course;
use App\CourseRequest as CrsRequest;
use App\CourseUser;
use App\Module;
use App\Notifications\CourseRequest;
use App\Notifications\UserChangePasswordNotification;
use App\Quiz;
use App\QuizResult;
use App\User;
use App\Chat;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Notifications\ChannelManager;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use App\Admin;

// use Illuminate\Support\Facades\Notification;

class UserController extends Controller
{
    use Notifiable;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
        // $user = User::find(1);
        $user = User::findOrFail(Auth::id());
        $news = Article::all();
        if (Auth::user()->suspend == 1) {
            Auth::guard('web')->logout();
            session()->flash('u_nt_fd', 'Sorry, your account has been suspended. Contact your Human Resourses Department for more details');
            return redirect()->route('hm');
        }
        // dd($user);
        // return view('user/profile', compact('user'));
        // 	return view('staff.profile', compact('user'));
        $total_req = CrsRequest::where('user_id', Auth::id())->count();
        $total_assigned = CourseUser::where('user_id', Auth::id())->count();
        // if($user->password_changed==0){
        //     session()->flash('user_deleted', 'Change default password');
        // }

        $completed = CourseUser::where("user_id", $user->id)
                                ->where("completed", "Yes")
                                ->orWhere("status", 1)
                                ->get();
        $requested = \App\CourseRequest::where("user_id", $user->id)->get();

        $data = [];
        $coursesId = CourseUser::select("course_id")
                                    ->where("user_id", $user->id)
                                    ->get();
        $coursesId->each(function (CourseUser $courseUser) use (&$data) {
            $data[] = $courseUser->course_id;
        });
        //dd($data);
        $collection = new Collection();

        $leaderboard = QuizResult::whereIn("course_id", $data)
                                    ->orderBy("score", "DESC")
                                    ->get()
                                    ->groupBy("course_id");

        $cats = Category::all();

        $courses = Course::get();

        /*$leaderboard->each(function ($l) {
            dd($l);
            dd($l->user->fullname());
        });*/

        return view('staff.profile', compact('user', 'total_req', 'total_assigned', 'completed', 'requested', 'leaderboard', 'courses', 'cats', 'news'));
    }


    public function completedCourses()
    {
        $user = User::findOrFail(Auth::id());
        $completed = CourseUser::where("user_id", $user->id)
            ->where("completed", "Yes")
            ->orWhere("status", 1)
            ->get();
        //dd($completed);
        return view("staff.completed-courses", compact("completed", "user"));

    }


    public function allAvailableCourse()
    {
        $user = User::findOrFail(Auth::id());
        $cats = Category::all();

        $courses = Course::get();
        return view("staff.all_courses", compact("courses", "user", "cats"));

    }

    public function searchCourse(Request $request)
    {
        $key = $request->keyword;
        $user = User::findOrFail(Auth::id());
        $cats = Category::all();

        $courses = Course::where("course_name", "LIKE", "%".$key."%")
                    ->orWhere("course_description", "LIKE", "%".$key."%")
                    ->get();
        return view("staff.all_courses", compact("courses", "user", "cats"));

    }

    public function requestedCourses()
    {
        $user = User::findOrFail(Auth::id());
        $requested = \App\CourseRequest::where("user_id", $user->id)->get();
        $news = Article::all();

        return view("staff.requested-courses", compact("requested", "user", "news"));

    }

    public function viewNews(Article $article)
    {
        $user = User::findOrFail(Auth::id());
        $requested = \App\CourseRequest::where("user_id", $user->id)->get();
        $news = Article::all();

        return view("staff.news", compact("article", "user", "requested", "news"));
    }

    public function editProfile()
    {
        $user = User::findOrFail(Auth::id());
        // return view('user/edit_profile', compact('user'));
        return view('staff/edit_profile', compact('user'));

    }

    public function updateProfile(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'dob' => 'required',
            'email' => 'required|email',
        ]);

        $img = $request->file('photo');
        $input = $request->all();
        $input = array_except($input, '_token');

        if ($img) {
            $name = $img->getClientOriginalName();
            $path = public_path('uploads/images');
            $img->move($path, $name);
            $input['photo'] = $name;
            // $user->save();
        }

        if (!$img) {
            $input['photo'] = $user->photo;
        }

        //process password
        $pass = $input['password'];
        if (!empty($pass) && !is_null($pass)) {
            $input['password_changed'] = 1;
            $input['password'] = bcrypt($pass);
            //send password change email
            $user->notify(new UserChangePasswordNotification($user, $pass));
        } else {
            $input['password'] = $user->password;
        }

        //update user
        $user->update($input);
        session()->flash('pro_edtd', 'Your Profile has been Updated Successfully');
        return redirect()->back();


    }

    public function staffRequestCourse()
    {
        $user = Auth::user();
        $cats = Category::all();
        // $courses = Course::paginate(3);
        $courses = Course::simplePaginate(3);
        // dd($courses);
        return view('staff.request_course', compact('user', 'courses', 'cats'));

    }



    public function myCourses()
    {
        // $courses = User::findOrFail(Auth::id())->courses;
        // $courses = User::join('course_users', 'users.id', '=', 'course_users.user_id')->get();
        $courses = CourseUser::where('user_id', Auth::id())
            ->leftJoin('courses', 'course_users.course_id', '=', 'courses.id')
            ->get();
        // dd($courses);
        return view('user/assigned_courses', compact('courses'));
    }

    public function userListCourses()
    {
        $courses = Course::all();
        return view('user/all_courses', compact('courses'));
    }

    public function takeCourse($id)
    {
        $user = Auth::user();
        $course = Course::findOrFail($id);
        $checkAssign = CourseUser::where([
            ['course_id', '=', $id],
            ['user_id', '=', Auth::id()]
        ])->get();

        $chatPost = Chat::where([
            ['course_id', '=', $id],
            ['user_id', '=', Auth::id()]
        ])->orderBy('chat_id', 'asc')->get();

        // $html = view('pdfs.example')->render();

        // $pdf = PDF::load($html)->output();

        $cid = $id;


        if (count($checkAssign) != 0) {
            // return view('user/course', compact('course'));
            $courseUser = CourseUser::where("user_id", \Auth::id())
                ->where("course_id", $id)
                ->first();
            if ($courseUser) {
                $courseUser->reminder_status = 1;
                $courseUser->save();
            }
            return view('staff/course', compact('course', 'chatPost', 'cid'));
        }

    }

    public function getChatMess(Request $request)
    {
        $this->validate($request, [
            'mess' => 'required',
        ]);
        $cid = $request->cid;
        $mess = $request->mess;
        $data['mess'] = $mess;
        $data['course_id'] = $cid;
        $data['user_id'] = Auth::id();
        // ['user_id' , '=', Auth::id()]
        Chat::create($data);
        $chatPost = Chat::where([
            ['course_id', '=', $cid],
        ])->orderBy('chat_id', 'asc')->get();
        $result = "";
        foreach ($chatPost as $post) {
            $result = $result . "<li>" . $post->mess . "</li>";
        }
        $result = $result . "<li id='new_post'></li>";
        echo $result;

    }

    public function requestCourse(Request $request, $cId)
    {
        $user = Auth::user();
        $course = Course::findOrFail($cId);

        //check if the user has the course already

        $crsReq = CrsRequest::where([
            ['user_id', '=', $user->id],
            ['course_id', '=', $cId],
        ])->first();

        $crsAss = CourseUser::where([
            ['user_id', '=', $user->id],
            ['course_id', '=', $cId],
        ])->first();

        // dd($check);

        /*if (count($crsReq) > 0) {
            session()->flash('u_av_crs', 'Your have once requested for this course');
            return redirect()->back();
        }*/

        if (!is_null($crsReq)) {
            session()->flash('u_av_crs', 'Your have once requested for this course');
            return redirect()->back();
        }


        //store requisition to DB
        $courseReq = CrsRequest::firstOrCreate([
            'user_id' => Auth::id(),
            'course_id' => $cId,
            'start_date' => $request->start_date ?? null
        ]);

        $admins = Admin::all();
        foreach ($admins as $admin) {
            if ($admin->role->name == "SuperAdmin") {
                $admin->notify(new CourseRequest($user, $course));
            }

           /* $to = env('HR_EMAIL', $admin->email);
            (new User)->forceFill([
                'email' => $to
            ])->notify(new CourseRequest($user, $course));*/

        }
        // $to = env('HR_EMAIL', 'olotudammy@gmail.com');

        // (new User)->forceFill([
        //     'email' => $to
        // ])->notify(new CourseRequest($user, $course));

        session()->flash('req_sub', 'You have Successfully Requested for ' . $course->course_name . ' Course');
        return redirect()->back();

    }

    public function staffAssignedCourse()
    {
        // $user = User::find(1);
        $user = User::findOrFail(Auth::id());

        // dd($user);
        // return view('user/profile', compact('user'));
        return view('staff.assigned', compact('user'));
    }


    public function downloadCertificate($cId)
    {
        $course = Course::where("id", $cId)->first();
        $user = \Auth::user();
        $courseUser = CourseUser::where("user_id", $user->id)
                                    ->where("course_id", $course->id)
                                    ->where("status", 1)
                                    ->where("completed", "Yes")
                                    ->first();
        if ($courseUser) {
            return view("certificate", compact("courseUser", "course", "user"));
        }

    }

    public function loadPdf()
    {

        $filename = public_path('uploads/docs/lms.pdf');
        header("Content-type: application/pdf");
        header("Content-Length: " . filesize($filename));
        readfile($filename);

        // return view('user.pdf', compact('filename'));

    }


    public function takeQuiz($course)
    {
        $courseId = (int)$course;
        // dd($courseId);
        $course = Course::findOrFail($course);
        $attempt = $this->checkQuizAttempt($courseId);
        /*$attempt = QuizResult::where([
            ['user_id', '=', Auth::id()],
            ['course_id', '=', $courseId],
        ])->get();*/

        if (count($attempt) > 0) {
            $score = $attempt[0]['score'];
            $crsName = $course->name;
            session()->flash('attempted', 'You have attempted this Quiz and your score was ' . $score);
            return redirect()->route('my_crs');
        }

        $questions = $course->quizes;
        // dd($questions);
        // return view('user.take_quiz', compact('questions', 'course'));
        return view('staff.take_quiz', compact('questions', 'course'));
    }

    public function checkQuizAttempt($cId, $mId)
    {
        $attempt = QuizResult::where([
            ['user_id', '=', Auth::id()],
            ['course_id', '=', $cId],
            ['module_id', '=', $mId],
        ])->get();

        return $attempt;

    }

    public function endOfModuleQuiz($cId, $mId)
    {
        $course = Course::findOrFail($cId);
        $mId = $mId;
        //check if previous module has been attempted
        $firstMod = $course->modules->first();
        $firstModId = $firstMod->id;
        // dd($firstModId);
        if ($firstModId == $mId) {
            $attempt = $this->checkQuizAttempt($cId, $mId);
            if (count($attempt) > 0) {
                $score = $attempt[0]['score'];
                $crsName = $course->name;
                session()->flash('attempted', 'You have attempted this Quiz and your score was ' . $score);
                return redirect()->route('tak_crs', ['id' => $cId]);
            }
            //get questions for the course module
            $questions = $course->moduleQues($cId, $mId);
            return view('staff.take_quiz', compact('questions', 'course', 'mId'));
            // dd('take quiz1');

        }
        if ($mId > $firstModId) {
            $preMod = $mId - 1;
            $check = $this->checkModResult($preMod);
            if ($check >= 1) {
                $attempt = $this->checkQuizAttempt($cId, $mId);
                if (count($attempt) > 0) {
                    $score = $attempt[0]['score'];
                    $crsName = $course->name;
                    session()->flash('attempted', 'You have attempted this Quiz and your score was ' . $score);
                    return redirect()->route('tak_crs', ['id' => $cId]);
                }
                //get questions for the course module
                $questions = $course->moduleQues($cId, $mId);
                return view('staff.take_quiz', compact('questions', 'course', 'mId'));
                // dd('pre mod done');
            }
            if ($check <= 0) {
                //check if the course has quiz
                $questions = $course->moduleQues($cId, $mId);
                if (count($questions) <= 0) {
                    //return redirect()->route('tak_crs', ['id'=>$cId]);
                    return view('staff.take_quiz', compact('questions', 'course', 'mId'));

                }


                session()->flash('pre_nt_dn', 'You are yet to attend to previous session, you cant take this');
                return redirect()->back();
                // dd('attend to pre mode');
            }
            // dd('a module ahead');
        }

        // $preCrsMod = $firstModId - 1;

        //check if the previoud module exit
        /*if ($preCrsMod == 0) {
            // $questions = $course->moduleQues($cId, $mId);
            // return view('staff.take_quiz', compact('questions', 'course', 'mId'));
            // dd('take quiz1');
        }

        if ($preCrsMod != 0) {
            $modChk = Module::findOrfail($preCrsMod);
            //check if user as a quiz record for this module
            $testRec = $this->checkModResult($modChk);
            if ($testRec == 0) {
                dd('u must take previous module');
            } else {
                dd('take_quiz2');
            }
        }*/

        // dd($modChk);

        /*$checkMod = $this->checkModResult($mId);
        if (count($checkMod) == 0) {
            dd('YET TO TAKE THE MODULE QUIZ');
        }
        // dd($course);
        $attempt = $this->checkQuizAttempt($cId, $mId);
        if (count($attempt) > 0) {
            $score = $attempt[0]['score'];
            $crsName = $course->name;
            session()->flash('attempted', 'You have attempted this Quiz and your score was '. $score);
            return redirect()->route('tak_crs', ['id'=>$cId]);
        }

        //get questions for the course module
        $questions = $course->moduleQues($cId, $mId);
        return view('staff.take_quiz', compact('questions', 'course', 'mId'));*/
    }

    public function checkModResult($mId)
    {
        // dd($mId);
        $res = QuizResult::where([
            ['user_id', '=', Auth::id()],
            ['module_id', '=', $mId]
        ])->first();
        if ($res) {
            return 1;
        } else {
            return 0;
        }

    }

    public function proQuiz(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $quizes = $course->quizes;
        foreach ($quizes as $quiz) {
            $correctAns[] = $quiz->answer;
        }
        // dd($correctAns);

        //user submitted answers
        $input = $request->all();
        $input = array_except($input, '_token');
        $submittedAns = $input;
        // dd($submittedAns);

        $score = count(array_intersect($correctAns, $submittedAns));
        // dd($score);

        //Store Quiz Result
        $data['user_id'] = Auth::id();
        $data['course_id'] = $id;
        $data['attempt'] = 1;
        $data['score'] = $score;
        QuizResult::firstOrCreate($data);

        return view('user.quiz_result', compact('score', 'course'));
    }


    public function proQuizModule(Request $request, $cId, $mId)
    {
        $course = Course::findOrFail($cId);
        $module = Module::findOrFail($mId);
        $quizes = $course->moduleQues($cId, $mId);

        $total_q = 0;
        foreach ($quizes as $quiz) {
            $correctAns[] = $quiz->answer;
            $total_q++;
        }

        //user submitted answers
        $input = $request->all();
        $input = array_except($input, '_token');
        $submittedAns = $input;

        $score = count(array_intersect($correctAns, $submittedAns));

        $data['user_id'] = Auth::id();
        $data['course_id'] = $cId;
        $data['module_id'] = $mId;
        $data['attempt'] = 1;
        $data['score'] = $score;
        QuizResult::firstOrCreate($data);

        //Update CourseUserRecord
        $crsUser = CourseUser::where([
            ['user_id', '=', Auth::id()],
            ['course_id', '=', $cId],
        ])->first();
        $crsUser->status = $crsUser->status + 1;
        $crsUser->save();
        return view('staff.quiz_result', compact('score', 'course', 'module', 'total_q'));


    }

    public function userResults()
    {
        $results = QuizResult::orderBy('score', 'desc')->where('user_id', Auth::id())
            ->leftJoin('users', 'quiz_results.user_id', '=', 'users.id')
            ->leftJoin('courses', 'quiz_results.course_id', '=', 'courses.id')
            ->leftJoin('modules', 'quiz_results.module_id', '=', 'modules.id')
            ->get();
        $user = User::where('id', Auth::id())->first();

        // dd($results);
        return view('staff.staff_results', compact('results', 'user'));
    }

    public function courseExam($cId)
    {
        $course = Course::findOrFail($cId);
        //Get number of modules in a course
        $modCount = count($course->modules);

        //get logged in user quiz_result count
        $results = QuizResult::where([
            ['user_id', '=', Auth::id()],
            ['course_id', '=', $cId]
        ])->get();
        $resultCount = count($results);

        //get the last course module
        $lastCourseModule = $course->modules->last();
        $mId = $lastCourseModule->id;
        $lastCourseModuleQue = $course->moduleQues($cId, $mId)->count();
        //dd($mId);


        $questions = Quiz::where([
            ['course_id', '=', $cId],
            ['course_exam', '=', 'Yes'],
        ])->get();

        if ($lastCourseModuleQue <= 0) {
            return view('staff.take_exam', compact('course', 'questions'));
        }

        if ($modCount != $resultCount) {
            session()->flash('u_cant_take', 'Sorry, you have to complete all sessions and take the quizes before taking the exam');
            return redirect()->back();
        }


        return view('staff.take_exam', compact('course', 'questions'));
        // dd($questions);
    }

    public function courseExamPro($cId)
    {
        $course = Course::findOrFail($cId);
        $quizes = $course->examQues($cId);
        $total_q = 0;
        foreach ($quizes as $quiz) {
            $correctAns[] = $quiz->answer;
            $total_q++;
        }

        //user submitted answers
        $input = $request->all();
        $input = array_except($input, '_token');
        $submittedAns = $input;

        $score = count(array_intersect($correctAns, $submittedAns));

        /*$data['user_id'] = Auth::id();
        $data['course_id'] = $cId;
        $data['module_id'] = $mId;
        $data['attempt'] = 1;
        $data['score'] = $score;*/
        //update user exam score
        $userExam = QuizResult::where([
            ['user_id', '=', Auth::id()],
            ['course_id', '=', $cId]
        ])->first();
        $userExam->exam_score = $score;
        $userExam->save();
        // QuizResult::firstOrCreate($data);

        //Update CourseUserRecord
        $crsUser = CourseUser::where([
            ['user_id', '=', Auth::id()],
            ['course_id', '=', $cId],
        ])->first();
        $crsUser->completed = 'Yes';
        $crsUser->save();
        return view('staff.quiz_result', compact('score', 'course', 'module', 'total_q'));
    }


}


/*->leftJoin('posts', 'users.id', '=', 'posts.user_id')
            ->get();*/
