<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseUser;
use App\Department;
use App\Notifications\StaffChangePassword;
use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth')->except(['welcome', 'course']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('home');
        $courses = Course::latest()->take(4)->get();
        return view('frontend/welcome', compact('courses'));
    }


    public function anything()
    {
        $staff = config("staff");
        foreach ($staff as $user) {
            //dd($user);
            $department = Department::where("name", $user["department"])->first();
            if (is_null($department)) {
                $department = Department::firstOrCreate(["name" => $user["department"]], ["name" => $user["department"]]);
            }

            $name = explode(" ", $user["staff_name"]);

            User::firstOrCreate(["email" => $user["staff_email"]], [
                "email" => $user["staff_email"],
                "department" => $department->name,
                "firstName" => $name[1],
                "lastName" => $name[0],
                "phone" => "080",
                "staff_id" => $user["staff_id"],
                "level" => $user["grade"],
                "password" => bcrypt("password")
            ]);
        }

    }

    public function signin()
    {
        return view("frontend.registration");
    }

    public function newIndex()
    {

        // return view('home');
        return view('new');
    }

    public function welcome()
    {
        // return view('welcome');
        $courses = Course::latest()->take(4)->get();
        return view('frontend/welcome', compact('courses'));
    }

    public function course($id)
    {
        $course = Course::findOrFail($id);

        if (Auth::check()) {
            $checkAssign = CourseUser::where([
                ['course_id', '=', $id],
                ['user_id', '=', Auth::id()]
            ])->get();
            if (count($checkAssign) != 0) {
                return redirect()->route('tak_crs', ['id' => $id]);
            }

            if (count($checkAssign) == 0) {
                return view('frontend/course', compact('course'));

            }
        }

        return view('frontend/course', compact('course'));
    }

    public function allCourses()
    {
        $courses = Course::paginate(6);
        $latest = Course::latest()->take(3)->get();
        return view('frontend/all_courses', compact('courses', 'latest'));
    }

    public function courseSearch(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            // 'cat' => 'required',
        ]);
        $cat = $request->cat;
        $name = $request->name;

        /*$courses = Course::where([
            ['course_name', 'LIKE', '%'.$name.'%'],
            ['course_cat', '=', $cat],
        ])->get();*/

        $courses = Course::where('course_name', 'LIKE', '%' . $name . '%')->paginate(6);
        $latest = Course::latest()->take(3)->get();
        return view('frontend/all_courses', compact('courses', 'latest'));
    }


    public function homeSearch(Request $request)
    {
        $name = $request->name;
        // $courses = Course::pluck('course_name');
        $courses = Course::where('course_name', 'LIKE', '%' . $name . '%')->get();
        if (count($courses) == 0) {
            $result[] = 'NO Result';
        } else {
            foreach ($courses as $course) {
                $result[] = $course->course_name;
            }
        }
        return $result;

    }

    public function beginnerSearch()
    {
        $courses = Course::where('course_cat', 'Beginner')->paginate(6);
        $latest = Course::latest()->take(3)->get();
        return view('frontend/all_courses', compact('courses', 'latest'));
    }

    public function intermediateSearch()
    {
        $courses = Course::where('course_cat', 'Intermediate')->paginate(6);
        $latest = Course::latest()->take(3)->get();
        // dd($courses);
        return view('frontend/all_courses', compact('courses', 'latest'));

    }

    public function advancedSearch()
    {
        $courses = Course::where('course_cat', 'Advanced')->paginate(6);
        $latest = Course::latest()->take(3)->get();
        return view('frontend/all_courses', compact('courses', 'latest'));

    }


    public function getForgotPasswordForm()
    {
        // return view('user.forgot_password');
        return view('staff.forgot_password');
    }

    public function getForgotPasswordPro(Request $request)
    {
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if (count($user) == 0) {
            session()->flash('u_nt_fd', 'No User Found with the Email');
            return redirect()->back();
        }

        $user->notify(new StaffChangePassword($user));
        session()->flash('pas_link', 'A password reset link has been sent to your email');
        return redirect()->back();
    }


    public function staffChangePasswordForm($email=null)
    {
        //$user = User::where('email', $email)->first();
        /*if (count($user) == 0 ) {
            session()->flash('u_nt_fd', 'No User Found with the Email');
            return redirect()->back();
        }*/
        return view('staff.change_password', compact('user'));


    }

    // public function staffChangePasswordPro(Request $request, $email)
    // {
    //     $user = User::where('email', $email)->first();
    //     $this->validate($request, [
    //         'password' => 'required|confirmed|min:6'
    //     ]);


    //     $user->password = bcrypt($request->password);
    //     $user->save();
    //     session()->flash('ps_chnages', 'You have Successfully Changed your Password, please login!');
    //     return redirect()->route('cus_login');

    // }
    public function staffChangePasswordPro(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);
        $email = $request->email;
        $user = User::where('email', $email)->first();

        if (count($user) == 0) {
            session()->flash('u_nt_fd', 'No User Found with this Email');
            return redirect()->back();
        }
        $password = str_random(15);
        $user->password = bcrypt($password);
        $user->save();
        $user->notify(new StaffChangePassword($user, $password));
        session()->flash('user_created', 'Your new password has been sent to your email, please change password when logged in!');
        return redirect()->route('hm');

    }

    public function customLogin()
    {
        return view('staff.staff_login');
    }
}
