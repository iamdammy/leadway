<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy("title", "asc")->get();
        return view("backend.article.index", compact("articles"));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.article.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "title" => "required|min:5",
            "body" => "required"
        ]);

        $article = Article::create([
            "title" => $request->title,
            "body" => $request->body
        ]);

        if ($article) {
            session()->flash("article_created", "Article Created Successfully");
        } else {
            session()->flash("article_nt_created", "Unable to create article");

        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view("backend.article.edit", compact("article"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $this->validate($request, [
            "title" => "required|min:5",
            "body" => "required"
        ]);
        $data = $request->only(["body", "title"]);

        if ($article->update($data)) {
            session()->flash("article_updated", "Article Updated Successfully");
        } else {
            session()->flash("article_nt_updated", "Article Not Updated");

        }

        return redirect()->route("news_all");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        session()->flash("article_deleted", "Article Successfully Deleted");
        return redirect()->back();
    }
}
