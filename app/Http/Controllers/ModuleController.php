<?php

namespace App\Http\Controllers;

use App\Course;
use App\Module;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:admin');
    }

    public function index()
    {
    	// $modules = Module::leftJoin('courses','courses.id', '=', 'modules.course_id')->get();
    	$modules = Module::orderBy("name", "asc")->get();
    	// dd($modules);
    	return view('backend.module.all_mods', compact('modules'));
    }

    public function create()
    {
    	$courses = Course::all();
    	return view('backend.module.create_mod', compact('courses'));

    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'course' => 'required',
    		'name' => 'required|min:6',
    	]);

    	$input = $request->all();
    	$input = array_except($input, '_token');
    	Module::firstOrCreate([
    		'course_id' => $input['course'],
    		'name' => $input['name'],
    	]);
    	session()->flash('mod_crtd', 'Module created successfully');
    	return redirect()->back();
    }

    public function edit($id)
    {
    	$mod = Module::findOrFail($id);
    	$courses = Course::all();
    	return view('backend.module.edit_mod', compact('mod','courses'));
    }

    public function update(Request $request, $id)
    {
    	$mod = Module::findOrFail($id);
    	$this->validate($request, [
    		'course' => 'required',
    		'name' => 'required|min:6',
    	]);

    	$input = $request->all();
    	$input = array_except($input, '_token');
    	$mod->update([
    		'course_id' => $input['course'],
    		'name' => $input['name'],
    	]);
    	session()->flash('mod_uptd', 'Module Updated Successfully');
    	return redirect()->route('mods');
    }


    public function delete($id)
    {
    	$mod = Module::findOrFail($id);
    	$mod->delete();
    	session()->flash('mod_del' , 'Module Deleted Successfully');
    	return redirect()->route('mods');

    }
}
