<?php

namespace App\Http\Controllers;

use App\Course;
use Cyberduck\LaravelExcel\ExporterFacade;
use Cyberduck\LaravelExcel\ImporterFacade;
use Facades\App\Custom\Utility;
use App\Quiz;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use League\Csv\Reader;


class QuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        
    }

    /*Display all quiz*/
    public function index()
    {
        $quizes = Quiz::orderBy("course_id", "asc")->get();
        return view('backend.quiz.all_quizes', compact('quizes'));
    }


    /*Show Quiz creation form*/
    public function create()
    {
        $courses = Course::all();
        return view('backend.quiz.add_quiz', compact('courses'));

    }

    /*Store Quiz*/
    public function store(Request $request)
    {
        $this->validate($request, [
            'course_id' => 'required',
            'module_id' => 'required',
            'question' => 'required',
            'option1' => 'required',
            'option2' => 'required',
            'option3' => 'required',
            'option4' => 'required',
            'answer' => 'required',
        ]);
        $input = $request->all();
        $input = array_except($input, '_token');
        Quiz::firstOrCreate($input);
        session()->flash('quiz_crtd', 'Quiz Created');
        return redirect()->back();

    }

    public function edit($id)
    {
        $quiz = Quiz::findOrFail($id);
        $courses = Course::all();
        return view('backend.quiz.edit_quiz', compact('quiz', 'courses'));
    }

    public function update(Request $request, $id)
    {
        $quiz = Quiz::findOrFail($id);
        $this->validate($request, [
            'question' => 'required',
            'option1' => 'required',
            'option2' => 'required',
            'option3' => 'required',
            'option4' => 'required',
            'answer' => 'required',
        ]);
        $input = $request->all();
        $input = array_except($input, '_token');
        $quiz->update($input);
        session()->flash('quiz_uptd', 'Quiz Updated');
        return redirect()->route('quizes');

    }


    public function destroy($id)
    {
        $quiz = Quiz::findOrFail($id);
        $quiz->delete();
        return redirect()->back();
    }

    public function downloadQuizUploadTemplate()
    {
        $collection = new Collection();
        $myData = ['question', 'option1', 'option2', 'option3', 'option4', 'answer'];

        $collection->push($myData);

        $excel = ExporterFacade::make('Excel');
        $excel->load($collection);
        return $excel->stream("quiz_upload_template.xlsx");
    }

    public function importQuizForm()
    {
        $courses = Course::all();
        return view('backend.quiz.import_quiz', compact('courses'));
    }

    public function importQuiz(Request $request)
    {
        /*'/path/to/your/csv/file.csv'*/
        $this->validate($request, [
            // 'course' => 'required',
            // 'module' => 'required',
            'csv_file' => 'required'
        ]);
        $crsId = $request->course_id;
        $modId = $request->module_id;
        $isExam = $request->exam;
        $csvFile = $request->file('csv_file');
        // dd($csvFile);
        if ($csvFile) {
            if (!ini_get("auto_detect_line_endings")) {
                ini_set("auto_detect_line_endings", '1');
            }
            $csvName = $csvFile->getClientOriginalName();
            $path = public_path('uploads/docs');
            $csvFile->move($path, $csvName);
            $csv = Reader::createFromPath(public_path('uploads/docs/'.$csvName))->setHeaderOffset(0);
            // dd($csv);
            if (empty($csv) || is_null($csv)) {
                dd('INVALID');
            }
            $queCreated = 0;
            $queNotCreated = 0;
            foreach($csv as $quiz) {
                dd($quiz);
                //Validate the inputs Field
                $checkQuiz = Utility::validateQuiz($quiz, $crsId, $modId, $isExam);
                if ($checkQuiz) {
                    dd('OLOTU');
                    $quiz['course_id'] = $crsId;
                    // $quiz['module_id'] = $modId;
                    if (!empty($request->exam) && !is_null($request->exam)) {
                        if ($request->exam == 'No') {
                            $quiz['module_id'] = $modId;
                        }

                        if ($request->exam == 'Yes') {
                            $quiz['module_id'] = NULL;
                            $quiz['course_exam'] = 'Yes';
                        }
                        $quiz['question']=htmlspecialchars(strip_tags($quiz['question']));
                        $quiz['option1']=htmlspecialchars(strip_tags($quiz['option1']));
                        $quiz['option2']=htmlspecialchars(strip_tags($quiz['option2']));
                        $quiz['option3']=htmlspecialchars(strip_tags($quiz['option3']));
                        $quiz['option4']=htmlspecialchars(strip_tags($quiz['option4']));
                        $quiz['answer']=htmlspecialchars(strip_tags($quiz['answer']));
                    }
                    Quiz::firstOrCreate($quiz);
                    $queCreated ++;
                    $toCreate[] = $quiz;
                }
                if (!$checkQuiz) {
                    $queNotCreated ++;
                    $dontCreate[] = $quiz;
                }
            }


            // dd($queCreated.'  '. $queNotCreated);
/*
            if (!empty($toCreate) && !is_null($toCreate) && count($toCreate) != 0 ) {
                foreach($toCreate as $create) {
                    $create['course_id'] = $crsId;
                    $create['module_id'] = $modId;
                    Quiz::firstOrCreate($create);
                }
                session()->flash('quiz_imptd', count($toCreate).' questions were imported, couldnt import (0) questions');
                return redirect()->back();
            }

            if (!empty($dontCreate) && !is_null($dontCreate) && count($dontCreate) != 0 ) {
                session()->flash('no_imptd', '(0) questions were imported, couldnt import '. count($dontCreate).' questions');
                return redirect()->back();
            }*/
        }

        // unlink(public_path('uploads/docs/'.$csvName));
        session()->flash('quiz_imptd', $queCreated.' question(s) were imported, couldnt import '.$queNotCreated.' existing question(s)');
        return redirect()->back();
    }


    public function importQuestions(Request $request)
    {
        $this->validate($request, [
            'csv_file' => 'required',
            "course_id" => "required|exists:courses,id"
        ]);

        $file = $request->file('csv_file');
        $name = $file->getClientOriginalName();
        $path = $file->getRealPath();
        $explodeName = explode('.', $name);
        $fileExt = $explodeName[1];


        $excel = ImporterFacade::make('Excel');
        $excel->load($path);
        $collection = $excel->getCollection();
        $collection = array_except($collection, 0);

        foreach ($collection as $i => $question) {
            $data['question'] = $question[0];
            $data['option1'] = $question[1];
            $data['option2'] = $question[2];
            $data['option3'] = $question[3];
            $data['option4'] = $question[4];
            $data['answer'] = $question[5];
            $data['course_id'] = $request->course_id;
            $data['module_id'] = $request->module_id ?? null;
            $data['course_exam'] = $request->exam;

            $quiz = Quiz::firstOrCreate([
                "course_id" => $request->course_id,
                "module_id" => $request->module_id,
                "question" => $data["question"],
                "option1" => $data["option1"],
                "option2" => $data["option2"],
                "option3" => $data["option3"],
                "option4" => $data["option4"],
                "answer" => $data["answer"],
            ], $data);
        }

        $totalUser = count($collection);
        session()->flash('quiz_imptd', ' question(s) were imported, couldnt import ');
        return redirect()->back();
    }

}
