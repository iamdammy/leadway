<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseCertificate;
use Illuminate\Http\Request;

class CourseCertificateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $certificates = CourseCertificate::orderBy("course_id", "asc")->get();
        return view("backend.certificate.index", compact("certificates"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        return view("backend.certificate.create", compact("courses"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "course_id" => "required|integer|exists:courses,id|unique:course_certificates,course_id",
            "title" => "required",
            "body" => "required|string|max:250"
        ]);

        $data["course_id"] = $request->course_id;
        $data["title"] = $request->title;
        $data["body"] = $request->body;

        $certificate = CourseCertificate::firstOrCreate([
            "course_id" => $request->course_id,
        ], $data);

        $request->session()->flash("course_cert_crt", "Certificate Created Successfully");

        //flash("Certificate Created Successfully")->success();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseCertificate $courseCertificate
     * @return \Illuminate\Http\Response
     */
    public function show(CourseCertificate $courseCertificate)
    {
        return view("backend.certificate.show", compact("courseCertificate"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseCertificate $courseCertificate
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseCertificate $courseCertificate)
    {
        $courses = Course::all();
        return view("backend.certificate.edit", compact("courseCertificate", "courses"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CourseCertificate $courseCertificate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseCertificate $courseCertificate)
    {
        $this->validate($request, [
            "course_id" => "required|integer|exists:courses,id|unique:course_certificates,course_id,".$courseCertificate->id,
            "title" => "required",
            "body" => "required"
        ]);

        $data["course_id"] = $request->course_id;
        $data["title"] = $request->title;
        $data["body"] = $request->body;

        $courseCertificate->update($data);

        //flash("Certificate Created Successfully")->success();
        $request->session()->flash("course_cert_upd", "Certificate Updated Successfully");

        return redirect()->route("certificate.all");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseCertificate $courseCertificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CourseCertificate $courseCertificate)
    {
        $courseCertificate->delete();
        //flash("Course Certificate Deleted")->success();
        $request->session()->flash("course_cert_del", "Certificate Deleted Successfully");

        return redirect()->route("certificate.all");
    }
}
