<?php

namespace App\Http\Controllers;

use App\Course;
use App\Module;
use App\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $topics = Topic::orderBy("name", "asc")->get();
        return view('backend.topic.all_topics', compact('topics'));
    }

    public function create()
    {
        $courses = Course::all();
        $mods = Module::all();
        return view('backend.topic.create_topic', compact('courses', 'mods'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'course' => 'required',
            'module' => 'required',
            'format' => 'required',
            'material' => 'required',
            'name' => 'required|min:6',
        ]);


        $input = $request->all();
        $input = array_except($input, '_token');
        //process pdf
        $doc = $request->file('material');
        if ($doc) {
            $this->validate($request, [
                'material' => 'mimes:pdf'
            ]);

            $name = uniqid() . $doc->getClientOriginalName();
            $ext = $doc->getClientOriginalExtension();
            $mime = $doc->getMimeType();
            $path = public_path('uploads/docs');
            $doc->move($path, $name);
            $input['material'] = $name;
        }

        /*if ($request->scrum_url) {
            $request->merge(["scrum_url" => $request->scrum_url]);
        }*/

        // dd($input);

        //dd($input);
        Topic::firstOrCreate([
            'course_id' => $input['course'],
            'module_id' => $input['module'],
            'format' => $input['format'],
            'material' => $input['material'],
            'name' => $input['name'],
            'scrum_url' => $input["scrum_url"]
        ]);
        session()->flash('topic_crtd', 'Topic created successfully');
        return redirect()->back();
    }

    public function edit($id)
    {
        $topic = Topic::findOrFail($id);
        $courses = Course::all();
        $mods = Module::all();
        return view('backend.topic.edit_topic', compact('mods', 'courses', 'topic'));
    }

    public function update(Request $request, $id)
    {
        $topic = Topic::findOrFail($id);
        $this->validate($request, [
            'course' => 'required',
            'module' => 'required',
            'format' => 'required',
            'material' => 'required',
            'name' => 'required|min:6',
        ]);

        $input = $request->all();
        $input = array_except($input, '_token');

        $doc = $request->file('material');
        if ($doc) {
            $this->validate($request, [
                'material' => 'mimes:pdf'
            ]);

            $name = uniqid() . $doc->getClientOriginalName();
            $ext = $doc->getClientOriginalExtension();
            $mime = $doc->getMimeType();
            $path = public_path('uploads/docs');
            $doc->move($path, $name);
            $input['material'] = $name;
        }



        // dd($input);
        $topic->update([
            'course_id' => $input['course'],
            'module_id' => $input['module'],
            'format' => $input['format'],
            'material' => $input['material'],
            'name' => $input['name'],
            'scrum_url' => $request->scrum_url ?? null
        ]);
        session()->flash('topic_uptd', 'Topic Updated Successfully');
        return redirect()->back();
    }


    public function delete($id)
    {
        $topic = Topic::findOrFail($id);
        $topic->delete();
        session()->flash('topic_del', 'Topic Deleted Successfully');
        return redirect()->route('topics');

    }
}
