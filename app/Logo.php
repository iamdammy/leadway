<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
    protected $fillable =['logo'];

    public function getLogoAttribute()
    {
    	if (! $this->attributes['logo']) {
            return 'default.png';
        }

        return $this->attributes['logo'];
    }
}
