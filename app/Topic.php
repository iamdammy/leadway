<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = [
    	'course_id', 'module_id', 'name', 'format', 'material', 'scrum_url'
    ];

    public function course()
    {
    	return $this->belongsTo(Course::class);
    }

    public function module()
    {
    	return $this->belongsTo(Module::class);
    }
}
