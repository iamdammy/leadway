<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facilitator extends Model
{
    protected $fillable = [
    	'name', 'email', 'photo', 'course_id'
    ];

    public function course()
    {
    	return $this->belongsTo(Course::class);
    }
}
