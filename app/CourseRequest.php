<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CourseRequest extends Model
{
    protected $fillable = [
    	'user_id',
    	'course_id',
        'start_date'
    ];

    public function mydate()
    {
    	return Carbon::parse($this->created_at)->diffForHumans();
    }
   
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function category()
    {
        return $this->hasMany(Category::class);
    }

    public function myCategory($id)
    {
        $name = Category::findOrFail($id);
        return $name->cat_name;
    }


    public function quizes()
    {
        return $this->hasMany(Quiz::class);
    }

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function facs()
    {
        return $this->hasMany(Facilitator::class);
    }

    public function totalStaff()
    {
        return count(CourseUser::where('course_id', $this->id)->get());
    }

    public function userCourse($uId, $cId)
    {
        $ck = CourseUser::where([
            ['user_id', '=', $uId],
            ['course_id', '=', $cId],
        ])->get();

        return count($ck);
    }


    public function usCrsReq($uId, $cId)
    {
        $ck = CourseRequest::where([
            ['user_id', '=', $uId],
            ['course_id', '=', $cId],
        ])->get();
        return count($ck);
    }

    public function moduleQues($cId, $mId)
    {
        $result = Quiz::where([
            ['course_id', '=', $cId],
            ['module_id', '=', $mId],
        ])->get();

        return $result;
    }

    public function getCourseImgAttribute()
    {
        if (! $this->attributes['course_img']) {
            return 'default.png';
        }

        return $this->attributes['course_img'];
    }

    public function examQues($cId)
    {
        $result = Quiz::where([
            ['course_id', '=', $cId],
            ['course_exam', '=', 'Yes'],
        ])->get();

        return $result;
    }

    

    
}
