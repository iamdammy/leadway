<?php

namespace App\Notifications;

use App\Course;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CourseRequestDeclinedNotification extends Notification
{
    use Queueable;
    public $user;
    public $course;
    public $comment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Course $course, $comment)
    {
        $this->course = $course;
        $this->user = $user;
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Course Request Declined")
                    ->line("Dear ".$this->user->fullname(). " , your request for ". $this->course->course_name. " has been declined.")
                    ->line("We are very sorry about this")
                    ->line("REASON : " . $this->comment)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
