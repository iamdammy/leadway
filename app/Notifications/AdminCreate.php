<?php

namespace App\Notifications;


use App\Admin;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AdminCreate extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $admin;
    public $pass;

    public function __construct(Admin $admin, $pass)
    {
        $this->admin = $admin;
        $this->pass = $pass;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            //->from('info@Leadway.com', 'Leadway Pensure Online Academy')
            ->from('info@vfdgroup.com', 'VFD Group')
            ->subject('VFD Admin Registration')
            ->greeting('Hello! ' . $this->admin->firstName . ' ' . $this->admin->lastName)
            ->line('Hr Registration Account ' . env('APP_NAME', 'VFD Group') . '')
            ->line('Email : ' . $this->admin->email . '')
            ->line('Password : ' . $this->pass . '')
            ->action('Login', url('/myadmin'))
            ->line('Happy Learning');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
