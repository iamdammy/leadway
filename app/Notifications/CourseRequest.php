<?php

namespace App\Notifications;

use App\Course;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CourseRequest extends Notification
{
    use Queueable;

    public $user;
    public $course;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Course $course)
    {
        $this->user = $user;
        $this->course = $course;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                     ->from('info@vfdgroup.com', 'VFD Group')
                    ->subject('VFD Staff Registration')
                    ->greeting('Hello! '.$this->user->firstName.' '.$this->user->lastName)
                    ->line('Course Requisition Email')
                    ->line('The below user Requested for '.$this->course->course_name)
                    ->line('Name: '. $this->user->fullname())
                    ->line('Email: '. $this->user->email)
                    ->line('Department: '. $this->user->department)
                    ->line('Gender: '. $this->user->gender)
                    // ->line('The introduction to the notification.')
                    ->action('Notification Action', route('ass_crs',['uId'=>$this->user->id, 'cId'=>$this->course->id]))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
