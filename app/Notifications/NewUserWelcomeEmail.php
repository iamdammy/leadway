<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewUserWelcomeEmail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $user;
    public $pass;

    public function __construct(User $user, $pass)
    {
        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            //->from('info@Leadway.com', 'Leadway Pensure Online Academy')
            ->from('info@vfdgroup.com', 'VFD Group')
            ->subject('VFD Staff Registration')
            ->greeting('Hello! ' . $this->user->firstName . ' ' . $this->user->lastName . ' ')
            ->line('Welcome to ' . env('APP_NAME', env("APP_NAME")) . '')
            ->line('Email : ' . $this->user->email . '')
            ->line('Password : ' . $this->pass . '')
            ->action('Login', url('/'))
            ->line('Happy Learning');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
