<?php

namespace App\Notifications;

use App\Course;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CourseAssignmentReminderNotification extends Notification
{
    use Queueable;
    public $user;
    public $course;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Course $course)
    {
        $this->course = $course;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Course Assignment Reminder")
            ->line("Dear, {$this->user->fullname()}, this is a reminder you have been assigned to {$this->course->course_name}")
            ->line('Click on the button below to take course')
            ->action('Start Course', route('tak_crs', ['id' => $this->course->id]))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
