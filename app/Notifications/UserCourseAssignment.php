<?php

namespace App\Notifications;

use App\Course;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserCourseAssignment extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $user;
    public $course;
    public $days;

    public function __construct(User $user, Course $course, $days = 30)
    {
        $this->user = $user;
        $this->course = $course;
        $this->days = $days;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $duration = $this->course->duration;
        if (is_numeric($duration)) {
            $duration = $duration . " day(s)";
        }
        return (new MailMessage)
            //->from('info@Leadway.com', 'Leadway Pensure Online Academy')
            ->from('info@vfdgroup.com', 'VFD Group')
            ->subject('VFD Course Assignment')
            ->greeting('Hello! ' . $this->user->firstName . ' ' . $this->user->lastName)
            ->line('You have been assigned to ' . $this->course->course_name . ' Course')
            ->line('You have ' . $this->days . 'day(s)' . ' to finish the course')
            ->action('Start Course', route('tak_crs', ['id' => $this->course->id]))
            ->line('Happy learning');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
