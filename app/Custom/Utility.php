<?php
namespace App\Custom;

use App\Course;
use App\CourseUser;
use App\Quiz;
use App\QuizResult;
use App\User;

class Utility
{
    public function csvToArray($file)
    {
        if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

        $header = NULL;
        $data = array();
        $delimiter = ',';
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    public static function userCourseProgress(User $user, Course $course)
    {
        $totalModule = count($course->modules);
        $eachModulePercentage = 100;
        if ($totalModule > 0) {
            $eachModulePercentage = 100 / $totalModule;

        }

        $quizResult = QuizResult::where("user_id", $user->id)
                                    ->where("course_id", $course->id)
                                    ->get();
        $completedModule = count($quizResult);
        $progress = ($eachModulePercentage * $completedModule);
        return round($progress)."%";

    }


    public function assignUserToCourse($userId, $cId)
    {
        $courseUser = CourseUser::where([
            ['user_id', '=', $userId],
            ['course_id', '=', $cId]
        ])->first();
        return $courseUser;

    }


    public function validateQuiz($quiz, $crsId, $modId, $isExam)
    {
        if (is_null($quiz) || empty($quiz)) {
            return false;
        }
        if (empty($quiz['question']) || empty($quiz['option1']) || empty($quiz['option2']) || empty($quiz['option3']) || empty($quiz['option4']) || empty($quiz['answer']) ) {
            return false;
        }


        if (is_null($quiz['question']) || is_null($quiz['option1']) || is_null($quiz['option2']) || is_null($quiz['option3']) || is_null($quiz['option4']) || is_null($quiz['answer']) ) {
            return false;
        }

        if (!empty($isExam) || !is_null($isExam)) {
            if ($isExam == 'Yes') {
                $myExam = Quiz::where([
                    ['course_id', '=', $crsId],
                    ['course_exam', '=', 'Yes'],
                    ['question', '=', $quiz['question']],
                    ['option1', '=', $quiz['option1']],
                    ['option2', '=', $quiz['option2']],
                    ['option3', '=', $quiz['option3']],
                    ['option4', '=', $quiz['option4']],
                    ['answer', '=', $quiz['answer']],
                ])->first();
                if (count($myExam) > 0) {
                    return false;
                }

            }
            
        }

        //Check if a Module in a Course has Same Question
        $checkQues = Quiz::where([
            ['course_id', '=', $crsId],
            ['module_id', '=', $modId],
            ['question', '=', $quiz['question']],
            ['option1', '=', $quiz['option1']],
            ['option2', '=', $quiz['option2']],
            ['option3', '=', $quiz['option3']],
            ['option4', '=', $quiz['option4']],
            ['answer', '=', $quiz['answer']],
        ])->first();

        if (count($checkQues) > 0 ) {
            return false;
        }

        return true;



    }
}