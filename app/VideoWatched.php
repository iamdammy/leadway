<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class VideoWatched extends Model
{
    protected $fillable = [
    	'user_id',
    	'course_id',
    	'topic_id',
    ];

    public function mydate()
    {
    	return Carbon::parse($this->created_at)->diffForHumans();
    }
}
