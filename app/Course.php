<?php

namespace App;

use App\CourseUser;
use App\Quiz;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Course extends Model
{
    use Notifiable;
    
    protected $fillable = [
    	'course_name',
		'category_id',
		'course_img',
		'course_video',
        'course_doc',
		'course_description',
		'duration',
        "scrum_url",
        "reminder_in_days",
        "pass_mark"
    ];

    public function users()
    {
    	return $this->hasMany(CourseUser::class);
    }

    public function certificate()
    {
        return $this->hasOne(CourseCertificate::class);
    }

    /*public function category()
    {
        return $this->hasMany(Category::class);
    }*/

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function myCategory($id)
    {
        $name = Category::findOrFail($id);
        return $name->cat_name;
    }

    public function courseRequests()
    {
        return $this->hasMany(CourseRequest::class);
    }


    public function quizes()
    {
        return $this->hasMany(Quiz::class);
    }

    public function quizResults()
    {
        return $this->hasMany(QuizResult::class);
    }

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function facs()
    {
        return $this->hasMany(Facilitator::class);
    }

    public function totalStaff()
    {
        return count(CourseUser::where('course_id', $this->id)->get());
    }

    public function userCourse($uId, $cId)
    {
        $ck = CourseUser::where([
            ['user_id', '=', $uId],
            ['course_id', '=', $cId],
        ])->get();

        return count($ck);
    }


    public function usCrsReq($uId, $cId)
    {
        $ck = CourseRequest::where([
            ['user_id', '=', $uId],
            ['course_id', '=', $cId],
        ])->get();
        return count($ck);
    }

    public function moduleQues($cId, $mId)
    {
        $result = Quiz::where([
            ['course_id', '=', $cId],
            ['module_id', '=', $mId],
        ])->get();

        return $result;
    }

    public function getCourseImgAttribute()
    {
        if (! $this->attributes['course_img']) {
            return 'default.png';
        }

        return $this->attributes['course_img'];
    }

    public function examQues($cId)
    {
        $result = Quiz::where([
            ['course_id', '=', $cId],
            ['course_exam', '=', 'Yes'],
        ])->get();

        return $result;
    }

    public function courseName($id)
    {
        return Course::where("id", $id)->first()->course_name;
    }

    

    
}
