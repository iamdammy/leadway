<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Licences extends Model
{    
    
    protected $fillable = [
    	'licence_id',
		'client_id',
		'used_licence',       
		'licences',		
    ];

    
}
