<?php

namespace App;

use App\Profile;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 'lastName', 'department', 'role', 'email', 'password','gender', 'photo', 'dob', 'phone', 'staff_id','level','password_changed','suspend',
    ];

    protected $appends = [
      "completed_courses", "full_name"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function getFullNameAttribute()
    {
        return $this->firstName. ' '. $this->lastName;

    }

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    public function courses()
    {
        return $this->hasMany(CourseUser::class);
    }

    public function courseRequests()
    {
        return $this->hasMany(CourseRequest::class);
    }

    public function quizResults()
    {
        return $this->hasMany(QuizResult::class);
    }

    public function getCompletedCoursesAttribute()
    {
        return CourseUser::where("user_id", $this->id)
                            ->where("status", 1)
                            ->where("completed", "Yes")
                            ->get();
    }

    public function fullname()
    {
        return $this->firstName. ' '. $this->lastName;
    }

    public function getRoleAttribute($value)
    {
        // $value = 'Staff';
        return strtolower($value);
    }

    public function getPhotoAttribute()
    {
       /* $photo = $this->photo;
        if ($photo == '' || is_null($photo)) {
            return 'default.png';
        } else {
            return $this->photo;
        }*/

        if (! $this->attributes['photo']) {
            return 'default.png';
        }

        return $this->attributes['photo'];
    }

    public function isAdmin()
    {
        $admin = Admin::where("email", $this->email)->first();
        if (! is_null($admin)) {
            return true;
        } else {
            return false;
        }
    }

    public function singleUserCrsStatus($uId, $cId)
    {
        $crs = Course::findOrFail($cId);
        // $crsM = Module::where('course_id', $cId)->get();
        $modCount = count($crs->modules);
        if ($modCount != 0) {
            // return redirect()->back();
            $staMax = 80;
            $sMod = $staMax/$modCount;

            $crsUser = CourseUser::where([
                ['user_id', '=', $uId],
                ['course_id', '=', $cId]
            ])->first();

            $statusCount = $crsUser->status;
            $currentStatus = $statusCount * $sMod;
            return ceil($currentStatus);
        } else {
            return 'No Session';
        }
        
    }
}
