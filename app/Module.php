<?php

namespace App;

use App\Topic;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
    	'course_id',
    	'name'
    ];

    public function topics()
    {
    	return $this->hasMany(Topic::class);
    }

    public function course()
    {
    	return $this->belongsTo(Course::class);
    }

    public function quizes()
    {
        return $this->hasMany(Quiz::class);
    }

    public function quizResults()
    {
        return $this->hasMany(QuizResult::class);
    }

    
}
