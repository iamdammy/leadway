<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class QuizResult extends Model
{
    protected $fillable = [
    	'user_id',
    	'course_id',
    	'module_id',
    	'attempt',
    	'score',
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function scopeHighest($query)
    {
        return $query->orderBy('score', 'desc');
    }

    public function videos()
    {
    	$user_id=$this->user_id;
    
        $crsVid = VideoWatched::where('user_id', $user_id)->get();
        return $crsVid;         
    }
    public function video_watched()
    {
    	$user_id=$this->user_id;
    
        $crsVid = VideoWatched::where('user_id', $user_id)->count();
        return $crsVid;         
    }


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('score', function (Builder $builder) {
            $builder->orderBy('score', 'desc')
                    ->take(10);
        });
    }
}

