<?php

namespace App\Listeners;

use App\Events\UserCreatedEvent;
use App\Notifications\NewUserWelcomeEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendUserEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreatedEvent  $event
     * @return void
     */
    public function handle(UserCreatedEvent $event)
    {
        $user = $event->user;
        $pass = $event->pass;
        $user->notify(new NewUserWelcomeEmail($user, $pass));
    }
}
